package binnie.craftgui.slick;

import org.newdawn.slick.Image;

import binnie.craftgui.core.geometry.Direction;
import binnie.craftgui.resource.ITexture;

public class SlickTexture implements ITexture {

	Image image;
	
	int leftPadding = 0;
	int rightPadding = 0;
	int topPadding = 0;
	int bottomPadding = 0;
	
	public SlickTexture(Image image) {
		this.image = image;
	}
	
	public SlickTexture(Image image, int leftPadding, int rightPadding, int topPadding, int bottomPadding) {
		this.image = image;
		this.leftPadding = leftPadding;
		this.rightPadding = rightPadding;
		this.topPadding = topPadding;
		this.bottomPadding = bottomPadding;
	}
	
	public Image getImage() {
		return image;
	}

	@Override
	public int u() {
		return 0;
	}

	@Override
	public int v() {
		return 0;
	}

	@Override
	public int w() {
		return getImage().getWidth();
	}

	@Override
	public int h() {
		return getImage().getHeight();
	}

	@Override
	public String text() {
		return null;
	}

	@Override
	public ITexture subTexture(Direction direction, int distance) {
		return null;
	}

	public int getLeftPadding() {
		return leftPadding;
	}

	public int getRightPadding() {
		return rightPadding;
	}

	public int getTopPadding() {
		return topPadding;
	}

	public int getBottomPadding() {
		return bottomPadding;
	}


	
	

}
