package binnie.craftgui.slick;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.Widget;
import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.core.renderer.IRendererWidget;

public class RendererSlickWidget implements IRendererWidget {

	protected RendererSlick renderer;
	public RendererSlickWidget(RendererSlick renderer) {
		this.renderer = renderer;
	}
	
	@Override
	public void preRender(IWidget widget) {
		renderer.preRender(widget);
	}

	@Override
	public void render(IWidget widget, int renderLevel) {
	}

	@Override
	public void postRender(Widget widget) {
		renderer.postRender(widget);
	}

	@Override
	public void renderTooltip(IPosition mousePosition, Tooltip tooltip) {
		// TODO Auto-generated method stub
		
	}

}
