package binnie.craftgui.slick;

import binnie.craftgui.core.TopLevelWidget;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;
import binnie.craftgui.resource.ITexture;

public abstract class Scene extends TopLevelWidget {

	public Scene() {
		super();
		setSize(new Vector2f(1024, 768));
		setRenderer(new RendererSlick());
	}

	@EventHandler(origin = Origin.Self)
	public final void onInitScene(EventSlick.Init event) {
		this.initScene();
	}
	
	abstract protected void initScene();

	@Override
	public ITexture getTexture(Object key) {
		return ResourceManager.getResource(key);
	}

}
