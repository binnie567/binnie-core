package binnie.craftgui.slick;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.state.StateBasedGame;

public class CraftGUISlick {

	public static UnicodeFont getFont(int size, boolean bold, boolean italic) {
		UnicodeFont font;
		try {
			font = new UnicodeFont("arial.ttf", size, bold, italic);
			font.addAsciiGlyphs();
			font.addGlyphs(400, 600);
			font.addGlyphs(9837, 9839);
			font.addGlyphs(8320, 8329);
			font.getEffects().add(new ColorEffect(java.awt.Color.WHITE));
			font.loadGlyphs();

			return font;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void init() throws SlickException {
		DEFAULT_FONT = getFont(24, true, false);
	}

	public static UnicodeFont DEFAULT_FONT = null;

	static GameContainer container = null;
	static StateBasedGame game = null;
	static int deltaT = 0;
	static Graphics graphics = null;

	public static void setContainer(GameContainer container2) {
		container = container2;
	};

	public static void setGame(StateBasedGame game2) {
		game = game2;
	};

	public static void setUpdateTime(int deltaT2) {
		deltaT = deltaT2;
	};

	public static void setGraphics(Graphics graphics2) {
		graphics = graphics2;
	};

	public static GameContainer getContainer() {
		return container;
	};

	public static StateBasedGame getGame() {
		return game;
	};

	public static int getDeltaT() {
		return deltaT;
	};

	public static Graphics getGraphics() {
		return graphics;
	};

}
