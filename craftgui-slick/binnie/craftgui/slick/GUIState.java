package binnie.craftgui.slick;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.renderer.IRendererWidget;
import binnie.craftgui.events.EventKey;
import binnie.craftgui.events.EventMouse;

public class GUIState extends BasicGameState {
	
	private static int guiID = 0;

	@Override
	public void keyPressed(int key, char c) {
		scene.callEvent(new EventKey.Down(scene, c, key));
	}

	@Override
	public void keyReleased(int key, char c) {
		scene.callEvent(new EventKey.Up(scene, c, key));
	}

	private IWidget getMousedOverWidget() {
		if (scene.getMousedOverWidget() != null)
			return scene.getMousedOverWidget();
		return scene;
	}
	
	private IWidget getDraggedWidget() {
		if (scene.getDraggedWidget() != null)
			return scene.getDraggedWidget();
		return scene;
	}

	@Override
	public void mouseClicked(int button, int x, int y, int clickCount) {
		//if (scene != null)
		//	scene.callEvent(new EventMouse.Down(getMousedOverWidget(),
		//			x, y, button));
	}

	@Override
	public void mouseDragged(int oldx, int oldy, int newx, int newy) {
		//if (scene != null)
		//	scene.callEvent(new EventMouse.Move(getDraggedWidget(), newx-oldx,
		//			newy-oldy));
	}

	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
		//if (scene != null)
		//	scene.callEvent(new EventMouse.Move(getMousedOverWidget(), newx-oldx,
		//			newy-oldy));
	}

	@Override
	public void mousePressed(int button, int x, int y) {
		if (scene != null)
			scene.callEvent(new EventMouse.Down(getMousedOverWidget(),
					x, y, button));
	}

	@Override
	public void mouseReleased(int button, int x, int y) {
		if (scene != null)
			scene.callEvent(new EventMouse.Up(getDraggedWidget(),
					x, y, button));
	}

	@Override
	public void mouseWheelMoved(int newValue) {
		if (scene != null)
			scene.callEvent(new EventMouse.Wheel(getMousedOverWidget(),
					newValue));
	}

	@Override
	public void enter(GameContainer container, StateBasedGame game)
			throws SlickException {
		super.enter(container, game);
		CraftGUISlick.setContainer(container);
		CraftGUISlick.setGame(game);
		if (scene != null) {
			scene.callEvent(new EventSlick.Enter(scene));
		}

	}

	@Override
	public void leave(GameContainer container, StateBasedGame game)
			throws SlickException {
		super.leave(container, game);
		CraftGUISlick.setContainer(container);
		CraftGUISlick.setGame(game);
		if (scene != null) {
			scene.callEvent(new EventSlick.Leave(scene));
		}
	}

	protected Scene scene;
	int id; 

	public GUIState(Scene scene) {
		this.scene = scene;
		id = guiID++;
	}

	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		CraftGUISlick.setContainer(container);
		CraftGUISlick.setGame(game);
		if (scene != null) {
			scene.callEvent(new EventSlick.Init(scene));
		}

	}

	@Override
	public void render(GameContainer container, StateBasedGame game,
			Graphics graphics) throws SlickException {
		CraftGUISlick.setContainer(container);
		CraftGUISlick.setGame(game);
		CraftGUISlick.setGraphics(graphics);
		if (scene != null) {
			scene.renderBackground();
			scene.renderForeground();
			scene.renderOverlay();
			if(scene.getMousedOverWidget() != null && scene.getMousedOverWidget() instanceof ITooltip) {
				Tooltip tooltip = new Tooltip();
				((ITooltip)scene.getMousedOverWidget()).getTooltip(tooltip);
				if(tooltip.exists()) {
					scene.getRenderer(IRendererWidget.class).renderTooltip(scene.getAbsoluteMousePosition(), tooltip);
				}
			}
		}
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int deltaT)
			throws SlickException {
		CraftGUISlick.setContainer(container);
		CraftGUISlick.setGame(game);
		CraftGUISlick.setUpdateTime(deltaT);
		if (scene != null) {
			Input input = container.getInput();
			scene.setMousePosition(input.getMouseX(), input.getMouseY());
			scene.update();
		}
	}

	@Override
	public int getID() {
		return id;
	}

}
