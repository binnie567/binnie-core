package binnie.craftgui.slick;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class ResourceManager {

	static Map<Object, SlickTexture> textures = new HashMap<Object, SlickTexture>();

	public static SlickTexture getResource(Object key) {
		return textures.get(key);
	}
	
	public static void registerTexture(Object object, String filename) throws SlickException {
		Image image = new Image(filename);
		textures.put(object, new SlickTexture(image));
	}
	
	public static void registerTexture(Object object, String filename, int width, int height) throws SlickException {
		Image image = new Image(filename);
		image = image.getScaledCopy(width, height);
		textures.put(object, new SlickTexture(image));
	}
	
	public static void registerTexture(Object object, String filename, int width, int height,
			int pad1, int pad2, int pad3, int pad4) throws SlickException {
		Image image = new Image(filename);
		image = image.getScaledCopy(width, height);
		textures.put(object, new SlickTexture(image, pad1, pad2, pad3, pad4));
	}
	
	public static void registerTexture(Object object, Image image, int width, int height, int pad1, int pad2, int pad3, int pad4) throws SlickException {
		textures.put(object, new SlickTexture(image, pad1, pad2, pad3, pad4));
	}

}
