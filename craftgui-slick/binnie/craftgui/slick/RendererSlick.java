package binnie.craftgui.slick;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Color;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.Widget;
import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.Direction;
import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.core.geometry.TextJustification;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.core.renderer.IRenderer;
import binnie.craftgui.core.renderer.IRendererWidget;
import binnie.craftgui.resource.ITexture;

public class RendererSlick implements IRenderer, IRendererWidget {

	@Override
	public void preRender(IWidget widget) {
		setColour(widget.getColour());
		setWidget(widget);
		if (widget.isCroppedWidet()) {
			IWidget cropWidget = widget.getCropWidget();
			IPosition translation = cropWidget.getAbsolutePosition().sub(
					widget.getAbsolutePosition());
			// IPosition start = widget.getPosition().add(translation);
			IArea area = widget.getCroppedZone();
			CraftGUISlick.getGraphics().setWorldClip(translation.x(), translation.y(),
					area.size().x(), area.size().y());
		}
	}

	
	@Override
	public void postRender(Widget widget) {
		if (widget.isCroppedWidet()) {
			CraftGUISlick.getGraphics().setWorldClip(null);
		}
		setWidget(null);
	}

	@Override
	public void renderTexture(Object key, IPosition position) {
		SlickTexture texture = ResourceManager.getResource(key);
		texture.getImage().draw(position.x(), position.y(), currentColour);
	}
	
	public void renderTexture(SlickTexture texture, float x, float y, float w, float h, float u, float v, float tw, float th, boolean tiled) {
		renderTexture(texture, new Area(x, y, w, h), new Area(u, v, tw, th), tiled);
	}

	public void renderTexture(SlickTexture texture, IArea area, IArea uv, boolean tiled) {
		if(uv == null) uv = new Area(0, 0, texture.w(), texture.h());
		if(tiled) {
			float y = area.pos().y();
			while(y < area.pos().y() + area.size().y()) {
				float h = Math.min(area.pos().y() + area.size().y() - y, uv.size().y());
				float x = area.pos().x();
				while(x < area.pos().x() + area.size().x()) {
					float w = Math.min(area.pos().x() + area.size().x() - x, uv.size().x());
					texture.getImage().draw(x, y, x + w, y + h, uv.pos().x(), uv.pos().y(), uv.pos().x() + w,
							uv.pos().y() + h, currentColour);
					x += w;
				}
				y += h;
			}
		}
		else {
			texture.getImage().draw(area.pos().x(), area.pos().y(), area.pos().x() + area.size().x(),
					area.pos().y() + area.size().y(), uv.pos().x(), uv.pos().y(), uv.pos().x() + uv.size().x(),
					uv.pos().y() + uv.size().y(), currentColour);
		}
	}
	
	public void renderTexture(Object key, IArea area, boolean tiled) {
		SlickTexture texture = ResourceManager.getResource(key);

		float x = area.pos().x();
		float y = area.pos().y();
		float w = area.size().x();
		float h = area.size().y();
		float tw = texture.w();
		float th = texture.h();
		float lPad = texture.getLeftPadding();
		float rPad = texture.getRightPadding();
		float bPad = texture.getBottomPadding();
		float tPad = texture.getTopPadding();

		renderTexture(texture, x, y, lPad, tPad, 0, 0, lPad, tPad, tiled);

		renderTexture(texture, x + lPad, y, w - rPad-lPad, tPad, lPad, 0,
				tw - lPad - rPad, tPad, tiled);

		renderTexture(texture, x + w - rPad, y, rPad, tPad, tw - rPad, 0,
				rPad, tPad, tiled);

		renderTexture(texture, x, y + tPad, lPad, h - bPad - tPad, 0, tPad,
				lPad, th - bPad - tPad, tiled);

		renderTexture(texture, x, y + h - bPad, lPad, bPad, 0, th - bPad,
				lPad, bPad, tiled);

		renderTexture(texture, x + lPad, y + h - bPad, w - rPad - lPad, bPad,
				lPad, th - bPad, tw - lPad - rPad, bPad, tiled);

		renderTexture(texture, x + w - rPad, y + h - bPad, rPad, bPad,
				tw - rPad, th - bPad, rPad, bPad, tiled);

		renderTexture(texture, x + w - rPad, y + tPad, rPad, h - bPad - tPad,
				tw - rPad, tPad, rPad, th - bPad - tPad, tiled);

		renderTexture(texture, x + lPad, y + tPad, w - rPad - lPad, h - bPad - tPad,
				lPad, tPad, tw - rPad - lPad, th - bPad - tPad, tiled);
				
				
	}

	@Override
	public void renderTextureTiled(Object key, IArea area) {
	}

	@Override
	public void setTexture(String texture) {
	}

	@Override
	public int getTextWidth(String text) {
		return CraftGUISlick.DEFAULT_FONT.getWidth(text);
	}

	@Override
	public int getTextHeight() {
		return CraftGUISlick.DEFAULT_FONT.getHeight("|");
	}

	Color currentColour = new Color(0xFFFFFF);

	@Override
	public void setColour(int c) {
		CraftGUISlick.getGraphics().setColor(new Color(c));
		currentColour = new Color(c);
	}

	@Override
	public void renderText(IPosition position, String text, int colour) {
		renderText(new Area(position, new Vector2f(0, 0)),
				TextJustification.TopLeft, text, colour);
	}

	@Override
	public void renderText(IArea area, TextJustification justification,
			String text, int colour) {
		float offY = (area.size().y() - getTextWidth(text))
				* justification.getXOffset();
		float offX = (area.size().y() - getTextWidth(text))
				* justification.getYOffset();
		if (area.size().y() == 0f)
			offY = 0;
		if (area.size().x() == 0f)
			offX = 0;
		setColour(colour);
		CraftGUISlick.DEFAULT_FONT.drawString(area.pos().x() + offX, area.pos().y()
				+ offY, text, new Color(colour));
	}

	@Override
	public void renderSolidArea(IArea area, int colour) {
	}

	@Override
	public void renderGradientRect(IArea area, int colour, int colour2) {
	}

	@Override
	public void limitArea(IArea area) {
	}

	@Override
	public float getTextHeight(String text, float width) {
		return CraftGUISlick.DEFAULT_FONT.getHeight(text);
	}

	@Override
	public void renderSubTexture(ITexture texture, IArea area,
			Direction direction, float percentage) {
	}

	@Override
	public void setWidget(IWidget widget) {
	}
	
	Map<Class<?>, Object> subRenderers = new HashMap<Class<?>, Object>();
	
	@Override
	public <T> void addSubRenderer(Class<T> rendererClass, T renderer) {
		subRenderers.put(rendererClass, renderer);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T subRenderer(Class<T> renderer) {
		if(subRenderers.containsKey(renderer))
			return (T) subRenderers.get(renderer);
		return (T) this;
	}

	@Override
	public void renderTooltip(IPosition mPos, Tooltip tooltip) {
	}

	@Override
	public void renderTexture(Object key, IArea area) {
		SlickTexture texture = ResourceManager.getResource(key);
		renderTexture(key, area, false);
	}


	@Override
	public void render(IWidget widget, int renderLevel) {
		
	}


	

}
