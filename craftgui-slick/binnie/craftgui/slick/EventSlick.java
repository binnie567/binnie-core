package binnie.craftgui.slick;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.Event;

public class EventSlick extends Event {

	public EventSlick(IWidget origin) {
		super(origin);
	}
	
	public static class Enter extends EventSlick {

		public Enter(IWidget origin) {
			super(origin);
		}
		
	}
	
	public static class Leave extends EventSlick {

		public Leave(IWidget origin) {
			super(origin);
		}
		
	}
	
	public static class Init extends EventSlick {

		public Init(IWidget origin) {
			super(origin);
		}
		
	}

}
