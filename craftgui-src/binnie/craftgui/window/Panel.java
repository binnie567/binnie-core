package binnie.craftgui.window;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;

public class Panel extends Control  {
	
	public static interface IPanelType {
		
	}

	public Panel(IWidget parent, float x, float y, float width, float height,
			IPanelType type) {
		super(parent, x, y, width, height);
		this.type = type;
	}

	IPanelType type;

	public IPanelType getType() {
		return type;
	}
	
}
