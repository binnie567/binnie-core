package binnie.craftgui.core;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import binnie.craftgui.events.Event;
import binnie.craftgui.events.core.EventHandler;

/**
 * Stores information about widget classes, such as events they handle, etc.
 * 
 * @author Alex Binnie
 * 
 */
public class WidgetData {

	Class<?> widgetClass;

	public WidgetData(Class<? extends IWidget> widgetClass) {
		this.widgetClass = widgetClass;
		Class<?> currentClass = widgetClass;
		while (!currentClass.equals(Widget.class)) {
			try {
				for (Method method : currentClass.getMethods()) {
					if (method.isAnnotationPresent(EventHandler.class)) {
						Class<? extends Event> eventClass = (Class<? extends Event>) method
								.getParameterTypes()[0];
						listeners.put(eventClass, new EventListener(this, method, eventClass));
					}
				}
				
			} catch (Exception e) {
				System.err.println("Error when registering widget class " + currentClass.getName());
				e.printStackTrace(System.err);
			}
			currentClass = currentClass.getSuperclass();
		}

	}

	public void handleEvent(IWidget widget, Event event) throws IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		if (listeners.containsKey(event.getClass())
				&& listeners.get(event.getClass()).handlesEvent(widget, event))
			listeners.get(event.getClass()).invoke(widget, event);
	}

	Map<Class<? extends Event>, EventListener> listeners = new HashMap<Class<? extends Event>, EventListener>();

	static class EventListener {

		WidgetData widgetdata;
		Method method;
		EventHandler eventHandler;
		Class<? extends Event> eventClass;

		private EventListener(WidgetData widgetdata, Method method,
				Class<? extends Event> eventClass) {
			this.widgetdata = widgetdata;
			this.method = method;
			this.eventClass = eventClass;
			this.eventHandler = method.getAnnotation(EventHandler.class);
		}

		public boolean handlesEvent(IWidget widget, Event event) {
			return eventHandler.origin().handlesEvent(widget, event);
		}

		public void invoke(IWidget widget, Event event) throws IllegalArgumentException,
				IllegalAccessException, InvocationTargetException {
			method.invoke(widget, new Object[] { event });
		}

	}

}
