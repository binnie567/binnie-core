package binnie.craftgui.core;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import binnie.craftgui.events.Event;

public class CraftGUI {

	static Map<Class<? extends IWidget>, WidgetData> widgetMap = new HashMap<Class<? extends IWidget>, WidgetData>();

	/**
	 * Auto registers widget classes on construction
	 * 
	 * @param cls
	 */
	public static void registerWidgetClass(Class<? extends IWidget> cls) {
		if (widgetMap.containsKey(cls))
			return;
		System.out.println("Registered " + cls);
		widgetMap.put(cls, new WidgetData(cls));
	}

	public static void handleEvent(IWidget widget, Event event) {
		try {
			Class<?> clss = widget.getClass();
			if (!widgetMap.containsKey(clss))
				return;
			widgetMap.get(clss).handleEvent(widget, event);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void registerWidgetClasses(IWidget widget) {
		Class<?> clss = widget.getClass();
		while (!clss.equals(Widget.class)) {
			registerWidgetClass((Class<? extends IWidget>) clss);
			clss = clss.getSuperclass();
		}
	}

}
