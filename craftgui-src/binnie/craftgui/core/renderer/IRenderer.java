package binnie.craftgui.core.renderer;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.geometry.Direction;
import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.core.geometry.TextJustification;
import binnie.craftgui.resource.ITexture;

public interface IRenderer {

	public void renderTexture(Object key, IPosition position);

	public void renderTexture(Object key, IArea area);

	public void renderTextureTiled(Object key, IArea area);

	public void setTexture(ITexture texture);

	public int getTextWidth(String text);

	public int getTextHeight();

	public void setColour(int c);

	public void renderText(IPosition position, String text, int colour);

	public void renderText(IArea area, TextJustification justification,
			String text, int colour);

	public void renderSolidArea(IArea area, int colour);

	public void renderGradientRect(IArea area, int colour, int colour2);

	public void limitArea(IArea area);

	public float getTextHeight(String text, float width);

	public void renderSubTexture(ITexture texture, IArea area,
			Direction direction, float percentage);

	/** Sets Widget currently being rendered */
	public void setWidget(IWidget widget);

	public <T> void addSubRenderer(Class<T> rendererClass, T renderer);

	public <T> T subRenderer(Class<T> renderer);

	
}