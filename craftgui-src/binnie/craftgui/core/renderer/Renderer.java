package binnie.craftgui.core.renderer;

import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.core.geometry.TextJustification;
import binnie.craftgui.core.geometry.Vector2f;

public class Renderer {

	/**
	 * Renders a text with the top left at position
	 * 
	 * @param position
	 *            The top left corner of the text
	 * @param text
	 *            The text
	 */
	public void renderText(IPosition position, IFont font, String text) {
		font.renderText(position, text);
	}

	public void renderText(IArea area, TextJustification justification, IFont font, String text) {

		String[] strings;
		if (area.size().x() > 0)
			strings = font.convertToMultiLine(text, area.size().x());
		else
			strings = new String[] { text };

		if (strings.length == 0)
			return;

		float textHeight = 0;
		for (String string : strings)
			textHeight += font.getHeight(string);

		float x = area.pos().x();
		float y = area.pos().y();

		if (area.size().y() > 0) {
			y += (area.size().y() - textHeight) * justification.getYOffset();
		}

		for (String string : strings) {
			float textWidth = font.getWidth(string);
			if (area.size().x() > 0) {
				x += (area.size().x() - textWidth) * justification.getXOffset();
			}
			this.renderText(new Vector2f(x, y), font, string);
			y += font.getHeight(string);
		}

	}

	public void renderTexture(Object key, IPosition position) {
		ITexture texture = getTexture(key);
		if(texture != null)
			texture.renderTexture(position);
	}

	private ITexture getTexture(Object key) {
		if(key instanceof ITexture)
			return (ITexture) key;
		return null;
	}

	public void renderTexture(Object key, IArea area) {
		renderTexture(key, area, TextureType.Stretched);
	}

	public void renderTexture(Object key, IArea area, TextureType type) {
		ITexture texture = getTexture(key);
		if(texture != null)
			texture.renderTexture(area, type);
	}

}
