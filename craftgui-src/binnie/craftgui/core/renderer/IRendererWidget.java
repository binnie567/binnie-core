package binnie.craftgui.core.renderer;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.Widget;
import binnie.craftgui.core.geometry.IPosition;

/**
 * Part of the renderer that deals with the rendering of widgets
 * @author Alex Binnie
 *
 */
public interface IRendererWidget {

	void preRender(IWidget widget);
	
	void render(IWidget widget, int renderLevel);

	void postRender(Widget widget);
	
	public void renderTooltip(IPosition mousePosition, Tooltip tooltip);


}
