package binnie.craftgui.core.renderer;

public enum TextureType {
	
	/**
	 * Indicates the texture should be stretched to fill an area
	 */
	Stretched,
	
	/**
	 * Indicates the texture should be tiled to fill an area
	 */
	Tiled

}
