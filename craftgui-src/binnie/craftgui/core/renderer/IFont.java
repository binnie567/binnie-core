package binnie.craftgui.core.renderer;

import binnie.craftgui.core.geometry.IPosition;

public interface IFont {
	
	public float getWidth(String text);
	
	public float getHeight(String text);

	public String[] convertToMultiLine(String text, float x);

	public void renderText(IPosition position, String text);

}
