package binnie.craftgui.core.renderer;

import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.IPosition;

public interface ITexture {
	
	public void renderTexture(IPosition position);
	
	public void renderTexture(IArea area, TextureType fillStyle);

}
