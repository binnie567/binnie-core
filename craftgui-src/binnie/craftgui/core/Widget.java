package binnie.craftgui.core;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.lwjgl.opengl.GL11;

import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.core.renderer.IRenderer;
import binnie.craftgui.core.renderer.IRendererWidget;
import binnie.craftgui.events.Event;
import binnie.craftgui.events.EventWidget;
import binnie.craftgui.resource.ITexture;

public abstract class Widget implements IWidget {

	public Widget(IWidget parent) {
		this.parent = parent;
		CraftGUI.registerWidgetClasses(this);
		if (parent != null)
			parent.addWidget(this);
	}

	// Hierarchy Functions

	private IWidget parent = null;
	private List<IWidget> subWidgets = new ArrayList<IWidget>();

	@Override
	public final void deleteChild(IWidget child) {
		child.delete();
		this.subWidgets.remove(child);
	}
	
	@Override
	public final void deleteAllChildren() {
		while(!subWidgets.isEmpty())
			deleteChild(subWidgets.get(0));
	}

	@Override
	public final IWidget getParent() {
		return parent;
	}

	@Override
	public final ITopLevelWidget getSuperParent() {
		return isTopLevel() ? (ITopLevelWidget) this : parent.getSuperParent();
	}

	@Override
	public final IWidget addWidget(IWidget widget) {
		subWidgets.add(widget);
		return widget;
	}

	@Override
	public final List<IWidget> getWidgets() {
		return subWidgets;
	}

	@Override
	public final boolean isTopLevel() {
		return this instanceof ITopLevelWidget;
	}

	// Dimension and Positioning Functions

	private IPosition position = new Vector2f(0.0f, 0.0f);
	private IPosition size = new Vector2f(0.0f, 0.0f);
	private IPosition offset = new Vector2f(0.0f, 0.0f);
	
	@Override
	public final IPosition pos() { return position.add(offset); };
	
	@Override
	public final IPosition size() { return size; };

	@Override
	public final IPosition getPosition() {
		return pos();
	}
	
	@Override
	public final IArea getArea() {
		return new Area(Vector2f.ZERO, size());
	}

	@Override
	public final IPosition getOriginalPosition() {
		return position;
	}

	IArea cropArea;
	IWidget cropWidget;
	boolean cropped = false;
	
	@Override
	public IArea getCroppedZone() {
		return cropArea;
	}
	
	@Override
	public void setCroppedZone(IWidget relative, IArea area) {
		cropArea = area;
		cropped = true;
		cropWidget = relative;
	}


	@Override
	public final IPosition getAbsolutePosition() {
		return isTopLevel() ? this.getPosition() : getParent()
				.getAbsolutePosition().add(getPosition());
	}

	@Override
	public final IPosition getOriginalAbsolutePosition() {
		return isTopLevel() ? this.getOriginalPosition() : 
				getParent().getOriginalPosition().sub(getOriginalPosition());
	}

	@Override
	public final IPosition getSize() {
		return size();
	}

	@Override
	public final IPosition getOffset() {
		return offset;
	}

	@Override
	public final void setPosition(IPosition vector) {
		if (!vector.equals(position)) {
			this.position = new Vector2f(vector);
			callEvent(new EventWidget.ChangePosition(this));
		}
	}

	@Override
	public final void setSize(IPosition vector) {
		if (!vector.equals(size)) {
			this.size = new Vector2f(vector);
			callEvent(new EventWidget.ChangeSize(this));
		}
	}

	@Override
	public final void setOffset(IPosition vector) {
		if (vector != offset) {
			this.offset = new Vector2f(vector);
			callEvent(new EventWidget.ChangeOffset(this));
		}
	}

	// Colour related Functions

	int colour = 0xFFFFFF;

	@Override
	public final void setColour(int colour) {
		if(this.colour != colour) {
			this.colour = colour;
			callEvent(new EventWidget.ChangeColour(this));
		}
	}

	@Override
	public final int getColour() {
		return colour;
	}

	// Widget Functionality

	@Override
	public boolean canMouseOver() {
		return false;
	}

	@Override
	public boolean canFocus() {
		return false;
	}

	// Event related Functions

	@Override
	public final void callEvent(Event event) {
		getSuperParent().recieveEvent(event);
	}

	@Override
	public final void recieveEvent(Event event) {

		CraftGUI.handleEvent(this, event);

		for (IWidget child : this.getWidgets())
			child.recieveEvent(event);
	}

	// Input Related Functions

	@Override
	public final IPosition getMousePosition() {
		return getSuperParent().getAbsoluteMousePosition();
	}

	@Override
	public final IPosition getRelativeMousePosition() {
		return isTopLevel() ? this.getMousePosition() :
				getParent().getRelativeMousePosition().sub(getPosition());
	}

	// Render Functions

	@Override
	public final <T> T getRenderer(Class<T> renderer) {
		return getSuperParent().getTopLevelRenderer(renderer);
	}

	@Override
	public boolean isCroppedWidet() {
		return cropped;
	}

	private final void preRender() {
		GL11.glPushMatrix();
		GL11.glTranslatef(this.getPosition().x(), this.getPosition().y(), 0.0F);

		getRenderer(IRendererWidget.class).preRender(this);
	}

	private final void postRender() {
		getRenderer(IRendererWidget.class).postRender(this);
		GL11.glPopMatrix();
		
	}
	
	public final IWidget getCropWidget() {
		return cropWidget == null ? this : cropWidget;
	}

	@Override
	public final void renderBackground() {
		if (this.isVisible()) {
			preRender();
			onRenderBackground();
			for (IWidget widget : getWidgets()) {
				widget.renderBackground();
			}
			postRender();
		}
	}

	@Override
	public final void renderForeground() {
		if (this.isVisible()) {
			preRender();
			onRenderForeground();
			for (IWidget widget : getWidgets()) {
				widget.renderForeground();
			}
			postRender();
		}
	}

	@Override
	public final void renderOverlay() {
		if (this.isVisible()) {
			preRender();
			onRenderOverlay();
			for (IWidget widget : getWidgets()) {
				widget.renderOverlay();
			}
			postRender();
		}
	}

	// Update functions

	@Override
	public final void update() {
		if (!this.isVisible())
			return;

		if (this.getSuperParent() == this)
			((ITopLevelWidget) this).updateTopLevel();

		this.onUpdate();

		for (IWidget widget : getWidgets()) {
			widget.update();
		}
	}

	@Override
	public final IWidget calculateMousedOverWidget() {

		boolean amIMousedOver = calculateIsMouseOver();

		if (!isVisible())
			return null;

		if (amIMousedOver || !isCroppedWidet()) {
			
			ListIterator<IWidget> li = getWidgets().listIterator(getWidgets().size());
			
			while (li.hasPrevious()) {
				IWidget child = li.previous();
				IWidget result = child.calculateMousedOverWidget();
				if (result != null)
					return result;
			}
		}

		if (canMouseOver() && amIMousedOver)
			return this;
		else
			return null;
	}

	@Override
	public final boolean calculateIsMouseOver() {
		IPosition mouse = getRelativeMousePosition();
		if(!cropped) {
			return isMouseOverWidget(mouse);
		}
		else {
			IWidget cropRelative = cropWidget != null ? cropWidget : this;
			IPosition pos = Vector2f.sub(cropRelative.getAbsolutePosition(), getAbsolutePosition());
			IPosition size = new Vector2f(cropArea.size().x(), cropArea.size().y());
			return mouse.x() > pos.x() && mouse.y() > pos.y() && mouse.x() < pos.x() + size.x()
					&& mouse.y() < pos.y() + size.y();
		}
		
	}
	
	@Override
	public boolean isMouseOverWidget(IPosition relativeMouse) {
		return getArea().contains(relativeMouse);
	}

	// State related Functions

	private boolean enabled = true;
	private boolean visible = true;

	@Override
	public final void enable() {
		enabled = true;
		callEvent(new EventWidget.Enable(this));
	}

	@Override
	public final void disable() {
		enabled = false;
		callEvent(new EventWidget.Disable(this));
	}

	@Override
	public final void show() {
		visible = true;
		callEvent(new EventWidget.Show(this));
	}

	@Override
	public final void hide() {
		visible = false;
		callEvent(new EventWidget.Hide(this));
	}

	@Override
	public boolean isEnabled() {
		return enabled
				&& (isTopLevel() ? true : getParent().isEnabled()
						&& getParent().isChildEnabled(this));
	}

	@Override
	public final boolean isVisible() {
		return visible
				&& (isTopLevel() ? true : getParent().isVisible()
						&& getParent().isChildVisible(this));
	}

	@Override
	public final boolean isFocused() {
		return getSuperParent().isFocused(this);
	}

	@Override
	public final boolean isDragged() {
		return getSuperParent().isDragged(this);
	}

	@Override
	public final boolean isMouseOver() {
		return getSuperParent().isMouseOver(this);
	}

	@Override
	public boolean isChildVisible(IWidget child) {
		return true;
	}

	@Override
	public boolean isChildEnabled(IWidget child) {
		return true;
	}

	// Methods to override

	@Override
	public void onRenderBackground() {
		getRenderer(IRendererWidget.class).render(this, 0);
	}

	@Override
	public void onRenderForeground() {
		getRenderer(IRendererWidget.class).render(this, 1);
	}

	@Override
	public void onRenderOverlay() {
		getRenderer(IRendererWidget.class).render(this, 2);
	}


	@Override
	public void onUpdate() {
	};

	@Override
	public final void delete() {
		this.getSuperParent().widgetDeleted(this);
		onDelete();
	}

	@Override
	public void onDelete() {
	}
	
	@Override
	public <T> T getWidget(Class<T> x) {
		for(IWidget child : this.getWidgets()) {
			if(x.isInstance(child)) return (T) child;
			T found = child.getWidget(x);
			if(found != null) return found;
		}
		return null;
	}
	
	@Override
	public abstract ITexture getTexture(Object key);
	
	@Override
	public final boolean contains(IPosition position) {
		return getArea().contains(position);
	}
	
	public IRenderer getRenderer() {
		return getRenderer(IRenderer.class);
	}
	

}
