package binnie.craftgui.core;

import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.core.renderer.IRenderer;

public interface ITopLevelWidget extends IWidget {

	public void setMousePosition(int x, int y);

	public IPosition getAbsoluteMousePosition();

	public IWidget getFocusedWidget();

	public IWidget getMousedOverWidget();

	public IWidget getDraggedWidget();

	public boolean isFocused(IWidget widget);

	public boolean isMouseOver(IWidget widget);

	public boolean isDragged(IWidget widget);

	public <T> T getTopLevelRenderer(Class<T> renderer);

	public void updateTopLevel();

	void setRenderer(IRenderer renderer);

	public void widgetDeleted(IWidget widget);
	
	public float getTextWidth(String text);

	public float getTextHeight();
	
	public float getTextHeight(String text, float width);

	public IPosition getDragDistance();
}
