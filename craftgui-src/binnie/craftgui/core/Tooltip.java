package binnie.craftgui.core;

import java.util.ArrayList;
import java.util.List;

public class Tooltip {
	
	public static interface ITooltipType {
		
	}
	
	public void add(String string) {
		tooltip.add(string);
	}
	
	public String getLine(int index) {
		String string = (String) getList().get(index);
		return string;
	}
	
	public void add(List list) {
		for(Object obj : list) {
			tooltip.add((String) obj);
		}
	}
	
	List<String> tooltip = new ArrayList<String>();

	public List<String> getList() {
		return tooltip;
	}
	
	public boolean exists() {
		return tooltip.size()>0;
	}
	
	
	
	public static enum Type implements ITooltipType {
		Standard,
		Help,
		Information
		;
	}
	
	public void setType(ITooltipType type) {
		this.type = type;
	}
	
	ITooltipType type = Type.Standard;
	public int maxWidth = 2000;
	public void setMaxWidth(int w) {
		this.maxWidth = w;
	}

	public ITooltipType getType() {
		return type;
	}

}
