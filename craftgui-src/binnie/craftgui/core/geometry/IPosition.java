package binnie.craftgui.core.geometry;

/**
 * Geometric representation of positions, sizes. Contains two coordinates as floats
 * @author Alex Binnie
 *
 */
public interface IPosition {
	/**
	 * Get the X Component of the IPosition
	 * @return The X Component
	 */
	public float x();
	/**
	 * Get the Y Component of the IPosition
	 * @return The Y Component
	 */
	public float y();
	/**
	 * Get a summation of this position and another
	 * @param other The IPosition to add
	 * @return The sum of the two vectors
	 */
	IPosition add(IPosition other);
	IPosition sub(IPosition other);
	void set(float x, float y);
	void setX(float x);
	void setY(float y);
	public IPosition copy();
	boolean equals(IPosition other);

}
