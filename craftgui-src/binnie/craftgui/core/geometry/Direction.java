package binnie.craftgui.core.geometry;

public enum Direction {
	
	Upwards,
	Right,
	Downwards, 
	Left
	;

}
