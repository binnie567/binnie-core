package binnie.craftgui.core.geometry;

public class Vector2f implements IPosition {

	public static final IPosition ZERO = new Vector2f(0, 0);
	float x = 0.0f;
	float y = 0.0f;

	public Vector2f(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Vector2f(IPosition o) {
		this.x = o.x();
		this.y = o.y();
	}

	public static IPosition add(IPosition a, IPosition b) {
		return new Vector2f(a.x() + b.x(), a.y() + b.y());
	}

	public static IPosition sub(IPosition a, IPosition b) {
		return new Vector2f(a.x() - b.x(), a.y() - b.y());
	}
	
	public IPosition sub(IPosition a) {
		return sub(this, a);
	}
	
	public IPosition add(IPosition other) {
		return add(this, other);
	}

	public Vector2f copy() {
		return new Vector2f(this);
	}

	@Override
	public float x() {
		return x;
	}

	@Override
	public float y() {
		return y;
	}

	@Override
	public void set(float x, float y) {
		setX(x);
		setY(y);
	}

	@Override
	public void setX(float x) {
		this.x = x;
	}

	@Override
	public void setY(float y) {
		this.y = y;
	}
	
	@Override
	public boolean equals(IPosition other) {
		return x() == other.x() && y() == other.y();
	}
	

}
