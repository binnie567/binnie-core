package binnie.craftgui.core.geometry;

/**
 * Enum used for justifying text within an area when rendering
 * @author Alex Binnie
 *
 */
public enum TextJustification {
	
	TopLeft(0f, 0f),
	TopCenter(0.5f, 0f),
	TopRight(1f, 0f),
	MiddleLeft(0f, 0.5f),
	MiddleCenter(0.5f, 0.5f),
	MiddleRight(1f, 0.5f),
	BottomLeft(0f, 1f),
	BottomCenter(0.5f, 1f),
	BottomRight(1f, 1f),
	;
	
	float xOffset;
	float yOffset;
	private TextJustification(float xOffset, float yOffset) {
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}
	public float getXOffset() {
		return xOffset;
	}
	public float getYOffset() {
		return yOffset;
	}
	
	
}
