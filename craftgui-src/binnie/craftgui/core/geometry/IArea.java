package binnie.craftgui.core.geometry;

/**
 * Geometric Intepretation of an Area, with a position and a size
 * 
 * @author Alex Binnie
 * 
 */
public interface IArea {

	/**
	 * Get the start position of the area. This should not be a copy, and be alterable directly
	 * @return IPosition relative to a certain position
	 */
	public IPosition getPosition();

	/**
	 * Set the start position of the area 
	 * @param position New position of the area
	 */
	public void setPosition(IPosition position);

	/**
	 * Get the size of the area. This should not be a copy, and be alterable directly
	 * @return IPosition representing the width/height
	 */
	public IPosition getSize();

	/**
	 * Set the start position of the area 
	 * @param position New position of the area
	 */
	public void setSize(IPosition size);

	/**
	 * Shorthand for getPosition().
	 * @return IPosition relative to a certain position
	 */
	public IPosition pos();

	/**
	 * Shorthand for getSize().
	 * @return IPosition representing the width/height
	 */
	public IPosition size();

	/**
	 * Checks if an IPosition is contained within an area
	 * @param position The position to check if it is located within the area
	 * @return True if the area contains the position, false otherwise
	 */
	public boolean contains(IPosition position);

}
