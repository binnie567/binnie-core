package binnie.craftgui.core.geometry;

import binnie.craftgui.core.IWidget;

public class CraftGUIUtil {
	
	public static void alignToWidget(IWidget target, IWidget relativeTo) {
		IPosition startPos = target.getAbsolutePosition();
		IPosition endPos = relativeTo.getAbsolutePosition();
		moveWidget(target, endPos.sub(startPos));
	}

	public static void moveWidget(IWidget target, IPosition movement) {
		target.setPosition(target.getPosition().add(movement));
	}
	
	public static IArea getPaddedArea(IArea area, int padding) {
		if(padding * 2 < area.size().x() && padding * 2 < area.size().y())
			return new Area(new Vector2f(area.pos().x()+padding, area.pos().y()+padding), area.size().sub(new Vector2f(padding*2, padding*2)));
		return area;
	}

}
