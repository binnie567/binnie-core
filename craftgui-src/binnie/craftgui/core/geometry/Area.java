package binnie.craftgui.core.geometry;

/**
 * Standard CraftGUI implementation of IArea. Should use this in all normal
 * circumstances
 * 
 * @author Alex Binnie
 * 
 */
public class Area implements IArea {

	private IPosition pos;
	private IPosition size;

	public Area(IArea area) {
		this(area.pos().x(), area.pos().y(), area.size().x(), area.size().y());
	}

	public Area(IPosition pos, IPosition size) {
		this(pos.x(), pos.y(), size.x(), size.y());
	}

	public Area(float x, float y, float w, float h) {
		setPosition(new Vector2f(x, y));
		setSize(new Vector2f(w, h));
	}

	@Override
	public IPosition pos() {
		return pos;
	}

	@Override
	public IPosition getPosition() {
		return pos;
	}

	@Override
	public void setPosition(IPosition position) {
		this.pos = position.copy();
	}

	@Override
	public IPosition size() {
		return size;
	}

	@Override
	public IPosition getSize() {
		return size;
	}

	@Override
	public void setSize(IPosition size) {
		this.size = size.copy();
	}

	@Override
	public String toString() {
		return super.toString() + " [" + pos().x() + "," + pos().y() + "] [" + size().x() + ","
				+ size().y() + "]";
	}

	@Override
	public boolean contains(IPosition position) {
		return position.x() > pos().x() && position.y() > pos.y()
				&& position.x() < pos().x() + size().x() && position.y() < pos().y() + size().y();
	}

}
