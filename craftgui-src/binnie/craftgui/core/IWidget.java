package binnie.craftgui.core;

import java.util.List;

import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.events.Event;
import binnie.craftgui.events.EventKey;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.resource.IStyleSheet;

/**
 * Base interface which all widgets implement. This includes the window, buttons, text, etc.
 * @author Alex Binnie
 *
 */
public interface IWidget extends IArea, IStyleSheet {
	
	// Hierarchy Methods

	public IWidget getParent();

	public void deleteChild(IWidget child);
	
	public void deleteAllChildren();

	public ITopLevelWidget getSuperParent();

	public <T> T getRenderer(Class<T> renderer);

	public boolean isTopLevel();
	
	public IPosition getPosition();
	
	public IPosition getSize();

	public IPosition getOriginalPosition();

	public IPosition getAbsolutePosition();

	public IPosition getOriginalAbsolutePosition();

	public IPosition getOffset();
	
	public IArea getArea();

	public void setOffset(IPosition offset);

	public IPosition getMousePosition();

	public IPosition getRelativeMousePosition();

	public void setColour(int colour);

	public int getColour();

	public void renderBackground();

	public void renderForeground();

	public void renderOverlay();

	public void update();

	public void enable();

	public void disable();

	public void show();

	public void hide();

	public IWidget calculateMousedOverWidget();

	public boolean calculateIsMouseOver();

	public boolean isEnabled();

	public boolean isVisible();

	public boolean isFocused();

	public boolean isMouseOver();

	public boolean isDragged();

	public boolean isChildVisible(IWidget child);

	public boolean isChildEnabled(IWidget child);

	public boolean canMouseOver();

	public boolean canFocus();

	public IWidget addWidget(IWidget widget);

	public List<IWidget> getWidgets();

	public void callEvent(Event event);

	public void recieveEvent(Event event);

	public void onRenderBackground();

	public void onRenderForeground();

	public void onRenderOverlay();

	public void onUpdate();

	public void delete();

	public void onDelete();

	<T> T getWidget(Class<T> x);

	IArea getCroppedZone();

	void setCroppedZone(IWidget relative, IArea area);

	boolean isCroppedWidet();

	public IWidget getCropWidget();

	boolean isMouseOverWidget(IPosition relativeMouse);

}
