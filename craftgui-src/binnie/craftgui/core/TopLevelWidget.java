package binnie.craftgui.core;

import org.lwjgl.input.Mouse;

import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.core.renderer.IRenderer;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.events.EventWidget;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;
import binnie.craftgui.resource.ITexture;

public abstract class TopLevelWidget extends Widget implements ITopLevelWidget {

	public TopLevelWidget() {
		super(null);
	}

	IWidget mousedOverWidget = null;
	IWidget draggedWidget = null;
	IWidget focusedWidget = null;

	public void setMousedOverWidget(IWidget widget) {
		if (mousedOverWidget == widget)
			return;
		if (mousedOverWidget != null)
			callEvent(new EventWidget.EndMouseOver(mousedOverWidget));
		mousedOverWidget = widget;
		if (mousedOverWidget != null)
			callEvent(new EventWidget.StartMouseOver(mousedOverWidget));
	}

	public void setDraggedWidget(IWidget widget) {
		this.setDraggedWidget(widget, -1);
	}
	
	public void setDraggedWidget(IWidget widget, int button) {
		if (draggedWidget == widget)
			return;
		if (draggedWidget != null)
			callEvent(new EventWidget.EndDrag(draggedWidget));
		draggedWidget = widget;
		if (draggedWidget != null)
			callEvent(new EventWidget.StartDrag(draggedWidget, button));
	}

	public void setFocusedWidget(IWidget widget) {
		IWidget newWidget = widget;
		if (focusedWidget == newWidget)
			return;
		if (newWidget != null && !newWidget.canFocus())
			newWidget = null;
		if (focusedWidget != null)
			callEvent(new EventWidget.LoseFocus(focusedWidget));
		focusedWidget = newWidget;
		if (focusedWidget != null)
			callEvent(new EventWidget.GainFocus(focusedWidget));
	}

	@Override
	public IWidget getMousedOverWidget() {
		return mousedOverWidget;
	}

	@Override
	public IWidget getDraggedWidget() {
		return draggedWidget;
	}

	@Override
	public IWidget getFocusedWidget() {
		return focusedWidget;
	}

	@Override
	public boolean isMouseOver(IWidget widget) {
		return getMousedOverWidget() == widget;
	}

	@Override
	public boolean isDragged(IWidget widget) {
		return getDraggedWidget() == widget;
	}

	@Override
	public boolean isFocused(IWidget widget) {
		return getFocusedWidget() == widget;
	}

	@Override
	public void updateTopLevel() {

		setMousedOverWidget(super.calculateMousedOverWidget());

		if (Mouse.isButtonDown(0)) {
		} else {
			if (draggedWidget != null)
				setDraggedWidget(null);
		}
	}

	@EventHandler(origin=Origin.Any)
	public void onMouseClick(EventMouse.Down event) {
		setDraggedWidget(mousedOverWidget, event.getButton());
		setFocusedWidget(mousedOverWidget);
	}

	@EventHandler(origin=Origin.Any)
	public void onMouseUp(EventMouse.Up event) {
		setDraggedWidget(null);
	}

	protected IPosition mousePosition = new Vector2f(0.0f, 0.0f);

	@Override
	public void setMousePosition(int x, int y) {

		float dx = x - mousePosition.x();
		float dy = y - mousePosition.y();

		if (dx != 0 || dy != 0) {
			if (getDraggedWidget() != null) {
				callEvent(new EventMouse.Drag(getDraggedWidget(), dx, dy));
			}
			else {
				callEvent(new EventMouse.Move(this, dx, dy));
			}
		}

		mousePosition = new Vector2f(x, y);
		
		setMousedOverWidget(calculateMousedOverWidget());
	}

	@Override
	public IPosition getAbsoluteMousePosition() {
		return mousePosition;
	}

	IRenderer renderer;

	@Override
	public void setRenderer(IRenderer renderer) {
		this.renderer = renderer;
	}

	@Override
	public <T> T getTopLevelRenderer(Class<T> renderer) {
		if(this.renderer == null)
			return null;
		return this.renderer.subRenderer(renderer);
	}

	@Override
	public void widgetDeleted(IWidget widget) {
		if (isMouseOver(widget))
			setMousedOverWidget(null);
		if (isDragged(widget))
			setDraggedWidget(null);
		if (isFocused(widget))
			setFocusedWidget(null);
	}

	@Override
	public abstract ITexture getTexture(Object key);
	
	public float getTextWidth(String text) {
		if(getRenderer(IRenderer.class) != null)
			return getRenderer(IRenderer.class).getTextWidth(text);
		return 0f;
	}

	public float getTextHeight() {
		if(getRenderer(IRenderer.class) != null)
			return getRenderer(IRenderer.class).getTextHeight();
		return 0f;
	}
	
	public float getTextHeight(String text, float width) {
		if(getRenderer(IRenderer.class) != null)
			return getRenderer(IRenderer.class).getTextHeight(text, width);
		return 0f;
	}

	IPosition dragStart = Vector2f.ZERO;
	
	@Override
	public IPosition getDragDistance() {
		return getRelativeMousePosition().sub(dragStart);
	}
	
	@EventHandler(origin = Origin.Any)
	public void onStartDrag(EventWidget.StartDrag event) {
		dragStart = getRelativeMousePosition();
	}


}
