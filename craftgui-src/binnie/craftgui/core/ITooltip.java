package binnie.craftgui.core;


public interface ITooltip {
	
	public void getTooltip(Tooltip tooltip);
	
}
