package binnie.craftgui.resource;

import binnie.craftgui.core.geometry.Direction;

public interface ITexture {
	
	public int u();
	
	public int v();
	
	public int w();
	
	public int h();
	
	/** Obtains a Sub Texture from the direction specified and distance pixels into the texture */
	public ITexture subTexture(Direction direction, int distance);

}
