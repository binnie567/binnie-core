package binnie.craftgui.resource;

import java.util.HashMap;
import java.util.Map;

public class StyleSheetManager {

	static IStyleSheet defaultSS = new DefaultStyleSheet();
	
	public static Map<Object, ITexture> defaultTextures = new HashMap<Object, ITexture>();
	
	public static ITexture getTexture(Object key) {
		return defaultSS.getTexture(key);
	}
	
	private static class DefaultStyleSheet implements IStyleSheet {

		@Override
		public ITexture getTexture(Object key) {
			if(!defaultTextures.containsKey(key)) {
				throw new RuntimeException("No ITexture found in Stylesheet for key " + key);
			}
			return defaultTextures.get(key);
		}

	}

	public static IStyleSheet getDefault() {
		return defaultSS;
	}
	
	

}
