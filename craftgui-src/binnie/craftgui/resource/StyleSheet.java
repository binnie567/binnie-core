package binnie.craftgui.resource;

import java.util.HashMap;
import java.util.Map;

public class StyleSheet implements IStyleSheet {
	
	protected Map<Object, ITexture> textures = new HashMap<Object, ITexture>();

	@Override
	public ITexture getTexture(Object key) {
		if(!textures.containsKey(key)) {
			return StyleSheetManager.getTexture(key);
		}
		return textures.get(key);
	}

}
