package binnie.craftgui.resource;


public interface ITexturePadded extends ITexture {
	
	public int l();

	public int r();

	public int t();

	public int b();

}
