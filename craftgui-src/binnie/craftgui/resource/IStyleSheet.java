package binnie.craftgui.resource;

public interface IStyleSheet {
	
	public ITexture getTexture(Object key);

}
