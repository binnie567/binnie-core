package binnie.craftgui.events.core;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.Event;

@Retention( RetentionPolicy.RUNTIME )
public @interface EventHandler {
	
	Origin origin() default Origin.Any;
	
	public static enum Origin {
		Any,
		Self,
		DirectChild,
		Parent,
		SuperParent,
		;
		
		public boolean handlesEvent(IWidget widget, Event event) {
			IWidget origin = event.getOrigin();
			switch(this) {
			case Any:
				return true;
			case DirectChild:
				for(IWidget child : widget.getWidgets()) {
					if(child == origin) return true;
				}
				return false;
			case Parent:
				return origin == widget.getParent();
			case Self:
				return origin == widget;
			case SuperParent:
				return origin == widget.getSuperParent();
			}
			return false;
		}
	}

}
