package binnie.craftgui.events;

import binnie.craftgui.core.IWidget;

public class Event {

	IWidget origin;

	public Event(IWidget origin) {
		super();
		this.origin = origin;
	}

	public IWidget getOrigin() {
		return origin;
	}

	public boolean isOrigin(IWidget widget) {
		return origin == widget;
	}

}
