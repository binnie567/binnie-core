package binnie.craftgui.events;

import binnie.craftgui.core.IWidget;

public abstract class EventKey extends Event {

	char character;
	int key;

	public EventKey(IWidget origin, char character, int key) {
		super(origin);
		this.character = character;
		this.key = key;
	}

	public char getCharacter() {
		return character;
	}

	public int getKey() {
		return key;
	}

	public static class Down extends EventKey {

		public Down(IWidget origin, char character, int key) {
			super(origin, character, key);
		}
		
	}
	
	public static class Up extends EventKey {

		public Up(IWidget origin, char character, int key) {
			super(origin, character, key);
		}
		
	}
	
}
