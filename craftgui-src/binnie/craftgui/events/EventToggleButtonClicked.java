package binnie.craftgui.events;

import binnie.craftgui.core.IWidget;

public class EventToggleButtonClicked extends Event {

	public EventToggleButtonClicked(IWidget origin, boolean toggled) {
		super(origin);
		this.toggled = toggled;
	}

	boolean toggled;

	public boolean isActive() {
		return toggled;
	}

}
