package binnie.craftgui.events;

import binnie.craftgui.core.ITopLevelWidget;
import binnie.craftgui.core.IWidget;

public abstract class EventMouse extends Event {

	public EventMouse(IWidget origin) {
		super(origin);
		
	}
	
	public static class Button extends EventMouse {

		int x;
		int y;
		int button;
		
		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

		public int getButton() {
			return button;
		}

		public Button(IWidget currentMousedOverWidget, int x, int y, int button) {
			super(currentMousedOverWidget);
			this.x = x;
			this.y = y;
			this.button = button;
		}
		
	}

	public static class Down extends Button {

		public Down(IWidget currentMousedOverWidget, int x, int y, int button) {
			super(currentMousedOverWidget, x, y, button);
		}
		
	}
	
	public static class Up extends Button {

		public Up(IWidget currentMousedOverWidget, int x, int y, int button) {
			super(currentMousedOverWidget, x, y, button);
		}
		
	}
	
	public static class Move extends EventMouse {

		public float getDx() {
			return dx;
		}

		public float getDy() {
			return dy;
		}

		public Move(IWidget origin, float dx, float dy) {
			super(origin);
			this.dx = dx;
			this.dy = dy;
		}

		float dx;
		float dy;

	}
	
	public static class Drag extends Move {

		public Drag(IWidget draggedWidget, float dx, float dy) {
			super(draggedWidget, dx, dy);
		}

	}
	
	public static class Wheel extends EventMouse {

		int dWheel = 0;
		
		public Wheel(IWidget origin, int dWheel) {
			super(origin);
			this.dWheel = dWheel/28;
		}
		
		public int getDWheel() {
			return dWheel;
		}

	}




}
