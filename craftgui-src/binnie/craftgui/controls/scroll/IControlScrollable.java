package binnie.craftgui.controls.scroll;

import binnie.craftgui.core.IWidget;

public interface IControlScrollable extends IWidget {

	public float getPercentageShown();

	public float getPercentageIndex();

	public void movePercentage(float percentage);
	
	public void setPercentageIndex(float index);

}
