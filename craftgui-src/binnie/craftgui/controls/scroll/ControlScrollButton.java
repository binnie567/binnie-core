package binnie.craftgui.controls.scroll;

import binnie.craftgui.controls.button.ControlButton;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.events.EventMouse;

public class ControlScrollButton extends ControlButton {

	enum Position {
		Top, Bottom
	}

	Position pos;
	ControlScroll parent;

	public ControlScrollButton(ControlScroll parent, Position position) {
		super(parent, 0, 0, 0, 0, "");
		this.parent = parent;
		setSize(new Vector2f(parent.getSize().x(), parent.getSize().x()));
		this.pos = position;
		if (position == Position.Top) {
			setPosition(Vector2f.ZERO);
		} else {
			setPosition(new Vector2f(0, (int) (parent.getSize().y() - parent.getSize().x())));
		}

	}

	@Override
	public void onMouseClick(EventMouse.Down event) {
		super.onMouseClick(event);
		pressButton();
	}

	public void pressButton() {
		float dir = 0;
		if (pos == Position.Top)
			dir = -1f;
		else
			dir = 1f;

		parent.getScrollableWidget().movePercentage(dir * 0.01f);

	}

	int c = 0;

	@Override
	public void onUpdate() {
		if (this.isDragged()) {
			c++;
			if (c > 5) {
				c = 0;
				pressButton();
			}
		}
	}

}
