package binnie.craftgui.controls.scroll;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;

public class ControlScrollBar extends Control {

	@EventHandler(origin=Origin.Self)
	public void onMouseDrag(EventMouse.Drag event) {
		parent.getScrollableWidget().movePercentage((event.getDy()) / maxHeight);
	}

	ControlScroll parent;

	float maxHeight = 0.0f;

	public ControlScrollBar(ControlScroll parent) {
		super(parent, 0, parent.getSize().x(), parent.getSize().x(), parent
				.getSize().y() - parent.getSize().x() * 2);
		maxHeight = parent.getSize().y() - parent.getSize().x() * 2;
		this.parent = parent;
		this.canMouseOver = true;
	}

	@Override
	public void onUpdate() {
		float yOffset = maxHeight * parent.getScrollableWidget().getPercentageIndex();
		setOffset(new Vector2f(0, yOffset));
	}
	
	@Override
	public boolean isEnabled() {
		return parent.getScrollableWidget().getPercentageShown()<1f;
	}
	
	public float getBarHeight() {
		return maxHeight * parent.getScrollableWidget().getPercentageShown();
	}

}
