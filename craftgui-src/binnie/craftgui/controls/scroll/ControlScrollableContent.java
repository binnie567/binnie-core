package binnie.craftgui.controls.scroll;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;

/**
 * Control that incorporates a scroll bar 
 * @author Alex Binnie
 *
 */
public class ControlScrollableContent<T extends IWidget> extends Control implements IControlScrollable {

	@EventHandler(origin=Origin.Any)
	public void onMouseWheel(EventMouse.Wheel event) {
		if (getRelativeMousePosition().x() > 0 && getRelativeMousePosition().y() > 0
				&& getRelativeMousePosition().x() < getSize().x()
				&& getRelativeMousePosition().y() < getSize().y()) {
			float percentageMove = 2f / getSize().y();
			movePercentage(percentageMove * -((EventMouse.Wheel) event).getDWheel());
		}
	}

	protected T controlChild;

	public ControlScrollableContent(IWidget parent, float x, float y, float w, float h, float scrollBarSize) {
		super(parent, x, y, w, h);
		new ControlScroll(this, this.getSize().x() - scrollBarSize, 0, scrollBarSize, this.getSize().y(), this);
	}

	public void setScrollableContent(T child) {
		this.controlChild = child;
		child.setCroppedZone(this, new Area(1, 1, getSize().x() - 14f, getSize().y() - 2f));
	}
	
	public T getContent() {
		return controlChild;
	}

	@Override
	public float getPercentageShown() {
		float shown = this.getSize().y() / this.controlChild.getSize().y();
		return Math.min(shown, 1f);
	}

	float percentageIndex = 0.0f;

	@Override
	public float getPercentageIndex() {
		return percentageIndex;
	}

	int percentageIndexOffset = 0;

	@Override
	public void movePercentage(float percentage) {
		percentageIndex += percentage;
		percentageIndexOffset = 0;
		if (percentageIndex + getPercentageShown() > 1f) {
			percentageIndex = 1f - getPercentageShown();
			percentageIndexOffset = 1;
		}
		if (percentageIndex < 0f)
			percentageIndex = 0f;

		controlChild.setOffset(new Vector2f(0, -percentageIndex * this.controlChild.getSize().y()
				- percentageIndexOffset));
	}

	@Override
	public void setPercentageIndex(float index) {
		movePercentage(index - percentageIndex);
	}
	
	@Override
	public void onUpdate() {
		setPercentageIndex(getPercentageIndex());
	}
	
	

}