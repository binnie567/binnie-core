package binnie.craftgui.controls.page;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.controls.core.IControlValues;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.EventValueChanged;

/** An Invisible Control that holds a range of values, and only renders the corresponding child */
public class ControlPages<T> extends Control implements IControlValues<T>,
		IControlValue<T> {

	@Override
	public boolean isChildVisible(IWidget child) {
		if (child == null)
			return false;
		return pages.get(getValue()) == child;
	}

	Map<T, IControlValue<T>> pages = new LinkedHashMap<T, IControlValue<T>>();

	public ControlPages(IWidget parent, float x, float y, float w, float h) {
		super(parent, x, y, w, h);
	}

	public void addChild(IControlValue<T> page) {
		pages.put(page.getValue(), page);
		if (getValue() == null)
			setValue(page.getValue());
	}

	T value = null;

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public void setValue(T value) {
		if (this.value != value) {
			this.value = value;
			this.callEvent(new EventValueChanged<T>(this, value));
		}
	}

	@Override
	public T[] getValues() {
		return (T[]) pages.keySet().toArray();
	}

	@Override
	public void setValues(T[] values) {
	}

	public Collection<IControlValue<T>> getPages() {
		return pages.values();
	}

}
