package binnie.craftgui.controls.page;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.core.IWidget;

public class ControlPage<T> extends Control implements IControlValue<T> {

	public ControlPage(IWidget parent, float x, float y, float w, float h, T value) {
		super(parent, x, y, w, h);
		setValue(value);
	}

	T value;
	
	@Override
	public T getValue() {
		return value;
	}

	@Override
	public void setValue(T value) {
		this.value = value;
	}

}
