package binnie.craftgui.controls.tab;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.Event;
import binnie.craftgui.events.EventValueChanged;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;

public class ControlTabBar<T> extends Control implements IControlValue<T> {

	T value;

	public ControlTabBar(IWidget parent, int x, int y, int width, int height) {
		super(parent, x, y, width, height);

	}

	public void setValues(T[] ts) {
		for (int i = 0; i < getWidgets().size();)
			deleteChild(getWidgets().get(0));
		float length = ts.length;
		int tabHeight = (int) (getSize().y() / length);
		for (int i = 0; i < length; i++) {
			IWidget tab = new ControlTab<T>(this, 0, i * tabHeight, getSize().x(), tabHeight, ts[i]);
		}
	}

	@EventHandler(origin=Origin.DirectChild)
	public void onValueChange(EventValueChanged event) {
		setValue(((EventValueChanged<T>) event).getValue());
	}

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public void setValue(T value) {
		this.value = value;
	}

}
