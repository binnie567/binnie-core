package binnie.craftgui.controls.tab;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.events.EventValueChanged;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;

public class ControlTab<T> extends Control implements ITooltip,
		IControlValue<T> {

	@EventHandler(origin=Origin.Self)
	public void onMouseClick(EventMouse.Down event) {
		callEvent(new EventValueChanged(this, this.value));
	}

	private ControlTabBar<T> tabBar;
	private T value;

	public ControlTab(ControlTabBar<T> parent, float x, float y, float w,
			float h, T value) {
		super(parent, x, y, w, h);
		setValue(value);
		this.tabBar = parent;
		canMouseOver = true;
	}

	@Override
	public void getTooltip(Tooltip tooltip) {
		tooltip.add(value.toString());
	}

	public T getValue() {
		return value;
	}

	@Override
	public void setValue(T value) {
		this.value = value;
	}
	
	public boolean isCurrentSelection() {
		return getValue() != null && getValue().equals(tabBar.getValue());
	}

}
