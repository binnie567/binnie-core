package binnie.craftgui.controls.tab;

import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.controls.core.IControlValues;
import binnie.craftgui.core.IWidget;

public class ControlTabBarWidget<T> extends ControlTabBar<T> {
	
	public ControlTabBarWidget(IWidget parent, int x, int y, int width,
			int height, IControlValue<T> tabbed) {
		super(parent, x, y, width, height);
		this.tabbedWidget = tabbed;
		if(tabbedWidget instanceof IControlValues<?>) {
			setValues(((IControlValues<T>) tabbedWidget).getValues());
		}
	}

	IControlValue<T> tabbedWidget;
	
	@Override
	public T getValue() {
		return tabbedWidget.getValue();
	}

	@Override
	public void setValue(T value) {
		tabbedWidget.setValue(value);
	}

}
