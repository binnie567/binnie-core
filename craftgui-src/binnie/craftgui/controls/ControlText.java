package binnie.craftgui.controls;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.core.geometry.TextJustification;
import binnie.craftgui.core.geometry.Vector2f;

public class ControlText extends Control implements IControlValue<String> {

	private String text;
	private TextJustification align;

	public ControlText(IWidget parent, IPosition pos, String text) {
		this(parent, new Area(pos, new Vector2f(500, 0)), text, TextJustification.TopLeft);
	}
	
	public ControlText(IWidget parent, String text, TextJustification align) {
		this(parent, parent.getArea(), text, align);
	}

	public ControlText(IWidget parent, IArea area,
			String text, TextJustification align) {
		super(parent, area.pos().x(), area.pos().y(), area.size().x(), area.size().y());
		setValue(text);
		this.align = align;
	}

	@Override
	public void onRenderBackground() {
		getRenderer().renderText(getArea(), align, text, getColour());
	}

	public void setValue(String text) {
		this.text = text;
	}

	public String getValue() {
		return text;
	}

}