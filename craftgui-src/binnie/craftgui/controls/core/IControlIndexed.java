package binnie.craftgui.controls.core;

public interface IControlIndexed {

	int getIndex();

	void setIndex(int value);

}
