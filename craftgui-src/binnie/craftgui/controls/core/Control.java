package binnie.craftgui.controls.core;

import java.util.ArrayList;
import java.util.List;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.Widget;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.resource.ITexture;

public class Control extends Widget {

	@Override
	public boolean canFocus() {
		return canFocus;
	}

	protected boolean canMouseOver = false;;
	protected boolean canFocus = false;;

	protected Control(IWidget parent, float x, float y, float w, float h) {
		super(parent);
		setPosition(new Vector2f(x, y));
		setSize(new Vector2f(w, h));
	}

	@Override
	public final boolean canMouseOver() {
		return canMouseOver;
	}

	@Override
	public void onUpdate() {
	}
	
	public void onGetHelp(Tooltip tooltip) {
		tooltip.add(helpStrings);
	}
	
	List<String> helpStrings = new ArrayList<String>();
	
	public void addHelp(String string) {
		helpStrings.add(string);
	}

	@Override
	public ITexture getTexture(Object key) {
		return getSuperParent().getTexture(key);
	}


}
