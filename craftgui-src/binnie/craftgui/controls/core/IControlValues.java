package binnie.craftgui.controls.core;

public interface IControlValues<T> extends IControlValue<T> {

	T[] getValues();

	void setValues(T[] values);

}
