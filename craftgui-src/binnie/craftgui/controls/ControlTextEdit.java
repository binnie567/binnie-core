package binnie.craftgui.controls;

import org.lwjgl.input.Keyboard;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.geometry.CraftGUIUtil;
import binnie.craftgui.core.geometry.TextJustification;
import binnie.craftgui.events.EventKey;
import binnie.craftgui.events.EventTextEdit;
import binnie.craftgui.events.EventValueChanged;
import binnie.craftgui.events.EventWidget;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;

public class ControlTextEdit extends Control implements IControlValue<String> {

	char[] allowedCharacters = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
			'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B',
			'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
			'T', 'U', 'V', 'W', 'X', 'T', 'Z', ' ', '(', ')', '[', ']' };

	@EventHandler(origin = Origin.Self)
	public void onKeyTyped(EventKey.Down event) {
		if (event.getKey() == Keyboard.KEY_BACK && text.getValue().length() > 0) {
			text.setValue(text.getValue().substring(0, text.getValue().length() - 1));
		} else {
			char character = event.getCharacter();
			for (char allowed : allowedCharacters) {
				if (character == (allowed)) {
					text.setValue(text.getValue() + allowed);
				}
			}
		}
		this.callEvent(new EventValueChanged<String>(this, text.getValue()));

	}

	ControlText text;

	public ControlTextEdit(IWidget parent, float x, float y, float width) {
		super(parent, x, y, width, 12);
		this.text = new ControlText(this, CraftGUIUtil.getPaddedArea(getArea(), 2), "",
				TextJustification.TopLeft);
		text.setColour(0x000000);
		setColour(0xAAAAAA);
		canFocus = true;
		canMouseOver = true;
	}

	@EventHandler(origin = Origin.Self)
	public void gainFocus(EventWidget.GainFocus event) {
		setColour(0xFFFFFF);
		text.setColour(0xFFFFFF);
	}
	
	@EventHandler(origin = Origin.Self)
	public void loseFocus(EventWidget.LoseFocus event) {
		setColour(0xAAAAAA);
		text.setColour(0x000000);
	}

	@Override
	public String getValue() {
		return text.getValue();
	}

	@Override
	public void setValue(String value) {
		text.setValue(value);
	}

	@EventHandler(origin = Origin.DirectChild)
	public void onTextChanged(EventValueChanged<String> event) {
		
	}
	
}
