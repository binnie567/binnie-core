package binnie.craftgui.controls.button;

import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.geometry.TextJustification;
import binnie.craftgui.events.EventButtonClicked;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;

public class ControlButton extends Control {

	@EventHandler(origin=Origin.Self)
	public void onMouseClick(EventMouse.Down event) {
		callEvent(new EventButtonClicked(this));
	}

	private ControlText textWidget;
	private String text;

	public ControlButton(IWidget parent, float x, float y, float width,
			float height) {
		super(parent, x, y, width, height);

		this.canMouseOver = true;
	}

	public ControlButton(IWidget parent, float x, float y, float width,
			float height, String text) {
		this(parent, x, y, width, height);

		int textHeight = (int) getSuperParent().getTextHeight();
		
		this.text = text;
		this.textWidget = new ControlText(this, getArea(), text, TextJustification.MiddleCenter);
	}

	@Override
	public void onUpdate() {
		if (textWidget != null)
			textWidget.setValue(getText());
	}

	public String getText() {
		return text;
	}

}
