package binnie.craftgui.controls.button;

import java.util.ArrayList;
import java.util.List;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.events.EventValueChanged;

public class ControlEnumButton<T extends Enum<T>> extends ControlButton {

	public static final String eventEnumChanged = "eventEnumButtonChanged";

	@Override
	public String getText() {
		return currentSelection.toString();
	}

	@Override
	public void onMouseClick(EventMouse.Down event) {
		int index = enumConstants.indexOf(currentSelection);
		if (index < (enumConstants.size() - 1))
			index += 1;
		else
			index = 0;

		T newEnum = enumConstants.get(index);

		setValue(newEnum);

	}

	public void setValue(T selection) {
		if (currentSelection != selection) {
			currentSelection = selection;
			callEvent(new EventValueChanged(this, this.getValue()));
		}
	}

	private T currentSelection;

	private List<T> enumConstants = new ArrayList<T>();

	public ControlEnumButton(IWidget parent, float x, float y, float width,
			float height, Class<T> enumClass) {
		super(parent, x, y, width, height, "");

		if (enumClass.getEnumConstants() != null) {
			int length = enumClass.getEnumConstants().length;
			float tabHeight = height / length;
			for (int i = 0; i < length; i++) {
				enumConstants.add(enumClass.getEnumConstants()[i]);
			}
			currentSelection = enumConstants.get(0);
		}
	}

	public T getValue() {
		return currentSelection;
	}

}
