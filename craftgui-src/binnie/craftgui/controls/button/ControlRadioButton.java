package binnie.craftgui.controls.button;

import binnie.craftgui.core.IWidget;

public class ControlRadioButton extends ControlEnumButton {

	enum Type2 {
		Ticked, Unticked
	}

	enum Type3 {
		Ticked, Unticked
	}

	public ControlRadioButton(IWidget parent, int x, int y, int width,
			int height, Class enumClass) {
		super(parent, x, y, width, height, enumClass);
	}

}
