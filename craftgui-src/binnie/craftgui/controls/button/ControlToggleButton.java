package binnie.craftgui.controls.button;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.events.EventToggleButtonClicked;

public class ControlToggleButton extends ControlButton {

	boolean toggled;

	@Override
	public void onMouseClick(EventMouse.Down event) {
		callEvent(new EventToggleButtonClicked(this, toggled));
	}

	public ControlToggleButton(IWidget parent, int x, int y, int width,
			int height) {
		super(parent, x, y, width, height);
	}

}
