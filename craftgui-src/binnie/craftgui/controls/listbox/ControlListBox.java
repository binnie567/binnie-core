package binnie.craftgui.controls.listbox;

import java.util.Collection;

import org.lwjgl.input.Keyboard;

import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.controls.scroll.ControlScrollableContent;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.Event;
import binnie.craftgui.events.EventKey;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;

public class ControlListBox<T> extends ControlScrollableContent<ControlList<T>> implements IControlValue<T> {

	@EventHandler(origin=Origin.Any)
	public void onKeyDown(EventKey.Down event) {
		EventKey.Down eventKey = (EventKey.Down) event;
		if (calculateIsMouseOver()) {
			int currentIndex = getContent().getCurrentIndex();
			if (eventKey.getKey() == Keyboard.KEY_DOWN) {
				currentIndex++;
				if (currentIndex >= getContent().getOptions().size())
					currentIndex = 0;
			} else if (eventKey.getKey() == Keyboard.KEY_UP) {
				currentIndex--;
				if (currentIndex < 0)
					currentIndex = getContent().getOptions().size() - 1;
			}
			getContent().setIndex(currentIndex);
		}
	}

	public ControlListBox(IWidget parent, float x, float y, float w, float h, float scrollBarSize) {
		super(parent, x, y, w, h, scrollBarSize);
		setScrollableContent(new ControlList<T>(this, 1, 1, w - 14, h - 2));
	}

	@Override
	public final T getValue() {
		return getContent().getValue();
	}

	@Override
	public final void setValue(T value) {
		getContent().setValue(value);
	}

	public void setOptions(Collection<T> options) {
		getContent().setOptions(options);
	}

	public IWidget createOption(T value, int y) {
		return new ControlOption<T>(getContent(), value, y);
	}

}
