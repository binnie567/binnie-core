package binnie.craftgui.controls.listbox;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.events.EventValueChanged;

public class ControlList<T> extends Control implements IControlValue<T> {
	
	protected ControlList(ControlListBox<T> parent, float x, float y, float w, float h) {
		super(parent, x, y, w, h);
		this.parent = parent;
	}
	
	ControlListBox<T> parent;
	
	T value = null;
	
	Map<T, IWidget> options = new LinkedHashMap<T, IWidget>();

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public void setValue(T value) {
		this.value = value;
		if(value != null && options.containsKey(value)) {
			float top = options.get(value).pos().y();
			float bottom = options.get(value).pos().y() + options.get(value).size().y();
			if(top / size().y() < parent.getPercentageIndex()) 
				parent.setPercentageIndex(top/size().y());
			if(bottom / size().y() > parent.getPercentageIndex() + parent.getPercentageShown()) 
				parent.setPercentageIndex(bottom/size().y() - parent.getPercentageShown());
		}
		if(!creating) {
			getParent().callEvent(new EventValueChanged<T>(getParent(), value));
		}
		creating = false;	
	}
	
	boolean creating = false;
	
	public void setOptions(Collection<T> options) {
		int height = 0;
		for (IWidget option : this.options.values()) {
			this.deleteChild(option);
		}
		this.options.clear();
		int i = 0;
		for (T option : options) {
			IWidget optionWidget = ((ControlListBox<T>) getParent()).createOption(option, height);
			if (optionWidget != null) {
				height += optionWidget.getSize().y();
				this.options.put(option, optionWidget);
			}
			i++;
		}
		creating = true;
		setValue(getValue());
		setSize(new Vector2f(getSize().x(), height));
	}
	
	public Collection<T> getOptions() {
		return options.keySet();
	}
	
	public int getIndexOf(T value) {
		int index = 0;
		for(T option : getOptions()) {
			if(option.equals(value))
				return index;
			index++;
		}
		return -1;
	}
	
	public int getCurrentIndex() {
		return getIndexOf(getValue());
	}

	public void setIndex(int currentIndex) {
		int index = 0;
		for(T option : getOptions()) {
			if(index == currentIndex) {
				setValue(option);
				return;
			}
			index++;
		}
		setValue(null);
	}

}
