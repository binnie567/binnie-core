package binnie.craftgui.controls.listbox;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.events.EventKey;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;

public class ControlOption<T> extends Control implements IControlValue<T> {

	@Override
	public void onUpdate() {
		if (getValue() == null)
			return;
		int colour = 0xA0A0A0;
		if (isCurrentSelection()) {
			colour = 0xFFFFFF;
		}
		setColour(colour);
	}

	@EventHandler(origin=Origin.Self)
	public void onMouseClick(EventMouse.Down event) {
		((IControlValue<T>) getParent()).setValue(getValue());
	}

	public ControlOption(ControlList<T> controlList, T option) {
		this(controlList, option, 16);
	}

	public ControlOption(ControlList<T> controlList, T option, int height) {
		super(controlList, 0, height, controlList.getSize().x(), 20);
		this.value = option;
		canMouseOver = value != null;
	}

	T value;

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public void setValue(T value) {
		this.value = value;
	}

	public boolean isCurrentSelection() {
		return getValue() != null
				&& getValue().equals(
						((IControlValue<T>) getParent()).getValue());
	}

}
