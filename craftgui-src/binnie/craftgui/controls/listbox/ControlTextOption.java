package binnie.craftgui.controls.listbox;

import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.core.geometry.TextJustification;
import binnie.craftgui.events.EventWidget;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;

public class ControlTextOption<T> extends ControlOption<T> {

	public ControlTextOption(ControlList<T> controlList, T option, String optionName, int y) {
		super(controlList, option, y);
		textWidget = new ControlText(this, getArea(), optionName, TextJustification.MiddleCenter);
	}
	
	public ControlTextOption(ControlList<T> controlList, T option, int y) {
		this(controlList, option, option.toString(), y);
	}

	protected ControlText textWidget = null;

	@EventHandler(origin=Origin.Self)
	public void onChangeColour(EventWidget.ChangeColour event) {
		textWidget.setColour(getColour());
	}

}
