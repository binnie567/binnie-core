package binnie.craftgui.controls;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.TextJustification;
import binnie.craftgui.core.geometry.Vector2f;

public class ControlTextCentered extends ControlText {

	public ControlTextCentered(IWidget parent, float y, String text) {
		super(parent, new Area(new Vector2f(0, y), new Vector2f(parent.size().x(), 0)), text, TextJustification.TopCenter);
	}

}
