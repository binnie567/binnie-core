package binnie.craftgui.controls;

public interface IControlSelectionOption<T> {

	public T getValue();

}
