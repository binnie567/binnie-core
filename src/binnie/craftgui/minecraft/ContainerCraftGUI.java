package binnie.craftgui.minecraft;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import binnie.core.BinnieCore;
import binnie.core.machines.Machine;
import binnie.core.machines.network.INetwork;
import binnie.core.machines.power.ErrorState;
import binnie.core.machines.power.IErrorStateSource;
import binnie.core.machines.power.IPoweredMachine;
import binnie.core.machines.power.ITankMachine;
import binnie.core.machines.power.PowerInfo;
import binnie.core.machines.power.TankInfo;
import binnie.core.machines.transfer.TransferRequest;
import binnie.core.network.packet.PacketCraftGUI;
import binnie.core.network.packet.PacketErrorUpdate;
import binnie.core.network.packet.PacketPowerUpdate;
import binnie.core.network.packet.PacketTankUpdate;
import cpw.mods.fml.relauncher.Side;

public class ContainerCraftGUI extends Container implements INetwork.CraftGUIAction {

	private Side getSide() {
		return window.isServer() ? Side.SERVER : Side.CLIENT;
	}

	@Override
	public Slot getSlot(int par1) {
		if (par1 < 0 || par1 >= this.inventorySlots.size())
			return null;
		return (Slot) this.inventorySlots.get(par1);
	}

	@Override
	public void putStackInSlot(int par1, ItemStack par2ItemStack) {
		if(this.getSlot(par1) != null)
			this.getSlot(par1).putStack(par2ItemStack);
	}

	@Override
	public void putStacksInSlots(ItemStack[] par1ArrayOfItemStack) {
		for (int i = 0; i < par1ArrayOfItemStack.length; ++i) {
			if (this.getSlot(i) != null)
				this.getSlot(i).putStack(par1ArrayOfItemStack[i]);
		}
	}

	@Override
	public void onCraftGuiClosed(EntityPlayer par1EntityPlayer) {
		super.onCraftGuiClosed(par1EntityPlayer);
		WindowInventory inventory = window.getWindowInventory();
		for (int i = 0; i < inventory.getSizeInventory(); i++) {
			ItemStack stack = inventory.getStackInSlot(i);
			if (stack == null)
				continue;
			stack = (new TransferRequest(stack, par1EntityPlayer.inventory)).transfer(true);
			// stack = TransferHandler.transfer(stack, inventory,
			// par1EntityPlayer.inventory, true);
			if (stack == null)
				continue;
			par1EntityPlayer.dropPlayerItem(stack);
		}
	}

	Window window;

	public ContainerCraftGUI(Window window) {
		super();
		this.window = window;
	}

	@Override
	public boolean canInteractWith(EntityPlayer var1) {
		return true;
	}

	public void newSlot(Slot slot) {
		this.addSlotToContainer(slot);
	}

	@Override
	public final ItemStack transferStackInSlot(EntityPlayer player, int slotID) {
		return shiftClick(player, slotID);
	}

	private ItemStack shiftClick(EntityPlayer player, int slotnumber) {
		TransferRequest request = getShiftClickRequest(player, slotnumber);

		ItemStack itemstack = request.transfer(true);
		Slot shiftClickedSlot = (Slot) inventorySlots.get(slotnumber);

		shiftClickedSlot.putStack(itemstack);
		shiftClickedSlot.onSlotChanged();

		return null;
	}

	private TransferRequest getShiftClickRequest(EntityPlayer player, int slotnumber) {
		Slot shiftClickedSlot = (Slot) inventorySlots.get(slotnumber);

		ItemStack itemstack = null;

		if (shiftClickedSlot.getHasStack())
			itemstack = shiftClickedSlot.getStack().copy();

		IInventory playerInventory = player.inventory;
		IInventory containerInventory = window.getInventory();
		IInventory windowInventory = window.getWindowInventory();

		int[] target = new int[36];
		for(int i = 0; i < 36; i++)
			target[i] = i;
		
		if (shiftClickedSlot.inventory == playerInventory) {
			return (new TransferRequest(itemstack, containerInventory))
					.setOrigin(shiftClickedSlot.inventory);
		} else {
			return (new TransferRequest(itemstack, playerInventory))
					.setOrigin(shiftClickedSlot.inventory).setTargetSlots(target);
		}
	}

	public final ItemStack tankClick(EntityPlayer player, int slotID) {

		if (player.inventory.getItemStack() == null)
			return null;

		ItemStack heldItem = player.inventory.getItemStack().copy();

		heldItem = (new TransferRequest(heldItem, window.getInventory()))
				.setOrigin(player.inventory).setTargetSlots(new int[0])
				.setTargetTanks(new int[] { slotID }).transfer(true);

		player.inventory.setItemStack(heldItem);

		if (player instanceof EntityPlayerMP)
			((EntityPlayerMP) player).updateHeldItem();

		return heldItem;

	}

	@Override
	public void updateProgressBar(int i, int j) {
		super.updateProgressBar(i, j);
		if (window.getInventory() instanceof INetworkedEntityGUI && window.isClient()) {
			((INetworkedEntityGUI) window.getInventory()).recieveGUINetworkData(i, j);
		}
	}

	@Override
	public void recieveNBT(Side side, EntityPlayer player, NBTTagCompound action) {
		if (this.handleNBT(player, action))
			return;
		window.recieveNBT(getSide(), player, action);
		INetwork.CraftGUIAction machine = Machine.getInterface(INetwork.CraftGUIAction.class,
				window.getInventory());
		if (machine != null)
			machine.recieveNBT(getSide(), player, action);
	}

	private boolean handleNBT(EntityPlayer player, NBTTagCompound action) {
		if (getSide() == Side.SERVER) {
			if (action.getName().equals("tank-click"))
				this.tankClick(player, action.getByte("id"));
		}

		if (action.getName().equals("tank-update"))
			this.onTankUpdate(action);
		else if (action.getName().equals("power-update"))
			this.onPowerUpdate(action);
		else if (action.getName().equals("error-update"))
			this.onErrorUpdate(action);
		else if (action.getName().equals("mouse-over-slot"))
			this.onMouseOverSlot(player, action);
		else if (action.getName().equals("shift-click-info"))
			this.onRecieveShiftClickHighlights(player, action);

		return false;
	}

	Map<Integer, Integer> lastSentData = new HashMap<Integer, Integer>();

	@Override
	public void detectAndSendChanges() {
		super.detectAndSendChanges();

		if (window.getInventory() instanceof INetworkedEntityGUI && window.isServer()) {
			Map<Integer, Integer> data = new LinkedHashMap<Integer, Integer>();
			((INetworkedEntityGUI) window.getInventory()).addGUINetworkData(data);
			for (Map.Entry<Integer, Integer> entry : data.entrySet()) {
				if (lastSentData.get(entry.getKey()) == entry.getValue())
					continue;
				for (Object crafter : crafters) {
					((ICrafting) crafter).sendProgressBarUpdate(this, entry.getKey(),
							entry.getValue());
				}
				lastSentData.put(entry.getKey(), entry.getValue());
			}
		}

		ITankMachine tanks = Machine.getInterface(ITankMachine.class, window.getInventory());
		IPoweredMachine powered = Machine
				.getInterface(IPoweredMachine.class, window.getInventory());
		IErrorStateSource error = Machine.getInterface(IErrorStateSource.class,
				window.getInventory());

		if (tanks != null && window.isServer()) {
			for (int i = 0; i < tanks.getTankInfos().length; i++) {
				TankInfo tank = (TankInfo) tanks.getTankInfos()[i];

				// If null has been synced
				if (getTankInfo(i).equals(tank))
					continue;

				for (int j = 0; j < this.crafters.size(); ++j) {
					if (this.crafters.get(j) instanceof EntityPlayerMP) {
						EntityPlayerMP player = (EntityPlayerMP) this.crafters.get(j);
						BinnieCore.proxy.sendToPlayer(BinnieCore.instance, new PacketTankUpdate(i,
								tank), player);
					}
				}
				syncedTanks.put(i, tank);

			}
		}

		if (powered != null && window.isServer()) {
			// If null has been synced
			if (!syncedPower.equals(powered.getPowerInfo())) {

				for (int j = 0; j < this.crafters.size(); ++j) {
					if (this.crafters.get(j) instanceof EntityPlayerMP) {
						EntityPlayerMP player = (EntityPlayerMP) this.crafters.get(j);
						BinnieCore.proxy.sendToPlayer(BinnieCore.instance, new PacketPowerUpdate(
								powered.getPowerInfo()), player);
					}
				}

			}

		}
		if (error != null && window.isServer()) {
			// If null has been synced

			for (int j = 0; j < this.crafters.size(); ++j) {
				if (this.crafters.get(j) instanceof EntityPlayerMP) {
					EntityPlayerMP player = (EntityPlayerMP) this.crafters.get(j);
					BinnieCore.proxy.sendToPlayer(BinnieCore.instance,
							new PacketErrorUpdate(error), player);
				}
			}

		}

	}

	private Map<Integer, TankInfo> syncedTanks = new HashMap<Integer, TankInfo>();
	private PowerInfo syncedPower = new PowerInfo();
	private int errorType = 0;
	private ErrorState error = null;

	public void onTankUpdate(NBTTagCompound nbt) {
		int tankID = nbt.getByte("tank");
		TankInfo tank = new TankInfo();
		tank.readFromNBT(nbt);
		syncedTanks.put(tankID, tank);
	}

	public void onPowerUpdate(NBTTagCompound nbt) {
		syncedPower = new PowerInfo();
		syncedPower.readFromNBT(nbt);
	}

	public PowerInfo getPowerInfo() {
		return syncedPower;
	}

	public TankInfo getTankInfo(int tank) {
		return syncedTanks.containsKey(tank) ? syncedTanks.get(tank) : new TankInfo();
	}

	public void onErrorUpdate(NBTTagCompound nbt) {
		errorType = nbt.getByte("type");
		if (nbt.hasKey("name")) {
			error = new ErrorState("", "");
			error.readFromNBT(nbt);
		} else {
			error = null;
		}
	}

	public ErrorState getErrorState() {
		return error;
	}

	public int getErrorType() {
		return errorType;
	}

	public CustomSlot[] getCustomSlots() {
		List<CustomSlot> slots = new ArrayList<CustomSlot>();
		for (Object object : inventorySlots)
			if (object instanceof CustomSlot)
				slots.add((CustomSlot) object);
		return slots.toArray(new CustomSlot[0]);
	}

	// Sycning of moused over slot and shift clicking

	int mousedOverSlotNumber = -1;

	public void setMouseOverSlot(Slot slot) {
		if (slot.slotNumber != mousedOverSlotNumber) {
			NBTTagCompound nbt = new NBTTagCompound("mouse-over-slot");
			nbt.setByte("slot", (byte) slot.slotNumber);
			BinnieCore.proxy.sendToServer(BinnieCore.instance, new PacketCraftGUI(nbt));
		}

	}

	private void onMouseOverSlot(EntityPlayer player, NBTTagCompound data) {
		int slotnumber = data.getByte("slot");
		TransferRequest request = getShiftClickRequest(player, slotnumber);
		request.transfer(false);
		NBTTagCompound nbt = new NBTTagCompound("shift-click-info");

		List<Integer> slots = new ArrayList<Integer>();
		for (TransferRequest.TransferSlot tslot : request.getInsertedSlots()) {
			Slot slot = getSlot(tslot.inventory, tslot.id);
			if(slot != null)
				slots.add(slot.slotNumber);
		}

		int[] array = new int[slots.size()];
		for (int i = 0; i < slots.size(); i++)
			array[i] = slots.get(i);

		nbt.setIntArray("slots", array);

		BinnieCore.proxy.sendToPlayer(BinnieCore.instance, new PacketCraftGUI(nbt), player);
	}

	private void onRecieveShiftClickHighlights(EntityPlayer player, NBTTagCompound data) {
		ControlSlot.highlighting.get(EnumHighlighting.ShiftClick).clear();
		for (int slotnumber : data.getIntArray("slots"))
			ControlSlot.highlighting.get(EnumHighlighting.ShiftClick).add(slotnumber);
	}

	private CustomSlot getSlot(IInventory inventory, int id) {
		for (Object o : this.inventorySlots) {
			CustomSlot slot = (CustomSlot) o;
			if (slot.inventory == inventory && slot.slotID == id)
				return slot;
		}
		return null;
	}

}
