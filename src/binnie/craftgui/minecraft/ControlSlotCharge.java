package binnie.craftgui.minecraft;

import binnie.core.machines.Machine;
import binnie.core.machines.inventory.IChargedSlots;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.geometry.CraftGUIUtil;
import binnie.craftgui.core.geometry.Direction;
import binnie.craftgui.resource.minecraft.CraftGUITexture;
import binnie.craftgui.window.Panel;

public class ControlSlotCharge extends Control {

	int slot;
	
	float getCharge() {
		IChargedSlots slots = Machine.getInterface(IChargedSlots.class, Window.get(this).getInventory());
		return slots == null ? 0 : slots.getCharge(slot);
	}
	
	@Override
	public void onRenderBackground() {
		getRenderer().renderTexture(CraftGUITexture.PanelBlack, getArea());
		getRenderer().renderSubTexture(getTexture(CraftGUITexture.SlotCharge), 
				CraftGUIUtil.getPaddedArea(getArea(), 1), Direction.Upwards, getCharge());
	}

	public ControlSlotCharge(IWidget parent, int x, int y, int slot) {
		super(parent, x, y, 4, 18);
		this.slot = slot;
		new Panel(this, 0, 0, 4, 18, MinecraftGUI.PanelType.Black);
	}

}
