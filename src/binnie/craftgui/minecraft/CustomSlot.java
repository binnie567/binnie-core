package binnie.craftgui.minecraft;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import binnie.core.machines.Machine;
import binnie.core.machines.inventory.IInventorySlots;
import binnie.core.machines.inventory.InventorySlot;


public class CustomSlot extends Slot {

	int slotID = 0;
	
	@Override
	public boolean isItemValid(ItemStack par1ItemStack) {
		return this.inventory.isStackValidForSlot(slotID, par1ItemStack);
	}

	public CustomSlot(IInventory inventory, int par2, int par3, int par4) {
		super(inventory, par2, par3, par4);
		slotID = par2;
	}
	
	public InventorySlot getInventorySlot() {
		if(this.inventory instanceof IInventorySlots)
			return ((IInventorySlots)inventory).getSlot(slotID);
		return null;
	}

}
