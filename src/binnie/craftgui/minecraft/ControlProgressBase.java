package binnie.craftgui.minecraft;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.geometry.Direction;
import binnie.craftgui.resource.ITexture;

public class ControlProgressBase extends Control {

	protected float progress;

	public ControlProgressBase(IWidget parent, float x, float y, float w, float h) {
		super(parent, x, y, w, h);
		progress = 0.0f;
	}

	public void setProgress(float progress) {
		this.progress = progress;
		if (this.progress < 0.0f)
			this.progress = 0.0f;
		else if (this.progress > 1.0f)
			this.progress = 1.0f;
	}

}
