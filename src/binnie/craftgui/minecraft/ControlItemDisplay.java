package binnie.craftgui.minecraft;

import net.minecraft.item.ItemStack;

import org.lwjgl.opengl.GL11;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.core.geometry.Vector2f;

public class ControlItemDisplay extends Control implements ITooltip {

	ItemStack itemStack = null;

	public boolean hastooltip = false;

	public void setTooltip() {
		hastooltip = true;
		canMouseOver = true;
	}

	public ControlItemDisplay(IWidget parent, int x, int y) {
		this(parent, x, y, 16);
	}

	public ControlItemDisplay(IWidget parent, int x, int y, int size) {
		super(parent, x, y, size, size);
	}

	@Override
	public void onRenderForeground() {
		IPosition relativeToWindow = getAbsolutePosition().sub(getSuperParent().getPosition());
		if(relativeToWindow.x() > Window.get(this).getSize().x()+100 || relativeToWindow.y() > Window.get(this).getSize().y()+100) return;
		if (itemStack != null) {
			if(getSize().x() != 16f) {
				GL11.glPushMatrix();
				float scale = getSize().x()/16f;
				GL11.glScalef(scale, scale, 1f);
				getRenderer().subRenderer(IRendererMinecraft.class).renderItem(Vector2f.ZERO, itemStack);
				GL11.glPopMatrix();
			}
			else {
				getRenderer().subRenderer(IRendererMinecraft.class).renderItem(Vector2f.ZERO, itemStack);
			}
			
		}
	}
	
	public void setItemStack(ItemStack itemStack) {
		this.itemStack = itemStack;
	}

	@Override
	public void getTooltip(Tooltip tooltip) {
		if (hastooltip && itemStack != null) {
			tooltip.add(itemStack.getTooltip(
					((Window) getSuperParent()).getPlayer(), false));
		}
	}

}
