package binnie.craftgui.minecraft;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.nbt.NBTTagCompound;

import org.lwjgl.opengl.GL11;

import binnie.core.machines.Machine;
import binnie.core.machines.inventory.MachineSide;
import binnie.core.machines.inventory.TankSlot;
import binnie.core.machines.power.ITankMachine;
import binnie.core.machines.power.TankInfo;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.CraftGUIUtil;
import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;
import binnie.craftgui.resource.minecraft.CraftGUITexture;

public class ControlLiquidTank extends Control implements ITooltip {

	public static List<Integer> tankError = new ArrayList<Integer>();

	@EventHandler(origin = Origin.Self)
	public void onMouseClick(EventMouse.Down event) {
		if (event.getButton() == 0) {
			NBTTagCompound nbt = new NBTTagCompound("tank-click");
			nbt.setByte("id", (byte) this.tankID);
			Window.get(this).sendClientAction(nbt);
		}
	}

	protected int tankID;

	public ControlLiquidTank(IWidget parent, int x, int y) {
		super(parent, x, y, 18, 60);
		canMouseOver = true;
	}

	public void setTankID(int tank) {
		this.tankID = tank;
	}

	public TankInfo getTank() {
		return Window.get(this).getContainer().getTankInfo(tankID);
	}

	public boolean isTankValid() {
		return !getTank().isEmpty();
	}

	public int getTankCapacity() {
		return (int) getTank().getCapacity();
	}

	@Override
	public void onRenderBackground() {

		getRenderer().renderTexture(CraftGUITexture.LiquidTank, Vector2f.ZERO);

		if (isMouseOver() && Window.get(this).getGui().getTooltipType() == Tooltip.Type.Help) {
			int c = 0xAA000000 + MinecraftTooltip.getOutline(Tooltip.Type.Help);
			getRenderer().renderGradientRect(new Area(1, 1, 16, 58), c, c);
		} else if (tankError.contains(tankID)) {
			int c = 0xAA000000 + MinecraftTooltip.getOutline(MinecraftTooltip.Type.Error);
			getRenderer().renderGradientRect(new Area(1, 1, 16, 58), c, c);
		} else if (getSuperParent().getMousedOverWidget() == this) {

			if (Window.get(this).getGui().getDraggedItem() != null)
				getRenderer().renderGradientRect(CraftGUIUtil.getPaddedArea(getArea(), 1),
						0xAAFF9999, 0xAAFF9999);
			else
				getRenderer().renderGradientRect(CraftGUIUtil.getPaddedArea(getArea(), 1),
						-2130706433, -2130706433);
		}
	}

	@Override
	public void onRenderForeground() {
		if (isTankValid()) {

			Object content = null;

			int squaled = (int) (58.0 * ((float) getTank().getAmount() / (getTank().getCapacity())));

			//Icon liquidIcon = getTank().getIcon();

			int yPos = 59;

			int block = Math.min(squaled, 16);

			GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0F);

			IPosition pos = this.getAbsolutePosition();
			IArea limited = CraftGUIUtil.getPaddedArea(getArea(), 1);
			getRenderer().limitArea(new Area(limited.pos().add(pos), limited.size()));

			GL11.glEnable(GL11.GL_SCISSOR_TEST);

			while (block > 0) {

				getRenderer().subRenderer(IRendererMinecraft.class).renderItem(
						new Vector2f(1, yPos - block), getTank().liquid.asItemStack());

				squaled -= 16;
				yPos -= 16;
				block = Math.min(squaled, 16);
			}

			GL11.glDisable(GL11.GL_SCISSOR_TEST);

		}

	}

	@Override
	public void onRenderOverlay() {
		getRenderer().renderTexture(CraftGUITexture.LiquidTankOverlay, Vector2f.ZERO);
		
		if (isMouseOver() && Window.get(this).getGui().getTooltipType() == Tooltip.Type.Help) {

			IArea area = getArea();
			getRenderer().setColour(MinecraftTooltip.getOutline(Tooltip.Type.Help));
			getRenderer().renderTexture(CraftGUITexture.Outline,
					CraftGUIUtil.getPaddedArea(area, -1));
		}
		
		if (tankError.contains((Integer) tankID)) {

			IArea area = getArea();
			getRenderer().setColour(MinecraftTooltip.getOutline(MinecraftTooltip.Type.Error));
			getRenderer().renderTexture(CraftGUITexture.Outline,
					CraftGUIUtil.getPaddedArea(area, -1));
		}
		
	}

	@Override
	public void getTooltip(Tooltip tooltip) {
		
		if(tooltip.getType() == Tooltip.Type.Help && getTankSlot() != null) {
			TankSlot slot = getTankSlot();
			tooltip.add(slot.getName());
			tooltip.add("Insert Side: " + MachineSide.asString(slot.getInputSides()));
			tooltip.add("Extract Side: " + MachineSide.asString(slot.getOutputSides()));
			if(slot.isReadOnly())
				tooltip.add("Output Only Tank");
			tooltip.add("Accepts: " + (slot.getValidator() == null ? "Any Item" : slot.getValidator().getTooltip()));
			return;
		}
		
		if (isTankValid()) {

			int percentage = (int) (100.0 * (getTank().getAmount()) / (getTankCapacity()));

			tooltip.add(getTank().getName());
			tooltip.add(percentage + "% full");

			return;

		}

		tooltip.add("Empty");
		tooltip.add("0% full");
	}

	private TankSlot getTankSlot() {
		ITankMachine tank = Machine.getInterface(ITankMachine.class, Window.get(this).getInventory());
		return tank != null ? tank.getTankSlot(tankID) : null;
	}

}
