package binnie.craftgui.minecraft;

import binnie.craftgui.core.ITooltip;


public interface IMinecraftTooltip extends ITooltip {
	
	public void getTooltip(MinecraftTooltip tooltip);
	
}
