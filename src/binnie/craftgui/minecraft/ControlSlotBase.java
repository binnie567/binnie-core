package binnie.craftgui.minecraft;

import net.minecraft.item.ItemStack;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.resource.minecraft.CraftGUITexture;

public abstract class ControlSlotBase extends Control implements ITooltip {

	ControlItemDisplay itemDisplay;
	
	public ControlSlotBase(IWidget parent, int x, int y) {
		this(parent, x, y, 18);
	}
	
	public ControlSlotBase(IWidget parent, int x, int y, int size) {
		super(parent, x, y, size, size);
		canMouseOver = true;
		itemDisplay = new ControlItemDisplay(this, 1, 1, size-2);
	}


	@Override
	public void onRenderBackground() {
		int size = (int) getSize().x();
		getRenderer().renderTexture(CraftGUITexture.Slot, getArea());
		

		if (getSuperParent().getMousedOverWidget() == this) {
			
			getRenderer().renderGradientRect(new Area(new Vector2f(1, 1), getArea().size().sub(new Vector2f(2f, 2f))), -2130706433,
					-2130706433);
		}

	}
	
	@Override
	public void onUpdate() {
		super.onUpdate();
		itemDisplay.setItemStack(getItemStack());
	}
	
	@Override
	public void getTooltip(Tooltip tooltip) {
		ItemStack item = getItemStack();

		if (item == null)
			return;
		
		if(tooltip.getType() == Tooltip.Type.Help)
			return;

		tooltip.add(item.getTooltip(((Window) getSuperParent()).getPlayer(), false));
	}

	public abstract ItemStack getItemStack();

}
