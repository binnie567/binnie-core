package binnie.craftgui.minecraft;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import binnie.core.BinnieCore;
import binnie.core.IBinnieMod;
import binnie.core.machines.Machine;
import binnie.core.machines.inventory.IInventoryMachine;
import binnie.core.machines.network.INetwork;
import binnie.core.network.packet.PacketCraftGUI;
import binnie.core.resource.BinnieResource;
import binnie.core.resource.ResourceManager;
import binnie.core.resource.ResourceType;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.ControlTextCentered;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.TopLevelWidget;
import binnie.craftgui.resource.IStyleSheet;
import binnie.craftgui.resource.ITexture;
import binnie.craftgui.resource.StyleSheetManager;
import binnie.craftgui.resource.minecraft.StandardTexture;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public abstract class Window extends TopLevelWidget implements INetwork.CraftGUIAction {

	public void getTooltip(Tooltip tooltip) {
		if (getMousedOverWidget() instanceof ITooltip) {
			((ITooltip) getMousedOverWidget()).getTooltip(tooltip);
		}
			
	}

	private GuiCraftGUI gui;
	private ContainerCraftGUI container;
	private WindowInventory windowInventory;
	private ControlText title;
	
	protected abstract IBinnieMod getMod();
	protected abstract String getName();
	
	public BinnieResource getBackgroundTextureFile(int i) {
		return ResourceManager.getPNG(getMod(), ResourceType.GUI, getName()+(i == 1 ? "" : i));
	}

	public Window(float width, float height, EntityPlayer player,
			IInventory inventory, Side side) {
		super();
		this.side = side;
		setInventories(player, inventory);
		container = new ContainerCraftGUI(this);
		windowInventory = new WindowInventory(this);
		if (!isServer()) {
			gui = new GuiCraftGUI(this, width, height);
			setRenderer(gui);
			this.stylesheet = StyleSheetManager.getDefault();
		}
		
		int by = 4;
		int bx = 4 - 22;
		
		if(Machine.getInterface(IInventoryMachine.class, getInventory()) != null)
			new ControlHelp(this, bx += 22, by);
		
		if(Machine.getInterface(IMachineInformation.class, getInventory()) != null)
			new ControlInfo(this, bx += 22, by);
		

	}

	// Window specific functions

	public void setTitle(String title) {
		if (this.title != null)
			this.title.setValue(title);
	}

	@SideOnly(Side.CLIENT)
	public final GuiCraftGUI getGui() {
		return gui;
	}

	public final ContainerCraftGUI getContainer() {
		return container;
	}

	public final WindowInventory getWindowInventory() {
		return windowInventory;
	}

	ITexture bgText1 = null;
	ITexture bgText2 = null;
	
	public boolean hasBeenInitialised = false;
	
	public final void initGui() {
		if(hasBeenInitialised) return;
		title = new ControlTextCentered(this, 9, " ");
		title.setColour(4210752);
		bgText1 = new StandardTexture(0, 0, 256, 256, getBackgroundTextureFile(1));
		if(this.getSize().x()>256f) {
			bgText2 = new StandardTexture(0, 0, 256, 256, getBackgroundTextureFile(2));
		}
		initialize();
		hasBeenInitialised= true;
	}

	public abstract void initialize();

	@Override
	public void onRenderBackground() {
		
		getRenderer(IRendererMinecraft.class).renderWindow(this);
	}

	@Override
	public void onRenderForeground() {
	}

	@Override
	public void onRenderOverlay() {
	}

	@Override
	public void onUpdate() {
		ControlSlot.highlighting.get(EnumHighlighting.Help).clear();
		ControlSlot.highlighting.get(EnumHighlighting.ShiftClick).clear();
	}

	private EntityPlayer player;
	private IInventory entityInventory;

	public EntityPlayer getPlayer() {
		return player;
	}

	public ItemStack getHeldItemStack() {
		if (player != null)
			return player.inventory.getItemStack();
		return null;
	}

	public IInventory getInventory() {
		return entityInventory;
	}

	public void setInventories(EntityPlayer player2, IInventory inventory) {
		player = player2;
		entityInventory = inventory;
	}

	public void onClose() {

	}

	public void setHeldItemStack(ItemStack stack) {
		if (player != null)
			player.inventory.setItemStack(stack);

	}

	// Side code

	Side side = Side.CLIENT;

	public boolean isServer() {
		return !isClient();
	}

	public boolean isClient() {
		return side == Side.CLIENT;
	}

	public World getWorld() {
		if (getPlayer() != null)
			return getPlayer().worldObj;
		return BinnieCore.proxy.getWorld();
	}

	public void onInventoryUpdate() {
	}

	public void sendClientAction(NBTTagCompound action) {
		PacketCraftGUI packet = new PacketCraftGUI(action);
		BinnieCore.proxy.sendToServer(BinnieCore.instance, packet);
	}
	
	@Override
	public void recieveNBT(Side side, EntityPlayer player, NBTTagCompound action) {
	}

	protected IStyleSheet stylesheet;
	
	@Override
	public ITexture getTexture(Object key) {
		return stylesheet.getTexture(key);
	}

	public void onWindowInventoryChanged() {
		
	}

	public ITexture getBackground1() {
		return bgText1;
	}
	
	public ITexture getBackground2() {
		return bgText2;
	}
	
	public static Window get(IWidget widget) {
		return (Window) widget.getSuperParent();
	}

}
