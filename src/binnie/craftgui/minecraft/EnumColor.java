package binnie.craftgui.minecraft;

public enum EnumColor {

	Black(0x000000), DarkBlue(0x00002A), DarkGreen(0x002A00), DarkAqua(0x002A2A), DarkRed(
			0x2A0000), Purple(0x2A002A), Gold(0x2A2A00), Grey(0x2A2A2A), DarkGrey(
			0x151515), Indigo(0x15153F), BrightGreen(0x153F15), Aqua(0x153F3F), Red(
			0x3F1515), Pink(0x3F153F), Yellow(0x3F3F15), White(0x3F3F3F),

	;

	int colour;

	EnumColor(int colour) {
		this.colour = colour;
	}

	public int getColour() {
		return colour;
	}

	public int getCode() {
		int n = this.ordinal();
		if (n < 10)
			return n;
		return 0;
	}

}
