package binnie.craftgui.minecraft;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.texture.Texture;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.texture.TextureStitched;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import binnie.core.BinnieCore;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.Tooltip.ITooltipType;
import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.Direction;
import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.core.geometry.TextJustification;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.core.renderer.IRendererWidget;
import binnie.craftgui.events.EventKey;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.resource.ITexture;
import binnie.craftgui.resource.ITexturePadded;
import binnie.craftgui.resource.minecraft.CraftGUITexture;
import binnie.craftgui.resource.minecraft.StandardTexture;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiCraftGUI extends GuiContainer implements IRendererMinecraft {

	Vector2f mousePos = new Vector2f(0, 0);

	@Override
	public void updateScreen() {
		window.update();
	}

	Window window;

	public Minecraft getMinecraft() {
		return mc;
	}

	public GuiCraftGUI(Window window, float width, float height) {
		super(window.getContainer());
		this.window = window;
		this.xSize = (int) width;
		this.ySize = (int) height;
		addSubRenderer(IRendererWidget.class, new RendererWidgetMinecraft(this));
	}

	// Does nothing

	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
	}

	@Override
	public void initGui() {
		super.initGui();
		this.mc.thePlayer.openContainer = this.inventorySlots;
		this.guiLeft = (this.width - this.xSize) / 2;
		this.guiTop = (this.height - this.ySize) / 2;
		window.setSize(new Vector2f(xSize, ySize));
		window.setPosition(new Vector2f(guiLeft, guiTop));
		window.initGui();
	}

	ItemStack draggedItem;

	public ItemStack getDraggedItem() {
		return draggedItem;
	};

	// Main Draw Function

	@Override
	public void drawScreen(int mouseX, int mouseY, float par3) {

		// Sets mouse position and determines moused over widget
		window.setMousePosition(mouseX - (int) window.getPosition().x(), mouseY
				- (int) window.getPosition().y());

		// Draws default GUI background
		this.drawDefaultBackground();

		// Applies effects used for the window background
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		RenderHelper.disableStandardItemLighting();
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_DEPTH_TEST);

		// Renders the window background
		window.renderBackground();
		window.renderForeground();
		window.renderOverlay();
		
		// Applies effects used for foreground
		RenderHelper.enableGUIStandardItemLighting();
		GL11.glPushMatrix();
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240 / 1.0F, 240 / 1.0F);

		// Renders window foreground, usually for items/icons
		//window.renderForeground();

		// Checks if there is currently an item being dragged
		InventoryPlayer playerInventory = this.mc.thePlayer.inventory;
		draggedItem = playerInventory.getItemStack();

		// Renders dragged Item
		if (draggedItem != null) {
			renderItem(new Vector2f(mouseX - 8, mouseY - 8), draggedItem, 200);
		}

		// Applies effects used for overlay
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();

		// Renders overlay
		//window.renderOverlay();

		// Applies effects used for tooltips
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_DEPTH_TEST);

		// Renders tooltip
		MinecraftTooltip tooltip = new MinecraftTooltip();
		tooltip.setType(getTooltipType());
		window.getTooltip(tooltip);
		
		if (tooltip.exists() && draggedItem == null) {
			renderTooltip(new Vector2f(mouseX, mouseY), tooltip);
		}

		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
	}

	public void renderTooltip(IPosition mousePosition, MinecraftTooltip tooltip) {
		int mouseX = (int) mousePosition.x();
		int mouseY = (int) mousePosition.y();
		FontRenderer font = this.fontRenderer;
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		RenderHelper.disableStandardItemLighting();
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		int k = 0;
		
		List<String> strings = new ArrayList<String>();
		for(String string : tooltip.getList())
			strings.addAll(font.listFormattedStringToWidth(string, tooltip.maxWidth));
		
		Iterator iterator = strings.iterator();

		while (iterator.hasNext()) {
			String s = (String) iterator.next();
			int l = font.getStringWidth(s);

			if (l > k) {
				k = l;
			}
		}

		int i1 = mouseX + 12;
		int j1 = mouseY - 12;
		int k1 = 8;

		if (strings.size() > 1) {
			k1 += 2 + (strings.size() - 1) * 10;
		}

		if (i1 + k > this.width) {
			i1 -= 28 + k;
		}

		if (j1 + k1 + 6 > this.height) {
			j1 = this.height - k1 - 6;
		}

		this.zLevel = 300.0F;
		itemRenderer.zLevel = 300.0F;
		int l1 = 0xf0100010;
		int i2 = 0x50000000 + MinecraftTooltip.getOutline(tooltip.getType());
		int j2 = i2;
		this.drawGradientRect(i1 - 3, j1 - 4, i1 + k + 3, j1 - 3, l1, l1);
		this.drawGradientRect(i1 - 3, j1 + k1 + 3, i1 + k + 3, j1 + k1 + 4, l1, l1);
		this.drawGradientRect(i1 - 3, j1 - 3, i1 + k + 3, j1 + k1 + 3, l1, l1);
		this.drawGradientRect(i1 - 4, j1 - 3, i1 - 3, j1 + k1 + 3, l1, l1);
		this.drawGradientRect(i1 + k + 3, j1 - 3, i1 + k + 4, j1 + k1 + 3, l1, l1);

		this.drawGradientRect(i1 - 3, j1 - 3 + 1, i1 - 3 + 1, j1 + k1 + 3 - 1, i2, j2);
		this.drawGradientRect(i1 + k + 2, j1 - 3 + 1, i1 + k + 3, j1 + k1 + 3 - 1, i2, j2);
		this.drawGradientRect(i1 - 3, j1 - 3, i1 + k + 3, j1 - 3 + 1, i2, i2);
		this.drawGradientRect(i1 - 3, j1 + k1 + 2, i1 + k + 3, j1 + k1 + 3, j2, j2);

		for (int k2 = 0; k2 < strings.size(); ++k2) {
			String s1 = (String) strings.get(k2);
			if(k2 == 0)
				s1 = tooltip.getTitle(tooltip.getType()) + s1;
			else
				s1 = tooltip.getBody(tooltip.getType()) + s1;
			font.drawStringWithShadow(s1, i1, j1, -1);

			if (k2 == 0) {
				j1 += 2;
			}

			j1 += 10;
		}

		this.zLevel = 0.0F;
		itemRenderer.zLevel = 0.0F;
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		RenderHelper.enableStandardItemLighting();
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
	}

	@Override
	protected void mouseClicked(int x, int y, int button) {
		IWidget origin = window;
		if (window.getMousedOverWidget() != null) {
			origin = window.getMousedOverWidget();
		}
		window.callEvent(new EventMouse.Down(origin, x, y, button));
	}

	public boolean isShiftDown() {
		return Keyboard.isKeyDown(this.mc.gameSettings.keyBindSneak.keyCode);
	}

	@Override
	protected void keyTyped(char c, int key) {

		if (key == 1
				|| (key == this.mc.gameSettings.keyBindInventory.keyCode && window
						.getFocusedWidget() == null)) {
			this.mc.thePlayer.closeScreen();
		}

		IWidget origin = window.getFocusedWidget() == null ? window : window.getFocusedWidget();

		window.callEvent(new EventKey.Down(origin, c, key));
	}

	@Override
	protected void mouseMovedOrUp(int x, int y, int button) {

		IWidget origin = window.getMousedOverWidget() == null ? window : window
				.getMousedOverWidget();

		if (button == -1) {
			// int x =
			// par1-(int)window.getPosition().x-(int)window.getMousePosition().x;
			// int y =
			// par2-(int)window.getPosition().y-(int)window.getMousePosition().y;
			float dx = (float) Mouse.getEventDX() * (float) this.width / this.mc.displayWidth;
			float dy = -(Mouse.getEventDY() * (float) this.height / this.mc.displayHeight);
			// window.callEvent(new EventMouseMoved(origin, dx, dy));
		} else {
			window.callEvent(new EventMouse.Up(origin, x, y, button));
		}
	}

	@Override
	public void handleMouseInput() {
		super.handleMouseInput();
		int dWheel = Mouse.getDWheel();
		IWidget origin = window.getFocusedWidget() == null ? window : window.getFocusedWidget();
		if (dWheel != 0)
			window.callEvent(new EventMouse.Wheel(window, dWheel));
	}

	@Override
	public void onGuiClosed() {
		window.onClose();
	}

	private void renderTexturedRect(float x, float y, float u, float v, float w, float h) {
		this.drawTexturedModalRect((int) x, (int) y, (int) u, (int) v, (int) w, (int) h);
	}

	private void renderTexturedRect(IArea area, IPosition uv) {
		renderTexturedRect(area.pos().x(), area.pos().y(), uv.x(), uv.y(), area.size().x(), area
				.size().y());
	}

	@Override
	public void renderTexture(Object key, IPosition position) {
		ITexture texture = getTexture(key);
		if (texture == null)
			return;
		setTexture(texture);
		renderTexturedRect(position.x(), position.y(), texture.u(), texture.v(), texture.w(),
				texture.h());
	}

	@Override
	public void renderTexture(Object key, IArea area) {
		ITexture texture = getTexture(key);
		if (texture == null)
			return;
		if (texture instanceof ITexturePadded
				&& !(texture.w() == area.size().x() && texture.h() == area.size().y()))
			renderTexturePadded((ITexturePadded) texture, area);
		else
			renderTexture(texture, area.pos());
	}

	@Override
	public void renderTextureTiled(Object key, IArea area) {
		ITexture texture = getTexture(key);
		renderTexturePadded(texture, area, new Area(0, 0, 0, 0));
	}

	private void renderTexturePadded(ITexturePadded texture, IArea area) {
		renderTexturePadded(texture, area,
				new Area(texture.l(), texture.r(), texture.t(), texture.b()));
	}

	private void renderTexturePadded(ITexture texture, IArea area, IArea padding) {

		setTexture(texture);

		int borderLeft = (int) padding.pos().x();
		int borderRight = (int) padding.pos().y();
		int borderTop = (int) padding.size().x();
		int borderBottom = (int) padding.size().y();

		int posX = (int) area.pos().x();
		int posY = (int) area.pos().y();
		int width = (int) area.size().x();
		int height = (int) area.size().y();

		int textWidth = (int) texture.w();
		int textHeight = (int) texture.h();

		int u = (int) texture.u();
		int v = (int) texture.v();

		IPosition origin = area.pos();

		this.drawTexturedModalRect(posX, posY, u, v, borderLeft, borderTop);

		// Top Right
		this.drawTexturedModalRect(posX + width - borderRight, posY, u + textWidth - borderRight,
				v, borderRight, borderTop);

		// Bottom Left
		this.drawTexturedModalRect(posX, posY + height - borderBottom, u, v + textHeight
				- borderBottom, borderLeft, borderBottom);

		// Bottom Right
		this.drawTexturedModalRect(posX + width - borderRight, posY + height - borderBottom, u
				+ textWidth - borderRight, v + textHeight - borderBottom, borderRight, borderBottom);

		// Top and Bottom Border
		int currentXPos = borderLeft;
		while (currentXPos < (width - borderRight)) {

			int distanceXRemaining = width - borderRight - currentXPos;

			// Width of the texture that will be rendered
			int texturingWidth = textWidth - borderLeft - borderRight;

			if (texturingWidth > distanceXRemaining)
				texturingWidth = distanceXRemaining;

			// Render Top Border
			this.drawTexturedModalRect(posX + currentXPos, posY, u + borderLeft, v, texturingWidth,
					borderTop);

			this.drawTexturedModalRect(posX + currentXPos, posY + height - borderBottom, u
					+ borderLeft, v + textHeight - borderBottom, texturingWidth, borderBottom);

			int currentYPos = borderTop;
			while (currentYPos < (height - borderBottom)) {
				int distanceYRemaining = height - borderBottom - currentYPos;

				// Width of the texture that will be rendered
				int texturingHeight = textHeight - borderTop - borderBottom;

				if (texturingHeight > distanceYRemaining)
					texturingHeight = distanceYRemaining;

				// Render Top Border
				this.drawTexturedModalRect(posX + currentXPos, posY + currentYPos, u + borderLeft,
						v + borderTop, texturingWidth, texturingHeight);

				currentYPos += texturingHeight;
			}

			currentXPos += texturingWidth;
		}

		// Top and Bottom Border
		int currentYPos = borderTop;
		while (currentYPos < (height - borderBottom)) {
			int distanceYRemaining = height - borderBottom - currentYPos;

			// Width of the texture that will be rendered
			int texturingHeight = textHeight - borderTop - borderBottom;

			if (texturingHeight > distanceYRemaining)
				texturingHeight = distanceYRemaining;

			// Render Top Border
			this.drawTexturedModalRect(posX, posY + currentYPos, u, v + borderTop, borderLeft,
					texturingHeight);

			this.drawTexturedModalRect(posX + width - borderRight, posY + currentYPos, u
					+ textWidth - borderRight, v + borderTop, borderRight, texturingHeight);
			currentYPos += texturingHeight;
		}

	}

	ITexture currentTexture;

	@Override
	public void setTexture(ITexture texture) {
		if(!(texture instanceof StandardTexture)) return;
		if (texture != currentTexture && texture != null) {
			BinnieCore.proxy.bindTexture(((StandardTexture)texture).getTexture());
		}
		setColour(currentColour);
	}

	@Override
	public int getTextWidth(String text) {
		return fontRenderer.getStringWidth(text);
	}

	@Override
	public int getTextHeight() {
		return fontRenderer == null ? 0 : fontRenderer.FONT_HEIGHT;
	}

	@Override
	public void renderText(IPosition pos, String text, int colour) {
		this.renderText(new Area(pos, new Vector2f(500, 500)), TextJustification.TopLeft, text,
				colour);
	}

	@Override
	public void renderText(IArea area, TextJustification justification, String text, int colour) {
		IPosition pos = area.pos();
		List<String> wrappedStrings = fontRenderer.listFormattedStringToWidth(text, (int) area
				.size().x());
		float totalHeight = wrappedStrings.size() * getTextHeight();
		float posY = area.pos().y();
		if (area.size().y() > totalHeight) {
			posY += (area.size().y() - totalHeight) * justification.getYOffset();
		}
		for (String string : wrappedStrings) {
			float stringWidth = getTextWidth(string);
			float posX = area.size().x() - stringWidth;
			posX *= justification.getXOffset();
			fontRenderer.drawString(string, (int) (pos.x() + posX), (int) (pos.y() + posY), colour);
			posY += getTextHeight();
		}
		GL11.glColor3f(1.0f, 1.0f, 1.0f);
	}

	@Override
	public void renderSolidArea(IArea area, int colour) {
		this.drawRect((int) area.pos().x(), (int) area.pos().y(), (int) (area.pos().x() + area
				.size().x()), (int) (area.pos().y() + area.size().y()), 0xFF000000 | colour);
		renderGradientRect(area, colour, colour);
	}

	@Override
	public void renderItem(IPosition pos, ItemStack item) {

		renderItem(pos, item, 100);

	}

	private void renderItem(IPosition pos, ItemStack item, int zLevel) 
	{
		
		RenderHelper.enableGUIStandardItemLighting();
		//OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240 / 1.0F, 240 / 1.0F);
		GL11.glPushMatrix();
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		
		GL11.glTranslatef(0.0F, 0.0F, 32.0F);
		this.zLevel = zLevel;
		itemRenderer.zLevel = zLevel;
		FontRenderer font = item.getItem().getFontRenderer(item);
		if (font == null)
			font = fontRenderer;
		itemRenderer.renderItemAndEffectIntoGUI(font, this.mc.renderEngine, item, (int) pos.x(),
				(int) pos.y());
		itemRenderer.renderItemOverlayIntoGUI(font, this.mc.renderEngine, item, (int) pos.x(),
				(int) pos.y(), null);
		this.zLevel = 0.0F;
		itemRenderer.zLevel = 0.0F;

		GL11.glTranslatef(0.0F, 0.0F, -32.0F);
		
		RenderHelper.disableStandardItemLighting();
		
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();
	}

	@Override
	public void renderBlockIcon(IPosition pos, Icon icon) {
		this.renderIcon(pos, icon, this.mc.renderEngine.textureMapBlocks);
	}
	
	@Override
	public void renderItemIcon(IPosition pos, Icon icon) {
		this.renderIcon(pos, icon, this.mc.renderEngine.textureMapItems);
	}
	
	private void renderIcon(IPosition pos, Icon icon, TextureMap map) {

		GL11.glTranslatef(0.0F, 0.0F, 32.0F);
		this.zLevel = 0;
		itemRenderer.zLevel = 0;
		
		if(map == this.mc.renderEngine.textureMapItems) {
			this.mc.renderEngine.bindTexture("/gui/items.png");
		}
		else {
			this.mc.renderEngine.bindTexture("/terrain.png");
		}
		
		
		itemRenderer.renderIcon((int) pos.x(), (int) pos.y(), icon, 16, 16);
		
		this.zLevel = 0.0F;
		itemRenderer.zLevel = 0.0F;

		GL11.glTranslatef(0.0F, 0.0F, -32.0F);

	}

	@Override
	public void renderGradientRect(IArea area, int c1, int c2) {
		this.drawGradientRect((int) area.pos().x(), (int) area.pos().y(),
				(int) (area.pos().x() + area.size().x()), (int) (area.pos().y() + area.size().y()),
				c1, c2);
	}

	int currentColour = 0xFFFFFF;

	@Override
	public void setColour(int hex) {

		this.currentColour = hex;

		int r = (hex & 0xFF0000) >> 16;
		int g = (hex & 0xFF00) >> 8;
		int b = (hex & 0xFF);

		GL11.glColor3f((r) / 255.0f, (g) / 255.0f, (b) / 255.0f);

	}

	@Override
	public void limitArea(IArea area) {
		int x = (int) area.pos().x();
		int y = (int) area.pos().y();
		int w = (int) area.size().x();
		int h = (int) area.size().y();
		// w *= 1.1f;
		// System.out.println(y);
		y = this.height - (y + h);
		float k = this.xSize;
		float scaleX = (float) this.width / (float) this.mc.displayWidth;
		float scaleY = (float) this.height / (float) this.mc.displayHeight;
		// GL11.glScissor((int)((x+guiLeft)/scaleX)+8,
		// (int)((y+guiTop)/scaleY)-100, (int)(w/scaleX)+2, (int)(h/scaleY));
		GL11.glScissor((int) ((x) / scaleX), (int) ((y) / scaleY), (int) (w / scaleX) + 2,
				(int) (h / scaleY));

	}

	@Override
	public float getTextHeight(String text, float width) {
		return fontRenderer.listFormattedStringToWidth(text, (int) width).size()
				* fontRenderer.FONT_HEIGHT;
	}

	@Override
	public void renderSubTexture(ITexture texture, IArea area, Direction direction, float percentage) {
		float x = area.pos().x();
		float y = area.pos().y();
		float w = area.size().x();
		float h = area.size().y();
		float u = texture.u();
		float v = texture.v();

		switch (direction) {
		case Downwards:
			h *= percentage;
			break;
		case Left:
			x += (1 - percentage) * w;
			u += (1 - percentage) * w;
			w *= percentage;
			break;
		case Right:
			w *= percentage;
			break;
		case Upwards:
			y += h - (int) (percentage * h);
			v += texture.h() - (int) (percentage * texture.h());
			h *= percentage;
			break;
		}
		renderTexturedRect(x, y, u, v, w, h);
	}

	IWidget currentWidget;

	private ITexture getTexture(Object key) {
		if (key instanceof ITexture)
			return (ITexture) key;
		return currentWidget.getTexture(key);
	}

	@Override
	public void setWidget(IWidget widget) {
		this.currentWidget = widget;
	}

	@Override
	public <T> T subRenderer(Class<T> renderer) {
		if(subRenderers.containsKey(renderer))
			return (T) subRenderers.get(renderer);
		return (T) this;
	}

	Map<Class<?>, Object> subRenderers = new HashMap<Class<?>, Object>();

	@Override
	public void renderWindow(Window window) {
		setColour(0xFFFFFF);

		renderTexture(window.getBackground1(), Vector2f.ZERO);
		if (window.getBackground2() != null)
			renderTexture(window.getBackground2(), new Vector2f(256, 0));

		setColour(window.getColour());

		renderTexture(CraftGUITexture.Window, window.getArea());
	}

	@Override
	public <T> void addSubRenderer(Class<T> rendererClass, T renderer) {
		this.subRenderers.put(rendererClass, renderer);
	}

	public ITooltipType getTooltipType() {
		if(Keyboard.isKeyDown(Keyboard.KEY_TAB)) {
			return Tooltip.Type.Help;
		}
		return Tooltip.Type.Standard;
	}
}