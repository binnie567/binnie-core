package binnie.craftgui.minecraft;

import java.util.ArrayList;
import java.util.List;

import binnie.core.machines.Machine;
import binnie.core.machines.power.IProcess;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.geometry.Direction;
import binnie.craftgui.resource.ITexture;

public class ControlProgress extends ControlProgressBase implements ITooltip {

	ITexture progressBlank;
	ITexture progressBar;
	protected Direction direction;
	public boolean isMachineProcess = true;

	public ControlProgress(IWidget parent, int x, int y, ITexture progressBlank, ITexture progressBar, Direction dir) {
		super(parent, x, y, progressBlank == null ? 0 : progressBlank.w(), progressBlank == null ? 0 : progressBlank.h());
		this.progressBlank = progressBlank;
		this.progressBar = progressBar;
		progress = 0.0f;
		direction = dir;
		canMouseOver = true;
	}

	@Override
	public void onRenderBackground() {
		
		getRenderer().renderTexture(progressBlank, getArea());
		getRenderer().renderSubTexture(progressBar, getArea(), direction, progress);

	}
	
	private IProcess getProcess() {
		return Machine.getInterface(IProcess.class, Window.get(this).getInventory());
	}
	
	@Override
	public void onUpdate() {
		IProcess process = getProcess();
		if(process != null && isMachineProcess)
			setProgress(process.getProgress()/100f);
	}
	
	public List<String> helpStrings = new ArrayList<String>();

	@Override
	public void getTooltip(Tooltip tooltip) {
		IProcess process = getProcess();
		if(tooltip.getType() == Tooltip.Type.Help && process != null) {
			if(isMachineProcess) {
				tooltip.add("Progress");
				if(progress == 0f) {
					tooltip.add("Not in Progress");
				}
				else {
					tooltip.add(process.getTooltip() + " (" + (int)process.getProgress() + "%)");
				}
				float eff = process.getEfficiency();
				if(eff == 0) {
					tooltip.add("Time Left: Infinite");
				}
				else {
					tooltip.add("Time Left: " + convertTime((int)((1f-progress)*process.getProcessLength()/eff)));
				}
				
				tooltip.add("Total Time: " + convertTime(process.getProcessLength()));
				tooltip.add("Energy Cost: " + process.getProcessEnergy() + " MJ");
			}
			else
				tooltip.add(helpStrings);
		}
	}

	private String convertTime(int time) {
		int seconds = (int) (time/20f);
		int minutes = 0;
		while(seconds > 60) {
			minutes += 1;
			seconds -= 60;
		}
		String ts = "";
		if(minutes > 0)
			ts += minutes + " minute" + (minutes == 1 ? "" : "s");
		if(seconds > 0) {
			if(ts.length() > 0)
				ts += " ";
			ts += seconds + " second" + (seconds == 1 ? "" : "s");
		}
		return ts;
	}

}
