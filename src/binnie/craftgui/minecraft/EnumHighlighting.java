package binnie.craftgui.minecraft;

import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.geometry.Area;

public enum EnumHighlighting {
	
	Error,
	Warning,
	Help,
	ShiftClick,
	
	;
	
	int getColour() {
		switch(this) {
		case Error:
			return MinecraftTooltip.getOutline(MinecraftTooltip.Type.Error);
		case Help:
			return MinecraftTooltip.getOutline(Tooltip.Type.Help);
		case ShiftClick:
			return 0xFFFF00;
		case Warning:
			return MinecraftTooltip.getOutline(MinecraftTooltip.Type.Warning);
		
		}
		return 0;
	}

}
