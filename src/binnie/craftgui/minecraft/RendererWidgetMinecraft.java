package binnie.craftgui.minecraft;

import org.lwjgl.opengl.GL11;

import binnie.craftgui.controls.ControlTextEdit;
import binnie.craftgui.controls.button.ControlButton;
import binnie.craftgui.controls.listbox.ControlOption;
import binnie.craftgui.controls.scroll.ControlScrollBar;
import binnie.craftgui.controls.tab.ControlTab;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.Widget;
import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.core.renderer.IRenderer;
import binnie.craftgui.core.renderer.IRendererWidget;
import binnie.craftgui.resource.minecraft.CraftGUITexture;
import binnie.craftgui.window.Panel;

public class RendererWidgetMinecraft implements IRendererWidget {
	
	IRenderer renderer;
	
	public RendererWidgetMinecraft(IRenderer renderer) {
		this.renderer = renderer;
	}
	
	IRenderer getRenderer() {
		return renderer;
	}

	@Override
	public void preRender(IWidget widget) {
		getRenderer().setColour(widget.getColour());

		getRenderer().setWidget(widget);

		if (widget.isCroppedWidet()) {
			IWidget cropRelative = widget.getCropWidget() != null ? widget.getCropWidget() : widget;
			IPosition pos = cropRelative.getAbsolutePosition();
			IArea cropZone = widget.getCroppedZone();
			getRenderer().limitArea(
					new Area(pos.add(cropZone.pos()), cropZone.size()));
			GL11.glEnable(GL11.GL_SCISSOR_TEST);
		}
	}

	@Override
	public void postRender(Widget widget) {
		if (widget.isCroppedWidet()) {
			GL11.glDisable(GL11.GL_SCISSOR_TEST);
		}
		getRenderer().setWidget(null);
	}
	
	@Override
	public void render(IWidget widget, int renderLevel) {
		if (widget instanceof Panel && renderLevel == 0) {
			renderPanel((Panel) widget);
			return;
		}
		if (widget instanceof ControlTextEdit && renderLevel == 0) {
			renderTextEdit((ControlTextEdit) widget);
			return;
		}
		if (widget instanceof ControlTab && renderLevel == 0) {
			renderTab((ControlTab) widget);
			return;
		}
		if (widget instanceof ControlScrollBar && renderLevel == 0) {
			renderScroll((ControlScrollBar) widget);
			return;
		}
		if (widget instanceof ControlOption && renderLevel == 0) {
			renderListBoxOption((ControlOption) widget);
			return;
		}
		if (widget instanceof ControlButton && renderLevel == 0) {
			renderButton((ControlButton) widget);
			return;
		}
	}

	private void renderButton(ControlButton widget) {
		Object texture = CraftGUITexture.ButtonDisabled;
		if (widget.isMouseOver())
			texture = CraftGUITexture.ButtonHighlighted;
		else if (widget.isEnabled())
			texture = CraftGUITexture.Button;

		getRenderer().renderTexture(texture, widget.getArea());
	}

	private void renderListBoxOption(ControlOption widget) {
		if (widget.isCurrentSelection()) {
			getRenderer().renderTexture(CraftGUITexture.Outline, widget.getArea());
		}
	}

	private void renderScroll(ControlScrollBar widget) {
		float height = widget.getBarHeight();
		if (height < 6)
			height = 6f;

		widget.setSize(new Vector2f(widget.getSize().x(), height));

		Object texture = CraftGUITexture.ScrollDisabled;
		if (widget.isMouseOver())
			texture = CraftGUITexture.ScrollHighlighted;
		else if (widget.isEnabled())
			texture = CraftGUITexture.Scroll;

		getRenderer().renderTexture(texture, widget.getArea());
	}

	public void renderPanel(Panel panel) {
		Panel.IPanelType panelType = panel.getType();
		if (panelType instanceof MinecraftGUI.PanelType) {
			switch ((MinecraftGUI.PanelType) panelType) {
			case Black:
				getRenderer().renderTexture(CraftGUITexture.PanelBlack, panel.getArea());
				break;
			case Gray:
				getRenderer().renderTexture(CraftGUITexture.PanelGray, panel.getArea());
				break;
			case Tinted:
				getRenderer().renderTexture(CraftGUITexture.PanelTinted, panel.getArea());
				break;
			case Coloured:
				break;
			}
		}

	}

	public void renderTextEdit(ControlTextEdit textEdit) {
		getRenderer().renderTexture(CraftGUITexture.PanelGray, textEdit.getArea());
	}

	public void renderTab(ControlTab widget) {
		Object texture = CraftGUITexture.TabDisabled;

		if (widget.isMouseOver())
			texture = CraftGUITexture.TabHighlighted;
		else if (widget.isCurrentSelection())
			texture = CraftGUITexture.Tab;

		IArea area = widget.getArea();
		if(texture == CraftGUITexture.TabDisabled) {
			area.setPosition(area.getPosition().add(new Vector2f(3, 0)));
			area.setSize(area.getSize().sub(new Vector2f(3, 0)));
		}
		getRenderer().renderTexture(texture, area);
	}

	@Override
	public void renderTooltip(IPosition mousePosition, Tooltip tooltip) {
	}

}
