package binnie.craftgui.minecraft;

import binnie.core.machines.Machine;
import binnie.core.machines.power.IProcess;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.geometry.Direction;
import binnie.craftgui.resource.ITexture;

public class ControlMachineProgress extends ControlProgress {

	public ControlMachineProgress(IWidget parent, int x, int y, ITexture base, ITexture progress, Direction dir) {
		super(parent, x, y, base, progress, dir);
	}

}
