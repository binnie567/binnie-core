package binnie.craftgui.minecraft;

import java.util.Map;

public interface INetworkedEntityGUI {

	public void addGUINetworkData(Map<Integer, Integer> data);
	public void recieveGUINetworkData(int id, int data);

}
