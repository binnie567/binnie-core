package binnie.craftgui.minecraft;

import net.minecraft.util.EnumChatFormatting;
import binnie.craftgui.core.Tooltip;

public class MinecraftTooltip extends Tooltip {
	
	public static enum Type implements Tooltip.ITooltipType {
		Error,
		Warning,
		;
	}

	public static int getOutline(ITooltipType type) {
		return TypeColour.valueOf(type.toString()).getOutline();
	}
	
	public static String getTitle(ITooltipType type) {
		return TypeColour.valueOf(type.toString()).getTitle();
	}
	
	public static String getBody(ITooltipType type) {
		return TypeColour.valueOf(type.toString()).getBody();
	}
	
	private static enum TypeColour {
		Standard(0x5000ff, EnumChatFormatting.WHITE, EnumChatFormatting.GRAY),
		Help(0x4CFF00, EnumChatFormatting.GREEN, EnumChatFormatting.DARK_GREEN),
		Information(0x00BFFF, EnumChatFormatting.AQUA, EnumChatFormatting.DARK_AQUA),
		Error(0xFF3100, EnumChatFormatting.RED, EnumChatFormatting.DARK_RED),
		Warning(0xFF9F00, EnumChatFormatting.YELLOW, EnumChatFormatting.GOLD),
		;
		
		int outline;
		String mainText;
		String bodyText;
		
		private TypeColour(int outline, EnumChatFormatting mainText, EnumChatFormatting bodyText) {
			this.outline = outline;
			this.mainText = mainText.toString();
			this.bodyText = bodyText.toString();
		}
		public int getOutline() {
			return outline;
		}
		public String getTitle() { return mainText; }
		public String getBody() { return bodyText; }
	}
	
}
