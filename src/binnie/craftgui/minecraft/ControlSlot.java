package binnie.craftgui.minecraft;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import binnie.core.machines.inventory.InventorySlot;
import binnie.core.machines.inventory.MachineSide;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.CraftGUIUtil;
import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;
import binnie.craftgui.resource.minecraft.CraftGUITexture;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ControlSlot extends ControlSlotBase {

	public static Map<EnumHighlighting, List<Integer>> highlighting = new HashMap<EnumHighlighting, List<Integer>>();

	static {
		for(EnumHighlighting h : EnumHighlighting.values())
			highlighting.put(h, new ArrayList<Integer>());
	}
	
	public Slot slot = null;

	@EventHandler(origin = Origin.Self)
	@SideOnly(Side.CLIENT)
	public void onMouseClick(EventMouse.Down event) {
		if (slot != null) {
			((Window) getSuperParent()).getGui().getMinecraft().playerController.windowClick(
					((Window) getSuperParent()).getContainer().windowId, slot.slotNumber,
					event.getButton(), Window.get(this).getGui().isShiftKeyDown() ? 1 : 0,
					((Window) getSuperParent()).getGui().getMinecraft().thePlayer);

		}
	}

	public ControlSlot(IWidget parent, int x, int y) {
		super(parent, x, y);
	}

	public ControlSlot(IWidget parent, int x, int y, Slot slot) {
		super(parent, x, y);
		this.slot = slot;
	}

	@Override
	public void onRenderBackground() {

		getRenderer().renderTexture(CraftGUITexture.Slot, Vector2f.ZERO);
		
		InventorySlot islot = this.getInventorySlot();
		if(islot != null && islot.getValidator() != null) {
			Icon icon = islot.getValidator().getIcon(!islot.getInputSides().isEmpty());
			if(icon != null)
				getRenderer(IRendererMinecraft.class).renderItemIcon(new Vector2f(1, 1), icon);
		}

		boolean highlighted = false;
		for (Map.Entry<EnumHighlighting, List<Integer>> highlight : highlighting.entrySet()) {
			if (!highlighted && highlight.getValue().contains(slot.slotNumber)) {
				highlighted = true;
				int c = 0xAA000000 + highlight.getKey().getColour();
				getRenderer().renderGradientRect(new Area(1, 1, 16, 16), c, c);
			}
		}
		if (!highlighted && getSuperParent().getMousedOverWidget() == this) {

			if (Window.get(this).getGui().getDraggedItem() != null
					&& !slot.isItemValid(Window.get(this).getGui().getDraggedItem()))
				getRenderer().renderGradientRect(new Area(1, 1, 16, 16), 0xAAFF9999, 0xAAFF9999);
			else
				getRenderer().renderGradientRect(new Area(1, 1, 16, 16), -2130706433, -2130706433);
		}
	}

	@Override
	public void onRenderOverlay() {

		boolean highlighted = false;
		
		for (Map.Entry<EnumHighlighting, List<Integer>> highlight : highlighting.entrySet()) {
			if (!highlighted && highlight.getValue().contains(slot.slotNumber)) {
				highlighted = true;
				int c = highlight.getKey().getColour();
				IArea area = getArea();
				if (getParent() instanceof ControlSlotArray
						|| getParent() instanceof ControlPlayerInventory) {
					area = getParent().getArea();
					area.setPosition(Vector2f.ZERO.sub(getPosition()));
				}
				getRenderer().setColour(c);
				getRenderer().renderTexture(CraftGUITexture.Outline,
						CraftGUIUtil.getPaddedArea(area, -1));
			}
		}
	}

	@Override
	public void onUpdate() {
		super.onUpdate();
		if(this.isMouseOver() && Window.get(this).getGui().isShiftKeyDown()) {
			Window.get(this).getContainer().setMouseOverSlot(slot);
		}
		if (Window.get(this).getGui().getTooltipType() == Tooltip.Type.Help) {

			if (isMouseOver()) {
				for (ControlSlot slot2 : getControlSlots()) {
					this.highlighting.get(EnumHighlighting.Help).add(slot2.slot.slotNumber);
				}
				return;
			}
		}
	}

	private List<ControlSlot> getControlSlots() {
		List<ControlSlot> slots = new ArrayList<ControlSlot>();
		if (getParent() instanceof ControlSlotArray
				|| getParent() instanceof ControlPlayerInventory) {
			for (IWidget child : getParent().getWidgets())
				slots.add((ControlSlot) child);
		} else {
			slots.add(this);
		}
		return slots;
	}

	public ItemStack getItemStack() {
		if (slot != null)
			return slot.getStack();
		return null;
	}

	public void create(IInventory inventory, int index) {
		if (slot != null)
			return;
		slot = createSlot(inventory, index);
		((Window) getSuperParent()).getContainer().newSlot(slot);
	}

	protected Slot createSlot(IInventory inventory, int index) {
		return new CustomSlot(inventory, index, 0, 0);
	}

	@Override
	public void getTooltip(Tooltip tooltip) {
		if (tooltip.getType() == Tooltip.Type.Help) {
			if (getInventorySlot() != null) {
				InventorySlot slot = getInventorySlot();
				tooltip.add(slot.getName());
				tooltip.add("Insert Side: " + MachineSide.asString(slot.getInputSides()));
				tooltip.add("Extract Side: " + MachineSide.asString(slot.getOutputSides()));
				if (slot.isReadOnly())
					tooltip.add("Pickup Only Slot");
				tooltip.add("Accepts: "
						+ (slot.getValidator() == null ? "Any Item" : slot.getValidator()
								.getTooltip()));
			} else if (slot.inventory instanceof InventoryPlayer)
				tooltip.add("Player Inventory");

			return;
		}

		super.getTooltip(tooltip);
	}

	public InventorySlot getInventorySlot() {
		return slot instanceof CustomSlot ? ((CustomSlot) slot).getInventorySlot() : null;
	}

}
