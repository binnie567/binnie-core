package binnie.craftgui.minecraft;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;

public class ControlPlayerInventory extends Control {

	private List<ControlSlot> slots = new ArrayList<ControlSlot>();

	public ControlPlayerInventory(IWidget parent) {
		super(parent, (int) (parent.getSize().x() / 2) - 81, (int) parent
				.getSize().y() - 76 - 12, 162, 76);
		for (int row = 0; row < 3; row++) {
			for (int column = 0; column < 9; column++) {
				ControlSlot slot = new ControlSlot(this, column * 18, row * 18);;
				slots.add(slot);
			}
		}

		// Player "hotbar"
		for (int i1 = 0; i1 < 9; i1++) {
			ControlSlot slot = new ControlSlot(this, i1 * 18, 58);
			slots.add(slot);

		}
		
		

		create();
	}
	
	public ControlPlayerInventory(IWidget parent, String vertical) {
		super(parent, (int) 12, (int) parent
				.getSize().y() - 18*12 - 12, 18*3, 18*12);
		for (int row = 0; row < 12; row++) {
			for (int column = 0; column < 3; column++) {
				ControlSlot slot = new ControlSlot(this, column * 18, row * 18);;
				slots.add(slot);
			}
		}

		create();
	}

	public void create() {
		for (int row = 0; row < 3; row++) {
			for (int column = 0; column < 9; column++) {
				ControlSlot slot = slots.get(column + row * 9);
				slot.create(Window.get(this).getPlayer().inventory, 9 + column + row
						* 9);
			}
		}

		// Player "hotbar"
		for (int i1 = 0; i1 < 9; i1++) {
			ControlSlot slot = slots.get(27 + i1);
			slot.create(Window.get(this).getPlayer().inventory, i1);

		}
	}

	public void addItem(ItemStack item) {
		if (item == null)
			return;
		for (ControlSlot slot : slots) {
			if (!slot.slot.getHasStack()) {
				slot.slot.putStack(item);
				return;
			}
		}
	}

	public void addInventory(IInventory inventory) {
		for (int i = 0; i < inventory.getSizeInventory(); i++) {
			addItem(inventory.getStackInSlot(i));
		}
	}

	public ControlSlot getSlot(int i) {
		if (i < 0 || i >= slots.size())
			return null;
		return slots.get(i);
	}
	
	@Override
	public void onUpdate() {
	}

}
