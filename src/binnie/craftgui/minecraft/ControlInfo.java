package binnie.craftgui.minecraft;

import org.lwjgl.opengl.GL11;

import binnie.core.machines.Machine;
import binnie.core.machines.inventory.InventorySlot;
import binnie.core.machines.inventory.MachineSide;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.resource.minecraft.CraftGUITexture;

public class ControlInfo extends Control implements ITooltip {

	protected ControlInfo(IWidget parent, float x, float y) {
		super(parent, x, y, 16, 16);
		canMouseOver = true;
	}
	
	@Override
	public void onRenderBackground() {
		getRenderer().renderTexture(CraftGUITexture.InfoButton, getArea());
	}
	
	@Override
	public void getTooltip(Tooltip tooltip) {
		tooltip.setType(Tooltip.Type.Information);
		tooltip.add("Info");
		tooltip.add(Machine.getInterface(IMachineInformation.class, Window.get(this).getInventory()).getInformation());
		tooltip.setMaxWidth(200);
	}

}
