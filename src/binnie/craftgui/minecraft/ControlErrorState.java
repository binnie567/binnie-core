package binnie.craftgui.minecraft;

import net.minecraft.entity.player.InventoryPlayer;
import binnie.core.machines.power.ErrorState;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.resource.minecraft.CraftGUITexture;

public class ControlErrorState extends Control implements ITooltip {

	@Override
	public void onRenderBackground() {
		Object texture = CraftGUITexture.StateWarning;
		if (errorState == null) {
			texture = CraftGUITexture.StateNone;
		} else if (type == 0) {
			texture = CraftGUITexture.StateError;
		}

		getRenderer().renderTexture(texture, Vector2f.ZERO);

		super.onRenderBackground();
	}

	public ErrorState getError() {
		return Window.get(this).getContainer().getErrorState();
	}

	@Override
	public final void onUpdate() {
		errorState = getError();
		type = Window.get(this).getContainer().getErrorType();

		ControlSlot.highlighting.get(EnumHighlighting.Error).clear();
		ControlSlot.highlighting.get(EnumHighlighting.Warning).clear();
		ControlLiquidTank.tankError.clear();
		ControlEnergyBar.isError = false;
		
		if (!isMouseOver() || errorState == null)
			return;
		
		ControlEnergyBar.isError = errorState.isPowerError();

		if (errorState.isItemError()) {
			for (int slot : errorState.getData()) {
				int id = -1;
				for (CustomSlot cslot : Window.get(this).getContainer().getCustomSlots()) {
					if (!(cslot.inventory instanceof InventoryPlayer) && cslot.slotID == slot)
						id = cslot.slotNumber;
				}
				if (id >= 0) {
					if(type == 0) {
						ControlSlot.highlighting.get(EnumHighlighting.Error).add(id);
					}
					else {
						ControlSlot.highlighting.get(EnumHighlighting.Warning).add(id);
					}
				}
			}
		}

		if (errorState.isTankError()) {
			for (int slot : errorState.getData()) {
				ControlLiquidTank.tankError.add(slot);
			}
		}

	}

	protected ErrorState errorState;
	protected int type = 0;

	public ControlErrorState(IWidget parent, float x, float y) {
		super(parent, x, y, 16, 16);
		canMouseOver = true;
	}

	@Override
	public void getTooltip(Tooltip tooltipOrig) {
		MinecraftTooltip tooltip = (MinecraftTooltip) tooltipOrig;
		if (errorState != null) {
			if (type == 0) {
				tooltip.setType(MinecraftTooltip.Type.Error);
			} else {
				tooltip.setType(MinecraftTooltip.Type.Warning);
			}

			tooltip.add(errorState.toString());
			if (errorState.getTooltip().length() > 0)
				tooltip.add(errorState.getTooltip());
		}

	}

	public ErrorState getErrorState() {
		return errorState;
	}

}
