package binnie.craftgui.minecraft;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import binnie.core.machines.inventory.SlotValidator;

public class WindowInventory implements IInventory {
	
	Window window;
	
	public WindowInventory(Window window) {
		this.window = window;
	}

	Map<Integer, ItemStack> inventory = new HashMap<Integer, ItemStack>();
	Map<Integer, SlotValidator> validators = new HashMap<Integer, SlotValidator>();

	@Override
	public int getSizeInventory() {
		if(inventory.size() == 0) return 0;
		int max = 0;
		for (int i : inventory.keySet()) {
			if (i > max)
				max = i;
		}
		return max + 1;
	}

	@Override
	public ItemStack getStackInSlot(int var1) {
		if (inventory.containsKey(var1))
			return inventory.get(var1);
		return null;
	}

	@Override
	public ItemStack decrStackSize(int index, int amount) {
		if (inventory.containsKey(index)) {
			ItemStack item = inventory.get(index);
			ItemStack output = item.copy();
			int available = item.stackSize;
			if (amount > available)
				amount = available;
			item.stackSize -= amount;
			output.stackSize = amount;
			return output;
		}
		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int var1) {
		return null;
	}

	@Override
	public void setInventorySlotContents(int var1, ItemStack var2) {
		inventory.put(var1, var2);
		onInventoryChanged();
	}

	@Override
	public String getInvName() {
		return "window.inventory";
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public void onInventoryChanged() {
		window.onWindowInventoryChanged();
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer var1) {
		return true;
	}

	@Override
	public void openChest() {
	}

	@Override
	public void closeChest() {
	}

	@Override
	public boolean isInvNameLocalized() {
		return false;
	}

	@Override
	public boolean isStackValidForSlot(int i, ItemStack itemstack) {
		if(validators.containsKey(i))
			return validators.get(i).isValid(itemstack);
		return true;
	}

	public void createSlot(int slot, SlotValidator validator) {
		inventory.put(slot, null);
		if(validator != null)
			validators.put(slot, validator);
	}

}
