package binnie.craftgui.minecraft;

import net.minecraft.inventory.IInventory;

import org.lwjgl.opengl.GL11;

import binnie.core.machines.Machine;
import binnie.core.machines.TileEntityMachine;
import binnie.core.machines.power.IPoweredMachine;
import binnie.core.machines.power.IProcess;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.CraftGUIUtil;
import binnie.craftgui.core.geometry.Direction;
import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.core.renderer.IRenderer;
import binnie.craftgui.minecraft.MinecraftTooltip.Type;
import binnie.craftgui.resource.minecraft.CraftGUITexture;

public class ControlEnergyBar extends Control implements ITooltip {
	
	public static boolean isError;
	
	protected Direction direction;

	public ControlEnergyBar(IWidget parent, int x, int y, int width,
			int height, Direction direction) {
		super(parent, x, y, width, height);
		this.direction = direction;
		canMouseOver = true;
	}
	
	public IPoweredMachine getClientPower() {
		IInventory inventory = Window.get(this).getInventory();
		TileEntityMachine machine = (TileEntityMachine) (inventory instanceof TileEntityMachine ? inventory
				: null);
		if (machine == null)
			return null;
		IPoweredMachine clientPower = machine.getMachine().getInterface(
				IPoweredMachine.class);
		return clientPower;
	}
	
	public float getPercentage() {
		float percentage = (100.0f * getStoredEnergy() / getMaxEnergy());
		
		if(percentage > 100) percentage = 100;
		return percentage;
	}

	private float getStoredEnergy() {
		return Window.get(this).getContainer().getPowerInfo().getStoredEnergy();
	}
	
	private float getMaxEnergy() {
		return Window.get(this).getContainer().getPowerInfo().getMaxEnergy();
	}

	@Override
	public void getTooltip(Tooltip tooltip) {
		
		if(tooltip.getType() == Tooltip.Type.Help) {
			tooltip.add("Energy Bar");
			tooltip.add("Current: " + getStoredEnergy() + " MJ (" + (int)getPercentage() + "%)");
			tooltip.add("Capacity: " + getMaxEnergy() + " MJ");
			tooltip.add("Max Input: " + Window.get(this).getContainer().getPowerInfo().getMaxInput() + " MJ");
			
			IProcess process = Machine.getInterface(IProcess.class, Window.get(this).getInventory());
			if(process != null)
				tooltip.add("Usage: " + (int) process.getEnergyPerTick() + " MJ");
			
			// energy - current / max (%)
			// Input - Current / Max (%) 
			// process - cost MJ
		}
		else {
			tooltip.add(((int)getPercentage()) + "% charged");

			tooltip.add(getStoredEnergy() + "/"
					+ getMaxEnergy() + " MJ");
		}
		
	

	}

	@Override
	public void onRenderBackground() {
		getRenderer().renderTexture(CraftGUITexture.EnergyBarBack, getArea());

		float percentage = getPercentage()/100f;
		

		getRenderer().setColour(getColourFromPercentage(percentage));

		IArea area = getArea();
		
		switch (direction) {
		case Downwards:
		case Upwards:
			float height = area.size().y() * percentage;
			area.setSize(new Vector2f(area.size().x(), height));
			
			break;
		case Left:
		case Right:
			float width = area.size().x() * percentage;
			area.setSize(new Vector2f(width, area.size().y()));
			break;
		}
		
		if (isMouseOver() && Window.get(this).getGui().getTooltipType() == Tooltip.Type.Help) {
			int c = 0xAA000000 + MinecraftTooltip.getOutline(Tooltip.Type.Help);
			getRenderer().renderGradientRect(CraftGUIUtil.getPaddedArea(getArea(), 1), c, c);
		} else if (isError) {
			int c = 0xAA000000 + MinecraftTooltip.getOutline(MinecraftTooltip.Type.Error);
			getRenderer().renderGradientRect(CraftGUIUtil.getPaddedArea(getArea(), 1), c, c);
		}
		
		getRenderer().renderTexture(CraftGUITexture.EnergyBarGlow, area);
		
		

		GL11.glColor3d(1.0, 1.0, 1.0);

		getRenderer().renderTexture(CraftGUITexture.EnergyBarGlass, getArea());

	}
	
	@Override
	public void onRenderOverlay() {
		
		if (isMouseOver() && Window.get(this).getGui().getTooltipType() == Tooltip.Type.Help) {
			IArea area = getArea();
			getRenderer().setColour(MinecraftTooltip.getOutline(Tooltip.Type.Help));
			getRenderer().renderTexture(CraftGUITexture.Outline,
					CraftGUIUtil.getPaddedArea(area, -1));
		} else if (isError) {
			IArea area = getArea();
			getRenderer().setColour(MinecraftTooltip.getOutline(MinecraftTooltip.Type.Error));
			getRenderer().renderTexture(CraftGUITexture.Outline,
					CraftGUIUtil.getPaddedArea(area, -1));
		}
		
	}

	public int getColourFromPercentage(float percentage) {
		int colour = 0xFFFFFF;
		if (percentage > 0.5) {
			int r = (int) ((1.0 - 2 * (percentage - 0.5)) * 0xFF);
			colour = (int) (r << 16) + 0xFF00;
		} else {
			int g = (int)(0xFF*( 2 * percentage));
			colour = 0xFF0000 + (g << 8);
		}
		return colour;
	}

}
