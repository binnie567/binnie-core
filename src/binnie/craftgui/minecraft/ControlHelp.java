package binnie.craftgui.minecraft;

import org.lwjgl.opengl.GL11;

import binnie.core.machines.inventory.InventorySlot;
import binnie.core.machines.inventory.MachineSide;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.resource.minecraft.CraftGUITexture;

public class ControlHelp extends Control implements ITooltip {

	protected ControlHelp(IWidget parent, float x, float y) {
		super(parent, x, y, 16, 16);
		canMouseOver = true;
	}
	
	@Override
	public void onRenderBackground() {
		getRenderer().renderTexture(CraftGUITexture.HelpButton, getArea());
	}
	
	@Override
	public void getTooltip(Tooltip tooltip) {
		tooltip.setType(Tooltip.Type.Help);
		tooltip.add("Help");
		tooltip.add("To activate help tooltips,");
		tooltip.add("hold down the tab key and");
		tooltip.add("mouse over slots.");
	}

}
