package binnie.craftgui.minecraft;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;

public class ControlSlotArray extends Control implements Iterable<ControlSlot> {

	private int rows;
	private int columns;
	protected List<ControlSlot> slots = new ArrayList<ControlSlot>();

	public ControlSlotArray(IWidget parent, int x, int y, int columns, int rows) {
		super(parent, x, y, columns * 18, rows * 18);
		this.rows = rows;
		this.columns = columns;
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				slots.add(createSlot(column * 18, row * 18));
			}
		}
	}

	public ControlSlot createSlot(int x, int y) {
		return new ControlSlot(this, x, y);
	}

	public void setItemStacks(ItemStack[] array) {
		int i = 0;
		for (ItemStack item : array) {
			if (i >= slots.size())
				return;
			this.slots.get(i).slot.putStack(item);
			i++;
		}
	}

	public ControlSlot getControlSlot(int i) {
		if (i < 0 || i >= slots.size()) {
			return null;
		}
		return slots.get(i);
	}

	public void create(IInventory inventory, int index) {
		for (ControlSlot slot : this.slots) {
			slot.create(inventory, index++);
		}
	}

	@Override
	public Iterator<ControlSlot> iterator() {
		return slots.iterator();
	}

}
