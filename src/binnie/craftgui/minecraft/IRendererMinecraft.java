package binnie.craftgui.minecraft;

import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import binnie.craftgui.core.geometry.IPosition;
import binnie.craftgui.core.renderer.IRenderer;

public interface IRendererMinecraft extends IRenderer {

	public void renderItem(IPosition position, ItemStack item);

	public void renderBlockIcon(IPosition position, Icon icon);
	public void renderItemIcon(IPosition position, Icon icon);

	public void renderWindow(Window window);

}
