package binnie.craftgui.mod.database;

import net.minecraft.util.Icon;
import binnie.core.genetics.BinnieGenetics;
import binnie.core.genetics.BreedingSystem;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.events.EventMouse;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;
import binnie.craftgui.minecraft.ControlItemDisplay;
import binnie.craftgui.minecraft.IRendererMinecraft;
import binnie.craftgui.minecraft.Window;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IIndividual;
import forestry.api.genetics.ISpeciesRoot;

public class ControlIndividualDisplay extends ControlItemDisplay implements
		ITooltip {
	/*
	 * @Override public void onRenderForeground() { switch (discovered) { case
	 * 0: super.onRenderForeground(); break; case 1:
	 * getRenderer().setTexture(ExtraBeeTexture.Icons.getTexture());
	 * getRenderer().renderRect(0, 0, 16, 16, 64, 16); break; case 2:
	 * getRenderer().setTexture(ExtraBeeTexture.Icons.getTexture());
	 * getRenderer().renderRect(0, 0, 16, 16, 64, 0); default: break; } }
	 */

	@EventHandler(origin=Origin.Self)
	public void onMouseClick(EventMouse.Down event) {
		if(event.getButton()==0 && species != null && EnumDiscoveryState.Show == discovered)
			((WindowAbstractDatabase)getSuperParent()).gotoSpeciesDelayed(species);
	}
	
	public void setSpecies(IAlleleSpecies species) {
		this.setSpecies(species, EnumDiscoveryState.Show);
	}

	public void setSpecies(IAlleleSpecies species, EnumDiscoveryState state) {
		
		ISpeciesRoot speciesRoot = BinnieGenetics.getSpeciesRoot(species);
		
		BreedingSystem system = BinnieGenetics.getSystem(speciesRoot.getUID());
		
		IIndividual ind = system.getSpeciesRoot().templateAsIndividual(system.getSpeciesRoot().getTemplate(species.getUID()));
		
		super.setItemStack(system.getSpeciesRoot().getMemberStack(ind, 0));
		this.species = species;
		
		String username = Window.get(this).getPlayer() != null ? Window.get(this).getPlayer().username : "";
		
		if(state == EnumDiscoveryState.Undetermined)
			state = system.isSpeciesDiscovered(species, Window.get(this).getWorld(), username) ? 
					EnumDiscoveryState.Discovered : EnumDiscoveryState.Undiscovered;

		if(Window.get(this) instanceof WindowAbstractDatabase)
			if(((WindowAbstractDatabase)Window.get(this)).isNEI)
				state = EnumDiscoveryState.Show;
		
		this.discovered = state;

		canMouseOver = true;
	}

	IAlleleSpecies species = null;

	EnumDiscoveryState discovered = EnumDiscoveryState.Show; // 0 - discovered, 1 - known, 2 - unkown

	public ControlIndividualDisplay(IWidget parent, int x, int y) {
		super(parent, x, y);
	}

	public ControlIndividualDisplay(IWidget parent, int x, int y, int size) {
		super(parent, x, y, size);
	}



	@Override
	public void onRenderForeground() {
		Icon icon = null;
		BreedingSystem system = ((WindowAbstractDatabase) getSuperParent())
				.getBreedingSystem();

		switch (discovered) {
		case Show:
			super.onRenderForeground();
			return;
		case Discovered:
			icon = system.getDiscoveredIcon();
			break;
		case Undiscovered:
			icon = system.getUndiscoveredIcon();
			break;
		}
		if(icon != null) getRenderer().subRenderer(IRendererMinecraft.class).renderItemIcon(Vector2f.ZERO, icon);
	}

	@Override
	public void getTooltip(Tooltip tooltip) {
		if (species != null) {
			switch (discovered) {
			case Show:
				tooltip.add(species.getName());
				break;
			case Discovered:
				tooltip.add("Discovered Species");
				break;
			case Undiscovered:
				tooltip.add("Undiscovered Species");
			default:
				break;
			}
		}
	}

}
