package binnie.craftgui.mod.database;

public enum EnumDiscoveryState {
	
	/** Show the species, no calculations */
	Show,
	
	/** Show the species as discovered or undiscovered depending on the tracker */
	Undetermined,
	
	Discovered,
	Undiscovered,

}
