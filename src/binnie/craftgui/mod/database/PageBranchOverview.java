package binnie.craftgui.mod.database;

import java.util.ArrayList;
import java.util.List;

import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.ControlTextCentered;
import binnie.craftgui.core.IWidget;
import forestry.api.genetics.IClassification;

public class PageBranchOverview extends PageBranch {

	public PageBranchOverview(IWidget parent, DatabaseTab tab) {
		super(parent, tab);

		pageBranchOverview_branchName = new ControlTextCentered(this, 8, "");

		pageBranchOverview_branchScientific = new ControlTextCentered(this, 32, "");
		pageBranchOverview_branchAuthority = new ControlTextCentered(this, 44, "");

	}

	ControlText pageBranchOverview_branchName;
	ControlText pageBranchOverview_branchScientific;
	ControlText pageBranchOverview_branchAuthority;
	List<ControlText> pageBranchOverview_branchDescription = new ArrayList<ControlText>();

	@Override
	public void onBranchChanged(IClassification branch) {
		pageBranchOverview_branchName.setValue("\u00a7n" + branch.getName()
				+ " Branch\u00a7r");
		pageBranchOverview_branchScientific.setValue("\u00a7oApidae "
				+ branch.getScientific() + "\u00a7r");
		pageBranchOverview_branchAuthority.setValue("Discovered by \u00a7l"
				+ branch.getMemberSpecies()[0].getAuthority() + "\u00a7r");

		for (IWidget widget : pageBranchOverview_branchDescription)
			deleteChild(widget);

		pageBranchOverview_branchDescription.clear();

		String desc = branch.getDescription();
		if (desc == null || desc == "")
			desc = "No Description Provided.";

		String line = "";

		List<String> descLines = new ArrayList<String>();

		for (String str : desc.split(" ")) {
			if (getRenderer().getTextWidth(line + " " + str) > 134) {
				descLines.add("\u00a7o" + line + "\u00a7r");
				line = "";
			}
			line = line + " " + str;
		}
		descLines.add(line);

		int i = 0;

		for (String dLine : descLines)
			pageBranchOverview_branchDescription.add(new ControlTextCentered(this, 84 + 12 * i++, dLine));
	}

}