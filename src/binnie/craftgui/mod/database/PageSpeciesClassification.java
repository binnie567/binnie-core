package binnie.craftgui.mod.database;

import java.util.LinkedHashMap;
import java.util.Map;

import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.ControlTextCentered;
import binnie.craftgui.core.IWidget;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IClassification;
import forestry.api.genetics.IClassification.EnumClassLevel;

public class PageSpeciesClassification extends PageSpecies {

	Map<IClassification.EnumClassLevel, ControlText> levels = new LinkedHashMap<IClassification.EnumClassLevel, ControlText>();

	ControlText genus;
	
	public PageSpeciesClassification(IWidget parent, DatabaseTab tab) {
		super(parent, tab);
		int y = 16;
		for (EnumClassLevel level : EnumClassLevel.values()) {
			ControlText text = new ControlTextCentered(this, y, "");
			text.setColour(level.getColour());
			levels.put(level, text);
			y += 12;
		}
		genus = new ControlTextCentered(this, y, "");
		genus.setColour(0xffba77);
	}

	@Override
	public void onSpeciesChanged(IAlleleSpecies species) {
		if (species != null) {
			for (ControlText text : levels.values())
				text.setValue("- - -");
			genus.setValue(species.getBinomial());

			IClassification classification = species.getBranch();

			while (classification != null) {
				EnumClassLevel level = classification.getLevel();
				String text = "";
				int n = level.ordinal();
				text += classification.getScientific();
				levels.get(level).setValue(text);
				classification = classification.getParent();
			}
		}
	}

}
