package binnie.craftgui.mod.database;

import binnie.core.genetics.BreedingSystem;
import binnie.craftgui.controls.ControlTextCentered;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.minecraft.Window;

public class PageBreeder extends Control {

	String player;

	public PageBreeder(IWidget parent, String player) {
		super(parent, 0, 0, 144, 176);
		this.player = player;
	}

	public void onPageRefresh() {
		
		while (this.getWidgets().size() > 0)
			this.deleteChild(getWidgets().get(0));

		BreedingSystem system = ((WindowAbstractDatabase) Window.get(this)).getBreedingSystem();
		
		String descriptor = system.getDescriptor();

		new ControlTextCentered(this, 8, "\u00a7n" + system.getDescriptor() + " Profile\u00a7r");

		new ControlTextCentered(this, 75, "" + system.discoveredSpeciesCount + "/"
				+ system.totalSpeciesCount + " Species");

		new ControlBreedingProgress(this, 20, 87, 102, 14, system, system.discoveredSpeciesPercentage);

		new ControlTextCentered(this, 115, "" + system.discoveredBranchCount + "/"
				+ system.totalBranchCount + " Branches");

		new ControlBreedingProgress(this, 20, 127, 102, 14, system, system.discoveredBranchPercentage);

		if (system.discoveredSecretCount > 0) {
			new ControlTextCentered(this, 155, "" + system.discoveredSecretCount + "/"
					+ system.totalSecretCount + " Secret Species");
		}

		new ControlTextCentered(this, 32, player);
		new ControlTextCentered(this, 44, "\u00a7o" + system.getEpitome() + "\u00a7r");

	}

}
