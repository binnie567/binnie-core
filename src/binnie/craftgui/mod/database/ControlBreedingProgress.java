package binnie.craftgui.mod.database;

import binnie.core.genetics.BreedingSystem;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.geometry.CraftGUIUtil;
import binnie.craftgui.core.geometry.IArea;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.resource.ITexture;
import binnie.craftgui.resource.minecraft.CraftGUITexture;
import binnie.craftgui.resource.minecraft.CraftGUITextureSheet;
import binnie.craftgui.resource.minecraft.StandardTexture;

public class ControlBreedingProgress extends Control {

	static ITexture Progress = new StandardTexture(80, 22, 4, 4, CraftGUITextureSheet.Controls2.getTexture());
	
	float percentage;
	int colour;

	public ControlBreedingProgress(IWidget parent, int x, int y, int width,
			int height, BreedingSystem system, float percentage) {
		super(parent, x, y, width, height);
		this.percentage = percentage;
		this.colour = system.getColour();
	}

	@Override
	public void onRenderBackground() {

		getRenderer().renderTexture(CraftGUITexture.PanelBlack, getArea());

		IArea area = CraftGUIUtil.getPaddedArea(getArea(), 1);
		
		area.setSize(new Vector2f(area.size().x()*percentage, area.size().y()));

		getRenderer().setColour(colour);
		
		getRenderer().renderTextureTiled(Progress, area);

	}


}
