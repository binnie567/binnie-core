package binnie.craftgui.mod.database;

import java.util.Collection;

import net.minecraft.entity.player.EntityPlayer;
import binnie.core.genetics.BreedingSystem;
import binnie.craftgui.controls.button.ControlEnumButton;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.controls.page.ControlPage;
import binnie.craftgui.controls.page.ControlPages;
import binnie.craftgui.controls.tab.ControlTabBar;
import binnie.craftgui.controls.tab.ControlTabBarWidget;
import binnie.craftgui.core.geometry.CraftGUIUtil;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.events.Event;
import binnie.craftgui.events.EventValueChanged;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;
import binnie.craftgui.minecraft.MinecraftGUI;
import binnie.craftgui.minecraft.Window;
import binnie.craftgui.window.Panel;
import cpw.mods.fml.relauncher.Side;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IBreedingTracker;
import forestry.api.genetics.IClassification;

public abstract class WindowAbstractDatabase extends Window {

	protected float selectionBoxWidth = 95f;;
	protected final float infoBoxWidth = 95f;;

	public void changeMode(Mode mode) {
		modePages.setValue(mode);
		
		if(mode == Mode.Breeder);
			pageBreeder.onPageRefresh();
	}
	
	@EventHandler(origin=Origin.Any)
	public void onValueChanged(EventValueChanged event) {
		if(speciesPages == null || branchPages == null) return;
		if (event.isOrigin(modeButton)) {

			Mode mode = ((EventValueChanged<Mode>) event).getValue();
			changeMode(mode);

		}

		if (event.isOrigin(selectionBoxSpecies)) {
			IAlleleSpecies selection = ((EventValueChanged<IAlleleSpecies>) event).getValue();

			if (selection == null) {
				speciesPages.hide();
			}
			else {
				speciesPages.show();
				this.callEvent(new EventSpeciesChanged(this, selection));
			}
				
		}

		if (event.isOrigin(selectionBoxBranches)) {
			IClassification selection = ((EventValueChanged<IClassification>) event).getValue();
			if (selection == null) {
				branchPages.hide();
			}
			else {
				branchPages.show();
				this.callEvent(new EventBranchChanged(this, selection));
			}
		}

	}

	public static enum Mode {
		Species, Branches, Breeder, ;
	}

	boolean isNEI;

	public boolean isNEI() {
		return isNEI;
	}

	public BreedingSystem getBreedingSystem() {
		return system;
	}

	BreedingSystem system;

	public WindowAbstractDatabase(EntityPlayer player, Side side, boolean nei,
			BreedingSystem system, float wid) {
		super(185 + wid, 192, player, null, side);
		isNEI = nei;
		this.system = system;
		selectionBoxWidth = wid;
	}

	public WindowAbstractDatabase(EntityPlayer player, Side side, boolean nei,
			BreedingSystem system) {
		this(player, side, nei, system, 95f);
	}

	Panel panelInformation = null;
	Panel panelSearch = null;

	protected ControlPages<Mode> modePages = null;
	
	ControlPage<Mode> modeBreeder;
	ControlPage<Mode> modeSpecies;
	ControlPage<Mode> modeBranches;
	
	protected ControlPages<DatabaseTab> branchPages = null;
	protected ControlPages<DatabaseTab> speciesPages = null;

	ControlTabBar<DatabaseTab> branchTabBar;
	ControlTabBar<DatabaseTab> speciesTabBar;

	ControlEnumButton<Mode> modeButton = null;

	ControlBranchBox selectionBoxBranches;
	ControlSpeciesBox selectionBoxSpecies;

	// IApiaristTracker breedingManager;

	PageBreeder pageBreeder;
	PageBreederStats pageBreederStats;
	
	@Override
	public void initialize() {

		panelInformation = new Panel(this, 24, 8, 144, 176, MinecraftGUI.PanelType.Black);
		panelInformation.setColour(0x0D2100);

		panelSearch = new Panel(this, 172, 32, selectionBoxWidth + 5, 151,
				MinecraftGUI.PanelType.Black);
		panelSearch.setColour(0x0D2100);

		modeButton = new ControlEnumButton<Mode>(this, 172, 8,
				selectionBoxWidth + 5f, 20, Mode.class);
		
		modePages = new ControlPages<WindowAbstractDatabase.Mode>(this, 0, 0, getSize().x(), getSize().y());
		
		modeSpecies = new ControlPage<Mode>(modePages, 0, 0, getSize().x(), getSize().y(), Mode.Species);
		modeBranches = new ControlPage<Mode>(modePages, 0, 0, getSize().x(), getSize().y(), Mode.Branches);
		modeBreeder = new ControlPage<Mode>(modePages, 0, 0, getSize().x(), getSize().y(), Mode.Breeder);
		
		modePages.addChild(modeSpecies);
		modePages.addChild(modeBranches);
		modePages.addChild(modeBreeder);
		
		
		pageBreeder = new PageBreeder(modeBreeder, getPlayer() == null ? "" : getPlayer().username);
		pageBreederStats = new PageBreederStats(modeBreeder, (int) (selectionBoxWidth + 5), 151, getPlayer() == null ? "" : getPlayer().username);

		CraftGUIUtil.alignToWidget(pageBreeder, panelInformation);
		CraftGUIUtil.alignToWidget(pageBreederStats, panelSearch);
		
		setupSpeciesView();
		setupBranchView();

		addTabs();

		for(IControlValue<DatabaseTab> page : speciesPages.getPages()) {
			CraftGUIUtil.alignToWidget(page, panelInformation);
		}
		
		for(IControlValue<DatabaseTab> page : branchPages.getPages()) {
			CraftGUIUtil.alignToWidget(page, panelInformation);
		}
		
		speciesTabBar = new ControlTabBarWidget<DatabaseTab>(modeSpecies, 8, 8, 16, 176,
				speciesPages);
		branchTabBar = new ControlTabBarWidget<DatabaseTab>(modeBranches, 8, 8, 16, 176, branchPages);

		changeMode(Mode.Species);

		// breedingManager = TreeManager.breedingManager
		// .getApiaristTracker(getPlayer().worldObj);

		if(selectionBoxSpecies.getValue() == null && selectionBoxSpecies.getContent().getOptions().size() > 0)
			selectionBoxSpecies.setValue(selectionBoxSpecies.getContent().getOptions().iterator().next());
		
		if (this.isServer()) {
			IBreedingTracker tracker = system.getSpeciesRoot().getBreedingTracker(getWorld(), getPlayer() == null ? "" : getPlayer().username);
			if(tracker != null)
				tracker.synchToPlayer(getPlayer());
		}

	}

	protected void addTabs() {
	}

	public void setupSpeciesView() {

		EntityPlayer player = getPlayer();
		String playerName = player == null ? "" : player.username;

		Collection<IAlleleSpecies> speciesList = !isNEI ? system
				.getDiscoveredSpecies(getWorld(), playerName) : system
				.getAllSpecies();

		selectionBoxSpecies = new ControlSpeciesBox(modeSpecies, 2, 2, selectionBoxWidth, 147);
		selectionBoxSpecies.setOptions(speciesList);
		CraftGUIUtil.alignToWidget(selectionBoxSpecies, panelSearch);
		CraftGUIUtil.moveWidget(selectionBoxSpecies, new Vector2f(2, 2));

		speciesPages = new ControlPages<DatabaseTab>(modeSpecies, 0, 0, 144, 176);

	}

	public void setupBranchView() {

		EntityPlayer player = getPlayer();
		String playerName = player == null ? "" : player.username;

		Collection<IClassification> branchList = !isNEI ? system
				.getDiscoveredBranches(getWorld(), playerName) : system
				.getAllBranches();

		selectionBoxBranches = new ControlBranchBox(modeBranches, 2, 2, selectionBoxWidth, 147);
		selectionBoxBranches.setOptions(branchList);
		CraftGUIUtil.alignToWidget(selectionBoxBranches, panelSearch);
		CraftGUIUtil.moveWidget(selectionBoxBranches, new Vector2f(2, 2));

		branchPages = new ControlPages<DatabaseTab>(modeBranches, 0, 0, 144, 176);

	}

	public void gotoSpecies(IAlleleSpecies value) {
		if (value != null) {
			this.modeButton.setValue(Mode.Species);
			changeMode(Mode.Species);
			this.selectionBoxSpecies.setValue(value);
		}

	}

	public void gotoSpeciesDelayed(IAlleleSpecies species) {
		gotoSpecies= species;
	}
	

	private IAlleleSpecies gotoSpecies = null;
	
	@Override
	public void onUpdate() {
		super.onUpdate();
		if(gotoSpecies!=null) {
			((WindowAbstractDatabase)getSuperParent()).gotoSpecies(gotoSpecies);
			gotoSpecies = null;
		}
	}
	

}
