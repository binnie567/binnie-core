package binnie.craftgui.mod.database;

import binnie.craftgui.controls.listbox.ControlList;
import binnie.craftgui.controls.listbox.ControlTextOption;
import binnie.craftgui.core.geometry.CraftGUIUtil;
import binnie.craftgui.core.geometry.Vector2f;
import forestry.api.genetics.IAlleleSpecies;

public class ControlSpeciexBoxOption extends ControlTextOption<IAlleleSpecies> {

	ControlIndividualDisplay controlBee;

	public ControlSpeciexBoxOption(ControlList<IAlleleSpecies> controlList, IAlleleSpecies option, int y) {
		super(controlList, option, option.getName(), y);
		setSize(new Vector2f(getSize().x(), 20));

		controlBee = new ControlIndividualDisplay(this, 2, 2);
		
		controlBee.setSpecies(getValue(), EnumDiscoveryState.Undetermined);
		
		if(controlBee.discovered == EnumDiscoveryState.Discovered)
			controlBee.discovered = EnumDiscoveryState.Show;
		
		textWidget.setValue(controlBee.discovered == EnumDiscoveryState.Show ? option.getName() : "Undiscovered");

		if (controlBee.discovered != EnumDiscoveryState.Show)
			canMouseOver = false;
		
		CraftGUIUtil.moveWidget(textWidget, new Vector2f(12, 0));

		//textWidget.setPosition(new Vector2f(12 + getSize().x() / 2, getSize().y() / 2 - 4));
	
	}

}
