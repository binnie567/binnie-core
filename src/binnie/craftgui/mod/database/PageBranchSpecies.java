package binnie.craftgui.mod.database;

import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.ControlTextCentered;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.EventValueChanged;
import binnie.craftgui.events.core.EventHandler;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IClassification;

public class PageBranchSpecies extends PageBranch {

	@EventHandler
	public void onHandleEvent(EventValueChanged<IAlleleSpecies> event) {
		if (event.isOrigin(pageBranchSpecies_speciesList)) {
			((WindowAbstractDatabase) getSuperParent()).gotoSpecies(event.getValue());
		}
	}

	public PageBranchSpecies(IWidget parent, DatabaseTab tab) {
		super(parent, tab);

		pageBranchSpecies_title = new ControlTextCentered(this, 8, "Species");

		// ControlListBox<String> options = new ControlListBox<String>(this, 4,
		// 20, 136, 152);
		//
		// List<String> opt = new ArrayList<String>();
		// opt.add("Hello");
		// opt.add("Goodbye");
		// opt.add("Au revoir");
		// opt.add("King");
		// opt.add("() !! XX");
		//
		// options.setOptions(opt);

		pageBranchSpecies_speciesList = new ControlSpeciesBox(this, 4, 20, 136, 152);
	}

	ControlText pageBranchSpecies_title;
	ControlSpeciesBox pageBranchSpecies_speciesList;

	@Override
	public void onBranchChanged(IClassification branch) {
		pageBranchSpecies_speciesList.setBranch(branch);
	}

}
