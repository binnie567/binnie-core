package binnie.craftgui.mod.database;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import binnie.craftgui.controls.listbox.ControlListBox;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.minecraft.Window;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IClassification;

public class ControlSpeciesBox extends ControlListBox<IAlleleSpecies> {

	@Override
	public IWidget createOption(IAlleleSpecies value, int y) {
		return new ControlSpeciexBoxOption(getContent(), value, y);
	}

	public ControlSpeciesBox(IWidget parent, float x, float y, float width, float height) {
		super(parent, x, y, width, height, 12);
	}

	IClassification branch = null;

	public void setBranch(IClassification branch) {
		if (branch != this.branch) {
			this.branch = branch;
			List<IAlleleSpecies> speciesList2 = new ArrayList<IAlleleSpecies>();
			movePercentage(-100.0f);
			setOptions(speciesList2);
			
			EntityPlayer player = Window.get(this).getPlayer();
			String playerName = player == null ? "" : player.username;
			
			WindowAbstractDatabase db = (WindowAbstractDatabase) Window.get(this);

			Collection<IAlleleSpecies> speciesList = !db.isNEI ? db.getBreedingSystem()
					.getDiscoveredSpecies(db.getWorld(), playerName) : db.getBreedingSystem()
					.getAllSpecies();
			
			if (branch != null) {
				for (IAlleleSpecies species : branch.getMemberSpecies()) {
					if(speciesList.contains(species))
						speciesList2.add((IAlleleSpecies) species);
				}
			}
			setOptions(speciesList2);
		}

	}

}
