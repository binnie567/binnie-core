package binnie.craftgui.mod.database;

import binnie.craftgui.controls.page.ControlPage;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.Event;
import binnie.craftgui.events.core.EventHandler;
import binnie.craftgui.events.core.EventHandler.Origin;
import forestry.api.genetics.IClassification;

public abstract class PageBranch extends ControlPage<DatabaseTab> {

	@EventHandler
	public void onHandleEvent(EventBranchChanged event) {
		onBranchChanged(event.getBranch());

	}

	public abstract void onBranchChanged(IClassification branch);

	public PageBranch(IWidget parent, DatabaseTab tab) {
		super(parent, parent.getPosition().x(), parent.getPosition().y(), parent.getSize().x(),
				parent.getSize().y(), tab);
	}

}
