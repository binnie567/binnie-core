package binnie.craftgui.mod.database;

import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.ControlTextCentered;
import binnie.craftgui.core.IWidget;
import forestry.api.genetics.IAlleleSpecies;

public class PageSpeciesResultant extends PageSpecies {

	public PageSpeciesResultant(IWidget parent, DatabaseTab tab) {
		super(parent, tab);

		pageSpeciesResultant_Title = new ControlTextCentered(this, 8, "Resultant Mutations");

		pageSpeciesResultant_List = new ControlMutationBox(this, 4, 20, 136,
				152, ControlMutationBox.Type.Resultant);
	}

	ControlText pageSpeciesResultant_Title;
	ControlMutationBox pageSpeciesResultant_List;

	@Override
	public void onSpeciesChanged(IAlleleSpecies species) {
		pageSpeciesResultant_List.setSpecies(species);
	}

}
