package binnie.craftgui.mod.database;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.Event;
import forestry.api.genetics.IClassification;

public class EventBranchChanged extends Event {

	IClassification branch;

	public IClassification getBranch() {
		return branch;
	}

	public EventBranchChanged(IWidget origin, IClassification branch) {
		super(origin);
		this.branch = branch;
	}

}
