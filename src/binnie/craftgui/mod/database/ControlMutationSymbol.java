package binnie.craftgui.mod.database;

import binnie.core.genetics.BreedingSystem;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Tooltip;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.minecraft.Window;
import binnie.craftgui.resource.ITexture;
import binnie.craftgui.resource.minecraft.CraftGUITextureSheet;
import binnie.craftgui.resource.minecraft.StandardTexture;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IMutation;

public class ControlMutationSymbol extends Control implements ITooltip {

	static ITexture MutationPlus = new StandardTexture(2, 94, 16, 16, CraftGUITextureSheet.Controls2.getTexture());
	static ITexture MutationArrow = new StandardTexture(20, 94, 32, 16, CraftGUITextureSheet.Controls2.getTexture());
	
	
	@Override
	public void onRenderBackground() {
		super.onRenderBackground();
		if(type == 0)
			getRenderer().renderTexture(MutationPlus, Vector2f.ZERO);
		else
			getRenderer().renderTexture(MutationArrow, Vector2f.ZERO);
	}

	protected ControlMutationSymbol(IWidget parent, int x, int y, int type) {
		super(parent, x, y, 16 + type * 16, 16);
		this.value = null;
		this.type = type;
		this.canMouseOver = true;
	}

	public void setValue(IMutation value) {
		this.value = value;

		boolean isNEI = ((WindowAbstractDatabase) getSuperParent()).isNEI();
		BreedingSystem system = ((WindowAbstractDatabase) getSuperParent())
				.getBreedingSystem();
		this.discovered = isNEI ? true : system.isMutationDiscovered(value,
				Window.get(this).getWorld(), Window.get(this).getPlayer() == null ? ""
						: Window.get(this).getPlayer().username);

		if (discovered)
			setColour(0xFFFFFF);
		else
			setColour(0x777777);
	}

	IMutation value;
	boolean discovered;
	int type;

	@Override
	public void getTooltip(Tooltip tooltip) {
		if (type == 1 && discovered) {
			IAllele species1 = value.getAllele0();
			IAllele species2 = value.getAllele1();
			
			BreedingSystem system = ((WindowAbstractDatabase) getSuperParent())
					.getBreedingSystem();
			float chance = system.getChance(value, Window.get(this).getPlayer(), species1, species2);
			tooltip.add("Current Chance - " + chance + "%");
			if(value.getSpecialConditions() != null)
				for(String string : value.getSpecialConditions())
					tooltip.add(string);
		}
	}

}
