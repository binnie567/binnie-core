package binnie.craftgui.mod.database;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.Event;
import forestry.api.genetics.IAlleleSpecies;

public class EventSpeciesChanged extends Event {

	IAlleleSpecies species;

	public IAlleleSpecies getSpecies() {
		return species;
	}

	public EventSpeciesChanged(IWidget origin, IAlleleSpecies species) {
		super(origin);
		this.species = species;
	}

}
