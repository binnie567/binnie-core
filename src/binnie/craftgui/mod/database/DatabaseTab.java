package binnie.craftgui.mod.database;

public class DatabaseTab {
	String name;
	int colour;

	public DatabaseTab(String name, int colour) {
		super();
		this.name = name;
		this.colour = colour;
	}

	public String toString() {
		return name;
	}
}
