package binnie.craftgui.mod.database;

import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.ControlTextCentered;
import binnie.craftgui.core.IWidget;
import forestry.api.genetics.IAlleleSpecies;

public class PageSpeciesMutations extends PageSpecies {

	public PageSpeciesMutations(IWidget parent, DatabaseTab tab) {
		super(parent, tab);

		pageSpeciesFurther_Title = new ControlTextCentered(this, 8,"Further Mutations");

		pageSpeciesFurther_List = new ControlMutationBox(this, 4, 20, 136, 152,
				ControlMutationBox.Type.Further);
	}

	ControlText pageSpeciesFurther_Title;
	ControlMutationBox pageSpeciesFurther_List;

	@Override
	public void onSpeciesChanged(IAlleleSpecies species) {
		pageSpeciesFurther_List.setSpecies(species);

	}

}
