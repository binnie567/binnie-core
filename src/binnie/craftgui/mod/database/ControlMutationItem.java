package binnie.craftgui.mod.database;

import binnie.core.genetics.BreedingSystem;
import binnie.craftgui.controls.listbox.ControlList;
import binnie.craftgui.controls.listbox.ControlOption;
import binnie.craftgui.minecraft.Window;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IMutation;

public class ControlMutationItem extends ControlOption<IMutation> {

	ControlIndividualDisplay itemWidget1;
	ControlIndividualDisplay itemWidget2;
	ControlIndividualDisplay itemWidget3;
	ControlMutationSymbol addSymbol;
	ControlMutationSymbol arrowSymbol;

	public ControlMutationItem(ControlList<IMutation> controlList, IMutation option, IAlleleSpecies species, int y) {
		super(controlList, option, y);
		itemWidget1 = new ControlIndividualDisplay(this, 4, 4);
		itemWidget2 = new ControlIndividualDisplay(this, 44, 4);
		itemWidget3 = new ControlIndividualDisplay(this, 104, 4);
		addSymbol = new ControlMutationSymbol(this, 24, 4, 0);
		arrowSymbol = new ControlMutationSymbol(this, 64, 4, 1);

		boolean isNEI = ((WindowAbstractDatabase) getSuperParent()).isNEI();
		BreedingSystem system = ((WindowAbstractDatabase) getSuperParent())
				.getBreedingSystem();
		if (getValue() != null) {
			boolean isMutationDiscovered = system.isMutationDiscovered(getValue(), Window.get(this)
					.getWorld(), Window.get(this).getPlayer() == null ? ""
					: Window.get(this).getPlayer().username);

			IAlleleSpecies allele = null;
			EnumDiscoveryState state = null;
			
			allele = (IAlleleSpecies) getValue().getAllele0();
			state = isNEI || isMutationDiscovered ? EnumDiscoveryState.Show : species == allele ? EnumDiscoveryState.Show : EnumDiscoveryState.Undetermined;
			itemWidget1.setSpecies(allele, state);
			allele = (IAlleleSpecies) getValue().getAllele1();
			state = isNEI || isMutationDiscovered? EnumDiscoveryState.Show : species == allele ? EnumDiscoveryState.Show : EnumDiscoveryState.Undetermined;
			itemWidget2.setSpecies(allele, state);
			allele = (IAlleleSpecies) getValue().getTemplate()[0];
			state = isNEI || isMutationDiscovered ? EnumDiscoveryState.Show : species == allele ? EnumDiscoveryState.Show : EnumDiscoveryState.Undetermined;
			itemWidget3.setSpecies(allele, state);
			addSymbol.setValue(getValue());
			arrowSymbol.setValue(getValue());
		}
	}

}
