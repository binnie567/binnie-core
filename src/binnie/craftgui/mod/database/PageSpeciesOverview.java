package binnie.craftgui.mod.database;

import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.ControlTextCentered;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.geometry.Area;
import binnie.craftgui.core.geometry.TextJustification;
import binnie.craftgui.core.geometry.Vector2f;
import binnie.craftgui.minecraft.Window;
import forestry.api.genetics.IAlleleSpecies;

public class PageSpeciesOverview extends PageSpecies {

	public PageSpeciesOverview(IWidget parent, DatabaseTab tab) {
		super(parent, tab);

		controlInd1 = new ControlIndividualDisplay(this, 5, 5);
		controlInd2 = new ControlIndividualDisplay(this, 123, 5);
		
		controlName = new ControlTextCentered(this, 8, "");

		controlScientific = new ControlTextCentered(this, 32, "");
		controlAuthority = new ControlTextCentered(this, 44, "");
		controlComplexity = new ControlTextCentered(this, 56, "");
		
		controlDescription = new ControlText(this, new Area(8, 84, getSize().x()-16, 0), "", TextJustification.MiddleCenter);

		controlSignature = new ControlText(this, new Area(8, 84, getSize().x()-16, 0), "", TextJustification.BottomRight);

		// controlBranch = new ControlText(this, 72, 156, "",
		// ControlText.Alignment.Center);
	}

	ControlText controlName;
	ControlText controlScientific;
	ControlText controlAuthority;
	ControlText controlComplexity;

	// ControlText controlBranch;

	ControlText controlDescription;
	ControlText controlSignature;
	
	ControlIndividualDisplay controlInd1;
	ControlIndividualDisplay controlInd2;
	
	@Override
	public void onSpeciesChanged(IAlleleSpecies species) {
		
		controlInd1.setSpecies(species, EnumDiscoveryState.Show);
		controlInd2.setSpecies(species, EnumDiscoveryState.Show);
		String branchBinomial = (species.getBranch() != null) ? species
				.getBranch().getScientific() : "<Unknown>";
		String branchName = (species.getBranch() != null) ? species.getBranch()
				.getName() : "Unknown";

		controlName.setValue("\u00a7n" + species.getName() + "\u00a7r");
		controlScientific.setValue("\u00a7o" + branchBinomial + " "
				+ species.getBinomial() + "\u00a7r");
		controlAuthority.setValue("Discovered by \u00a7l"
				+ species.getAuthority() + "\u00a7r");
		controlComplexity.setValue("Complexity: " + species.getComplexity());
		// controlBranch.setText("<< \u00a7n" + branchName +
		// " Branch\u00a7r >>");

		String desc = species.getDescription();

		String descBody = "\u00a7o";
		String descSig = "";

		if (desc == null || desc == "") {
			descBody += "No Description Provided.";
		} else {
			String[] descStrings = desc.split("\\|");
			descBody += descStrings[0];
			for (int i = 1; i < descStrings.length - 1; i++) {
				descBody += " " + descStrings[i];
			}
			if (descStrings.length > 1)
				descSig += descStrings[descStrings.length - 1];
		}

		controlDescription.setValue(descBody + "\u00a7r");
		controlSignature.setValue(descSig + "\u00a7r");

		float descHeight = Window.get(this).getTextHeight(controlDescription.getValue(), controlDescription.getSize().x());
		
		controlSignature.setPosition(new Vector2f(controlSignature.pos().x(),
				controlDescription.getPosition().y() + descHeight + 10));

	}

}