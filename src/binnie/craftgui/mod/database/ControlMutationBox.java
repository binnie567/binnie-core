package binnie.craftgui.mod.database;

import java.util.List;

import binnie.core.genetics.BreedingSystem;
import binnie.craftgui.controls.listbox.ControlListBox;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.minecraft.Window;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IMutation;

public class ControlMutationBox extends ControlListBox<IMutation> {

	@Override
	public IWidget createOption(IMutation value, int y) {
		return new ControlMutationItem(getContent(), value, species, y);
	}

	enum Type {
		Resultant, Further
	}

	private int index;
	private Type type;

	public ControlMutationBox(IWidget parent, int x, int y, int width,
			int height, Type type) {
		super(parent, x, y, width, height, 12);
		this.type = type;
	}

	IAlleleSpecies species = null;

	public void setSpecies(IAlleleSpecies species) {
		if (species != this.species) {
			this.species = species;
			this.index = 0;
			movePercentage(-100.0f);
			
			BreedingSystem system = ((WindowAbstractDatabase)getSuperParent()).getBreedingSystem();
			
			List<IMutation> discovered = system.getDiscoveredMutations(Window.get(this).getWorld(), Window.get(this).getPlayer().username);
			
			if (species != null) {
				if (this.type == Type.Resultant) {
					setOptions(system.getResultantMutations(species));
				} else {
					List<IMutation> mutations = system.getFurtherMutations(species);
					for(int i = 0; i < mutations.size();) {
						IMutation mutation = mutations.get(i);
						if(!discovered.contains(mutations)&&!((IAlleleSpecies)mutation.getTemplate()[0]).isCounted())
							mutations.remove(i);
						else
							i++;
					}
					setOptions(mutations);
				}
			}
		}

	}

}
