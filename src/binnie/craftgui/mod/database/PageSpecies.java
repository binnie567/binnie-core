package binnie.craftgui.mod.database;

import binnie.craftgui.controls.page.ControlPage;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.core.EventHandler;
import forestry.api.genetics.IAlleleSpecies;

public abstract class PageSpecies extends ControlPage<DatabaseTab> {

	public PageSpecies(IWidget parent, DatabaseTab tab) {
		super(parent, parent.getPosition().x(), parent.getPosition().y(), parent.getSize().x(),
				parent.getSize().y(), tab);
	}

	@EventHandler
	public void onHandleEvent(EventSpeciesChanged event) {
		onSpeciesChanged(event.getSpecies());
	}

	public abstract void onSpeciesChanged(IAlleleSpecies species);

}
