package binnie.craftgui.resource.minecraft;

import binnie.craftgui.resource.StyleSheetManager;

public enum CraftGUITexture {
	
	Window,
	PanelGray,
	PanelBlack,
	PanelTinted,
	ButtonDisabled,
	Button,
	ButtonHighlighted,
	Slot,
	SlotBorder,
	SlotOverlay,
	SlotCharge,
	LiquidTank,
	LiquidTankOverlay,
	StateError,
	StateWarning,
	StateNone,
	EnergyBarBack,
	EnergyBarGlow,
	EnergyBarGlass,
	TabDisabled,
	Tab, 
	TabHighlighted,
	ScrollDisabled,
	Scroll,
	ScrollHighlighted,
	Outline, 
	HelpButton,
	InfoButton,
	;
	
	static {
		StyleSheetManager.defaultTextures.put(CraftGUITexture.Window, new PaddedTexture(6, 6, 64, 64, 4,  CraftGUITextureSheet.Panel2, 8, 8, 8, 8));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.PanelGray, new PaddedTexture(78, 4, 32, 32, 2, CraftGUITextureSheet.Panel2, 2, 2, 2, 2));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.PanelBlack, new PaddedTexture(116, 4, 32, 32, 2, CraftGUITextureSheet.Panel2, 2, 2, 2, 2));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.PanelTinted, new PaddedTexture(154, 4, 32, 32, 2, CraftGUITextureSheet.Panel2, 2, 2, 2, 2));

		StyleSheetManager.defaultTextures.put(CraftGUITexture.ButtonDisabled, new PaddedTexture(4, 4, 20, 20, 2, CraftGUITextureSheet.Controls2, 4, 4, 4, 4));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.Button, new PaddedTexture(30, 4, 20, 20, 2, CraftGUITextureSheet.Controls2, 4, 4, 4, 4));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.ButtonHighlighted, new PaddedTexture(56, 4, 20, 20, 2, CraftGUITextureSheet.Controls2, 4, 4, 4, 4));
		
		StyleSheetManager.defaultTextures.put(CraftGUITexture.Slot, new PaddedTexture(4, 4, 18, 18, 2, CraftGUITextureSheet.Slots, 2, 2, 2, 2));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.SlotBorder, new PaddedTexture(28, 4, 18, 18, 2, CraftGUITextureSheet.Slots, 2, 2, 2, 2));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.SlotOverlay, new PaddedTexture(52, 4, 18, 18, 2, CraftGUITextureSheet.Slots, 2, 2, 2, 2));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.SlotCharge, new StandardTexture(54, 60, 2, 16, CraftGUITextureSheet.Slots));
		
		StyleSheetManager.defaultTextures.put(CraftGUITexture.LiquidTank, new StandardTexture(8, 28, 18, 60, CraftGUITextureSheet.Slots));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.LiquidTankOverlay, new StandardTexture(32, 28, 18, 60, CraftGUITextureSheet.Slots));
		
		StyleSheetManager.defaultTextures.put(CraftGUITexture.StateError, new StandardTexture(4, 30, 14, 14, 2, CraftGUITextureSheet.Controls2));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.StateWarning, new StandardTexture(24, 30, 14, 14, 2, CraftGUITextureSheet.Controls2));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.StateNone, new StandardTexture(44, 30, 14, 14, 2, CraftGUITextureSheet.Controls2));
		
		StyleSheetManager.defaultTextures.put(CraftGUITexture.EnergyBarBack, new PaddedTexture(40, 50, 12, 14, 2, CraftGUITextureSheet.Controls2, 1, 1, 1, 1));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.EnergyBarGlow, new PaddedTexture(4, 50, 12, 14, 2, CraftGUITextureSheet.Controls2, 1, 1, 1, 1));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.EnergyBarGlass, new PaddedTexture(22, 50, 12, 14, 2, CraftGUITextureSheet.Controls2, 1, 1, 1, 1));
		
		StyleSheetManager.defaultTextures.put(CraftGUITexture.TabDisabled, new PaddedTexture(4, 70, 20, 20, 2, CraftGUITextureSheet.Controls2, 4, 4, 4, 4));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.Tab, new PaddedTexture(30, 70, 20, 20, 2, CraftGUITextureSheet.Controls2, 4, 4, 4, 4));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.TabHighlighted, new PaddedTexture(56, 70, 20, 20, 2, CraftGUITextureSheet.Controls2, 4, 4, 4, 4));
		
		StyleSheetManager.defaultTextures.put(CraftGUITexture.ScrollDisabled, new PaddedTexture(100, 30, 12, 14, 2, CraftGUITextureSheet.Controls2, 2, 2, 2, 2));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.Scroll, new PaddedTexture(64, 30, 12, 14, 2, CraftGUITextureSheet.Controls2, 2, 2, 2, 2));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.ScrollHighlighted, new PaddedTexture(82, 30, 12, 14, 2, CraftGUITextureSheet.Controls2, 2, 2, 2, 2));

		StyleSheetManager.defaultTextures.put(CraftGUITexture.Outline, new PaddedTexture(76, 40, 16, 16, 0, CraftGUITextureSheet.Panel2, 2, 2, 2, 2));


		StyleSheetManager.defaultTextures.put(CraftGUITexture.HelpButton, new PaddedTexture(88, 4, 16, 16, 2, CraftGUITextureSheet.Controls2, 2, 2, 2, 2));
		StyleSheetManager.defaultTextures.put(CraftGUITexture.InfoButton, new PaddedTexture(110, 4, 16, 16, 2, CraftGUITextureSheet.Controls2, 2, 2, 2, 2));
		
	}
	
	
}
