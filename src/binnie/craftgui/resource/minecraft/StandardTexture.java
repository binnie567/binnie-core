package binnie.craftgui.resource.minecraft;

import binnie.core.resource.BinnieResource;
import binnie.core.resource.IBinnieTexture;
import binnie.craftgui.core.geometry.Direction;
import binnie.craftgui.resource.ITexture;


public class StandardTexture implements ITexture {

	int u;
	int v;
	int w;
	int h;
	
	public StandardTexture(int u, int v, int w, int h, IBinnieTexture textureFile) {
		this(u, v, w, h, 0, textureFile.getTexture());
	}
	
	public StandardTexture(int u, int v, int w, int h, int padding, IBinnieTexture textureFile) {
		this(u, v, w, h, padding, textureFile.getTexture());
	}
	
	public StandardTexture(int u, int v, int w, int h, BinnieResource textureFile) {
		this(u, v, w, h, 0, textureFile);
	}

	public StandardTexture(int u, int v, int w, int h, int padding, BinnieResource textureFile) {
		this.u = u;
		this.v = v;
		this.w = w;
		this.h = h;
		this.textureFile = textureFile;
	}

	BinnieResource textureFile;
	
	@Override
	public int u() {
		return u;
	}

	@Override
	public int v() {
		return v;
	}

	@Override
	public int w() {
		return w;
	}

	@Override
	public int h() {
		return h;
	}

	public BinnieResource getTexture() {
		return textureFile;
	}

	@Override
	public ITexture subTexture(Direction direction, int distance) {
		switch (direction) {
		case Downwards:
			return new StandardTexture(u(), v(), w,  distance, 0, textureFile);
		case Left:
			return new StandardTexture(u(), v(), distance, h, 0, textureFile);
		case Right:
			return new StandardTexture(u()+w-distance, v(), distance, h, 0, textureFile);
		case Upwards:
			return new StandardTexture(u(), v()+h-distance, w(), distance, 0, textureFile);
		default:
			return this;
		}
	}

}
