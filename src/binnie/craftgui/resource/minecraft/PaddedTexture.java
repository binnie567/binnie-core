package binnie.craftgui.resource.minecraft;

import binnie.core.resource.IBinnieTexture;
import binnie.craftgui.resource.ITexturePadded;


public class PaddedTexture extends StandardTexture implements ITexturePadded {

	int leftPadding;
	int rightPadding;
	int topPadding;
	int bottomPadding;
	
	public PaddedTexture(int u, int v, int w, int h, int offset, IBinnieTexture textureFile, int leftPadding,
			int rightPadding, int topPadding, int bottomPadding) {
		super(u, v, w, h, offset, textureFile);
		this.leftPadding = leftPadding;
		this.rightPadding = rightPadding;
		this.topPadding = topPadding;
		this.bottomPadding = bottomPadding;
	}

	public int l() {
		return this.leftPadding;
	}

	public int r() {
		return this.rightPadding;
	}

	public int t() {
		return this.topPadding;
	}

	public int b() {
		return this.bottomPadding;
	}
	
	

}
