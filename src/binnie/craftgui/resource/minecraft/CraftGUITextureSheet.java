package binnie.craftgui.resource.minecraft;

import binnie.core.BinnieCore;
import binnie.core.resource.BinnieResource;
import binnie.core.resource.IBinnieTexture;
import binnie.core.resource.ResourceManager;
import binnie.core.resource.ResourceType;


public enum CraftGUITextureSheet implements IBinnieTexture {
	
	Controls2("craftgui-controls"),
	Panel2("craftgui-panels"),
	Slots("craftgui-slots"),
	
	;
	
	String texture;
	
	private CraftGUITextureSheet(String name) {
		texture = name;
	}

	@Override
	public BinnieResource getTexture() {
		return ResourceManager.getPNG(BinnieCore.instance, ResourceType.GUI, texture);
	}
	
}
