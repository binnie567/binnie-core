package binnie.core;

public class BinnieException extends RuntimeException {

	public BinnieException(String error) {
		super(error);
	}
	
}
