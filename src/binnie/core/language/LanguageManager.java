package binnie.core.language;

import binnie.core.IBinnieMod;

public class LanguageManager {
	
	public LocalisedString register(IBinnieMod mod, String id, String english) {
		return new LocalisedString(english, id, mod);
	}

	public String getString(LocalisedString localisedString) {
		return localisedString.english;
	}
	
}
