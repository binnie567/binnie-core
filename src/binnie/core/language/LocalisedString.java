package binnie.core.language;

import binnie.core.Binnie;
import binnie.core.IBinnieMod;

public class LocalisedString {
	
	String english;
	String id;
	IBinnieMod mod;
	
	public LocalisedString(String english, String id, IBinnieMod mod) {
		super();
		this.english = english;
		this.id = id;
		this.mod = mod;
	}

	public String toString() {
		return Binnie.Language.getString(this);
	}

}
