package binnie.core.item;

import net.minecraft.creativetab.CreativeTabs;

public class ItemManager {
	
	public static ItemMisc registerMiscItems(IItemMisc[] items, int id, CreativeTabs tab) {
		return new ItemMisc(id, tab, items);
	}

}
