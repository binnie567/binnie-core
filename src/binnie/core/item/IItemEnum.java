package binnie.core.item;

import net.minecraft.item.ItemStack;

public interface IItemEnum {
	
	boolean isActive();
	
	public String getName(ItemStack stack);

	int ordinal();

	ItemStack get(int i);

}
