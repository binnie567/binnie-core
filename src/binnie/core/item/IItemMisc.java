package binnie.core.item;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public interface IItemMisc extends IItemEnum {
	
	Icon getIcon(ItemStack stack);
	
	void registerIcons(IconRegister register);

	void addInformation(List par3List);
	
}
