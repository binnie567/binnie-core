package binnie.core.item;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemMisc extends Item {
	
	

	public ItemMisc(int id, CreativeTabs tab, IItemMisc[] items2) {
		super(id);
		setCreativeTab(tab);
		setHasSubtypes(true);
		items = items2;
	}
	
	IItemMisc[] items;
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List par3List) {
		for(IItemMisc item : items)
			if(item.isActive())
				par3List.add(getStack(item, 1));
	}

	private IItemMisc getItem(int damage) {
		return damage >= items.length ? items[0] : items[damage];
	}
	
	public ItemStack getStack(IItemMisc type, int size) {
		return new ItemStack(itemID, size, type.ordinal());
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack par1ItemStack,
			EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
		super.addInformation(par1ItemStack, par2EntityPlayer, par3List, par4);
		IItemMisc item = getItem(par1ItemStack.getItemDamage());
		if(item != null) item.addInformation(par3List);
		
	}

	@Override
	public String getItemDisplayName(ItemStack stack) {
		IItemMisc item = getItem(stack.getItemDamage());
		return item != null ? item.getName(stack) : "null";
	}

	@Override
	public Icon getIcon(ItemStack stack, int pass) {
		IItemMisc item = getItem(stack.getItemDamage());
		return item != null ? item.getIcon(stack) : null;
	}
	
	@Override
	public Icon getIconFromDamage(int damage) {
		IItemMisc item = getItem(damage);
		return item != null ? item.getIcon(null) : null;
	}
	

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		for(IItemMisc item : this.items)
			if(item.isActive())
				item.registerIcons(register);
	}

}
