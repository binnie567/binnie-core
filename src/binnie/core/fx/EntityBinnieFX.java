//package binnie.core.fx;
//
//import net.minecraft.client.particle.EntityFX;
//import net.minecraft.client.renderer.Tessellator;
//import net.minecraft.world.World;
//
//import org.lwjgl.util.vector.Vector3f;
//
//import binnie.core.BinnieCore;
//import cpw.mods.fml.relauncher.Side;
//import cpw.mods.fml.relauncher.SideOnly;
//
//@SideOnly(Side.CLIENT)
//public abstract class EntityBinnieFX extends EntityFX {
//
//	public EntityBinnieFX(World world, Vector3f position, Vector3f motion) {
//		super(world, position.x, position.y, position.z, motion.x, motion.y,
//				motion.z);
//		this.motionX = motion.x;
//		this.motionY = motion.y;
//		this.motionZ = motion.z;
//	}
//
//	public static void createFX(EntityBinnieFX fx) {
//		BinnieCore.proxy.getMinecraftInstance().effectRenderer.addEffect(fx);
//	}
//	
//	public abstract String getTexture();
//
//	@Override
//	public void renderParticle(Tessellator par1Tessellator, float par2,
//			float par3, float par4, float par5, float par6, float par7) {
//		par1Tessellator.draw();
//		BinnieCore.proxy.bindTexture(getTexture());
//		par1Tessellator.startDrawingQuads();
//		super.renderParticle(par1Tessellator, par2, par3, par4, par5, par6,
//				par7);
//		par1Tessellator.draw();
//		BinnieCore.proxy.bindTexture("/particles.png");
//		par1Tessellator.startDrawingQuads();
//	}
//
//	public void setColor(int color) {
//		particleRed = (color >> 16 & 255) / 255.0F;
//		particleGreen = (color >> 8 & 255) / 255.0F;
//		particleBlue = (color & 255) / 255.0F;
//	}
//
//}
