package binnie.core.machines;

import net.minecraft.tileentity.TileEntity;

// Contains ComponentFactories to create the components
// Referenced in each machine
// Contains map from interface/class to component
public abstract class MachinePackage {
	
	String name;
	
	String uid;
	
	boolean active = true;
	
	int metadata = -1;

	public String getUID() {
		return uid;
	}
	
	protected MachinePackage(String uid, String name) {
		super();
		this.name = name;
		this.uid = uid;
	}

	public abstract void createMachine(Machine machine);
	
	public abstract TileEntity createTileEntity();

	// This is where you must register the TE
	public abstract void register();
	
	public final String getDisplayName() {
		return name;
	}

	public final Integer getMetadata() {
		return metadata;
	}
	
	public void assignMetadata(int meta) {
		this.metadata = meta;
	}
	
	MachineGroup group;

	public MachineGroup getGroup() {
		return group;
	}
	
	public void setGroup(MachineGroup group) {
		this.group = group;
	}

	public abstract void renderMachine(Machine machine, double x, double y, double z,
			float var8);

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
