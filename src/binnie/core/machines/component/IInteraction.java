package binnie.core.machines.component;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public interface IInteraction {
	
	public interface RightClick {
		void onRightClick(World world, EntityPlayer player, int x, int y, int z);
	}
	
	

}
