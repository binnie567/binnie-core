package binnie.core.machines.component;

import net.minecraft.item.ItemStack;

public interface IComponentRecipe {
	
	public boolean isRecipe();
	
	public ItemStack doRecipe(boolean takeItem);
	
	public ItemStack getProduct();

}
