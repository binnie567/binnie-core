package binnie.core.machines.component;

import java.util.List;

import binnie.core.triggers.TriggerData;
import buildcraft.api.gates.IAction;
import buildcraft.api.gates.IActionReceptor;
import buildcraft.api.gates.ITrigger;

public interface IBuildcraft {
	
	public interface ActionProvider extends IActionReceptor {
		void getActions(List<IAction> actions);
	}
	
	public interface TriggerProvider {
		void getTriggers(List<TriggerData> triggers);
	}

}
