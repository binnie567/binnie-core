package binnie.core.machines.inventory;

import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import binnie.core.BinnieCore;
import forestry.api.genetics.AlleleManager;

public abstract class SlotValidator extends Validator<ItemStack> {

	public static ValidatorIcon IconBee;
	public static ValidatorIcon IconFrame;
	public static ValidatorIcon IconCircuit;
	public static ValidatorIcon IconBlock;

	public SlotValidator(ValidatorIcon icon) {
		this.icon = icon;
	}

	ValidatorIcon icon;

	public Icon getIcon(boolean input) {
		return icon.getIcon(input).getIcon();
	}

	public static class Item extends SlotValidator {

		ItemStack target;

		public Item(ItemStack target, ValidatorIcon icon) {
			super(icon);
			this.target = target;

		}

		@Override
		public boolean isValid(ItemStack itemStack) {
			return itemStack.isItemEqual(target);
		}

		@Override
		public String getTooltip() {
			return target.getDisplayName();
		}

	}

	public static class Individual extends SlotValidator {

		public Individual(ValidatorIcon icon) {
			super(icon);
		}

		@Override
		public boolean isValid(ItemStack itemStack) {
			return AlleleManager.alleleRegistry.getIndividual(itemStack) != null;
		}

		@Override
		public String getTooltip() {
			return "Breedable Individual";
		}

	}

}
