package binnie.core.machines.inventory;

import net.minecraftforge.liquids.LiquidStack;
import binnie.core.liquid.LiquidManager;

public abstract class TankValidator extends Validator<LiquidStack> {

	public static class Basic extends TankValidator {

		LiquidStack liquid;
		
		public Basic(int id, int meta) {
			liquid = new LiquidStack(id, 1, meta);
		}
		
		public Basic(String id) {
			LiquidStack liquid = LiquidManager.getLiquidStack(id, 1);
			if(liquid != null) {
				this.liquid = liquid;
			}
			else {
				throw new RuntimeException("Tank Validator is using a non existant liquid " + id);
			}

		}			
		
		@Override
		public boolean isValid(LiquidStack stack) {
			return liquid.isLiquidEqual(stack);
		}

		@Override
		public String getTooltip() {
			return liquid.asItemStack().getDisplayName();
		}
		
	}

}
