package binnie.core.machines.inventory;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import binnie.core.machines.IMachine;
import binnie.core.machines.MachineComponent;
import binnie.core.machines.transfer.TransferRequest;

/**
 * Used for inventories where things like buffers are used
 * 
 */
public class ComponentInventoryTransfer extends MachineComponent {

	public ComponentInventoryTransfer(IMachine machine) {
		super(machine);
	}

	private interface Transfer {
		void doTransfer(IInventory inv);
	}

	private class Restock implements Transfer {
		int[] buffer;
		int destination;
		int limit;

		private Restock(int[] buffer, int destination, int limit) {
			super();
			this.buffer = buffer;
			this.destination = destination;
			this.limit = limit;
		}

		private Restock(int[] buffer, int destination) {
			this(buffer, destination, 64);
		}

		@Override
		public void doTransfer(IInventory inv) {
			if (inv.getStackInSlot(destination) == null) {
				for (int i : buffer) {
					if (inv.getStackInSlot(i) != null) {
						ItemStack newStack = inv.decrStackSize(i, limit);
						if (newStack == null)
							continue;
						inv.setInventorySlotContents(destination, newStack);
						return;
					}
				}
			}
		}
	}

	private class Storage implements Transfer {
		int source;
		int[] destination;

		private Storage(int source, int[] destination) {
			super();
			this.source = source;
			this.destination = destination;
		}

		@Override
		public void doTransfer(IInventory inv) {
			if (inv.getStackInSlot(source) != null) {
				inv.setInventorySlotContents(
						source,
						(new TransferRequest(inv.getStackInSlot(source), inv))
								.setTargetSlots(destination).ignoreValidation().transfer(true));
			}
		}
	}

	List<Transfer> transfers = new ArrayList<Transfer>();

	public void addRestock(int[] buffer, int destination, int limit) {
		transfers.add(new Restock(buffer, destination, limit));
	}

	public void addRestock(int[] buffer, int destination) {
		transfers.add(new Restock(buffer, destination));
	}

	public void addStorage(int source, int[] destination) {
		transfers.add(new Storage(source, destination));
	}

	public void performTransfer(int source, int destination[]) {
		(new Storage(source, destination)).doTransfer(getMachine().getInterface(
				IInventoryMachine.class));
	}

	@Override
	public void onUpdate() {
		for (Transfer transfer : transfers)
			transfer.doTransfer(getMachine().getInterface(IInventoryMachine.class));
	}

}
