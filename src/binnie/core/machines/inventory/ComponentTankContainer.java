package binnie.core.machines.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.ILiquidTank;
import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidStack;
import net.minecraftforge.liquids.LiquidTank;
import binnie.core.machines.IMachine;
import binnie.core.machines.Machine;
import binnie.core.machines.MachineComponent;
import binnie.core.machines.network.INetwork;
import binnie.core.machines.power.ITankMachine;
import binnie.core.machines.power.TankInfo;

public class ComponentTankContainer extends MachineComponent implements
		ITankMachine {

	Map<Integer, TankSlot> tanks = new LinkedHashMap<Integer, TankSlot>();

	public ComponentTankContainer(IMachine machine) {
		super(machine);
	}
	
	public final void addTank(int index, String name, int capacity) {
		tanks.put(index, new TankSlot(index, name, capacity));
	}

	@Override
	public final int fill(ForgeDirection from, LiquidStack resource,
			boolean doFill) {
		int index = getTankIndexToFill(from, resource);
		if (tanks.containsKey(index)) {
			return fill(index, resource, doFill);
		} else
			return 0;
	}

	@Override
	public final LiquidStack drain(ForgeDirection from, int maxDrain,
			boolean doDrain) {
		int index = getTankIndexToDrain(from);
		if (tanks.containsKey(index)) {
			return drain(index, maxDrain, doDrain);
		} else
			return null;
	}

	@Override
	public final int fill(int tankIndex, LiquidStack resource, boolean doFill) {
		if (!this.tanks.containsKey(tankIndex))
			return 0;
		if(!this.isLiquidValidForTank(resource, tankIndex))
			return 0;
		TankSlot tank = tanks.get(tankIndex);
		return tank.getTank().fill(resource, doFill);
	}

	@Override
	public final LiquidStack drain(int tankIndex, int maxDrain, boolean doDrain) {
		if (!this.tanks.containsKey(tankIndex))
			return null;
		TankSlot tank = tanks.get(tankIndex);
		return tank.getTank().drain(maxDrain, doDrain);
	}

	@Override
	public final ILiquidTank[] getTanks(ForgeDirection direction) {
		return getTanks();
	}

	@Override
	public ILiquidTank getTank(ForgeDirection direction, LiquidStack type) {
		return getTanks(direction).length > 0 ? getTanks(direction)[0] : null;
	}

	// Custom Liquid

	public int getTankIndexToFill(ForgeDirection from, LiquidStack resource) {
		return 0;
	}

	public int getTankIndexToDrain(ForgeDirection from) {
		return 0;
	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		super.readFromNBT(nbttagcompound);
		if (nbttagcompound.hasKey("liquidTanks")) {
			NBTTagList tanksNBT = nbttagcompound.getTagList("liquidTanks");
			for (int i = 0; i < tanksNBT.tagCount(); i++) {
				NBTTagCompound tankNBT = (NBTTagCompound) tanksNBT.tagAt(i);
				int index = tankNBT.getInteger("index");
				if (tanks.containsKey(index)) {
					tanks.get(index).readFromNBT(tankNBT);
				}
			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {
		super.writeToNBT(nbttagcompound);

		NBTTagList tanksNBT = new NBTTagList();

		for (Map.Entry<Integer, TankSlot> entry : tanks.entrySet()) {
			NBTTagCompound tankNBT = new NBTTagCompound();
			tankNBT.setInteger("index", entry.getKey());
			entry.getValue().writeToNBT(tankNBT);
			tanksNBT.appendTag(tankNBT);

		}

		nbttagcompound.setTag("liquidTanks", tanksNBT);
	}

	@Override
	public boolean isTankReadOnly(int tank) {
		return tanks.get(tank).isReadOnly();
	}

	@Override
	public boolean isLiquidValidForTank(LiquidStack liquid, int tank) {
		return tanks.get(tank).isValid(liquid);
	}
	
	@Override
	public TankInfo[] getTankInfos() {
		return TankInfo.get(this);
	}
	
	@Override
	public ILiquidTank getTank(int index) {
		return getTanks()[index];
	}

	@Override
	public ILiquidTank[] getTanks() {
		List<ILiquidTank> ltanks = new ArrayList<ILiquidTank>();
			for(TankSlot tank : tanks.values()) ltanks.add(tank.getTank());
		return ltanks.toArray(new ILiquidTank[0]);
	}

	@Override
	public TankSlot getTankSlot(int index) {
		return tanks.get(index);
	};
	

}
