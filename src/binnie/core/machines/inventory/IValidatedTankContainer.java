package binnie.core.machines.inventory;

import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidStack;

public interface IValidatedTankContainer extends ITankContainer {
	
	public boolean isTankReadOnly(int tank);
	public boolean isLiquidValidForTank(LiquidStack liquid, int tank);

}
