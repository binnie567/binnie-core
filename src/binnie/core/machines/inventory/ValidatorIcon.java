package binnie.core.machines.inventory;

import java.util.ArrayList;
import java.util.List;

import binnie.core.IBinnieMod;
import binnie.core.resource.BinnieIcon;
import binnie.core.resource.ResourceManager;

public class ValidatorIcon {
	
	List<BinnieIcon> iconsInput = new ArrayList<BinnieIcon>();
	List<BinnieIcon> iconsOutput = new ArrayList<BinnieIcon>();
	
	public ValidatorIcon(IBinnieMod mod, String pathInput, String pathOutput) {
		iconsInput.add(ResourceManager.getItemIcon(mod, pathInput));
		iconsOutput.add(ResourceManager.getItemIcon(mod, pathOutput));
	}

	public BinnieIcon getIcon(boolean input) {
		return input ? iconsInput.get(0) : iconsOutput.get(0);
	}

}
