package binnie.core.machines.inventory;

import net.minecraft.inventory.IInventory;

public interface IValidatedInventory extends IInventory {
	
	public boolean isReadOnly(int slot);

}
