package binnie.core.machines.inventory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.ForgeDirection;
import binnie.core.machines.IMachine;
import binnie.core.machines.Machine;
import binnie.core.machines.transfer.TransferHandler;
import binnie.core.machines.transfer.TransferRequest;
import buildcraft.api.inventory.ISpecialInventory;

public class ComponentInventorySlots extends ComponentInventory implements
		IInventoryMachine {

	public ComponentInventorySlots(IMachine machine) {
		super(machine);
	}

	Map<Integer, InventorySlot> inventory = new LinkedHashMap<Integer, InventorySlot>();

	@Override
	public int getSizeInventory() {
		int size = 0;
		for (Integer index : inventory.keySet())
			size = Math.max(size, index+1);
		return size;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		if (inventory.containsKey(index)) {	
			return inventory.get(index).getContent();
		}
		return null;
	}

	@Override
	public ItemStack decrStackSize(int index, int amount) {
		if (inventory.containsKey(index)) {
			ItemStack stack = inventory.get(index).decrStackSize(amount);
			onInventoryChanged();
			return stack;
		}
		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int var1) {
		return null;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack itemStack) {
		if (inventory.containsKey(index)&&(itemStack==null||inventory.get(index).isValid(itemStack)))
			inventory.get(index).setContent(itemStack);
		onInventoryChanged();
	}

	protected void transferItem(int indexFrom, int indexTo) {
		if (inventory.containsKey(indexFrom) && inventory.containsKey(indexTo)) {
			ItemStack newStack = inventory.get(indexFrom).getContent().copy();
			inventory.get(indexFrom).setContent(null);
			inventory.get(indexTo).setContent(newStack);
		}
		onInventoryChanged();
	}

	@Override
	public String getInvName() {
		return "";
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer var1) {
		return true;
	}

	@Override
	public void openChest() {

	}

	@Override
	public void closeChest() {

	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		super.readFromNBT(nbttagcompound);

		if (nbttagcompound.hasKey("inventory")) {
			NBTTagList inventoryNBT = nbttagcompound.getTagList("inventory");
			for (int i = 0; i < inventoryNBT.tagCount(); i++) {
				NBTTagCompound slotNBT = (NBTTagCompound) inventoryNBT.tagAt(i);
				int index = slotNBT.getInteger("id");
				if (slotNBT.hasKey("Slot"))
					index = slotNBT.getByte("Slot") & 0xff;
				if (inventory.containsKey(index)) {
					inventory.get(index).readFromNBT(slotNBT);
				}
			}
		}
		
		onInventoryChanged();
	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {
		super.writeToNBT(nbttagcompound);

		NBTTagList inventoryNBT = new NBTTagList();

		for (Map.Entry<Integer, InventorySlot> entry : inventory.entrySet()) {
			NBTTagCompound slotNBT = new NBTTagCompound();
			slotNBT.setInteger("id", entry.getKey());
			entry.getValue().writeToNBT(slotNBT);
			inventoryNBT.appendTag(slotNBT);
		}

		nbttagcompound.setTag("inventory", inventoryNBT);
	}

	@Override
	public final void addSlot(int index, String name) {
		inventory.put(index, new InventorySlot(index, name));
	}

	@Override
	public final void addSlotArray(int[] indexes, String name) {
		for (int k : indexes) {
			addSlot(k, name);
		}
	}

	@Override
	public InventorySlot getSlot(int index) {
		if (inventory.containsKey(index))
			return inventory.get(index);
		return null;
	}

	@Override
	public InventorySlot[] getAllSlots() {
		return inventory.values().toArray(new InventorySlot[0]);
	}

	@Override
	public InventorySlot[] getSlots(int[] indexes) {
		List<InventorySlot> list = new ArrayList<InventorySlot>();
		for (int i : indexes) {
			if (getSlot(i) != null)
				list.add(getSlot(i));
		}
		return list.toArray(new InventorySlot[0]);
	}


	@Override
	public boolean isReadOnly(int slot) {
		InventorySlot iSlot = getSlot(slot);
		return iSlot==null ? true : iSlot.isReadOnly();
	}

	@Override
	public boolean isInvNameLocalized() {
		return true;
	}

	@Override
	public boolean isStackValidForSlot(int slot, ItemStack itemStack) {
		InventorySlot iSlot = getSlot(slot);
		return iSlot==null ? false : (iSlot.isValid(itemStack) && !isReadOnly(slot));
	}

	@Override
	public int addItem(ItemStack stack, boolean doAdd, ForgeDirection from) {
		List<Integer> acceptingSlots = new ArrayList<Integer>();
		for(Map.Entry<Integer, InventorySlot> entry : this.inventory.entrySet()) { 
			if(canInsertItem(entry.getKey(), null, from.ordinal()))
				acceptingSlots.add(entry.getKey());
		}
		int[] array = new int[acceptingSlots.size()];
		for(int i = 0; i < acceptingSlots.size(); i++)
			array[i] = acceptingSlots.get(i);
		ItemStack result = (new TransferRequest(stack, this)).setTargetSlots(array).transfer(doAdd);
		return result == null ? stack.stackSize : stack.stackSize - result.stackSize;
	}

	@Override
	public ItemStack[] extractItem(boolean doRemove, ForgeDirection from,
			int maxItemCount) {
		List<ItemStack> output = new ArrayList<ItemStack>();
		for(Map.Entry<Integer, InventorySlot> entry : this.inventory.entrySet()) {
			if(canExtractItem(entry.getKey(), null, from.ordinal()) && entry.getValue().getContent() != null) {
				ItemStack fetched = null;
				if(doRemove) {
					fetched = decrStackSize(entry.getKey(), maxItemCount);
				}
				else {
					fetched = entry.getValue().getContent();
					if(fetched != null) {
						fetched.stackSize = Math.min(maxItemCount, fetched.stackSize);
						if(fetched.stackSize == 0)
							fetched = null;
					}
				}
				
				if (fetched != null) {
					output.add(fetched);
					break;
				}
			}
		}
		return output.toArray(new ItemStack[0]);
	}
	
	@Override
	public void onDestruction() {
		for(InventorySlot slot : inventory.values()) {
			ItemStack stack = slot.getContent();
			if (stack != null) {

				float f = getMachine().getWorld().rand.nextFloat() * 0.8F + 0.1F;
				float f1 = getMachine().getWorld().rand.nextFloat() * 0.8F + 0.1F;
				float f2 = getMachine().getWorld().rand.nextFloat() * 0.8F + 0.1F;

				if (stack.stackSize == 0)
					stack.stackSize = 1;

				EntityItem entityitem = new EntityItem(getMachine().getWorld(), 
						getMachine().getTileEntity().xCoord + f, 
						getMachine().getTileEntity().yCoord + f1, 
						getMachine().getTileEntity().zCoord + f2, 
						stack.copy());
				float accel = 0.05F;
				entityitem.motionX = (float) getMachine().getWorld().rand.nextGaussian()
						* accel;
				entityitem.motionY = (float) getMachine().getWorld().rand.nextGaussian()
						* accel + 0.2F;
				entityitem.motionZ = (float) getMachine().getWorld().rand.nextGaussian()
						* accel;
				getMachine().getWorld().spawnEntityInWorld(entityitem);

			}
		}
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int var1) {
		List<Integer> slots = new ArrayList<Integer>();
		for(InventorySlot slot : inventory.values())
			if(slot.canInsert() || slot.canExtract())
				slots.add(slot.getIndex());
		int[] ids = new int[slots.size()];
		for(int i = 0; i < slots.size(); i++)
			ids[i] = slots.get(i);
		return ids;
	}

	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j) {
		return this.isStackValidForSlot(i, itemstack) && getSlot(i).canInsert(ForgeDirection.getOrientation(j));
	}

	@Override
	public boolean canExtractItem(int i, ItemStack itemstack, int j) {
		return getSlot(i).canExtract(ForgeDirection.getOrientation(j));
	}

}
