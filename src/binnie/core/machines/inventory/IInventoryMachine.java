package binnie.core.machines.inventory;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import buildcraft.api.inventory.ISpecialInventory;

public interface IInventoryMachine extends IInventory, ISpecialInventory, ISidedInventory,
		IInventorySlots, IValidatedInventory {

}
