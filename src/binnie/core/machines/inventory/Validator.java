package binnie.core.machines.inventory;

import net.minecraft.item.ItemStack;
import forestry.api.genetics.AlleleManager;

public abstract class Validator<T> {
	
	public abstract boolean isValid(T itemStack);
	
	public abstract String getTooltip();

}
