package binnie.core.machines.inventory;

import net.minecraft.inventory.IInventory;
import binnie.core.machines.IMachine;
import binnie.core.machines.Machine;
import binnie.core.machines.MachineComponent;

public abstract class ComponentInventory extends MachineComponent implements
		IInventory {

	public ComponentInventory(IMachine machine) {
		super(machine);
	}

	@Override
	public final void onInventoryChanged() {
		if(getMachine() != null)
			getMachine().onInventoryUpdate();
	}

}
