package binnie.core.machines.inventory;

public interface IInventorySlots {

	void addSlot(int index, String name);

	void addSlotArray(int[] indexes, String name);

	InventorySlot getSlot(int index);

	InventorySlot[] getSlots(int[] indexes);

	InventorySlot[] getAllSlots();

}
