package binnie.core.machines.inventory;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.nbt.NBTTagCompound;
import binnie.core.machines.Machine;
import binnie.core.machines.MachineComponent;
import binnie.core.machines.network.INetwork;

/*
 * For things like forestry fermenter, where one item can last a long time
 */
public class ComponentChargedSlots extends MachineComponent implements INetwork.GUI, IChargedSlots {
	
	public ComponentChargedSlots(Machine machine) {
		super(machine);
	}
	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		super.readFromNBT(nbttagcompound);
	}
	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {
		super.writeToNBT(nbttagcompound);
	}
	
	Map<Integer, Float> charges = new HashMap<Integer, Float>();
	Map<Integer, Integer> gui = new HashMap<Integer, Integer>();
	
	public void addCharge(int slot) {
		charges.put(slot, 0f);
		gui.put(slot, getUniqueProgressBarID());
	}
	
	@Override
	public void addGUINetworkData(Map<Integer, Integer> data) {
		for(int i : charges.keySet()) {
			data.put(gui.get(i), (int)(charges.get(i)*100));
		}
	}
	@Override
	public void recieveGUINetworkData(int id, int data) {
		for(int i : charges.keySet()) {
			if(id == gui.get(i))
				charges.put(i, (float)(data)/100f);
		}
	}
	@Override
	public float getCharge(int slot) {
		return charges.containsKey(slot) ? charges.get(slot) : 0;
	}
	@Override
	public void setCharge(int slot, float charge) {
		if(charge > 1f) charge = 1f;
		if(charge < 0f) charge = 0f;
		if(charges.containsKey(slot))
			charges.put(slot, charge);
	}
	
	@Override
	public void onUpdate() {
		for(int slot : charges.keySet()) {
			if(getCharge(slot) <= 0f) {
				if(getUtil().decreaseStack(slot, 1) != null)
					setCharge(slot, 1f);
			}
		}
	}
	@Override
	public void alterCharge(int slot, float charge) {
		setCharge(slot, getCharge(slot) + charge);
	}
	

}
