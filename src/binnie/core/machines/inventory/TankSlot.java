package binnie.core.machines.inventory;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.liquids.ILiquidTank;
import net.minecraftforge.liquids.LiquidStack;
import net.minecraftforge.liquids.LiquidTank;

public class TankSlot extends BaseSlot<LiquidStack> {

	LiquidTank tank;
	
	public TankSlot(int index, String name, int capacity) {
		super(index, name);
		tank = new LiquidTank(capacity);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		LiquidStack liquid = LiquidStack
				.loadLiquidStackFromNBT(nbttagcompound);
		setContent(liquid);
	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {
		if(getContent() != null)
			getContent().writeToNBT(nbttagcompound);
	}

	@Override
	public LiquidStack getContent() {
		return tank.getLiquid();
	}

	@Override
	public void setContent(LiquidStack itemStack) {
		tank.setLiquid(itemStack);
	}

	public ILiquidTank getTank() {
		return tank;
	}
	
}
