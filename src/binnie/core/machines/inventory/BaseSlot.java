package binnie.core.machines.inventory;

import java.util.EnumSet;

import net.minecraftforge.common.ForgeDirection;
import forestry.api.core.INBTTagable;

/**
 * Parent of both TankSlots and InventorySlots. Deals with read only etc.
 * @author Alex Binnie
 *
 */
public abstract class BaseSlot<T> implements INBTTagable {
	
	EnumSet<MachineSide> inputSides = EnumSet.allOf(MachineSide.class);
	EnumSet<MachineSide> outputSides = EnumSet.allOf(MachineSide.class);
	
	Validator<T> validator = null;
	boolean readOnly = false;
	int index;

	public BaseSlot(int index, String name) {
		setIndex(index);
		setName(name);
	}

	public void setReadOnly() {
		readOnly = true;
		forbidInsertion();
	}

	public boolean isValid(T item) {
		if(item == null)
			return true;
		if (validator != null)
			return validator.isValid(item);
		return true;
	}

	public abstract T getContent();

	public abstract void setContent(T itemStack);

	public void setValidator(Validator<T> val) {
		validator = val;
	}

	public boolean isEmpty() {
		return getContent() == null;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public int getIndex() {
		return index;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}
	
	/**
	 * Does this slot accept input from any direction
	 */
	public boolean canInsert() {
		return !inputSides.isEmpty();
	}
	
	/**
	 * Does this slot allow extraction from any direction
	 */
	public boolean canExtract() {
		return !outputSides.isEmpty();
	}
	
	/**
	 * Internal machine slots, not to be touched by pipes
	 */
	public void forbidInteraction() {
		forbidInsertion();
		forbidExtraction();
	}
	
	public void setInputSides(EnumSet<MachineSide> sides) {
		this.inputSides = sides;
	}
	
	public void setOutputSides(EnumSet<MachineSide> sides) {
		this.outputSides = sides;
	}

	public void forbidExtraction() {
		setOutputSides(MachineSide.None);
	}
	
	public void forbidInsertion() {
		setInputSides(MachineSide.None);
	}
	
	public boolean canInsert(ForgeDirection dir) {
		return MachineSide.contains(inputSides, dir);
	}
	
	public boolean canExtract(ForgeDirection dir) {
		return MachineSide.contains(outputSides, dir);
	}

	public EnumSet<MachineSide> getInputSides() {
		return inputSides;
	}
	
	public EnumSet<MachineSide> getOutputSides() {
		return outputSides;
	}
	
	String name = "Slot";

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public Validator<T> getValidator() {
		return validator;
	}

}
