package binnie.core.machines.inventory;

import java.util.EnumSet;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.ForgeDirection;

public class InventorySlot extends BaseSlot<ItemStack> {
	
	public static String NameInput = "Input Slot";
	public static String NameProcess = "Processing Slot";
	public static String NameOutput = "Output Slot";
	
	ItemStack itemStack = null;

	public InventorySlot(int index, String name) {
		super(index, name);
	}

	public ItemStack getContent() {
		return itemStack;
	}
	
	public ItemStack getItemStack() {
		return getContent();
	}

	public void setContent(ItemStack itemStack) {
		this.itemStack = itemStack;
	}

	public ItemStack decrStackSize(int amount) {
		if (itemStack == null)
			return null;

		if (itemStack.stackSize <= amount) {
			ItemStack returnStack = itemStack.copy();
			itemStack = null;
			return returnStack;
		} else {
			ItemStack returnStack = itemStack.copy();
			itemStack.stackSize -= amount;
			returnStack.stackSize = amount;
			return returnStack;
		}
	}

	public void readFromNBT(NBTTagCompound slotNBT) {
		if (slotNBT.hasKey("item")) {
			NBTTagCompound itemNBT = slotNBT.getCompoundTag("item");
			itemStack = ItemStack.loadItemStackFromNBT(itemNBT);
		} else
			itemStack = null;
	}

	public void writeToNBT(NBTTagCompound slotNBT) {
		NBTTagCompound itemNBT = new NBTTagCompound();
		if (itemStack != null)
			itemStack.writeToNBT(itemNBT);
		slotNBT.setCompoundTag("item", itemNBT);
	}

	public void setItemStack(ItemStack duplicate) {
		setContent(duplicate);
	}
	
	public SlotValidator getValidator() {
		return (SlotValidator) validator;
	}

}
