package binnie.core.machines.inventory;

import java.util.EnumSet;

import net.minecraftforge.common.ForgeDirection;

public enum MachineSide {
	
	TypeTop,
	TypeBottom,
	TypeSides,
	;
	
	public static EnumSet<MachineSide> All = EnumSet.allOf(MachineSide.class);
	public static EnumSet<MachineSide> TopAndBottom = EnumSet.of(TypeTop, TypeBottom);
	public static EnumSet<MachineSide> None = EnumSet.noneOf(MachineSide.class);

	public static EnumSet<MachineSide> Top = EnumSet.of(TypeTop);
	public static EnumSet<MachineSide> Bottom = EnumSet.of(TypeBottom);
	public static EnumSet<MachineSide> Sides = EnumSet.of(TypeSides);
	
	public static boolean contains(EnumSet<MachineSide> sides, ForgeDirection dir) {
		switch(dir) {
		case DOWN:
			return sides.contains(TypeBottom);
		case EAST:
		case NORTH:
		case SOUTH:
		case WEST:
			return sides.contains(TypeSides);
		case UP:
			return sides.contains(TypeTop);
			
		case UNKNOWN:
		default:
			return false;
		
		}
	}

	public static String asString(EnumSet<MachineSide> sides) {
		if(sides.containsAll(All))
			return "Any";
		if(sides.isEmpty())
			return "None";
		String text = "";
		int i = 0;
		for(MachineSide side : sides) {
			text += side.getName();
			i++;
			if(i < sides.size())
				text += ", ";
		}
		return text;
	}

	private String getName() {
		switch(this) {
		case TypeBottom:
			return "Bottom";
		case TypeSides:
			return "Sides";
		case TypeTop:
			return "Top";
		default:
			return "None";
		
		}
	}
}
