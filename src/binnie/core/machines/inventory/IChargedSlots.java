package binnie.core.machines.inventory;

public interface IChargedSlots {
	
	public float getCharge(int slot);
	public void setCharge(int slot, float charge);
	public void alterCharge(int slot, float charge);
}
