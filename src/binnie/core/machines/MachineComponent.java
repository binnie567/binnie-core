package binnie.core.machines;

import net.minecraft.inventory.IInventory;
import net.minecraft.nbt.NBTTagCompound;
import binnie.core.network.packet.MachinePayload;
import forestry.api.core.INBTTagable;

public class MachineComponent implements INBTTagable {

	IMachine machine;
	
	public MachineComponent(IMachine machine) {
		setMachine(machine);
			machine.addComponent(this);
	}
	
	public void setMachine(IMachine machine) {
		this.machine = machine;
	}
	
	public IMachine getMachine() {
		return machine;
	}

	public void readFromNBT(NBTTagCompound nbttagcompound) {
	}

	public void writeToNBT(NBTTagCompound nbttagcompound) {
	}

	public void onUpdate() {
	}

	public Class[] getComponentInterfaces() {
		return MachineManager.getComponentInterfaces(this.getClass());
	}

	public void onInventoryUpdate() {
	}
	
	public final MachinePayload getPayload() {
		return new MachinePayload(MachineManager.getNetworkID(this.getClass()));
	}

	public void recieveData(MachinePayload payload) {
	}
	
	public MachineUtil getUtil() {
		return getMachine().getMachineUtil();
	}

	public void onDestruction() {
	}
	
	public int getUniqueProgressBarID() {
		return getMachine().getUniqueProgressBarID();
	}
	
	public IInventory getInventory() {
		return getMachine().getInterface(IInventory.class);
	}

}
