package binnie.core.machines.network;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import binnie.craftgui.minecraft.INetworkedEntityGUI;
import cpw.mods.fml.relauncher.Side;

public interface INetwork {
	
	public interface GUI extends INetworkedEntityGUI {
		
	}
	
	public interface CraftGUIAction {
		void recieveNBT(Side side, EntityPlayer player, NBTTagCompound nbt);
	}

}
