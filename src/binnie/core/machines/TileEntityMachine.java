package binnie.core.machines;

import java.util.Map;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.ILiquidTank;
import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidStack;
import binnie.core.machines.base.TileEntityMachineBase;
import binnie.core.machines.inventory.IValidatedInventory;
import binnie.core.machines.inventory.IValidatedTankContainer;
import binnie.core.network.INetworkedEntity;
import binnie.core.network.packet.PacketPayload;
import binnie.craftgui.minecraft.INetworkedEntityGUI;
import buildcraft.api.inventory.ISpecialInventory;
import buildcraft.api.power.IPowerProvider;
import buildcraft.api.power.IPowerReceptor;

public class TileEntityMachine extends TileEntityMachineBase implements INetworkedEntity, INetworkedEntityGUI, ISpecialInventory {

	@Override
	public void updateEntity() {
		super.updateEntity();
		if(machine != null)
			machine.onUpdate();
	}

	@Override
	public boolean canUpdate() {
		return super.canUpdate();
	}

	public TileEntityMachine() {
		super();
	}
	
	public TileEntityMachine(MachinePackage pack) {
		super();
		setMachine(pack);
	}
	
	public void setMachine(MachinePackage pack) {
		if(pack != null)
			this.machine = new Machine(pack, this);
	}

	Machine machine;
	
	@Override
	public void readFromNBT(NBTTagCompound nbtTagCompound) {
		super.readFromNBT(nbtTagCompound);
		String name = nbtTagCompound.getString("name");
		String group = nbtTagCompound.getString("group");
		username = nbtTagCompound.hasKey("username") ? nbtTagCompound.getString("username") : null;
		MachinePackage pack = MachineManager.getPackage(group, name);
		setMachine(pack);
		getMachine().readFromNBT(nbtTagCompound);
	}

	@Override
	public void writeToNBT(NBTTagCompound nbtTagCompound) {
		super.writeToNBT(nbtTagCompound);
		String name = machine.getPackage().getUID();
		String group = machine.getPackage().getGroup().getUID();
		nbtTagCompound.setString("group", group);
		nbtTagCompound.setString("name", name);
		if(username != null)
			nbtTagCompound.setString("username", username);
		getMachine().writeToNBT(nbtTagCompound);
	}

	// / *******************************************************************
	// / INetworkedEntity Implementation ///
	// / *******************************************************************

	@Override
	public void writeToPacket(PacketPayload payload) {
		machine.writeToPacket(payload);
	}

	@Override
	public void readFromPacket(PacketPayload payload) {
		machine.readFromPacket(payload);
	}
	
	/// *******************************************************************
	/// IGUINetworkData Implementation ///
	/// *******************************************************************

	@Override
	public void addGUINetworkData(Map<Integer, Integer> data) {
		machine.addGUINetworkData(data);
	}

	@Override
	public void recieveGUINetworkData(int id, int data) {
		machine.recieveGUINetworkData(id, data);
	}
	
	
	public Machine getMachine() {
		return machine;
	}
	
	/// *******************************************************************
	/// User related functionality ///
	/// *******************************************************************
	
	String username;

	
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public boolean isInvNameLocalized() {
		return false;
	}

	public void onBlockDestroy() {
		machine.onBlockDestroy();
	}

}