package binnie.core.machines;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import net.minecraft.creativetab.CreativeTabs;
import cpw.mods.fml.common.registry.GameRegistry;

public class MachineGroup {
	
	int blockID;
	String blockName;
	String name;
	
	public MachineGroup(String name, int blockID, String blockName, IMachineType[] types) {
		super();
		this.name = name;
		this.blockID = blockID;
		this.blockName = blockName;
		for(IMachineType type : types) {
			try {
				MachinePackage pack = type.getPackageClass().newInstance();
				pack.assignMetadata(type.ordinal());
				pack.setActive(type.isActive());
				addPackage(pack);
			}
			catch(Exception e) {
				throw new RuntimeException(e);
			}
		}
		MachineManager.registerMachineGroup(this);
	}

	Map<String, MachinePackage> packages = new LinkedHashMap<String, MachinePackage>();
	Map<Integer, MachinePackage> packagesID = new LinkedHashMap<Integer, MachinePackage>();
	
	private void addPackage(MachinePackage pack) {
		this.packages.put(pack.getUID(), pack);
		this.packagesID.put(pack.getMetadata(), pack);
		pack.setGroup(this);
	}
	
	public Collection<MachinePackage> getPackages() {
		return packages.values();
	}
	
	BlockMachine block;
	
	public BlockMachine getBlock() {
		return block;
	}

	public MachinePackage getPackage(int metadata) {
		return packagesID.get(metadata);
	}
	
	public MachinePackage getPackage(String name) {
		return packages.get(name);
	}

	public final void init() {
		block = new BlockMachine(blockID, this, blockName);
	}
	
	public void register() {
		if(block != null) {
			GameRegistry.registerBlock(block, ItemMachine.class, blockName);
			for(MachinePackage pack : this.getPackages())
				pack.register();
		}
		
	}

	public String getUID() {
		return name;
	}
	
	boolean renderedTileEntity = true;
	
	boolean isTileEntityRenderered() {
		return renderedTileEntity;
	}
	
	public void renderAsBlock() {
		renderedTileEntity = false;
	}

	public void setCreativeTab(CreativeTabs tab) {
		this.block.setCreativeTab(tab);
	}

}
