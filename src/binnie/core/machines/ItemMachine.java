package binnie.core.machines;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemMachine extends ItemBlock {

	BlockMachine associatedBlock;

	public ItemMachine(int i) {
		super(i);
		setMaxDamage(0);
		setHasSubtypes(true);
		this.associatedBlock = (BlockMachine) Block.blocksList[getBlockID()];
	}

	@Override
	public int getMetadata(int i) {
		return i;
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		return associatedBlock.getMachineName(itemstack.getItemDamage());
	}

}
