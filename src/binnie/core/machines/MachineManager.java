package binnie.core.machines;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import binnie.core.BinnieCore;
import binnie.core.BinnieException;
import binnie.core.machines.inventory.SlotValidator;
import binnie.core.machines.inventory.ValidatorIcon;

public class MachineManager {

	static Map<Class, Class[]> componentInterfaceMap = new HashMap<Class, Class[]>();

	static Map<String, MachineGroup> machineGroups = new HashMap<String, MachineGroup>();
	
	static Map<Integer, Class> networkIDToComponent = new HashMap<Integer, Class>();
	static Map<Class, Integer> componentToNetworkID = new HashMap<Class, Integer>();
	
	static int nextNetworkID = 0;
	
	public static void registerMachineGroup(MachineGroup group) {
		machineGroups.put(group.getUID(), group);
		group.init();
	}
	
	public static MachineGroup getGroup(String name) {
		return machineGroups.get(name);
	}
	
	public static MachinePackage getPackage(String group, String name) {
		MachineGroup machineGroup = getGroup(group);
		return machineGroup == null ? null : machineGroup.getPackage(name);
	}
	
	private static void registerComponentClass(Class component) {

		if(componentInterfaceMap.containsKey(component))
			throw new BinnieException("Attempted to reregister component "+component);
		
        Set<Class> interfaces = new HashSet<Class>();
        
        Class currentClass = component;
        
        while(currentClass != null) {
        	 for(Class clss : currentClass.getInterfaces()) {
             	interfaces.add(clss);
             }
        	 currentClass = currentClass.getSuperclass();
        	 if(currentClass == Object.class) currentClass = null;
        }
       

        interfaces.remove(forestry.api.core.INBTTagable.class);

        componentInterfaceMap.put(component, interfaces.toArray(new Class[0]));
        
        int networkID = nextNetworkID++;
        
        networkIDToComponent.put(networkID, component);
        componentToNetworkID.put(component, networkID);

	}
	
	public static int getNetworkID(Class component) {
		return componentToNetworkID.get(component);
	}
	
	public static Class getComponentClass(int networkID) {
		return networkIDToComponent.get(networkID);
	}
	
	static int machineRenderID;
	
	public static int getMachineRenderID() {
		return machineRenderID;
	}

	public static void doInit() {
		machineRenderID = BinnieCore.proxy.getUniqueRenderID();
		
		SlotValidator.IconBee = new ValidatorIcon(BinnieCore.instance,
				"validator/bee.0", "validator/bee.1");
		SlotValidator.IconFrame = new ValidatorIcon(BinnieCore.instance,
				"validator/frame.0", "validator/frame.1");
		SlotValidator.IconCircuit = new ValidatorIcon(BinnieCore.instance,
				"validator/circuit.0", "validator/circuit.1");
		SlotValidator.IconBlock = new ValidatorIcon(BinnieCore.instance,
				"validator/block.0", "validator/block.1");
	}
	
	public static void postInit() {
		for(MachineGroup group : machineGroups.values())
			group.register();
		BinnieCore.proxy.registerBlockRenderer(BinnieCore.proxy.createObject("binnie.core.machines.RendererMachine"));
		BinnieCore.proxy.registerTileEntity(TileEntityMachine.class, "binnie.tile.machine", 
				BinnieCore.proxy.createObject("binnie.core.machines.RendererMachine"));	
	}

	public static Class[] getComponentInterfaces (
			Class<? extends MachineComponent> clss) {
		
		if(!componentInterfaceMap.containsKey(clss))
			registerComponentClass(clss);
		
		return componentInterfaceMap.get(clss);
	}

}
