package binnie.core.machines.transfer;

import binnie.core.machines.Machine;
import binnie.core.machines.inventory.IValidatedTankContainer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.ILiquidTank;
import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidContainerData;
import net.minecraftforge.liquids.LiquidContainerRegistry;
import net.minecraftforge.liquids.LiquidStack;

// static class for moving items between inventories. Used for pipe input and shift clicks
public class TransferHandler {
/*
	public static ItemStack transfer(ItemStack item, IInventory origin, IInventory destination, boolean doAdd) {
		if (item == null || destination == null)
			return item;
		
		int[] slots = new int[destination.getSizeInventory()];
		for (int i = 0; i < destination.getSizeInventory(); i++)
			slots[i] = i;
		return transfer(item, origin, destination, slots, doAdd);
	}
	
	public static ItemStack transfer(ItemStack item, IInventory origin, IInventory destination, int[] targetSlots, boolean doAdd) {
		if (item == null || destination == null)
			return item;
		
		ItemStack ret = item.copy();
		
		ret = transferToInventory(ret, destination, targetSlots, doAdd, false);
		
		if(ret == null) return null;
		
		if(destination instanceof ITankContainer) {
			ret = transferContainerIntoTank(ret, origin, (ITankContainer) destination, doAdd);
			ret = transferTankIntoContainer(ret, origin, (ITankContainer) destination, doAdd);
		}
		
		return ret;
	}
	
	public static ItemStack transferItemToInventory(ItemStack item, IInventory destination, boolean doAdd) {

		if (item == null || destination == null)
			return item;

		ItemStack addition = item.copy();

		for (int i = 0; i < destination.getSizeInventory(); i++) {
			addition = transferToInventory(addition, destination, new int[] { i }, doAdd, false);
			if (addition == null) {
				return null;
			}
		}

		return addition;

	}

	public static ItemStack transferToInventory(ItemStack item, IInventory destination,
			int[] targetSlots, boolean doAdd, boolean ignoreValidation) {

		if(item == null || destination == null)
			return item;
		
		for (int i : targetSlots) {
			if (!destination.isStackValidForSlot(i, item) && !ignoreValidation)
				continue;

			if (destination.getStackInSlot(i) == null) {
				if (doAdd)
					destination.setInventorySlotContents(i, item.copy());
				return null;
			}

			if (item.isStackable()) {
				ItemStack merged = destination.getStackInSlot(i).copy();
				ItemStack[] newStacks = mergeStacks(item.copy(), merged.copy());
				item = newStacks[0];
				if (doAdd)
					destination.setInventorySlotContents(i, newStacks[1]);
				if (item == null)
					return null;
			}
		}

		return item;

	}

	public static ItemStack[] mergeStacks(ItemStack itemstack, ItemStack merged) {
		if (ItemStack.areItemStackTagsEqual(itemstack, merged) && itemstack.isItemEqual(merged)) {
			int space = merged.getMaxStackSize() - merged.stackSize;
			if (space > 0) {
				if (itemstack.stackSize > space) {
					itemstack.stackSize -= space;
					merged.stackSize += space;
				} else if (itemstack.stackSize <= space) {
					merged.stackSize += itemstack.stackSize;
					itemstack = null;
				}
			}
		}

		return new ItemStack[] { itemstack, merged };
	}

	public static ItemStack transferContainerIntoTank(ItemStack item, IInventory origin, ITankContainer destination,
			boolean doAdd) {
		if(item == null) return null;
		ILiquidTank[] tanks = destination.getTanks(ForgeDirection.UNKNOWN);
		ItemStack stack = item.copy();
		for(int i = 0; i < tanks.length; i++) {
			stack = transferToTank(stack, origin, destination, i, doAdd);
		}
		return stack;
	}
	
	public static ItemStack transferTankIntoContainer(ItemStack item, IInventory origin, ITankContainer destination,
			boolean doAdd) {
		if(item == null) return null;
		ILiquidTank[] tanks = destination.getTanks(ForgeDirection.UNKNOWN);
		ItemStack stack = item.copy();
		for(int i = 0; i < tanks.length; i++) {
			stack = transferFromTank(stack, origin, destination, i, doAdd);
		}
		return stack;
	}
	
	public static ItemStack transferToTank(ItemStack item, IInventory origin, ITankContainer destination, int tankID, boolean doAdd) {

		if(item == null) return item;
		
		LiquidStack containerLiquid = null;
		LiquidContainerData containerLiquidData = null;
		for (LiquidContainerData data : LiquidContainerRegistry.getRegisteredLiquidContainerData()) {
			if (data.filled.isItemEqual(item)) {
				containerLiquidData = data;
				containerLiquid = data.stillLiquid.copy();
				break;
			}
		}

		if (containerLiquid == null)
			return item;

		ILiquidTank tank = destination.getTanks(ForgeDirection.UNKNOWN)[tankID];

		IValidatedTankContainer validated = Machine.getInterface(IValidatedTankContainer.class, destination);
		
		if (validated != null)
			if (!validated.isLiquidValidForTank(containerLiquid,
					tankID))
				return item;

		LiquidStack largeAmountOfLiquid = containerLiquid.copy();
		largeAmountOfLiquid.amount = tank.getCapacity();
		int amountAdded = tank.fill(largeAmountOfLiquid, false);
		
		int numberOfContainersToAdd = amountAdded / containerLiquid.amount;
		
		if(numberOfContainersToAdd > item.stackSize)
			numberOfContainersToAdd = item.stackSize;
				
		ItemStack leftOverContainers = item.copy();
		leftOverContainers.stackSize -= numberOfContainersToAdd;
		if(leftOverContainers.stackSize <= 0) leftOverContainers = null;
		
		ItemStack emptyContainers = containerLiquidData.container.copy();
		emptyContainers.stackSize = 0;
		emptyContainers.stackSize += numberOfContainersToAdd;
		if(emptyContainers.stackSize <= 0) emptyContainers = null;
		
		ItemStack containersThatCantBeDumped = TransferHandler.transferItemToInventory(emptyContainers, origin, false);
		if(containersThatCantBeDumped != null) return item;
		
		// Can finally do everything, as it is valid
		
		if(doAdd) {
			LiquidStack liquidToFillTank = containerLiquid.copy();
			liquidToFillTank.amount *= numberOfContainersToAdd;
			tank.fill(liquidToFillTank, true);
			TransferHandler.transferItemToInventory(emptyContainers, origin, true);
		}
		
		return leftOverContainers;
	}
	
	public static ItemStack transferFromTank(ItemStack item, IInventory origin, ITankContainer destination, int tankID, boolean doAdd) {

		if(item == null) return item;
		
		ILiquidTank tank = destination.getTanks(ForgeDirection.UNKNOWN)[tankID];
		LiquidStack liquidInTank = tank.getLiquid();
		if(liquidInTank == null) return item;
		
		LiquidContainerData containerLiquidData = null;
		for (LiquidContainerData data : LiquidContainerRegistry.getRegisteredLiquidContainerData()) {
			if (data.container.isItemEqual(item) && liquidInTank.isLiquidEqual(data.stillLiquid)) {
				containerLiquidData = data;
				break;
			}
		}

		if (containerLiquidData == null)
			return item;

		int maximumExtractedLiquid = item.stackSize * containerLiquidData.stillLiquid.amount;

		LiquidStack drainedLiquid = tank.drain(maximumExtractedLiquid, false);
		int amountInTank = drainedLiquid == null ? 0 : drainedLiquid.amount;
		
		int numberOfContainersToFill = amountInTank / containerLiquidData.stillLiquid.amount;
		
		if(numberOfContainersToFill > item.stackSize)
			numberOfContainersToFill = item.stackSize;
				
		ItemStack leftOverContainers = item.copy();
		leftOverContainers.stackSize -= numberOfContainersToFill;
		if(leftOverContainers.stackSize <= 0) leftOverContainers = null;
		
		ItemStack filledContainers = containerLiquidData.filled.copy();
		filledContainers.stackSize = 0;
		filledContainers.stackSize += numberOfContainersToFill;
		if(filledContainers.stackSize <= 0) filledContainers = null;
		
		ItemStack containersThatCantBeDumped = TransferHandler.transferItemToInventory(filledContainers, origin, false);
		if(containersThatCantBeDumped != null) return item;
		
		// Can finally do everything, as it is valid
		
		if(doAdd) {
			tank.drain(maximumExtractedLiquid, true);
			TransferHandler.transferItemToInventory(filledContainers, origin, true);
		}
		
		return leftOverContainers;
	}
*/
	
}
