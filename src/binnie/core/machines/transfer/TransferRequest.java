package binnie.core.machines.transfer;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.ILiquidTank;
import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidContainerData;
import net.minecraftforge.liquids.LiquidContainerRegistry;
import net.minecraftforge.liquids.LiquidStack;
import binnie.core.machines.Machine;
import binnie.core.machines.inventory.IValidatedTankContainer;
import binnie.core.machines.power.ITankMachine;

public class TransferRequest {

	ItemStack itemToTransfer = null;
	ItemStack returnItem = null;

	IInventory origin;
	IInventory destination;

	int[] targetSlots = new int[0];
	int[] targetTanks = new int[0];

	boolean transferLiquids = true;

	boolean ignoreReadOnly = false;
	
	public TransferRequest(ItemStack toTransfer, IInventory destination) {
		int[] target = new int[destination.getSizeInventory()];
		for (int i = 0; i < target.length; i++) {
			target[i] = i;
		}
		int[] targetTanks = new int[0];
		if(destination instanceof ITankMachine) {
			targetTanks = new int[((ITankMachine)destination).getTanks().length];
			for (int i = 0; i < targetTanks.length; i++) {
				targetTanks[i] = i;
			}
		}
		if(toTransfer != null) {
			setItemToTransfer(toTransfer.copy());
			setReturnItem(toTransfer.copy());
		}
		
		setOrigin(null);
		setDestination(destination);
		setTargetSlots(target);
		setTargetTanks(targetTanks);
		this.transferLiquids = true;
	}

	private void setItemToTransfer(ItemStack itemToTransfer) {
		this.itemToTransfer = itemToTransfer;
	}

	private void setReturnItem(ItemStack returnItem) {
		this.returnItem = returnItem;
	}

	public TransferRequest setOrigin(IInventory origin) {
		this.origin = origin;
		return this;
	}

	private void setDestination(IInventory destination) {
		this.destination = destination;
	}

	public TransferRequest setTargetSlots(int[] targetSlots) {
		this.targetSlots = targetSlots;
		return this;
	}
	
	public TransferRequest setTargetTanks(int[] targetTanks) {
		this.targetTanks = targetTanks;
		return this;
	}

	public TransferRequest ignoreValidation() {
		this.ignoreReadOnly = true;
		return this;
	}

	public ItemStack getReturnItem() {
		return this.returnItem;
	}

	public ItemStack transfer(boolean doAdd) {

		ItemStack item = returnItem;

		if (item == null || destination == null)
			return null;

		// Transfer to an empty slot
		
		for (int slot : targetSlots) {

			if (!destination.isStackValidForSlot(slot, item) && !ignoreReadOnly)
				continue;

			if (destination.getStackInSlot(slot) == null)
				continue;

			if (item.isStackable()) {
				ItemStack merged = destination.getStackInSlot(slot).copy();
				ItemStack[] newStacks = mergeStacks(item.copy(), merged.copy());
				item = newStacks[0];
				if(!areItemsEqual(merged, newStacks[1]))
					insertedSlots.add(new TransferSlot(slot, destination));
				if (doAdd)
					destination.setInventorySlotContents(slot, newStacks[1]);
				if (item == null)
					return null;
			}

		}
		
		for (int slot : targetSlots) {

			if (!destination.isStackValidForSlot(slot, item) && !ignoreReadOnly)
				continue;

			if (destination.getStackInSlot(slot) == null && item != null) {
				insertedSlots.add(new TransferSlot(slot, destination));
				if (doAdd)
					destination.setInventorySlotContents(slot, item.copy());
				return null;
			}
		}
		
		if(this.transferLiquids && destination instanceof ITankMachine) {
			for(int tankID : targetTanks) {
				item = transferToTank(item, origin, (ITankMachine)destination, tankID, doAdd);
				item = transferFromTank(item, origin, (ITankMachine)destination, tankID, doAdd);
			}
			
		}

		setReturnItem(item);
		
		return getReturnItem();
	}

	private static boolean areItemsEqual(ItemStack merged, ItemStack itemstack) {
		return ItemStack.areItemStackTagsEqual(itemstack, merged) && itemstack.isItemEqual(merged);
	}

	public static ItemStack[] mergeStacks(ItemStack itemstack, ItemStack merged) {
		if (areItemsEqual(itemstack, merged)) {
			int space = merged.getMaxStackSize() - merged.stackSize;
			if (space > 0) {
				if (itemstack.stackSize > space) {
					itemstack.stackSize -= space;
					merged.stackSize += space;
				} else if (itemstack.stackSize <= space) {
					merged.stackSize += itemstack.stackSize;
					itemstack = null;
				}
			}
		}

		return new ItemStack[] { itemstack, merged };
	}

	private ItemStack transferToTank(ItemStack item, IInventory origin, ITankContainer destination, int tankID, boolean doAdd) {

		if(item == null) return item;
		
		LiquidStack containerLiquid = null;
		LiquidContainerData containerLiquidData = null;
		for (LiquidContainerData data : LiquidContainerRegistry.getRegisteredLiquidContainerData()) {
			if (data.filled.isItemEqual(item)) {
				containerLiquidData = data;
				containerLiquid = data.stillLiquid.copy();
				break;
			}
		}

		if (containerLiquid == null)
			return item;

		ILiquidTank tank = destination.getTanks(ForgeDirection.UNKNOWN)[tankID];

		IValidatedTankContainer validated = Machine.getInterface(IValidatedTankContainer.class, destination);
		
		if (validated != null)
			if (!validated.isLiquidValidForTank(containerLiquid,
					tankID))
				return item;

		LiquidStack largeAmountOfLiquid = containerLiquid.copy();
		largeAmountOfLiquid.amount = tank.getCapacity();
		int amountAdded = tank.fill(largeAmountOfLiquid, false);
		
		int numberOfContainersToAdd = amountAdded / containerLiquid.amount;
		
		if(numberOfContainersToAdd > item.stackSize)
			numberOfContainersToAdd = item.stackSize;
				
		ItemStack leftOverContainers = item.copy();
		leftOverContainers.stackSize -= numberOfContainersToAdd;
		if(leftOverContainers.stackSize <= 0) leftOverContainers = null;
		
		ItemStack emptyContainers = containerLiquidData.container.copy();
		emptyContainers.stackSize = 0;
		emptyContainers.stackSize += numberOfContainersToAdd;
		if(emptyContainers.stackSize <= 0) emptyContainers = null;
		
		TransferRequest containersDump = (new TransferRequest(emptyContainers, origin));
		
		ItemStack containersThatCantBeDumped = containersDump.transfer(false);
		if(containersThatCantBeDumped != null) return item;
		
		// Can finally do everything, as it is valid
		
		if(doAdd) {
			LiquidStack liquidToFillTank = containerLiquid.copy();
			liquidToFillTank.amount *= numberOfContainersToAdd;
			tank.fill(liquidToFillTank, true);
			containersDump.transfer(true);
		}
		
		return leftOverContainers;
	}
	
	private ItemStack transferFromTank(ItemStack item, IInventory origin, ITankContainer destination, int tankID, boolean doAdd) {

		if(item == null) return item;
		
		ILiquidTank tank = destination.getTanks(ForgeDirection.UNKNOWN)[tankID];
		LiquidStack liquidInTank = tank.getLiquid();
		if(liquidInTank == null) return item;
		
		LiquidContainerData containerLiquidData = null;
		for (LiquidContainerData data : LiquidContainerRegistry.getRegisteredLiquidContainerData()) {
			if (data.container.isItemEqual(item) && liquidInTank.isLiquidEqual(data.stillLiquid)) {
				containerLiquidData = data;
				break;
			}
		}

		if (containerLiquidData == null)
			return item;

		int maximumExtractedLiquid = item.stackSize * containerLiquidData.stillLiquid.amount;

		LiquidStack drainedLiquid = tank.drain(maximumExtractedLiquid, false);
		int amountInTank = drainedLiquid == null ? 0 : drainedLiquid.amount;
		
		int numberOfContainersToFill = amountInTank / containerLiquidData.stillLiquid.amount;
		
		if(numberOfContainersToFill > item.stackSize)
			numberOfContainersToFill = item.stackSize;
				
		ItemStack leftOverContainers = item.copy();
		leftOverContainers.stackSize -= numberOfContainersToFill;
		if(leftOverContainers.stackSize <= 0) leftOverContainers = null;
		
		ItemStack filledContainers = containerLiquidData.filled.copy();
		filledContainers.stackSize = 0;
		filledContainers.stackSize += numberOfContainersToFill;
		if(filledContainers.stackSize <= 0) filledContainers = null;
		
		TransferRequest containersDump = (new TransferRequest(filledContainers, origin));
		
		
		ItemStack containersThatCantBeDumped = containersDump.transfer(false);
		if(containersThatCantBeDumped != null) return item;
		
		// Can finally do everything, as it is valid
		
		if(doAdd) {
			tank.drain(maximumExtractedLiquid, true);
			containersDump.transfer(true);
		}
		
		return leftOverContainers;
	}
	
	List<TransferSlot> insertedSlots = new ArrayList<TransferSlot>();
	List<Integer> insertedTanks = new ArrayList<Integer>();

	public List<TransferSlot> getInsertedSlots() {
		return insertedSlots;
	}
	
	public List<Integer> getInsertedTanks() {
		return insertedTanks;
	}

	
	public static class TransferSlot {
		public int id;
		public IInventory inventory;
		public TransferSlot(int id, IInventory inventory) {
			this.id = id;
			this.inventory = inventory;
		}
		
	}
	
}
