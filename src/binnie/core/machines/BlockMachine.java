package binnie.core.machines;

import java.util.List;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import binnie.core.BinnieCore;

public class BlockMachine extends BlockContainer {
	
	MachineGroup group;
	
	public BlockMachine(int id, MachineGroup group, String blockName) {
		super(id, Material.iron);
		this.group = group;
		setHardness(1.5f);
		setUnlocalizedName(blockName);
	}

	@Override
	public void getSubBlocks(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		for(MachinePackage pack : group.getPackages()) {
			if(pack.isActive())
				itemList.add(new ItemStack(this, 1, pack.getMetadata()));
		}
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public int getRenderType() {
		return MachineManager.getMachineRenderID();
	}

	@Override
	public TileEntity createTileEntity(World world, int metadata) {
		if(group.getPackage(metadata)==null)
			return null;
		else
			return group.getPackage(metadata).createTileEntity();
	}

	public String getMachineName(int meta) {
		if(group.getPackage(meta)==null)
			return "Unnamed Machine";
		else
			return group.getPackage(meta).getDisplayName();
	}

	@Override
	public int damageDropped(int par1) {
		return par1;
	}

	@Override
	public TileEntity createNewTileEntity(World var1) {
		return new TileEntityMachine();
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z,
			EntityPlayer player, int par6, float par7, float par8, float par9) {

		if (!BinnieCore.proxy.isSimulating(world)) {
			return true;
		} else {

			if (player.isSneaking())
				return true;

			TileEntity entity = world.getBlockTileEntity(x, y, z);

			if (entity instanceof TileEntityMachine) {
				((TileEntityMachine) entity).getMachine().onRightClick(world, player, x, y, z);
			}

			return true;
		}
	}
	
	@Override
	public void onBlockPlacedBy(World world, int i, int j, int k, EntityLiving entityliving, ItemStack stack) {

		if (!BinnieCore.proxy.isSimulating(world))
			return;

		TileEntityMachine tile = (TileEntityMachine) world.getBlockTileEntity(i, j, k);
		if (entityliving instanceof EntityPlayer) {
			tile.setUsername(((EntityPlayer) entityliving).username);
		}
	}
	
	@Override
	public Icon getBlockTexture(IBlockAccess world, int x,
			int y, int z, int side) {
		TileEntity entity = world.getBlockTileEntity(x, y, z);

		if (entity instanceof TileEntityMachine && ((TileEntityMachine) entity).getMachine().hasInterface(IMachineTexturedFaces.class)) {
			return ((TileEntityMachine) entity).getMachine().getInterface(IMachineTexturedFaces.class).getIcon(side);
		}
		
		return null;
	}
	
	@Override
	public void breakBlock(World world, int x, int y, int z, int par5, int par6) {

		TileEntity tileentity = world.getBlockTileEntity(x, y, z);

		if (!(tileentity instanceof TileEntityMachine))
			return;

		TileEntityMachine entity = (TileEntityMachine) tileentity;

		if (entity != null) {
			entity.onBlockDestroy();
		}

		super.breakBlock(world, x, y, z, par5, par6);
	}
	
	public interface IMachineTexturedFaces {
		Icon getIcon(int side);
	}

}
