package binnie.core.machines.power;

import java.util.LinkedList;
import java.util.List;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.ForgeDirection;
import binnie.core.machines.IMachine;
import binnie.core.machines.MachineComponent;
import binnie.core.machines.component.IBuildcraft;
import binnie.core.triggers.TriggerData;
import binnie.core.triggers.TriggerPower;
import buildcraft.api.power.IPowerProvider;
import buildcraft.api.power.IPowerReceptor;
import buildcraft.api.power.PowerFramework;

public class ComponentPowerReceptor extends MachineComponent implements
		IPoweredMachine, IBuildcraft.TriggerProvider {

	int currentEnergy;
	int maxStorage;
	
	
	float previousPower = 0f;
	
	LinkedList<Float> inputs = new LinkedList<Float>();
	final static int inputAverageTicks = 20;

	public ComponentPowerReceptor(IMachine machine) {
		this(machine, 1000);
	}

	public ComponentPowerReceptor(IMachine machine, int storage) {
		super(machine);
		setPowerProvider(PowerFramework.currentFramework.createPowerProvider());
		setConfig(10, 5, 40, 20, storage);
		getPowerProvider().configurePowerPerdition(0, 100);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		super.readFromNBT(nbttagcompound);

		if (nbttagcompound.hasKey("powerProvider")
				&& getPowerProvider() != null) {
			getPowerProvider().readFromNBT(nbttagcompound.getCompoundTag("powerProvider"));
			getPowerProvider().configure(config[0], config[1], config[2], config[3], config[4]);
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {
		super.writeToNBT(nbttagcompound);
		if (getPowerProvider() != null) {
			NBTTagCompound powerNBT = new NBTTagCompound();
			getPowerProvider().writeToNBT(powerNBT);
			nbttagcompound.setCompoundTag("powerProvider", powerNBT);
		}
	}

	private IPowerProvider powerProvider = null;

	@Override
	public final void setPowerProvider(IPowerProvider provider) {
		powerProvider = provider;
	}

	@Override
	public final IPowerProvider getPowerProvider() {
		return powerProvider;
	}

	@Override
	public final void doWork() {
	}

	@Override
	public final int powerRequest(ForgeDirection from) {
		float space = powerProvider.getMaxEnergyStored()
				- powerProvider.getEnergyStored();

		if (space < powerProvider.getMaxEnergyReceived())
			if (space > powerProvider.getMinEnergyReceived())
				return (int) space;

		return powerProvider.getMaxEnergyReceived();
	}

	@Override
	public void onUpdate() {
		currentEnergy = (int) getPowerProvider().getEnergyStored();
		maxStorage = getPowerProvider().getMaxEnergyStored();
		getPowerProvider().update((IPowerReceptor) getMachine().getTileEntity());
	}

	@Override
	public PowerInfo getPowerInfo() {
		return new PowerInfo(this, 0f);
	}
	
	@Override
	public final void getTriggers(List<TriggerData> triggers) {
		triggers.add(TriggerPower.powerNone(this));
		triggers.add(TriggerPower.powerLow(this));
		triggers.add(TriggerPower.powerMedium(this));
		triggers.add(TriggerPower.powerHigh(this));
		triggers.add(TriggerPower.powerFull(this));
	}
	
	int[] config;

	@Override
	public void setConfig(int latency, int minEnergy, int maxEnergy, int activeEnergy, int storage) {
		config = new int[] {latency, minEnergy, maxEnergy, activeEnergy, storage};
		this.getPowerProvider().configure(latency, minEnergy, maxEnergy, activeEnergy, storage);
	}

}
