package binnie.core.machines.power;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import binnie.core.machines.IMachine;
import binnie.core.machines.MachineComponent;
import binnie.core.machines.component.IBuildcraft;
import binnie.core.machines.network.INetwork;
import binnie.core.triggers.BinnieAction;
import binnie.core.triggers.TriggerData;
import binnie.core.triggers.TriggerWorking;
import buildcraft.api.gates.IAction;
import buildcraft.api.gates.IActionReceptor;
import buildcraft.api.power.IPowerProvider;

public class ComponentProcess extends MachineComponent implements INetwork.GUI, IProcess, IBuildcraft.ActionProvider, IBuildcraft.TriggerProvider,
IActionReceptor {
	
	protected float energyPerTick = 0.1f;
	protected float progressPerTick = 1f;
	float minimumEfficiency = 0.1f;
	LinkedList<Float> efficiencies = new LinkedList<Float>();
	
	float progressAmount = 0f;
	
	int processLength;
	int processEnergy;
	
	int guiProgressAmount;
	int guiEfficiency;
	int guiEnergyCost;
	int guiTimeTaken;
	
	public ComponentProcess(IMachine machine, int mjCost, int timePeriod) {
		super(machine);
		processLength = timePeriod;
		processEnergy = mjCost;
		guiProgressAmount = getUniqueProgressBarID();
		guiEfficiency = getUniqueProgressBarID();
		guiEnergyCost = getUniqueProgressBarID();
		guiTimeTaken = getUniqueProgressBarID();
		
	}

	private IPowerProvider getPowerProvider() {
		return getMachine().getInterface(IPoweredMachine.class).getPowerProvider();
	}

	@Override
	public float getEnergyPerTick() {
		return (float) getProcessEnergy() / (float) getProcessLength();
	}
	
	@Override
	public float getProgressPerTick() {
		return 100f / (float) getProcessLength();
	}
	
	public float getEfficiency() {
		if(clientEffeciency >= 0)
			return clientEffeciency;
		if(efficiencies.size() == 0)
			return 0f;
		float f = 0f;
		for(Float f2 : efficiencies)
			f += f2;

		f /= efficiencies.size();
		
		return f;
	}
	
	float actionPauseProcess = 0f;
	float actionCancelTask = 0f;
	
	@Override
	public void onUpdate() {
		progressPerTick = getProgressPerTick();
		energyPerTick = getEnergyPerTick();
		
		float energyAvailable = getPowerProvider().useEnergy(
				minimumEfficiency*energyPerTick, 
				energyPerTick, 
				false);
		
		float eff = energyAvailable / energyPerTick;
		
		efficiencies.add(eff);
		//System.out.println(eff + "   -   " + getEfficiency());
		
		if(efficiencies.size() > 60)
			efficiencies.poll();
		
		if (canWork() == null) {
			if (!isInProgress() && canProgress()==null) {
				onStartTask();
				progressAmount += 0.01f;
			}
			else if(canProgress()==null) {
				progressTick();
				onTickTask();
			}
		} else {
			if (isInProgress()) {
				onCancelTask();
				progressAmount = 0.0f;
				efficiencies.clear();
			}
		}
		if (progressAmount >= 100.0f) {
			onFinishTask();
			progressAmount = 0.0f;
			efficiencies.clear();
		}

		if (actionPauseProcess > 0)
			actionPauseProcess--;
		if (actionCancelTask > 0)
			actionCancelTask--;
		
		super.onUpdate();
	}
	

	private void progressTick() {
		getPowerProvider().useEnergy(
				minimumEfficiency*energyPerTick, 
				energyPerTick, 
				true);
		
		progressAmount += progressPerTick;
	}

	public ErrorState canWork() {
		return actionCancelTask == 0 ? null : new ErrorState("Task Cancelled",
				"Cancelled by Buildcraft Gate");
	}

	public ErrorState canProgress() {
		if (actionPauseProcess != 0)
			return new ErrorState("Process Paused", "Paused by Buildcraft Gate");
		return getPowerProvider().getEnergyStored() < (minimumEfficiency*energyPerTick) ? new ErrorState.InsufficientPower() : null;
	}
	
	public boolean isInProgress() {
		return progressAmount > 0.0f;
	}
	
	public float getProgress() {
		return progressAmount;
	}
	
	
	
	
	
	protected void onCancelTask() {
	}

	protected void onStartTask() {
	}
	
	protected void onFinishTask() {
	}
	
	protected void onTickTask() {
	}

	@Override
	public void addGUINetworkData(Map<Integer, Integer> data) {
		data.put(guiProgressAmount, (int) progressAmount*100);
		data.put(guiEfficiency, (int) (getEfficiency() * 1000));
		data.put(guiEnergyCost, (int) getProcessEnergy());
		data.put(guiTimeTaken, (int) getProcessLength());
	}
	
	float clientEffeciency = -1f;

	@Override
	public void recieveGUINetworkData(int id, int data) {
		if(id == guiProgressAmount) progressAmount = data/100f;
		if(id == guiEfficiency) clientEffeciency = data / 1000f;
		if(id == guiEnergyCost) processEnergy = data;
		if(id == guiTimeTaken) processLength = data;
	}

	public int getProcessLength() {
		return processLength >= 0 ? processLength : 0;
	}
	
	public int getProcessEnergy() {
		return processEnergy >= 0 ? processEnergy : 0;
	}

	@Override
	public String getTooltip() {
		return "Processing";
	}
	
	@Override
	public final void actionActivated(IAction action) {
		if (action == BinnieAction.actionCancelTask) {
			this.actionCancelTask = 20;
			this.onCancelTask();
		} else if (action == BinnieAction.actionPauseProcess) {
			this.actionPauseProcess = 20;
		}
	}

	@Override
	public void getTriggers(List<TriggerData> triggers) {
		triggers.add(TriggerWorking.canWork(this));
		triggers.add(TriggerWorking.cannotWork(this));
		triggers.add(TriggerWorking.isWorking(this));
		triggers.add(TriggerWorking.isNotWorking(this));
	}

	@Override
	public void getActions(List<IAction> actions) {
		actions.add(BinnieAction.actionPauseProcess);
		actions.add(BinnieAction.actionCancelTask);
	}
	
}