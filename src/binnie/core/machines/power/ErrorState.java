package binnie.core.machines.power;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import forestry.api.core.INBTTagable;

public class ErrorState implements INBTTagable {

	String name = "";
	String desc = "";
	int[] data = new int[0];
	
	boolean progress = false;
	
	public ErrorState(String name, String desc) {
		this.name = name;
		this.desc = desc;
	}
	
	public ErrorState(String name, String desc, int[] data) {
		this.name = name;
		this.desc = desc;
		this.data = data;
	}
	
	public String toString() { return name; };
	public String getTooltip() { return desc; };
	public int[] getData() { return data; };
	
	public boolean isProgress() {
		return progress;
	}
	
	public void setIsProgress() {
		this.progress = true;
	}
	
	public static class Item extends ErrorState {

		public Item(String name, String desc, int[] slots) {
			super(name, desc, slots);
		}
		
	}
	
	public static class Tank extends ErrorState {

		public Tank(String name, String desc, int[] slots) {
			super(name, desc, slots);
		}
		
	}
	
	public static class NoItem extends Item {

		public NoItem(String desc, int slot) {
			this(desc, new int[] {slot});
		}
		
		public NoItem(String desc, int[] slots) {
			super("No Item", desc, slots);
		}
		
	}
	
	public static class InvalidItem extends Item {

		public InvalidItem(String desc, int slot) {
			this("Invalid Item", desc, slot);
		}

		public InvalidItem(String name, String desc, int slot) {
			super(name, desc, new int[] {slot});
		}
		
	}
	
	public static class NoSpace extends Item {
		
		public NoSpace(String desc, int[] slots) {
			super("No Space", desc, slots);
		}

	}
	
	public static class InsufficientPower extends ErrorState {

		public InsufficientPower() {
			super("Insufficient Power", "Not enough power to operate");
		}
		
	}
	
	public static class TankSpace extends Tank {

		public TankSpace(String desc, int tank) {
			super("Tank Full", desc, new int[] {tank});
		}

	}
	
	public static class InsufficientLiquid extends Tank {

		public InsufficientLiquid(String desc, int tank) {
			super("Insufficient Liquid", desc, new int[] {tank});
		}

		
	}
	
	public static class InvalidRecipe extends Item {

		public InvalidRecipe(String string, int[] slots) {
			super("Invalid Recipe", string, slots);
		}

		
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		name = nbt.getString("name");
		desc = nbt.getString("desc");
		data = nbt.getIntArray("data");
		itemError = nbt.getBoolean("item");
		tankError = nbt.getBoolean("tank");
		powerError = nbt.getBoolean("power");
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		nbt.setString("name", toString());
		nbt.setString("desc", getTooltip());
		nbt.setIntArray("data", data);
		if(isItemError())
			nbt.setBoolean("item", true);
		if(isTankError())
			nbt.setBoolean("tank", true);
		if(isPowerError())
			nbt.setBoolean("power", true);
	}
	
	boolean itemError = false;
	boolean tankError = false;
	boolean powerError = false;
	
	public boolean isItemError() {
		return itemError || this instanceof ErrorState.Item;
	}
	
	public boolean isTankError() {
		return tankError || this instanceof ErrorState.Tank;
	}

	public boolean isPowerError() {
		return powerError || this instanceof ErrorState.InsufficientPower;
	}


}
