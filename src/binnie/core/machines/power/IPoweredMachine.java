package binnie.core.machines.power;

import buildcraft.api.power.IPowerReceptor;

public interface IPoweredMachine extends IPowerReceptor {
	
	PowerInfo getPowerInfo();

	void setConfig(int latency, int minEnergy, int maxEnergy, int activeEnergy,
			int storage);
	
}
