package binnie.core.machines.power;

import binnie.core.machines.inventory.IValidatedTankContainer;
import binnie.core.machines.inventory.TankSlot;
import binnie.core.machines.inventory.TankValidator;
import net.minecraftforge.liquids.ILiquidTank;
import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidTank;

public interface ITankMachine extends ITankContainer, IValidatedTankContainer {
	
	public TankInfo[] getTankInfos();
	
	public void addTank(int index, String name, int capacity);

	public ILiquidTank getTank(int index);
	
	public TankSlot getTankSlot(int index);
	
	public ILiquidTank[] getTanks();

}
