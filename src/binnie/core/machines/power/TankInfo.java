package binnie.core.machines.power;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.Icon;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.ILiquidTank;
import net.minecraftforge.liquids.LiquidStack;
import forestry.api.core.INBTTagable;

public class TankInfo implements INBTTagable {

	public LiquidStack liquid;
	float capacity = 0f;

	public TankInfo(ILiquidTank tank) {
		this.capacity = tank.getCapacity();
		this.liquid = tank.getLiquid();
	}

	public TankInfo() {
		// TODO Auto-generated constructor stub
	}

	public float getAmount() {
		return liquid == null ? 0f : liquid.amount;
	}

	public float getCapacity() {
		return capacity;
	}

	public boolean isEmpty() {
		return liquid == null;
	}

	public Icon getIcon() {
		ItemStack liquidStack = liquid.asItemStack();
		return liquidStack.getItem().getIconIndex(liquidStack);
	}

	public String getName() {
		return liquid == null ? "" : liquid.asItemStack().getDisplayName();
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		this.capacity = nbt.getInteger("capacity");
		if(nbt.hasKey("liquid"))
			liquid = LiquidStack.loadLiquidStackFromNBT(nbt.getCompoundTag("liquid"));
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		nbt.setInteger("capacity", (int) getCapacity());
		if(liquid == null) return;
		NBTTagCompound tag = new NBTTagCompound();
		liquid.writeToNBT(tag);
		nbt.setTag("liquid", tag);
	}

	public static TankInfo[] get(ITankMachine machine) {
		TankInfo[] info = new TankInfo[machine.getTanks(ForgeDirection.UNKNOWN).length];
		for (int i = 0; i < info.length; i++) {
			info[i] = new TankInfo(machine.getTanks(ForgeDirection.UNKNOWN)[i]);
		}
		return info;
	}
}
