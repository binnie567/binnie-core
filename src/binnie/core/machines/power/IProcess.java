package binnie.core.machines.power;

public interface IProcess extends IErrorStateSource {
	
	float getProgress();
	
	int getProcessLength();
	
	int getProcessEnergy();

	float getEnergyPerTick();
	
	float getProgressPerTick();
	
	float getEfficiency();
	
	/**
	 * Help tooltip, such as "Extracting Liquid DNA"
	 * @return
	 */
	String getTooltip();
}
