package binnie.core.machines.power;

import net.minecraft.nbt.NBTTagCompound;
import forestry.api.core.INBTTagable;

public class PowerInfo implements INBTTagable {
	float currentEnergy = 0f;
	float maxEnergy = 0f;
	float currentInput = 0f;
	float maxInput = 0f;
	public PowerInfo(IPoweredMachine machine, float currentInput) {
		currentEnergy = machine.getPowerProvider().getEnergyStored();
		maxEnergy = machine.getPowerProvider().getMaxEnergyStored();
		this.currentInput = currentInput;
		maxInput = machine.getPowerProvider().getMaxEnergyReceived();
	}
	public PowerInfo() {
		
	}
	public float getStoredEnergy() {
		return currentEnergy;
	}
	public float getMaxEnergy() {
		return maxEnergy;
	}
	public float getMaxInput() {
		return maxInput;
	}
	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		currentEnergy = nbttagcompound.getInteger("current");
		maxEnergy = nbttagcompound.getInteger("max");
		maxInput = nbttagcompound.getInteger("maxinput")/10f;
	}
	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {
		nbttagcompound.setInteger("current", (int)getStoredEnergy());
		nbttagcompound.setInteger("max", (int)getMaxEnergy());
		nbttagcompound.setInteger("maxinput", (int)getMaxInput()*10);
	}
}
