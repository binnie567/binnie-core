package binnie.core.machines.base;

import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.ILiquidTank;
import net.minecraftforge.liquids.LiquidStack;
import binnie.core.machines.inventory.TankSlot;
import binnie.core.machines.power.ITankMachine;
import binnie.core.machines.power.TankInfo;

public class DefaultTankContainer implements ITankMachine {

	@Override
	public int fill(ForgeDirection from, LiquidStack resource, boolean doFill) {
		return 0;
	}

	@Override
	public int fill(int tankIndex, LiquidStack resource, boolean doFill) {
		return 0;
	}

	@Override
	public LiquidStack drain(ForgeDirection from, int maxDrain, boolean doDrain) {
		return null;
	}

	@Override
	public LiquidStack drain(int tankIndex, int maxDrain, boolean doDrain) {
		return null;
	}

	@Override
	public ILiquidTank[] getTanks(ForgeDirection direction) {
		return new ILiquidTank[0];
	}

	@Override
	public ILiquidTank getTank(ForgeDirection direction, LiquidStack type) {
		return null;
	}

	@Override
	public TankInfo[] getTankInfos() {
		return new TankInfo[0];
	}

	@Override
	public boolean isTankReadOnly(int tank) {
		return false;
	}

	@Override
	public boolean isLiquidValidForTank(LiquidStack liquid, int tank) {
		return false;
	}

	@Override
	public void addTank(int index, String name, int capacity) {
	}

	@Override
	public ILiquidTank getTank(int index) {
		return null;
	}

	@Override
	public TankSlot getTankSlot(int slot) {
		return null;
	}
	
	@Override
	public ILiquidTank[] getTanks() {
		return new ILiquidTank[0];
	}

}
