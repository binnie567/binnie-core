package binnie.core.machines.base;

import net.minecraftforge.common.ForgeDirection;
import binnie.core.machines.power.IPoweredMachine;
import binnie.core.machines.power.PowerInfo;
import buildcraft.api.power.IPowerProvider;

public class DefaultPower implements IPoweredMachine {

	@Override
	public void setPowerProvider(IPowerProvider provider) {
	}

	@Override
	public IPowerProvider getPowerProvider() {
		return null;
	}

	@Override
	public void doWork() {
	}

	@Override
	public int powerRequest(ForgeDirection from) {
		return 0;
	}

	@Override
	public PowerInfo getPowerInfo() {
		return new PowerInfo(this, 0);
	}

	@Override
	public void setConfig(int latency, int minEnergy, int maxEnergy, int activeEnergy, int storage) {
	}

}
