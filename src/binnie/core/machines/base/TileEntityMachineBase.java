package binnie.core.machines.base;

import java.util.Map;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.ILiquidTank;
import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidStack;
import binnie.core.machines.Machine;
import binnie.core.machines.inventory.IInventoryMachine;
import binnie.core.machines.inventory.IValidatedInventory;
import binnie.core.machines.inventory.IValidatedTankContainer;
import binnie.core.machines.inventory.InventorySlot;
import binnie.core.machines.inventory.TankSlot;
import binnie.core.machines.inventory.TankValidator;
import binnie.core.machines.power.IPoweredMachine;
import binnie.core.machines.power.ITankMachine;
import binnie.core.machines.power.PowerInfo;
import binnie.core.machines.power.TankInfo;
import binnie.core.network.INetworkedEntity;
import binnie.core.network.packet.PacketPayload;
import binnie.craftgui.minecraft.INetworkedEntityGUI;
import buildcraft.api.inventory.ISpecialInventory;
import buildcraft.api.power.IPowerProvider;
import buildcraft.api.power.IPowerReceptor;

public class TileEntityMachineBase extends TileEntity implements IInventoryMachine, ITankMachine,
		IPoweredMachine {

	public IInventoryMachine getInventory() {
		IInventoryMachine inv = Machine.getInterface(IInventoryMachine.class, this);
		return inv == null ? new DefaultInventory() : inv;
	}

	public ITankMachine getTankContainer() {
		ITankMachine inv = Machine.getInterface(ITankMachine.class, this);
		return inv == null ? new DefaultTankContainer() : inv;
	}

	public IPoweredMachine getPower() {
		IPoweredMachine inv = Machine.getInterface(IPoweredMachine.class, this);
		return inv == null ? new DefaultPower() : inv;
	}

	// //////////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////////
	// IInventory

	@Override
	public int getSizeInventory() {
		return getInventory().getSizeInventory();
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return getInventory().getStackInSlot(index);
	}

	@Override
	public ItemStack decrStackSize(int index, int amount) {
		return getInventory().decrStackSize(index, amount);
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int var1) {
		return getInventory().getStackInSlotOnClosing(var1);
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack itemStack) {
		getInventory().setInventorySlotContents(index, itemStack);
	}

	@Override
	public String getInvName() {
		return getInventory().getInvName();
	}

	@Override
	public int getInventoryStackLimit() {
		return getInventory().getInventoryStackLimit();
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		return getInventory().isUseableByPlayer(entityplayer);
	}

	@Override
	public void openChest() {
		getInventory().openChest();
	}

	@Override
	public void closeChest() {
		getInventory().closeChest();
	}

	@Override
	public boolean isInvNameLocalized() {
		return getInventory().isInvNameLocalized();
	}

	@Override
	public boolean isStackValidForSlot(int slot, ItemStack itemStack) {
		return getInventory().isStackValidForSlot(slot, itemStack);
	}

	// //////////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////////
	// ISpecialInventory

	@Override
	public int addItem(ItemStack stack, boolean doAdd, ForgeDirection from) {
		return getInventory().addItem(stack, doAdd, from);
	}

	@Override
	public ItemStack[] extractItem(boolean doRemove, ForgeDirection from, int maxItemCount) {
		return getInventory().extractItem(doRemove, from, maxItemCount);
	}

	// //////////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////////
	// ISidedInventory

	@Override
	public int[] getAccessibleSlotsFromSide(int var1) {
		return getInventory().getAccessibleSlotsFromSide(var1);
	}

	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j) {
		return getInventory().canInsertItem(i, itemstack, j);
	}

	@Override
	public boolean canExtractItem(int i, ItemStack itemstack, int j) {
		return getInventory().canExtractItem(i, itemstack, j);
	}

	// //////////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////////
	// IInventory Slots

	@Override
	public final void addSlot(int index, String name) {
		getInventory().addSlot(index, name);
	}

	@Override
	public final void addSlotArray(int[] indexes, String name) {
		getInventory().addSlotArray(indexes, name);
	}

	@Override
	public InventorySlot getSlot(int index) {
		return getInventory().getSlot(index);
	}

	@Override
	public InventorySlot[] getAllSlots() {
		return getInventory().getAllSlots();
	}

	@Override
	public InventorySlot[] getSlots(int[] indexes) {
		return getInventory().getSlots(indexes);
	}

	// //////////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////////
	// IValidatedInventory Slots

	@Override
	public boolean isReadOnly(int slot) {
		return getInventory().isReadOnly(slot);
	}

	// //////////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////////
	// ITankContainer

	@Override
	public int fill(ForgeDirection from, LiquidStack resource, boolean doFill) {
		return getTankContainer().fill(from, resource, doFill);
	}

	@Override
	public int fill(int tankIndex, LiquidStack resource, boolean doFill) {
		return getTankContainer().fill(tankIndex, resource, doFill);
	}

	@Override
	public LiquidStack drain(ForgeDirection from, int maxDrain, boolean doDrain) {
		return getTankContainer().drain(from, maxDrain, doDrain);
	}

	@Override
	public LiquidStack drain(int tankIndex, int maxDrain, boolean doDrain) {
		return getTankContainer().drain(tankIndex, maxDrain, doDrain);
	}

	@Override
	public ILiquidTank[] getTanks(ForgeDirection direction) {
		return getTankContainer().getTanks(direction);
	}

	@Override
	public ILiquidTank getTank(ForgeDirection direction, LiquidStack type) {
		return getTankContainer().getTank(direction, type);
	}

	@Override
	public void setPowerProvider(IPowerProvider provider) {
	}

	@Override
	public IPowerProvider getPowerProvider() {
		return getPower().getPowerProvider();
	}

	@Override
	public void doWork() {
		getPower().doWork();
	}

	@Override
	public int powerRequest(ForgeDirection from) {
		return getPower().powerRequest(from);
	}

	@Override
	public PowerInfo getPowerInfo() {
		return getPower().getPowerInfo();
	}

	@Override
	public TankInfo[] getTankInfos() {
		return getTankContainer().getTankInfos();
	}

	@Override
	public boolean isTankReadOnly(int tank) {
		return getTankContainer().isTankReadOnly(tank);
	}

	@Override
	public boolean isLiquidValidForTank(LiquidStack liquid, int tank) {
		return getTankContainer().isLiquidValidForTank(liquid, tank);
	}

	@Override
	public void addTank(int index, String name, int capacity) {
		getTankContainer().addTank(index, name, capacity);
	}

	@Override
	public ILiquidTank getTank(int index) {
		return getTankContainer().getTank(index);
	}

	@Override
	public TankSlot getTankSlot(int index) {
		return getTankContainer().getTankSlot(index);
	}

	@Override
	public ILiquidTank[] getTanks() {
		return getTankContainer().getTanks();
	}

	@Override
	public void setConfig(int latency, int minEnergy, int maxEnergy, int activeEnergy, int storage) {
		getPower().setConfig(latency, minEnergy, maxEnergy, activeEnergy, storage);
	}

}