package binnie.core.machines;

import java.util.Random;

import binnie.core.machines.power.ITankMachine;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.ILiquidTank;
import net.minecraftforge.liquids.LiquidStack;


/**
 * Utility methods for inventories etc for use in other components
 *
 */
public class MachineUtil {

	public MachineUtil(IMachine machine) {
		super();
		this.machine = machine;
	}

	IMachine machine;
	
	
	private IInventory getInventory() {
		return machine.getInterface(IInventory.class);
	}
	
	private ITankMachine getTankContainer() {
		return machine.getInterface(ITankMachine.class);
	}
	
	
	public boolean isSlotEmpty(int slot) {
		return getInventory().getStackInSlot(slot) == null;
	}
	public ILiquidTank getTank(int id) {
		return getTankContainer().getTanks(ForgeDirection.UNKNOWN)[id];
	}
	
	public boolean spaceInTank(int id, int amount) {
		ILiquidTank tank = getTank(id);
		int space = tank.getCapacity() - (tank.getLiquid()==null ? 0 : tank.getLiquid().amount);
		return amount < space;
	}

	public ItemStack getStack(int slot) {
		return getInventory().getStackInSlot(slot);
	}

	public void deleteStack(int slot) {
		setStack(slot, null);
	}
	
	public ItemStack decreaseStack(int slotWood, int amount) {
		return getInventory().decrStackSize(slotWood, amount);
	}
	
	public void setStack(int slot, ItemStack stack) {
		getInventory().setInventorySlotContents(slot, stack);
	}

	public void fillTank(int id, LiquidStack liquidStack) {
		ILiquidTank tank = getTank(id);
		tank.fill(liquidStack, true);
	}

	public void addStack(int slot, ItemStack addition) {
		if(isSlotEmpty(slot)) {
			setStack(slot, addition);
		}
		else {
			ItemStack merge = getStack(slot);
			if(merge.isItemEqual(addition) && merge.stackSize + addition.stackSize <= merge.getMaxStackSize()) {
				merge.stackSize += addition.stackSize;
				setStack(slot, merge);
			}
		}
	}

	public void drainTank(int tank, int amount) {
		getTank(tank).drain(amount, true);
	}
	
	public boolean liquidInTank(int tank, int amount) {
		return getTank(tank).drain(amount, false) != null && getTank(tank).drain(amount, false).amount == amount;
	}
	public void damageItem(int slot, int damage) {
		ItemStack item = getStack(slot);
		if(damage < 0)
			item.setItemDamage(Math.max(0, item.getItemDamage()+damage));
		else {
			if(item.attemptDamageItem(damage, new Random())) 
				setStack(slot, null);
		}
		setStack(slot, item);
	}

}
