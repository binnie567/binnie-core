package binnie.core.machines;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.IInventory;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.liquids.ITankContainer;
import binnie.core.BinnieCore;
import binnie.core.machines.component.IInteraction;
import binnie.core.machines.network.INetwork;
import binnie.core.network.INetworkedEntity;
import binnie.core.network.packet.PacketPayload;
import binnie.craftgui.minecraft.INetworkedEntityGUI;
import buildcraft.api.power.IPowerReceptor;
import cpw.mods.fml.relauncher.Side;
import forestry.api.core.INBTTagable;

public class Machine implements INetworkedEntity, INBTTagable, INetworkedEntityGUI,
		INetwork.CraftGUIAction, IMachine {

	MachinePackage machinePackage;

	Map<Class, List<MachineComponent>> componentInterfaceMap = new LinkedHashMap<Class, List<MachineComponent>>();
	Map<Class<? extends MachineComponent>, MachineComponent> componentMap = new LinkedHashMap<Class<? extends MachineComponent>, MachineComponent>();

	TileEntity tile;

	public Machine(MachinePackage pack, TileEntity tile) {
		pack.createMachine(this);
		machinePackage = pack;
		this.tile = tile;
	}

	// Component Related Stuff

	public void addComponent(MachineComponent component) {
		if (component == null)
			throw new NullPointerException("Can't have a null machine component!");
		component.setMachine(this);
		componentMap.put(component.getClass(), component);
		for (Class inter : component.getComponentInterfaces()) {
			if (!componentInterfaceMap.containsKey(inter))
				componentInterfaceMap.put(inter, new ArrayList<MachineComponent>());
			componentInterfaceMap.get(inter).add(component);
		}

	}

	public Collection<MachineComponent> getComponents() {
		return componentMap.values();
	}

	public <T extends MachineComponent> T getComponent(Class<T> componentClass) {
		return hasComponent(componentClass) ? componentClass.cast(componentMap.get(componentClass))
				: null;
	}

	public <T> T getInterface(Class<T> interfaceClass) {
		if (hasInterface(interfaceClass))
			return getInterfaces(interfaceClass).get(0);
		if (interfaceClass.isInstance(getPackage()))
			return interfaceClass.cast(getPackage());
		for(MachineComponent component : getComponents())
			if(interfaceClass.isInstance(component))
				return interfaceClass.cast(component);
		return null;
	}

	public <T> List<T> getInterfaces(Class<T> interfaceClass) {
		ArrayList<T> interfaces = new ArrayList<T>();
		if (!hasInterface(interfaceClass))
			return interfaces;
		for (MachineComponent component : componentInterfaceMap.get(interfaceClass))
			interfaces.add(interfaceClass.cast(component));
		return interfaces;
	}

	public boolean hasInterface(Class<?> interfaceClass) {
		return componentInterfaceMap.containsKey(interfaceClass);
	}

	public boolean hasComponent(Class<? extends MachineComponent> componentClass) {
		return componentMap.containsKey(componentClass);
	}

	public TileEntity getTileEntity() {
		return tile;
	}

	public void sendPacket() {
		if (!BinnieCore.proxy.isSimulating(getTileEntity().getWorldObj()))
			return;

		BinnieCore.proxy.sendNetworkEntityPacket((INetworkedEntity) getTileEntity());
	}

	public Side getSide() {
		return BinnieCore.proxy.isSimulating(getTileEntity().worldObj) ? Side.SERVER : Side.CLIENT;
	}

	@Override
	public void writeToPacket(PacketPayload payload) {
		for (MachineComponent component : this.getComponents())
			if (component instanceof INetworkedEntity)
				((INetworkedEntity) component).writeToPacket(payload);
	}

	@Override
	public void readFromPacket(PacketPayload payload) {
		for (MachineComponent component : this.getComponents())
			if (component instanceof INetworkedEntity)
				((INetworkedEntity) component).readFromPacket(payload);
	}

	public void onRightClick(World world, EntityPlayer player, int x, int y, int z) {
		for (IInteraction.RightClick component : getInterfaces(IInteraction.RightClick.class))
			component.onRightClick(world, player, x, y, z);
	}

	public void onInventoryUpdate() {
		for (MachineComponent component : getComponents()) {
			if (component instanceof IInventory)
				component.onInventoryUpdate();
		}
		for (MachineComponent component : getComponents()) {
			if (!(component instanceof IInventory))
				component.onInventoryUpdate();
		}
	}

	public void onUpdate() {
		if (BinnieCore.proxy.isSimulating(getWorld())) {
			for (MachineComponent component : getComponents()) {
				component.onUpdate();
			}
		}
	}

	public IInventory getInventory() {
		return getInterface(IInventory.class);
	};

	public ITankContainer getTankContainer() {
		return getInterface(ITankContainer.class);
	};

	public IPowerReceptor getPowerReceptor() {
		return getInterface(IPowerReceptor.class);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		for (MachineComponent component : getComponents())
			component.readFromNBT(nbttagcompound);
	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {
		for (MachineComponent component : getComponents())
			component.writeToNBT(nbttagcompound);
	}

	public MachinePackage getPackage() {
		return machinePackage;
	};

	/*
	 * 
	 * private PacketMachine getPacket(MachinePayload payload) { return new
	 * PacketMachine(this, payload); }
	 * 
	 * @SideOnly(Side.CLIENT) public void sendDataToServer(MachinePayload
	 * payload) { PacketMachine packet = getPacket(payload);
	 * BinnieCore.proxy.sendToServer(packet, "BIN"); }
	 * 
	 * @SideOnly(Side.SERVER) public void sendDataToClients(MachinePayload
	 * payload) { PacketMachine packet = getPacket(payload);
	 * BinnieCore.proxy.sendNetworkPacket(packet, "BIN", getTileEntity().xCoord,
	 * getTileEntity().yCoord, getTileEntity().zCoord); }
	 * 
	 * @SideOnly(Side.SERVER) public void sendDataToClient(MachinePayload
	 * payload, EntityPlayer player) { PacketMachine packet =
	 * getPacket(payload); BinnieCore.proxy.sendToPlayer(packet, "BIN", player);
	 * }
	 * 
	 * public void recieveData(MachinePayload payload) { int id =
	 * payload.getID(); Class componentClass =
	 * MachineManager.getComponentClass(id); MachineComponent component =
	 * getComponent(componentClass); if(component != null)
	 * component.recieveData(payload); }
	 */

	@Override
	public void addGUINetworkData(Map<Integer, Integer> data) {
		for (INetwork.GUI component : getInterfaces(INetwork.GUI.class))
			component.addGUINetworkData(data);
	}

	@Override
	public void recieveGUINetworkData(int id, int data) {
		for (INetwork.GUI component : getInterfaces(INetwork.GUI.class))
			component.recieveGUINetworkData(id, data);
	}

	public static IMachine getMachine(Object inventory) {
		if (inventory != null && inventory instanceof IMachine)
			return (IMachine) inventory;
		if(inventory != null && inventory instanceof TileEntityMachine)
			return ((TileEntityMachine) inventory).getMachine();
		if(inventory != null && inventory instanceof MachineComponent)
			return ((MachineComponent)inventory).getMachine();
		return null;
	}

	public static <T> T getInterface(Class<T> interfac, Object inventory) {
		IMachine machine = getMachine(inventory);
		if (machine != null)
			return machine.getInterface(interfac);
		if (interfac.isInstance(inventory))
			return interfac.cast(inventory);
		return null;
	}

	public MachineUtil getMachineUtil() {
		return new MachineUtil(this);
	}

	@Override
	public void recieveNBT(Side side, EntityPlayer player, NBTTagCompound action) {
		for (INetwork.CraftGUIAction component : getInterfaces(INetwork.CraftGUIAction.class))
			component.recieveNBT(side, player, action);
	}

	public World getWorld() {
		return getTileEntity().getWorldObj();
	}

	public void onBlockDestroy() {
		for (MachineComponent component : getComponents())
			component.onDestruction();
	}

	int nextProgressBarID = 0;

	public int getUniqueProgressBarID() {
		return nextProgressBarID++;
	}

}
