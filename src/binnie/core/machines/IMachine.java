package binnie.core.machines;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public interface IMachine {

	void addComponent(MachineComponent component);

	MachineUtil getMachineUtil();

	int getUniqueProgressBarID();
	
	<T> T getInterface(Class<T> interfac);

	void onInventoryUpdate();

	World getWorld();

	TileEntity getTileEntity();

}
