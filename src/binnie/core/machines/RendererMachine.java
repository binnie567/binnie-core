package binnie.core.machines;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;

public class RendererMachine extends TileEntitySpecialRenderer implements
		ISimpleBlockRenderingHandler {
	
	public RendererMachine() {
	}


	@Override
	public void renderTileEntityAt(TileEntity entity, double x, double y,
			double z, float var8) {
		renderModel((TileEntityMachine) entity, x, y, z, var8);
	}

	public void renderModel(TileEntityMachine entity, double x, double y,
			double z, float var8) {
		if(entity != null && entity.getMachine() != null) {
			MachinePackage machinePackage = entity.getMachine().getPackage();
			machinePackage.renderMachine(entity.getMachine(), x, y, z, var8);
		}
	}

	public void renderInvBlock(RenderBlocks renderblocks, Block block, int i,
			int j) {

		TileEntity entity = block.createTileEntity(null, i);
		renderModel((TileEntityMachine) entity, 0.0, -0.1, 0.0, 0.0625F);

	}

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID,
			RenderBlocks renderer) {
		if (modelID == MachineManager.getMachineRenderID()) {
			this.renderInvBlock(renderer, block, metadata, modelID);
		}
	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z,
			Block block, int modelId, RenderBlocks renderer) {
		return false;
	}

	@Override
	public boolean shouldRender3DInInventory() {
		return true;
	}

	@Override
	public int getRenderId() {
		return MachineManager.getMachineRenderID();
	}

}
