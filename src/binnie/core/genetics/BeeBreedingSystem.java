package binnie.core.genetics;

import net.minecraft.entity.player.EntityPlayer;
import binnie.core.resource.ResourceManager;
import binnie.extrabees.ExtraBees;
import forestry.api.apiculture.EnumBeeChromosome;
import forestry.api.apiculture.IApiaristTracker;
import forestry.api.apiculture.IBeeMutation;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IBreedingTracker;
import forestry.api.genetics.IMutation;
import forestry.api.genetics.ISpeciesRoot;

public class BeeBreedingSystem extends BreedingSystem {

	public BeeBreedingSystem() {
		super();
		iconUndiscovered = ResourceManager.getItemIcon(ExtraBees.instance, "icon/undiscoveredBee");
		iconDiscovered = ResourceManager.getItemIcon(ExtraBees.instance, "icon/discoveredBee");
	}
	
	@Override
	public float getChance(IMutation mutation, EntityPlayer player, IAllele species1, IAllele species2) {
		return ((IBeeMutation)mutation).getChance(new VirtualBeeHousing(player), species1, species2, getSpeciesRoot()
				.templateAsGenome(getSpeciesRoot()
						.getTemplate(species1.getUID())),
				getSpeciesRoot()
						.templateAsGenome(getSpeciesRoot()
								.getTemplate(species2.getUID())));
	}
	
	public String getEpitome(float discoveredPercentage) {
		if(discoveredPercentage == 1f) {
			return "Sengir's Chosen";
		}
		else if(discoveredPercentage < 0.1f) {
			return "Bee Swatter";
		}
		else if(discoveredPercentage < 0.3f) {
			return "Amateur Beekeeper";
		}
		else if(discoveredPercentage < 0.5f) {
			return "Apiarist-In-Training";		
		}
		else if(discoveredPercentage < 0.7f) {
			return "Professional Beekeeper";
		}
		else if(discoveredPercentage < 0.9f) {
			return "Master Apiarist";
		}
		else if(discoveredPercentage < 1f) {
			return "God amoungst Apiarists";
		}
		return "";
	}

	@Override
	public String getDescriptor() {
		return "Apiarist";
	}

	@Override
	public ISpeciesRoot getSpeciesRoot() {
		return AlleleManager.alleleRegistry.getSpeciesRoot("rootBees");
	}
	@Override
	public String getChromosomeName(int i) {
		EnumBeeChromosome chromo = (EnumBeeChromosome) getChromosome(i);
		switch(chromo) {
		case CAVE_DWELLING:
			return "Cave Dwelling";
		case EFFECT:
			return "Effect";
		case FERTILITY:
			return "Fertility";
		case FLOWERING:
			return "Pollination";
		case FLOWER_PROVIDER:
			return "Flowers";
		case HUMIDITY:
			return "Humidity";
		case HUMIDITY_TOLERANCE:
			return "Humidity Tolerance";
		case LIFESPAN:
			return "Lifespan";
		case NOCTURNAL:
			return "Nocturnal";
		case SPECIES:
			return "Species";
		case SPEED:
			return "Production";
		case TEMPERATURE_TOLERANCE:
			return "Temperature Tolerance";
		case TERRITORY:
			return "Territory";
		case TOLERANT_FLYER:
			return "Rain Tolerance";
		default:
			return "";
		}
	}

	@Override
	public int getColour() {
		return 0xFFD900;
	}

	@Override
	public Class<? extends IBreedingTracker> getTrackerClass() {
		return IApiaristTracker.class;
	}

}
