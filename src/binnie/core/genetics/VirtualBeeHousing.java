package binnie.core.genetics;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.apiculture.IBeeHousing;
import forestry.api.core.EnumHumidity;
import forestry.api.core.EnumTemperature;
import forestry.api.genetics.IIndividual;

public class VirtualBeeHousing implements IBeeHousing {

	EntityPlayer player;

	public VirtualBeeHousing(EntityPlayer player) {
		this.player = player;
	}

	@Override
	public float getTerritoryModifier(IBeeGenome genome, float currentModifier) {
		// TODO Auto-generated method stub
		return 1.0f;
	}

	@Override
	public float getMutationModifier(IBeeGenome genome, IBeeGenome mate, float currentModifier) {
		// TODO Auto-generated method stub
		return 1.0f;
	}

	@Override
	public float getLifespanModifier(IBeeGenome genome, IBeeGenome mate, float currentModifier) {
		// TODO Auto-generated method stub
		return 1.0f;
	}

	@Override
	public float getProductionModifier(IBeeGenome genome, float currentModifier) {
		// TODO Auto-generated method stub
		return 1.0f;
	}

	@Override
	public int getXCoord() {
		// TODO Auto-generated method stub
		return player.serverPosX;
	}

	@Override
	public int getYCoord() {
		// TODO Auto-generated method stub
		return player.serverPosY;
	}

	@Override
	public int getZCoord() {
		// TODO Auto-generated method stub
		return player.serverPosZ;
	}

	@Override
	public ItemStack getQueen() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack getDrone() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setQueen(ItemStack itemstack) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDrone(ItemStack itemstack) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getBiomeId() {
		// TODO Auto-generated method stub
		return player.worldObj.getBiomeGenForCoords(player.serverPosX,
				player.serverPosY).biomeID;
	}

	@Override
	public EnumTemperature getTemperature() {
		return EnumTemperature.NORMAL;
	}

	@Override
	public EnumHumidity getHumidity() {
		// TODO Auto-generated method stub
		return EnumHumidity.NORMAL;
	}

	@Override
	public World getWorld() {
		// TODO Auto-generated method stub
		return player.worldObj;
	}

	@Override
	public void setErrorState(int state) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getErrorOrdinal() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean canBreed() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean addProduct(ItemStack product, boolean all) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void wearOutEquipment(int amount) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onQueenChange(ItemStack queen) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isSealed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSelfLighted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSunlightSimulated() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isHellish() {
		// TODO Auto-generated method stub
		return getBiomeId() == BiomeGenBase.hell.biomeID;
	}

	@Override
	public String getOwnerName() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public float getFloweringModifier(IBeeGenome genome, float currentModifier) {
		// TODO Auto-generated method stub
		return 1f;
	}

	@Override
	public void onQueenDeath(IBee queen) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPostQueenDeath(IBee queen) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onPollenRetrieved(IBee queen, IIndividual pollen,
			boolean isHandled) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean onEggLaid(IBee queen) {
		// TODO Auto-generated method stub
		return false;
	}

}
