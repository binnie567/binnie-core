package binnie.core.genetics;

import net.minecraft.world.World;
import forestry.api.apiculture.IBeeRoot;
import forestry.api.arboriculture.ITreeRoot;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.ISpeciesRoot;
import forestry.api.lepidopterology.IButterflyRoot;

public class BinnieGenetics {
	public static BreedingSystem beeBreedingSystem;
	public static BreedingSystem treeBreedingSystem;
	public static BreedingSystem mothBreedingSystem;
	

	public static boolean isSpeciesDiscovered(IAlleleSpecies species,
			World world, boolean nei) {
		return true;
	}

	public static ITreeRoot getTreeRoot() {
		return (ITreeRoot) AlleleManager.alleleRegistry.getSpeciesRoot("rootTrees");
	}
	
	public static IBeeRoot getBeeRoot() {
		return (IBeeRoot) AlleleManager.alleleRegistry.getSpeciesRoot("rootBees");
	}
	
	public static IButterflyRoot getButterflyRoot() {
		return (IButterflyRoot) AlleleManager.alleleRegistry.getSpeciesRoot("rootButterflies");
	}

	public static BreedingSystem getSystem(String string) {
		for(BreedingSystem system : BreedingSystem.BREEDING_SYSTEMS.values())
			if(system.getIdent().equals(string))
				return system;
		return null;
	}

	public static ISpeciesRoot getSpeciesRoot(IAlleleSpecies species) {
		for(ISpeciesRoot root : AlleleManager.alleleRegistry.getSpeciesRoot().values())
			if(root.getKaryotype()[0].getAlleleClass().isInstance(species))
				return root;
		return null;
	}
}
