package binnie.core.genetics;

import net.minecraft.entity.player.EntityPlayer;
import binnie.core.resource.ResourceManager;
import binnie.extratrees.ExtraTrees;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IBreedingTracker;
import forestry.api.genetics.IMutation;
import forestry.api.genetics.ISpeciesRoot;
import forestry.api.lepidopterology.EnumButterflyChromosome;
import forestry.api.lepidopterology.ILepidopteristTracker;

public class MothBreedingSystem extends BreedingSystem {

	public MothBreedingSystem() {
		super();
		iconUndiscovered = ResourceManager.getItemIcon(ExtraTrees.instance, "icon/undiscoveredMoth");
		iconDiscovered = ResourceManager.getItemIcon(ExtraTrees.instance, "icon/discoveredMoth");
	}
	
	
	@Override
	public float getChance(IMutation mutation, EntityPlayer player,
			IAllele species1, IAllele species2) {
		return 0;
	}

	@Override
	public String getDescriptor() {
		return "Lepidopterist";
	}

	@Override
	public ISpeciesRoot getSpeciesRoot() {
		return BinnieGenetics.getButterflyRoot();
	}
	
	@Override
	public String getChromosomeName(int i) {
		EnumButterflyChromosome chromo = (EnumButterflyChromosome) getChromosome(i);
		switch(chromo) {
		case EFFECT:
			return "Effect";
		case FERTILITY:
			return "Fertility";
		case FIRE_RESIST:
			return "Fire Resistance";
		case FLOWER_PROVIDER:
			return "Flowers";
		case HUMIDITY_TOLERANCE:
			return "Humidity Tolerance";
		case LIFESPAN:
			return "Lifespan";
		case METABOLISM:
			return "Metabolism";
		case NOCTURNAL:
			return "Nocturnal";
		case SIZE:
			return "Size";
		case SPECIES:
			return "Species";
		case SPEED:
			return "Production";
		case TEMPERATURE_TOLERANCE:
			return "Temperature Tolerance";
		case TOLERANT_FLYER:
			return "Rain Tolerance";
		default:
			return "";
		
		}
	}
	
	public String getEpitome(float discoveredPercentage) {
		if(discoveredPercentage == 1f) {
			return "Monarch of the Butterflies";
		}
		else if(discoveredPercentage < 0.1f) {
			return "Bug Catcher";
		}
		else if(discoveredPercentage < 0.3f) {
			return "Moth Chaser";
		}
		else if(discoveredPercentage < 0.5f) {
			return "Scoop Wizard";		
		}
		else if(discoveredPercentage < 0.7f) {
			return "Caterpiller Lord";
		}
		else if(discoveredPercentage < 0.9f) {
			return "Winged Assassin";
		}
		else if(discoveredPercentage < 1f) {
			return "Moth King";
		}
		return "";
	}

	@Override
	public int getColour() {
		return 0x00F2F2;
	}
	
	@Override
	public Class<? extends IBreedingTracker> getTrackerClass() {
		return ILepidopteristTracker.class;
	}

}
