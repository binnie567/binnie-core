package binnie.core.genetics;

import net.minecraft.item.ItemStack;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IClassification;
import forestry.api.genetics.ISpeciesRoot;

public interface IBreedingMessage {



	String getTitle();
	String getBody();
	ItemStack getIcon();
	
	
	public static class MessageSpeciesDiscovered implements IBreedingMessage{
		
		IAlleleSpecies species;
		ItemStack stack;
		
		public MessageSpeciesDiscovered(IAlleleSpecies species) {
			this.species = species;
			ISpeciesRoot root = null;
			for(ISpeciesRoot sRoot : AlleleManager.alleleRegistry.getSpeciesRoot().values()) {
				if(sRoot.getKaryotype()[0].getAlleleClass().isInstance(species))
						root = sRoot;
			}
			if(root != null) {
				stack = root.getMemberStack(root.templateAsIndividual(
						root.getTemplate(species.getUID())), 0);
			}
		}
		
		public String getTitle() {
			return "Species Discovered";
		}
		
		public String getBody() {
			return species.getName();
		}

		public ItemStack getIcon() {
			return stack;
		}
	}
	
	
	
	public static class BranchDiscovered implements IBreedingMessage {
		
		IAlleleSpecies species;
		IClassification classification;
		ItemStack stack;
		
		public BranchDiscovered(IAlleleSpecies species, IClassification classification) {
			this.species = species;
			this.classification = classification;
			ISpeciesRoot root = null;
			for(ISpeciesRoot sRoot : AlleleManager.alleleRegistry.getSpeciesRoot().values()) {
				if(sRoot.getKaryotype()[0].getAlleleClass().isInstance(species))
						root = sRoot;
			}
			if(root != null) {
				stack = root.getMemberStack(root.templateAsIndividual(
						root.getTemplate(species.getUID())), 0);
			}
		}
		
		public String getTitle() {
			return "Branch Discovered";
		}
		
		public String getBody() {
			return classification.getScientific();
		}

		public ItemStack getIcon() {
			return stack;
		}
	}
	
	
	
	public static class EpithetGained implements IBreedingMessage {
		
		String epithet;
		ItemStack stack;
		
		public EpithetGained(String epithet, ISpeciesRoot root) {
			this.epithet = epithet;
				stack = root.getMemberStack(root.templateAsIndividual(
						root.getDefaultTemplate()), 0);
		}
		
		public String getTitle() {
			return "Epithet Gained";
		}
		
		public String getBody() {
			return epithet;
		}

		public ItemStack getIcon() {
			return stack;
		}
	}



}
