package binnie.core.genetics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import binnie.core.resource.BinnieIcon;
import forestry.api.core.ForestryEvent;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IBreedingTracker;
import forestry.api.genetics.IChromosomeType;
import forestry.api.genetics.IClassification;
import forestry.api.genetics.IMutation;
import forestry.api.genetics.ISpeciesRoot;

public abstract class BreedingSystem {

	protected BinnieIcon iconUndiscovered;
	protected BinnieIcon iconDiscovered;
	
	public static final Map<ISpeciesRoot, BreedingSystem> BREEDING_SYSTEMS = new HashMap<ISpeciesRoot, BreedingSystem>();

	public BreedingSystem() {
		BREEDING_SYSTEMS.put(this.getSpeciesRoot(), this);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public abstract ISpeciesRoot getSpeciesRoot();

	List<IClassification> allBranches = new ArrayList<IClassification>();
	List<IAlleleSpecies> allSpecies = new ArrayList<IAlleleSpecies>();
	List<IMutation> allMutations = new ArrayList<IMutation>();

	Map<IAlleleSpecies, List<IMutation>> resultantMutations = new HashMap<IAlleleSpecies, List<IMutation>>();
	Map<IAlleleSpecies, List<IMutation>> furtherMutations = new HashMap<IAlleleSpecies, List<IMutation>>();

	public final List<IClassification> getAllBranches() {
		return allBranches;
	}

	public final Collection<IAlleleSpecies> getAllSpecies() {
		return allSpecies;
	}

	public final Collection<IMutation> getAllMutations() {
		return allMutations;
	}

	public final void calculateArrays() {

		// All Species

		Collection<IAllele> allAlleles = AlleleManager.alleleRegistry.getRegisteredAlleles()
				.values();

		resultantMutations = new HashMap<IAlleleSpecies, List<IMutation>>();
		furtherMutations = new HashMap<IAlleleSpecies, List<IMutation>>();

		allSpecies = new ArrayList<IAlleleSpecies>();
		for (IAllele species : allAlleles) {
			if (getSpeciesRoot().getTemplate(species.getUID()) != null) {
				resultantMutations.put((IAlleleSpecies) species, new ArrayList<IMutation>());
				furtherMutations.put((IAlleleSpecies) species, new ArrayList<IMutation>());
				if (!isBlacklisted(species))
					allSpecies.add((IAlleleSpecies) species);
			}

		}

		allMutations = new ArrayList<IMutation>();

		Collection<IClassification> allRegBranches = AlleleManager.alleleRegistry
				.getRegisteredClassifications().values();

		allBranches = new ArrayList<IClassification>();

		for (IClassification branch : allRegBranches) {
			if (branch.getMemberSpecies().length > 0)
				if (getSpeciesRoot().getTemplate(branch.getMemberSpecies()[0].getUID()) != null) {
					boolean possible = false;

					for (IAlleleSpecies species : branch.getMemberSpecies()) {
						if (allSpecies.contains(species))
							possible = true;
					}

					if (possible)
						allBranches.add(branch);

				}

		}
		if (getSpeciesRoot().getMutations(false) != null) {
			for (IMutation mutation : getSpeciesRoot().getMutations(false)) {

				allMutations.add(mutation);

				Set<IAlleleSpecies> participatingSpecies = new LinkedHashSet<IAlleleSpecies>();
				if (mutation.getAllele0() instanceof IAlleleSpecies
						&& allSpecies.contains(mutation.getAllele0()))
					participatingSpecies.add((IAlleleSpecies) mutation.getAllele0());
				if (mutation.getAllele1() instanceof IAlleleSpecies
						&& allSpecies.contains(mutation.getAllele1()))
					participatingSpecies.add((IAlleleSpecies) mutation.getAllele1());
				for (IAlleleSpecies species : participatingSpecies) {
					furtherMutations.get(species).add(mutation);
				}

				if (resultantMutations.containsKey(mutation.getTemplate()[0]))
					resultantMutations.get(mutation.getTemplate()[0]).add(mutation);
			}
		}

	}

	public final boolean isBlacklisted(IAllele allele) {
		return AlleleManager.alleleRegistry.isBlacklisted(allele.getUID());
	}

	public final List<IMutation> getResultantMutations(IAlleleSpecies species) {
		return resultantMutations.get(species);
	}

	public final List<IMutation> getFurtherMutations(IAlleleSpecies species) {
		return furtherMutations.get(species);
	}

	public final boolean isMutationDiscovered(IMutation mutation, World world, String name) {
		return isMutationDiscovered(mutation, getSpeciesRoot().getBreedingTracker(world, name));
	}

	public final boolean isMutationDiscovered(IMutation mutation, IBreedingTracker tracker) {
		if (tracker == null)
			return true;
		return tracker.isDiscovered(mutation);
	}

	public final boolean isSpeciesDiscovered(IAlleleSpecies species, World world, String name) {
		return isSpeciesDiscovered(species, getSpeciesRoot().getBreedingTracker(world, name));
	}

	public final boolean isSpeciesDiscovered(IAlleleSpecies species, IBreedingTracker tracker) {
		if (tracker == null)
			return true;
		return tracker.isDiscovered(species);
	}

	public final boolean isSecret(IAlleleSpecies species) {
		return !species.isCounted();
	}

	public final boolean isSecret(IClassification branch) {
		for (IAlleleSpecies species : branch.getMemberSpecies())
			if (!isSecret(species))
				return false;
		return true;
	}

	public final Collection<IClassification> getDiscoveredBranches(World world, String player) {
		List<IClassification> branches = new ArrayList<IClassification>();
		for (IClassification branch : getAllBranches()) {
			boolean discovered = false;
			for (IAlleleSpecies species : branch.getMemberSpecies())
				if (isSpeciesDiscovered(species, world, player))
					discovered = true;
			if (discovered)
				branches.add(branch);
		}
		return branches;
	}

	public final Collection<IClassification> getDiscoveredBranches(IBreedingTracker tracker) {
		List<IClassification> branches = new ArrayList<IClassification>();
		for (IClassification branch : getAllBranches()) {
			boolean discovered = false;
			for (IAlleleSpecies species : branch.getMemberSpecies())
				if (isSpeciesDiscovered(species, tracker))
					discovered = true;
			if (discovered)
				branches.add(branch);
		}
		return branches;
	}

	public final Collection<IAlleleSpecies> getDiscoveredSpecies(World world, String player) {
		List<IAlleleSpecies> speciesList = new ArrayList<IAlleleSpecies>();

		for (IAlleleSpecies species : getAllSpecies())
			if (isSpeciesDiscovered(species, world, player))
				speciesList.add(species);

		return speciesList;
	}

	public final Collection<IAlleleSpecies> getDiscoveredSpecies(IBreedingTracker tracker) {
		List<IAlleleSpecies> speciesList = new ArrayList<IAlleleSpecies>();

		for (IAlleleSpecies species : getAllSpecies())
			if (isSpeciesDiscovered(species, tracker))
				speciesList.add(species);

		return speciesList;
	}

	public final List<IMutation> getDiscoveredMutations(World world, String player) {
		List<IMutation> speciesList = new ArrayList<IMutation>();

		for (IMutation species : getAllMutations())
			if (isMutationDiscovered(species, world, player))
				speciesList.add(species);

		return speciesList;
	}

	public final List<IMutation> getDiscoveredMutations(IBreedingTracker tracker) {
		List<IMutation> speciesList = new ArrayList<IMutation>();

		for (IMutation species : getAllMutations())
			if (isMutationDiscovered(species, tracker))
				speciesList.add(species);

		return speciesList;
	}
	
	public final int getDiscoveredBranchMembers(IClassification branch, IBreedingTracker tracker) {
		int discoveredSpecies = 0;
		for(IAlleleSpecies species : branch.getMemberSpecies()) {
			if(isSpeciesDiscovered(species, tracker))
				discoveredSpecies++;
		}
		return discoveredSpecies;
	}

	public Icon getUndiscoveredIcon() {
		return iconUndiscovered.getIcon();
	}

	public Icon getDiscoveredIcon() {
		return iconDiscovered.getIcon();
	}

	public abstract float getChance(IMutation mutation, EntityPlayer player, IAllele species1,
			IAllele species2);

	public abstract Class<? extends IBreedingTracker> getTrackerClass();

	public float discoveredSpeciesPercentage;
	public int totalSpeciesCount;
	public int discoveredSpeciesCount;
	public int totalSecretCount;
	public int discoveredSecretCount;
	
	public float discoveredBranchPercentage;
	public int totalBranchCount;
	public int discoveredBranchCount;
	public int totalSecretBranchCount;
	public int discoveredSecretBranchCount;

	String currentEpithet = getEpitome(0f);
	
	@ForgeSubscribe
	public void onSyncBreedingTracker(ForestryEvent.SyncedBreedingTracker event) {

		
		
		IBreedingTracker tracker = event.tracker;

		if (!getTrackerClass().isInstance(tracker))
			return;

		discoveredSpeciesPercentage = 0f;

		totalSpeciesCount = 0;
		discoveredSpeciesCount = 0;

		totalSecretCount = 0;
		discoveredSecretCount = 0;

		Collection<IAlleleSpecies> discoveredSpecies = getDiscoveredSpecies(tracker);
		Collection<IAlleleSpecies> allSpecies = getAllSpecies();

		for (IAlleleSpecies species : allSpecies) {
			if (!isSecret(species)) {
				totalSpeciesCount++;
				if (isSpeciesDiscovered(species, tracker))
					discoveredSpeciesCount++;
			} else {
				totalSecretCount++;
				if (isSpeciesDiscovered(species, tracker))
					discoveredSecretCount++;
			}

		}
		
		discoveredBranchPercentage = 0f;

		totalBranchCount = 0;
		discoveredBranchCount = 0;

		Collection<IClassification> discoveredBranches = getDiscoveredBranches(tracker);
		Collection<IClassification> allBranches = getAllBranches();

		for (IClassification branch : allBranches) {
			if(!isSecret(branch)) {
				totalBranchCount++;
				if (discoveredBranches.contains(branch))
					discoveredBranchCount++;
			}
			else {
				totalSecretBranchCount++;
				if (discoveredBranches.contains(branch))
					discoveredSecretBranchCount++;
			}
			
		}

		discoveredSpeciesPercentage = ((float) discoveredSpeciesCount)
				/ ((float) totalSpeciesCount);
		discoveredBranchPercentage = ((float) discoveredBranchCount)
				/ ((float) totalBranchCount);
		
		
		String epithet = getEpitome();
		
		if(!epithet.equals(currentEpithet)) {
			currentEpithet = epithet;
			BreedingMessageHandler.instance.messages.add(new IBreedingMessage.EpithetGained(epithet, getSpeciesRoot()));
		}
			

	}

	public String getEpitome(float discoveredPercentage) {
		return getDescriptor();
	}

	public String getEpitome() {
		return getEpitome(discoveredSpeciesPercentage);
	}

	public abstract String getDescriptor();

	public final String getIdent() {
		return getSpeciesRoot().getUID();
	}

	public final IChromosomeType getChromosome(int i) {
		for (IChromosomeType chromosome : getSpeciesRoot().getKaryotype())
			if (i == chromosome.ordinal())
				return chromosome;
		return null;
	}

	public abstract String getChromosomeName(int i);

	public abstract int getColour();

	public static BreedingSystem get(ISpeciesRoot root) {
		return BinnieGenetics.getSystem(root.getUID());
	}

}