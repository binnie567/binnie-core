package binnie.core.genetics;

import java.util.EnumSet;
import java.util.LinkedList;
import java.util.Queue;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.event.ForgeSubscribe;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import binnie.core.BinnieCore;
import binnie.core.resource.BinnieResource;
import binnie.core.resource.ResourceManager;
import binnie.core.resource.ResourceType;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IBreedingTracker;
import forestry.api.genetics.IClassification;
import forestry.api.genetics.ISpeciesRoot;

@SideOnly(Side.CLIENT)
public class BreedingMessageHandler implements ITickHandler {

	public static BreedingMessageHandler instance;

	/** Holds the instance of the game (Minecraft) */
	private Minecraft theGame;

	/** Holds the latest width scaled to fit the game window. */
	private int achievementWindowWidth;

	/** Holds the latest height scaled to fit the game window. */
	private int achievementWindowHeight;

	/** Holds the achievement that will be displayed on the GUI. */
	private IBreedingMessage currentMessage;
	private long messageTime;
	
	BinnieResource resource = ResourceManager.getPNG(BinnieCore.instance, ResourceType.GUI, "message");

	/**
	 * Holds a instance of RenderItem, used to draw the achievement icons on
	 * screen (is based on ItemStack)
	 */
	private RenderItem itemRender;

	Queue<IBreedingMessage> messages = new LinkedList<IBreedingMessage>();

	public BreedingMessageHandler(Minecraft par1Minecraft) {
		this.theGame = par1Minecraft;
		this.itemRender = new RenderItem();
		instance = this;
	}

	@Override
	public void tickStart(EnumSet<TickType> type, Object... tickData) {
	}

	@Override
	public void tickEnd(EnumSet<TickType> type, Object... tickData) {
	}

	@Override
	public EnumSet<TickType> ticks() {
		return EnumSet.of(TickType.RENDER);
	}

	@Override
	public String getLabel() {
		return "Binnie";
	}

	@ForgeSubscribe
	public void onRender(RenderGameOverlayEvent.Post event) {
		if (event.type != ElementType.HOTBAR)
			return;

		float percentageTime = ((theGame.getSystemTime() - messageTime) / 3000f);
		if (percentageTime < 0f || percentageTime >= 1f)
			currentMessage = null;

		if (currentMessage == null) {
			currentMessage = messages.poll();
			messageTime = theGame.getSystemTime();
			if (currentMessage == null)
				return;
		}

		String messageTitle = currentMessage.getTitle();
		String messageBody = currentMessage.getBody();
		ItemStack icon = currentMessage.getIcon();

		GuiIngame gui = theGame.ingameGUI;

		int width = 160;
		int i = event.resolution.getScaledWidth() - width;
		int j = 0;

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		BinnieCore.proxy.bindTexture(resource);
		GL11.glDisable(GL11.GL_LIGHTING);
		gui.drawTexturedModalRect(i, j, 96, 202, width, 32);

		this.theGame.fontRenderer.drawString(messageTitle, i + 30, j + 7, 0xFFFFFF);
		this.theGame.fontRenderer.drawString(messageBody, i + 30, j + 18, 0xFFFFFF);

		RenderHelper.enableGUIStandardItemLighting();
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		GL11.glEnable(GL11.GL_COLOR_MATERIAL);
		GL11.glEnable(GL11.GL_LIGHTING);
		itemRender.renderItemAndEffectIntoGUI(this.theGame.fontRenderer, this.theGame.renderEngine,
				icon, i + 8, j + 8);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
	}

	public void recieve(NBTTagCompound nbt, EntityPlayer player) {
		if(nbt.hasKey("species")) {
			IAllele allele = AlleleManager.alleleRegistry.getAllele(nbt.getString("species"));
			if(!(allele instanceof IAlleleSpecies)) return;
			IAlleleSpecies species = (IAlleleSpecies) allele;
			messages.add(new IBreedingMessage.MessageSpeciesDiscovered(species));
			
			ISpeciesRoot root = null;
			for(ISpeciesRoot root2 : AlleleManager.alleleRegistry.getSpeciesRoot().values())
				if(root2.getKaryotype()[0].getAlleleClass().isInstance(allele))
					root = root2;
			
			if(root == null) return;
			
			IBreedingTracker tracker = root.getBreedingTracker(player.worldObj, player.username);
			BreedingSystem system = BreedingSystem.BREEDING_SYSTEMS.get(root);
			IClassification branch = species.getBranch();
			if(branch == null) return;
			int discovedBranchSpecies = system.getDiscoveredBranchMembers(branch, tracker);
			if(discovedBranchSpecies == 1)
				messages.add(new IBreedingMessage.BranchDiscovered(species, branch));
		}
	}
}
