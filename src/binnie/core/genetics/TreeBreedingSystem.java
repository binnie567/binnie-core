package binnie.core.genetics;

import net.minecraft.entity.player.EntityPlayer;
import binnie.core.resource.ResourceManager;
import binnie.extratrees.ExtraTrees;
import forestry.api.arboriculture.EnumTreeChromosome;
import forestry.api.arboriculture.IArboristTracker;
import forestry.api.arboriculture.ITreeMutation;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IBreedingTracker;
import forestry.api.genetics.IGenome;
import forestry.api.genetics.IMutation;
import forestry.api.genetics.ISpeciesRoot;

public class TreeBreedingSystem extends BreedingSystem {

	public TreeBreedingSystem() {
		super();
		iconUndiscovered = ResourceManager.getItemIcon(ExtraTrees.instance, "icon/undiscoveredTree");
		iconDiscovered = ResourceManager.getItemIcon(ExtraTrees.instance, "icon/discoveredTree");
	}
	
	
	@Override
	public float getChance(IMutation mutation, EntityPlayer player,
			IAllele species1, IAllele species2) {
		IGenome genome0 = getSpeciesRoot().templateAsGenome(getSpeciesRoot().getTemplate(species1.getUID()));
		IGenome genome1 = getSpeciesRoot().templateAsGenome(getSpeciesRoot().getTemplate(species2.getUID()));
		return ((ITreeMutation)mutation).getChance(player.worldObj, (int)player.posX, (int)player.posY, (int)player.posZ, species1, species2, genome0, genome1);
	}
	
	@Override
	public String getDescriptor() {
		return "Arborist";
	}
	
	public String getEpitome(float discoveredPercentage) {
		if(discoveredPercentage == 1f) {
			return "Mother Nature";
		}
		else if(discoveredPercentage < 0.1f) {
			return "Wannabe Tree Farmer";
		}
		else if(discoveredPercentage < 0.3f) {
			return "Lumberjack";
		}
		else if(discoveredPercentage < 0.5f) {
			return "Green Thumbed";		
		}
		else if(discoveredPercentage < 0.7f) {
			return "Renowned Farmer";
		}
		else if(discoveredPercentage < 0.9f) {
			return "Agricultural Master";
		}
		else if(discoveredPercentage < 1f) {
			return "Lord of the Trees";
		}
		return "";
	}

	@Override
	public ISpeciesRoot getSpeciesRoot() {
		return AlleleManager.alleleRegistry.getSpeciesRoot("rootTrees");
	}
	
	@Override
	public String getChromosomeName(int i) {
		EnumTreeChromosome chromo = (EnumTreeChromosome) getChromosome(i);
		switch(chromo) {
		case EFFECT:
			return "Effect";
		case FERTILITY:
			return "Fertility";
		case FRUITS:
			return "Fruit";
		case GIRTH:
			return "Girth";
		case GROWTH:
			return "Growth";
		case HEIGHT:
			return "Height";
		case MATURATION:
			return "Maturation";
		case PLANT:
			return "Plant Type";
		case SAPPINESS:
			return "Sappiness";
		case SPECIES:
			return "Species";
		case TERRITORY:
			return "Territory";
		case YIELD:
			return "Yield";
		default:
			return "";
		
		}
	}

	@Override
	public int getColour() {
		return 0x00CF0E;
	}
	
	@Override
	public Class<? extends IBreedingTracker> getTrackerClass() {
		return IArboristTracker.class;
	}

}
