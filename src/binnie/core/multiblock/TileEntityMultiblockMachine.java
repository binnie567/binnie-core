package binnie.core.multiblock;

import net.minecraft.tileentity.TileEntity;
import binnie.core.machines.Machine;
import binnie.core.machines.TileEntityMachine;

public class TileEntityMultiblockMachine extends TileEntity {

	boolean inStructure;
	int tileX;
	int tileY;
	int tileZ;

	boolean inStructure() {
		return inStructure;
	}

	public Machine getMachine() {
		return getMasterMachine();
	}

	private Machine getMasterMachine() {
		if(!inStructure) return null;
		TileEntity tile = worldObj.getBlockTileEntity(xCoord + tileX, yCoord
				+ tileY, zCoord + tileZ);
		if (tile instanceof TileEntityMachine) {
			return ((TileEntityMachine) tile).getMachine();
		}
		return null;
	}

}
