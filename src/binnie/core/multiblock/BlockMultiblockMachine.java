package binnie.core.multiblock;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockMultiblockMachine extends BlockContainer {

	public BlockMultiblockMachine(int id, String blockName) {
		super(id, Material.iron);
		setHardness(1.5f);
		setUnlocalizedName(blockName);
	}

	@Override
	public TileEntity createTileEntity(World world, int metadata) {
		return new TileEntityMultiblockMachine();
	}

	@Override
	public TileEntity createNewTileEntity(World var1) {
		return new TileEntityMultiblockMachine();
	}

}
