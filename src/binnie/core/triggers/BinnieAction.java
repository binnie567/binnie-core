package binnie.core.triggers;

import net.minecraft.client.renderer.IconFlipped;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;
import binnie.core.BinnieCore;
import binnie.core.IBinnieMod;
import binnie.core.resource.BinnieIcon;
import binnie.core.resource.BinnieResource;
import binnie.core.resource.ResourceManager;
import binnie.extrabees.ExtraBees;
import buildcraft.api.core.IIconProvider;
import buildcraft.api.gates.ActionManager;
import buildcraft.api.gates.IAction;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BinnieAction implements IAction {

	public static int incrementalID = 800;

	public static IAction actionPauseProcess;
	public static IAction actionCancelTask;

	String desc;
	BinnieIcon icon;
	public int id = 0;
	
	public BinnieAction(String desc,String iconFile) {
		this(desc, BinnieCore.instance, iconFile);
	}
	
	public BinnieAction(String desc, IBinnieMod mod, String iconFile) {
		id = (incrementalID++);
		ActionManager.actions[id] = this;
		ActionProvider.instance.actions.add(this);
		this.icon = ResourceManager.getItemIcon(mod, iconFile);
		this.desc = desc;
	}
	
	

//	@Override
//	public String getTexture() {
//		return ExtraBeeTexture.Triggers.getTexture();
//	}
	
	public static void setup() {

		actionPauseProcess = new BinnieAction("Pause Process", "actions/PauseProcess");
		actionCancelTask = new BinnieAction("Cancel Task", "actions/CancelTask");

		ActionManager.registerActionProvider(new ActionProvider());
	}

	@Override
	public String getDescription() {
		return desc;
	}

	@Override
	public IIconProvider getIconProvider() {
		return ActionProvider.instance;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public boolean hasParameter() {
		return false;
	}

	@Override
	public int getIconIndex() {
		return id;
	}
	
	@SideOnly(Side.CLIENT)
	public Icon getIcon(IconRegister register) {
		return icon.getIcon(register);
	}

}
