package binnie.core.triggers;

import java.util.Map;

import buildcraft.api.gates.ITrigger;

public class TriggerData implements Map.Entry<ITrigger, Boolean> {

	private final ITrigger key;
    private Boolean value;

    public TriggerData(ITrigger key, Boolean value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public ITrigger getKey() {
        return key;
    }

    @Override
    public Boolean getValue() {
        return value;
    }

    @Override
    public Boolean setValue(Boolean value) {
    	Boolean old = this.value;
        this.value = value;
        return old;
    }
	
}
