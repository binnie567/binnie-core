package binnie.core.triggers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import binnie.core.machines.component.IBuildcraft;
import buildcraft.api.core.IIconProvider;
import buildcraft.api.gates.IAction;
import buildcraft.api.gates.IActionProvider;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ActionProvider implements IActionProvider, IIconProvider {

	static ActionProvider instance = new ActionProvider();
	public static List<BinnieAction> actions = new ArrayList<BinnieAction>();
	static Map<Integer, Icon> icons = new HashMap<Integer, Icon>();
	
	@Override
	public LinkedList<IAction> getNeighborActions(Block block, TileEntity tile) {
		LinkedList<IAction> list = new LinkedList<IAction>();
		if (tile instanceof IBuildcraft.ActionProvider)
			((IBuildcraft.ActionProvider) tile).getActions(list);
		return list;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIcon(int iconIndex) {
		return icons.get(iconIndex);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister iconRegister) {
		for(BinnieAction action : actions) {
			icons.put(action.getId(), action.getIcon(iconRegister));
		}
	}

}
