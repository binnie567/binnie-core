package binnie.core.triggers;

import binnie.core.machines.Machine;
import binnie.core.machines.power.IPoweredMachine;

public class TriggerPower {
	
	public static TriggerData powerNone(Object tile) {
		return new TriggerData(BinnieTrigger.triggerPowerNone, getPercentage(tile) < 0.05f);
	}
	
	public static TriggerData powerLow(Object tile) {
		return new TriggerData(BinnieTrigger.triggerPowerLow, getPercentage(tile) < 0.35f);
	}
	
	public static TriggerData powerMedium(Object tile) {
		double p = getPercentage(tile);
		return new TriggerData(BinnieTrigger.triggerPowerMedium, p >= 0.35f && p <= 0.65f);
	}
	
	public static TriggerData powerHigh(Object tile) {
		double p = getPercentage(tile);
		return new TriggerData(BinnieTrigger.triggerPowerHigh, getPercentage(tile) > 0.65f);
	}

	public static TriggerData powerFull(Object tile) {
		double p = getPercentage(tile);
		return new TriggerData(BinnieTrigger.triggerPowerFull, getPercentage(tile) > 0.95f);
	}
	
	private static double getPercentage(Object tile) {
		IPoweredMachine process = Machine.getInterface(IPoweredMachine.class, tile);
		if (process != null) {
			double percentage = process.getPowerProvider().getEnergyStored()
					/ process.getPowerProvider().getMaxEnergyStored();
			return percentage;
		}
		return 0f;
	}
	

}
