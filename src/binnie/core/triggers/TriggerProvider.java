package binnie.core.triggers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import binnie.core.machines.component.IBuildcraft;
import buildcraft.api.core.IIconProvider;
import buildcraft.api.gates.ITrigger;
import buildcraft.api.gates.ITriggerProvider;
import buildcraft.api.transport.IPipe;

public class TriggerProvider implements ITriggerProvider, IIconProvider {

	static TriggerProvider instance = new TriggerProvider();
	public static List<BinnieTrigger> triggers = new ArrayList<BinnieTrigger>();
	static Map<Integer, Icon> icons = new HashMap<Integer, Icon>();
	
	
	@Override
	public LinkedList<ITrigger> getPipeTriggers(IPipe pipe) {
		return null;
	}

	@Override
	public LinkedList<ITrigger> getNeighborTriggers(Block block, TileEntity tile) {
		LinkedList<TriggerData> list = new LinkedList<TriggerData>();
		
		LinkedList<ITrigger> triggerData = new LinkedList<ITrigger>();
	
		if (tile instanceof IBuildcraft.TriggerProvider)
			((IBuildcraft.TriggerProvider) tile).getTriggers(list);
		
		for(TriggerData data : list)
			triggerData.add(data.getKey());
		
		return triggerData;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIcon(int iconIndex) {
		return icons.get(iconIndex);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister iconRegister) {
		for(BinnieTrigger action : triggers) {
			icons.put(action.getId(), action.getIcon(iconRegister));
		}
	}
	
	public static boolean isTriggerActive(ITrigger trigger, TileEntity tile) {
		LinkedList<TriggerData> list = new LinkedList<TriggerData>();
		
		LinkedList<ITrigger> triggerData = new LinkedList<ITrigger>();
	
		if (tile instanceof IBuildcraft.TriggerProvider)
			((IBuildcraft.TriggerProvider) tile).getTriggers(list);
		
		for(TriggerData data : list)
			if(data.getKey() == trigger)
				return data.getValue();
		
		return false;
	}

}
