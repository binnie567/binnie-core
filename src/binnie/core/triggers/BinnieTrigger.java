package binnie.core.triggers;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraftforge.common.ForgeDirection;
import binnie.core.BinnieCore;
import binnie.core.IBinnieMod;
import binnie.core.resource.BinnieIcon;
import binnie.core.resource.ResourceManager;
import binnie.extrabees.ExtraBees;
import buildcraft.api.core.IIconProvider;
import buildcraft.api.gates.ActionManager;
import buildcraft.api.gates.ITrigger;
import buildcraft.api.gates.ITriggerParameter;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public final class BinnieTrigger implements ITrigger {

	public static int incrementalID = 800;

	protected static ITrigger triggerNoBlankTemplate;
	protected static ITrigger triggerNoTemplate;

	protected static ITrigger triggerIsWorking;
	protected static ITrigger triggerIsNotWorking;
	protected static ITrigger triggerCanWork;
	protected static ITrigger triggerCannotWork;

	protected static ITrigger triggerPowerNone;
	protected static ITrigger triggerPowerLow;
	protected static ITrigger triggerPowerMedium;
	protected static ITrigger triggerPowerHigh;
	protected static ITrigger triggerPowerFull;

	protected static ITrigger triggerSerumFull;
	protected static ITrigger triggerSerumPure;
	protected static ITrigger triggerSerumEmpty;
	
	protected static ITrigger triggerAcclimatiserNone;
	protected static ITrigger triggerAcclimatiserHot;
	protected static ITrigger triggerAcclimatiserCold;
	protected static ITrigger triggerAcclimatiserWet;
	protected static ITrigger triggerAcclimatiserDry;

	String desc;
	BinnieIcon icon;
	public int id = 0;
	
	public BinnieTrigger(String desc,String iconFile) {
		this(desc, BinnieCore.instance, iconFile);
	}
	
	public BinnieTrigger(String desc, IBinnieMod mod, String iconFile) {
		id = (incrementalID++);
		ActionManager.triggers[id] = this;
		TriggerProvider.triggers.add(this);
		this.icon = ResourceManager.getItemIcon(mod, iconFile);
		this.desc = desc;
	}

//	@Override
//	public String getTextureFile() {
//		return ExtraBeeTexture.Triggers.getTexture();
//	}

	
	
	
	
	

	public static void setup() {
		triggerNoBlankTemplate = new BinnieTrigger("No Blank Template", ExtraBees.instance, "triggers/NoBlankTemplate");

		triggerNoTemplate = new BinnieTrigger("No Template", ExtraBees.instance, "triggers/NoTemplate");

		triggerIsWorking = new BinnieTrigger("Is Working", "triggers/IsWorking");
		triggerIsNotWorking = new BinnieTrigger("Is Not Working", "triggers/IsNotWorking");
		triggerCanWork = new BinnieTrigger("Can Work", "triggers/CanWork");
		triggerCannotWork = new BinnieTrigger("Cannot Work", "triggers/CannotWork");

		triggerPowerNone = new BinnieTrigger("Power None", "triggers/PowerNone");
		triggerPowerLow = new BinnieTrigger("Power Low", "triggers/PowerLow");
		triggerPowerMedium = new BinnieTrigger("Power Medium", "triggers/PowerMedium");
		triggerPowerHigh = new BinnieTrigger("Power High", "triggers/PowerHigh");
		triggerPowerFull = new BinnieTrigger("Power Full", "triggers/PowerFull");
		
		triggerSerumFull = new BinnieTrigger("Serum Full", ExtraBees.instance, "triggers/SerumFull");
		triggerSerumPure = new BinnieTrigger("Serum Pure", ExtraBees.instance, "triggers/SerumPure");
		triggerSerumEmpty = new BinnieTrigger("Serum Empty", ExtraBees.instance, "triggers/SerumEmpty");

		ActionManager.registerTriggerProvider(new TriggerProvider());

	}

	@Override
	public ITriggerParameter createParameter() {
		return null;
	}

	@Override
	public String getDescription() {
		return desc;
	}

	@Override
	public IIconProvider getIconProvider() {
		return TriggerProvider.instance;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public boolean hasParameter() {
		return false;
	}

	@Override
	public boolean isTriggerActive(ForgeDirection side, TileEntity tile,
			ITriggerParameter parameter) {
		return TriggerProvider.isTriggerActive(this, tile);
	}

	@Override
	public int getIconIndex() {
		return id;
	}

	@SideOnly(Side.CLIENT)
	public Icon getIcon(IconRegister register) {
		return icon.getIcon(register);
	}

}
