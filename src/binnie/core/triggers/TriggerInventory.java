package binnie.core.triggers;

import binnie.extrabees.ExtraBees;
import binnie.extrabees.engineering.ModuleEngineering;
import binnie.extrabees.machines.tile.TileEntityPurifier;
import binnie.extrabees.machines.tile.TileEntitySynthesizer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class TriggerInventory {
	
	public static TriggerData noBlankTemplate(IInventory inventory, int slot) {
		return new TriggerData(BinnieTrigger.triggerNoBlankTemplate, isSlotEmpty(inventory, slot));
	}
	
	public static TriggerData noTemplate(IInventory inventory, int slot) {
		return new TriggerData(BinnieTrigger.triggerNoTemplate, isSlotEmpty(inventory, slot));
	}
	
	public static TriggerData serumFull(IInventory inventory, int slot) {
		return new TriggerData(BinnieTrigger.triggerSerumFull, isSerumFull(inventory, slot));
	}
	
	public static TriggerData serumPure(IInventory inventory, int slot) {
		return new TriggerData(BinnieTrigger.triggerSerumPure, isSerumPure(inventory, slot));
	}
	
	public static TriggerData serumEmpty(IInventory inventory, int slot) {
		return new TriggerData(BinnieTrigger.triggerSerumEmpty, isSerumEmpty(inventory, slot));
	}
	
	
	private static Boolean isSlotEmpty(IInventory inventory, int slot) {
		return inventory.getStackInSlot(slot) != null;
	}
	
	private static Boolean isSerumFull(IInventory inventory, int slot) {
		ItemStack serum =  inventory.getStackInSlot(slot);
		if(serum == null) return false;
		return !serum.isItemDamaged();
	}
	
	private static Boolean isSerumPure(IInventory inventory, int slot) {
		ItemStack serum =  inventory.getStackInSlot(slot);
		if(serum == null) return false;
		return ModuleEngineering.getQuality(serum) == 10;
	}
	
	private static Boolean isSerumEmpty(IInventory inventory, int slot) {
		ItemStack serum =  inventory.getStackInSlot(slot);
		if(serum == null) return false;
		return serum.getItemDamage() >= ExtraBees.serum
				.getMaxDamage();
	}

}
