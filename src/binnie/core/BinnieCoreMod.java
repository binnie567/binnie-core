package binnie.core;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import binnie.core.gui.IBinnieGUID;
import binnie.core.network.IPacketID;
import binnie.core.plugin.ForestryBinniePlugin;
import binnie.core.plugin.IBinnieModule;
import binnie.core.proxy.IProxyCore;
import binnie.core.resource.IBinnieTexture;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.ModMetadata;

public abstract class BinnieCoreMod implements IBinnieMod {
	
	private Logger log;
	
	public BinnieCoreMod() {
		BinnieCore.registerMod(this);
	}
	
	public abstract String getChannel();

	@Override
	public IPacketID[] getPacketIDs() {
		return new IPacketID[0];
	}

	@Override
	public IBinnieGUID[] getGUIDs() {
		return new IBinnieGUID[0];
	}

	@Override
	public IBinnieTexture[] getTextures() {
		return new IBinnieTexture[0];
	}

	@Override
	public Class[] getConfigs() {
		return new Class[0];
	}

	public abstract IProxyCore getProxy();

	public abstract String getId();

	@Override
	public void addModMetadata(ModMetadata metadata) {
	}
	
	@Override
	public Logger log() {
		return log;
	}
	
	public void init() {
		getProxy().doInit();
		log = Logger.getLogger(getId());
		log.setParent(FMLLog.getLogger());
		for(IBinnieModule module : modules) module.doInit();
	}
	
	public void preInit() {
		registerModules(modules);
		getProxy().preInit();
		for(IBinnieModule module : modules) module.preInit();
	}
	
	public void postInit() {
		getProxy().postInit();
		for(IBinnieModule module : modules) module.postInit();
	}
	
	List<IBinnieModule> modules = new ArrayList<IBinnieModule>();

	public abstract void registerModules(List<IBinnieModule> list);

}
