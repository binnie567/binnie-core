package binnie.core.plugin;

import java.util.ArrayList;
import java.util.List;

import binnie.core.BinnieCoreMod;
import forestry.api.core.IPlugin;

public abstract class ForestryBinniePlugin implements IPlugin {

	@Override
	public boolean isAvailable() {
		return true;
	}
	
	@Override
	public final void preInit() {
		//for(IBinnieModule module : modules) module.preInit();
	}

	@Override
	public final void doInit() {
		//for(IBinnieModule module : modules) module.doInit();
	}

	@Override
	public final void postInit() {
		//for(IBinnieModule module : modules) module.postInit();
	}
	
	
}
