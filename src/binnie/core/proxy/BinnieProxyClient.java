package binnie.core.proxy;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.liquids.LiquidDictionary;
import net.minecraftforge.liquids.LiquidStack;

import org.lwjgl.opengl.GL11;

import binnie.core.genetics.BreedingMessageHandler;
import binnie.core.network.IPacketProvider;
import binnie.core.network.packet.BinniePacket;
import binnie.core.resource.BinnieResource;
import binnie.core.resource.ResourceManager;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;

public class BinnieProxyClient extends BinnieProxy implements IBinnieProxy {

	@Override
	public void bindTexture(BinnieResource texture) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		getMinecraftInstance().renderEngine.bindTexture(texture.getFullPath());
	}
	
	@Override
	public boolean isSimulating(World world) {
		return !world.isRemote;
	}

	@Override
	public void registerCustomItemRenderer(int itemID,
			IItemRenderer itemRenderer) {
		MinecraftForgeClient.registerItemRenderer(itemID, itemRenderer);
	}

	@Override
	public World getWorld() {
		return getMinecraftInstance().theWorld;
	}

	@Override
	public Minecraft getMinecraftInstance() {
		return FMLClientHandler.instance().getClient();
	}

	@Override
	public void sendToServer(IPacketProvider mod, BinniePacket packet) {
		packet.setChannel(mod.getChannel());
		getMinecraftInstance().getNetHandler()
				.addToSendQueue(packet.getPacket());
	}

	@Override
	public boolean isClient() {
		return true;
	}

	@Override
	public boolean isServer() {
		return false;
	}

	@Override
	public File getDirectory() {
		return Minecraft.getMinecraftDir();
	}

	@Override
	public void registerTileEntity(Class<? extends TileEntity> tile, String id,
			Object renderer) {

		if (renderer != null && renderer instanceof TileEntitySpecialRenderer)
			ClientRegistry.registerTileEntity(tile, id,
					(TileEntitySpecialRenderer) renderer);
		else
			GameRegistry.registerTileEntity(tile, id);

	}

	@Override
	public void registerBlockRenderer(Object renderer) {
		if (renderer != null
				&& renderer instanceof ISimpleBlockRenderingHandler)
			RenderingRegistry
					.registerBlockHandler((ISimpleBlockRenderingHandler) renderer);
	}

	@Override
	public void createPipe(Item pipe) {
	//	registerCustomItemRenderer(pipe.itemID,
	//			TransportProxyClient.pipeItemRenderer);
	}

	@Override
	public Object createObject(String renderer) {

		Object object = null;

		try {
			Class rendererClass = Class.forName(renderer);

			if (rendererClass != null) {
				object = rendererClass.newInstance();
			}
		} catch (Exception e) {
		}

		return object;
	}
	
	@Override
	public Icon getIcon(IconRegister register, String mod, String name) {
		return register.registerIcon(mod + ":" + name);
	}
	
	@Override
	public void preInit() {
		super.preInit();
		new BreedingMessageHandler(getMinecraftInstance());
		TickRegistry.registerTickHandler(BreedingMessageHandler.instance, Side.CLIENT);
		MinecraftForge.EVENT_BUS.register(BreedingMessageHandler.instance);
	}
	
	List<String> liquids = new ArrayList<String>();
	
	@Override
	public LiquidStack registerLiquid(String name, int id, int meta) {
		LiquidStack liquid = super.registerLiquid(name, id, meta);
		liquids.add(name);
		return liquid;
	}
	
	@Override
	public void handleTextureRefresh(IconRegister register, int type) {
		ResourceManager.registerIcons(register, type);
	}
	
	@Override
	public void handlePostTextureRefresh(IconRegister register, int type) {
		if(type == 0) return;
		for(String liquidName : liquids) {
			LiquidStack liquid = LiquidDictionary.getCanonicalLiquid(liquidName);
			if(liquid!=null) {
				Item item = liquid.asItemStack().getItem();
				if(item==null) continue;
				Icon icon =liquid.asItemStack().getItem().getIconFromDamage(liquid.itemMeta);
				if(icon == null)
					throw new RuntimeException("[Binnie] Liquid " + liquid.asItemStack().getDisplayName() + " has invalid icon");
				liquid.setRenderingIcon(icon);
				liquid.setTextureSheet("/gui/items.png");
			}
		}
		
	}

}