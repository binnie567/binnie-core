package binnie.core.proxy;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Icon;
import binnie.core.gui.IBinnieGUID;
import binnie.core.network.packet.BinniePacket;

public interface IBinnieModProxy extends IProxyCore {
	
	public void openGui(IBinnieGUID ID, EntityPlayer player, int x, int y, int z);

	public void sendNetworkPacket(BinniePacket packet, int x, int y, int z);

	public void sendToPlayer(BinniePacket packet, EntityPlayer entityplayer);

	public void sendToServer(BinniePacket packet);
	
	public Icon getIcon(IconRegister register, String string);

}