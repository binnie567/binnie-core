package binnie.core.proxy;

import java.io.File;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import net.minecraftforge.client.IItemRenderer;
import binnie.core.IBinnieMod;
import binnie.core.network.INetworkedEntity;
import binnie.core.network.IPacketProvider;
import binnie.core.network.packet.BinniePacket;
import binnie.core.resource.BinnieResource;

public interface IBinnieProxy extends IProxyCore {

	// Identification Functions

	public boolean isClient();

	public boolean isServer();

	// File based Functions

	public File getDirectory();

	// Render Functions

	public void bindTexture(BinnieResource texture);

	public int getUniqueRenderID();

	public void registerCustomItemRenderer(int itemID,
			IItemRenderer itemRenderer);

	// GUI Functions

	public void openGui(IBinnieMod mod, int id, EntityPlayer player, int x, int y,
			int z);

	// Game Functions

	public boolean isSimulating(World world);

	public World getWorld();

	public Minecraft getMinecraftInstance();

	// Network Functions

	public void sendNetworkPacket(IPacketProvider mod, BinniePacket packet, int x,
			int y, int z);
	
	public void sendNetworkEntityPacket(INetworkedEntity entity);

	public void sendToPlayer(IPacketProvider mod, BinniePacket packet,
			EntityPlayer entityplayer);

	public void sendToServer(IPacketProvider mod, BinniePacket packet);

	public boolean needsTagCompoundSynched(Item item);

	// Plugin Functions

	// TE Functions

	public Object createObject(String renderer);

	public void registerTileEntity(Class<? extends TileEntity> tile, String id,
			Object renderer);

	// BC Functions

	public void createPipe(Item pipe);

	//public void addTexture(TextureFX texture);

	public boolean isDebug();

	void registerBlockRenderer(Object renderer);

	Icon getIcon(IconRegister register, String mod, String name);
}
