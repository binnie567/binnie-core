package binnie.core.proxy;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Icon;
import binnie.core.BinnieCore;
import binnie.core.IBinnieMod;
import binnie.core.gui.IBinnieGUID;
import binnie.core.network.packet.BinniePacket;

public class BinnieModProxy implements IBinnieModProxy {
	
	IBinnieMod mod;
	
	public BinnieModProxy(IBinnieMod mod) {
		this.mod = mod;
	}

	@Override
	public void openGui(IBinnieGUID ID, EntityPlayer player, int x, int y, int z) {
		BinnieCore.proxy.openGui(mod, ID.ordinal(), player, x, y, z);
	}

	@Override
	public void sendNetworkPacket(BinniePacket packet, int x, int y, int z) {
		BinnieCore.proxy.sendNetworkPacket(mod, packet, x, y, z);
	}

	@Override
	public void sendToPlayer(BinniePacket packet, EntityPlayer entityplayer) {
		BinnieCore.proxy.sendToPlayer(mod, packet, entityplayer);
	}

	@Override
	public void sendToServer(BinniePacket packet) {
		BinnieCore.proxy.sendToServer(mod, packet);
	}
	
	public Icon getIcon(IconRegister register, String string) {
		return BinnieCore.proxy.getIcon(register, mod.getId(), string);
	}

	@Override
	public void preInit() {
	}

	@Override
	public void doInit() {
	}

	@Override
	public void postInit() {
	}
	
	

}