package binnie.core.proxy;

public interface IProxyCore {

	public void preInit();

	public void doInit();

	public void postInit();

}
