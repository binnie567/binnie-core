package binnie.core.proxy;

import java.io.File;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.liquids.LiquidDictionary;
import net.minecraftforge.liquids.LiquidStack;
import binnie.core.BinnieCore;
import binnie.core.IBinnieMod;
import binnie.core.network.BinnieCorePacketID;
import binnie.core.network.INetworkedEntity;
import binnie.core.network.IPacketProvider;
import binnie.core.network.packet.BinniePacket;
import binnie.core.network.packet.PacketUpdate;
import binnie.core.resource.BinnieResource;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class BinnieProxy implements IBinnieProxy {

	@Override
	public void bindTexture(BinnieResource texture) {

	}

	@Override
	public int getUniqueRenderID() {
		return RenderingRegistry.getNextAvailableRenderId();
	}

	@Override
	public void openGui(IBinnieMod mod, int id, EntityPlayer player, int x, int y, int z) {
		player.openGui(mod, id, player.worldObj, x, y, z);
	}

	@Override
	public boolean isSimulating(World world) {
		return true;
	}

	@Override
	public void registerCustomItemRenderer(int itemID, IItemRenderer itemRenderer) {
	}

	@Override
	public void sendNetworkPacket(IPacketProvider mod, BinniePacket packet, int x, int y, int z) {

		if (packet == null)
			return;
		packet.setChannel(mod.getChannel());

		World[] worlds = DimensionManager.getWorlds();
		for (int i = 0; i < worlds.length; i++) {
			for (int j = 0; j < worlds[i].playerEntities.size(); j++) {
				EntityPlayerMP player = (EntityPlayerMP) worlds[i].playerEntities.get(j);

				if (Math.abs(player.posX - x) <= 50 && Math.abs(player.posY - y) <= 50
						&& Math.abs(player.posZ - z) <= 50) {
					player.playerNetServerHandler.sendPacketToPlayer(packet.getPacket());
				}
			}
		}
	}

	@Override
	public void sendToPlayer(IPacketProvider mod, BinniePacket packet, EntityPlayer entityplayer) {
		packet.setChannel(mod.getChannel());
		EntityPlayerMP player = (EntityPlayerMP) entityplayer;
		player.playerNetServerHandler.sendPacketToPlayer(packet.getPacket());
	}

	@Override
	public void sendToServer(IPacketProvider mod, BinniePacket packet) {
	}

	@Override
	public boolean needsTagCompoundSynched(Item item) {
		return item.getShareTag();
	}

	@Override
	public World getWorld() {
		return null;
	}

	public void throwException(String message, Throwable e) {
		FMLCommonHandler.instance().raiseException(e, message, true);
	}

	public void addName(Object instance, String name) {
		LanguageRegistry.instance().addName(instance, name);
	}

	@Override
	public Minecraft getMinecraftInstance() {
		return null;
	}

	@Override
	public boolean isClient() {
		return false;
	}

	@Override
	public boolean isServer() {
		return true;
	}

	@Override
	public File getDirectory() {
		return new File("./");
	}

	@Override
	public void preInit() {
	}

	@Override
	public void doInit() {
	}

	@Override
	public void postInit() {
	}

	@Override
	public void registerTileEntity(Class<? extends TileEntity> tile, String id, Object renderer) {
		GameRegistry.registerTileEntity(tile, id);
	}

	@Override
	public void createPipe(Item pipe) {
	}

	// @Override
	// public void addTexture(TextureFX texture) {
	// }

	@Override
	public boolean isDebug() {
		return System.getenv().containsKey("BINNIE_DEBUG");
	}

	@Override
	public void registerBlockRenderer(Object renderer) {
	}

	@Override
	public Object createObject(String renderer) {
		return null;
	}

	@Override
	public void sendNetworkEntityPacket(INetworkedEntity entity) {
		PacketUpdate packet = new PacketUpdate(BinnieCorePacketID.NetworkEntityUpdate.ordinal(),
				entity);
		BinnieCore.proxy.sendNetworkPacket(BinnieCore.instance, packet,
				((TileEntity) entity).xCoord, ((TileEntity) entity).yCoord,
				((TileEntity) entity).zCoord);
	}

	@Override
	public Icon getIcon(IconRegister register, String mod, String name) {
		return null;
	}

	public void log(String string) {
		System.out.println("Binnie - " + string);
	}

	public LiquidStack registerLiquid(String name, int id, int meta) {
		return LiquidDictionary.getOrCreateLiquid(name, new LiquidStack(id, 1000, meta));
	}

	public void handleTextureRefresh(IconRegister register, int type) {

	}

	public void handlePostTextureRefresh(IconRegister register, int type) {

	}

	short uniqueTextureUID = 1200;

	public short getUniqueTextureUID() {
		return uniqueTextureUID++;
	}

}