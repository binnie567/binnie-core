package binnie.core.network;


/**
 * Interface that means something is an enum with a set ordinal
 * @author Alex Binnie
 *
 */
public interface IOrdinaled {
	
	int ordinal();

}
