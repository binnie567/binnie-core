package binnie.core.network;

/**
 * Has a Channel which can be used to send packets, and a set of packet IDs
 * @author Alex Binnie
 *
 */
public interface IPacketProvider {
	
	String getChannel();
	
	IPacketID[] getPacketIDs();

}
