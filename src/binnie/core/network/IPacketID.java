package binnie.core.network;

import java.io.DataInputStream;
import java.io.IOException;

import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import cpw.mods.fml.common.network.Player;

public interface IPacketID extends IOrdinaled {

	void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player,
			DataInputStream data) throws IOException;

}
