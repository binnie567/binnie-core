package binnie.core.network;

import binnie.core.network.packet.PacketPayload;

/**
 * Entity that can send data from server to client via a PacketPayload
 * @author Alex Binnie
 *
 */
public interface INetworkedEntity {

	public void writeToPacket(PacketPayload payload);

	public void readFromPacket(PacketPayload payload);

}
