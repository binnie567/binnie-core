package binnie.core.network.packet;

import net.minecraft.nbt.NBTTagCompound;
import binnie.core.machines.power.PowerInfo;
import binnie.core.machines.power.TankInfo;
import binnie.core.network.BinnieCorePacketID;

public class PacketPowerUpdate extends PacketCraftGUI {
	
	public PacketPowerUpdate(PowerInfo tankInfo) {
		this();
		NBTTagCompound nbt = new NBTTagCompound("power-update");
		tankInfo.writeToNBT(nbt);
		this.setTagCompound(nbt);
	}

	public PacketPowerUpdate() {
		super();
	}

}
