package binnie.core.network.packet;

import net.minecraft.nbt.NBTTagCompound;
import binnie.core.network.BinnieCorePacketID;

public class PacketCraftGUI extends PacketNBT {

	public PacketPayload payload;

	public PacketCraftGUI() {
		super(BinnieCorePacketID.CraftGUIAction.ordinal());
	}
	
	public PacketCraftGUI(NBTTagCompound action) {
		super(BinnieCorePacketID.CraftGUIAction.ordinal());
		this.action = action;
	}
	
	public NBTTagCompound action = null;

	@Override
	public NBTTagCompound getTagCompound() {
		return action;
	}

	@Override
	void setTagCompound(NBTTagCompound nbt) {
		action = nbt;
	}

}