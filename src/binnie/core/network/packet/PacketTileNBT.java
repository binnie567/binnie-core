package binnie.core.network.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class PacketTileNBT extends PacketNBT implements IPacketLocation {

	private int posX;
	private int posY;
	private int posZ;
	
	NBTTagCompound nbttagcompound;

	public PacketTileNBT(int id, TileEntity tile) {
		super(id);

		posX = tile.xCoord;
		posY = tile.yCoord;
		posZ = tile.zCoord;

		this.nbttagcompound = new NBTTagCompound();
		tile.writeToNBT(nbttagcompound);
	}

	@Override
	public void writeData(DataOutputStream data) throws IOException {

		data.writeInt(posX);
		data.writeInt(posY);
		data.writeInt(posZ);

		super.writeData(data);
	}

	@Override
	public void readData(DataInputStream data) throws IOException {

		posX = data.readInt();
		posY = data.readInt();
		posZ = data.readInt();

		super.readData(data);
	}

	@Override
	public TileEntity getTarget(World world) {
		return world.getBlockTileEntity(posX, posY, posZ);
	}

	@Override
	public int getX() {
		return posX;
	}

	@Override
	public int getY() {
		return posY;
	}

	@Override
	public int getZ() {
		return posZ;
	}

	@Override
	public NBTTagCompound getTagCompound() {
		return nbttagcompound;
	}

	@Override
	void setTagCompound(NBTTagCompound nbt) {
		nbttagcompound = nbt;
	}

}
