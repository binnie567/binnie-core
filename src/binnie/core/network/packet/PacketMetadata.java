package binnie.core.network.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import binnie.core.network.BinnieCorePacketID;

public class PacketMetadata extends PacketCoordinates {

	public int meta;

	public PacketMetadata(int posX, int posY, int posZ, int meta) {
		super(BinnieCorePacketID.TileMetadata.ordinal(), posX, posY, posZ);
		this.meta = meta;
	}

	public PacketMetadata() {
	}

	@Override
	public void writeData(DataOutputStream data) throws IOException {
		super.writeData(data);
		data.writeInt(meta);

	}

	@Override
	public void readData(DataInputStream data) throws IOException {
		super.readData(data);
		meta = data.readInt();

	}

}
