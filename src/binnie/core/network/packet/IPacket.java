package binnie.core.network.packet;

import net.minecraft.network.packet.Packet;

public interface IPacket {

	public Packet getPacket();

}
