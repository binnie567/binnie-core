package binnie.core.network.packet;

import net.minecraft.nbt.NBTTagCompound;
import binnie.core.machines.power.TankInfo;
import binnie.core.network.BinnieCorePacketID;

public class PacketTankUpdate extends PacketCraftGUI {
	
	public PacketTankUpdate(int tank, TankInfo tankInfo) {
		this();
		NBTTagCompound nbt = new NBTTagCompound("tank-update");
		tankInfo.writeToNBT(nbt);
		nbt.setByte("tank", (byte)tank);
		this.setTagCompound(nbt);
	}

	public PacketTankUpdate() {
		super();
	}

}
