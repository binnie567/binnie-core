package binnie.core.network.packet;

import net.minecraft.nbt.NBTTagCompound;
import binnie.core.machines.power.ErrorState;
import binnie.core.machines.power.IErrorStateSource;
import binnie.core.machines.power.TankInfo;
import binnie.core.network.BinnieCorePacketID;

public class PacketErrorUpdate extends PacketCraftGUI {
	
	public PacketErrorUpdate(IErrorStateSource error) {
		this();
		NBTTagCompound nbt = new NBTTagCompound("error-update");
		ErrorState state = null;
		if(error.canWork() != null) {
			nbt.setByte("type", (byte)0);
			state = error.canWork();
		}
		else if(error.canProgress() != null) {
			nbt.setByte("type", (byte)1);
			state = error.canProgress();
		}
		if(state != null) {
			state.writeToNBT(nbt);
			
		}
		this.setTagCompound(nbt);
	}

	public PacketErrorUpdate() {
		super();
	}

}
