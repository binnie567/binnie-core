package binnie.core.network.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import binnie.core.network.INetworkedEntity;

public class PacketUpdate extends PacketCoordinates {

	public PacketPayload payload;

	public PacketUpdate() {
	}
	public PacketUpdate(int id, INetworkedEntity tile) {
		super(id, ((TileEntity) tile).xCoord, ((TileEntity) tile).yCoord, ((TileEntity) tile).zCoord);
		this.payload = new PacketPayload();
		tile.writeToPacket(payload);
	}
	
	@Override
	public void writeData(DataOutputStream data) throws IOException {

		super.writeData(data);

		// No payload means no data
		if (payload == null) {
			data.writeInt(0);
			data.writeInt(0);
			data.writeInt(0);
			return;
		}

		data.writeInt(payload.intPayload.size());
		data.writeInt(payload.floatPayload.size());
		data.writeInt(payload.stringPayload.size());

		for (int intData : payload.intPayload)
			data.writeInt(intData);
		for (float floatData : payload.floatPayload)
			data.writeFloat(floatData);
		for (String stringData : payload.stringPayload)
			data.writeUTF(stringData);

	}

	@Override
	public void readData(DataInputStream data) throws IOException {

		super.readData(data);

		payload = new PacketPayload();

		int intLength = data.readInt();
		int floatLength = data.readInt();
		int stringLength = data.readInt();

		payload.intPayload.clear();
		payload.floatPayload.clear();
		payload.stringPayload.clear();

		for (int i = 0; i < intLength; i++)
			payload.addInteger(data.readInt());
		for (int i = 0; i < floatLength; i++)
			payload.addFloat(data.readFloat());
		for (int i = 0; i < stringLength; i++)
			payload.addString(data.readUTF());

	}

	public TileEntity getTarget(World world) {
		return world.getBlockTileEntity(posX, posY, posZ);
	}

}