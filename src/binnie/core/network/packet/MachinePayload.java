package binnie.core.network.packet;

import java.util.ArrayList;
import java.util.List;

public class MachinePayload {

	public List<Integer> intPayload = new ArrayList<Integer>();
	public List<Float> floatPayload = new ArrayList<Float>();
	public List<String> stringPayload = new ArrayList<String>();

	int id = 0;
	
	public MachinePayload(int id) {
		this.id = id;
		intPayload.clear();
		floatPayload.clear();
		stringPayload.clear();
	}

	public MachinePayload() {
	}

	public void addInteger(int a) {
		intPayload.add(a);
	}

	public void addFloat(float a) {
		floatPayload.add(a);
	}

	public void addString(String a) {
		stringPayload.add(a);
	}

	public int getInteger() {
		return intPayload.remove(0);
	}

	public float getFloat() {
		return floatPayload.remove(0);
	}

	public String getString() {
		return stringPayload.remove(0);
	}

	public void append(MachinePayload other) {
		if (other == null)
			return;

		intPayload.addAll(other.intPayload);
		floatPayload.addAll(other.floatPayload);
		stringPayload.addAll(other.stringPayload);
	}

	public boolean isEmpty() {
		return intPayload.isEmpty() && floatPayload.isEmpty()
				&& stringPayload.isEmpty();
	}

	public int getID() {
		return id;
	}

	public void setID(int readInt) {
		this.id = readInt;
	}

}
