package binnie.core.network.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;

public class PacketNBT extends BinniePacket {

	NBTTagCompound nbt;
	
	public NBTTagCompound getTagCompound() {
		return nbt;
	}
	void setTagCompound(NBTTagCompound nbt) {
		this.nbt = nbt;
	}

	public PacketNBT(int id) {
		super(id);
	}
	
	public PacketNBT(int id, NBTTagCompound nbt) {
		this(id);
		setTagCompound(nbt);
	}
	
	public PacketNBT() {

	}
	
	@Override
	public void writeData(DataOutputStream data) throws IOException {
		byte[] compressed = CompressedStreamTools.compress(getTagCompound());
		data.writeShort(compressed.length);
		data.write(compressed);
	}

	@Override
	public void readData(DataInputStream data) throws IOException {
		short length = data.readShort();
		byte[] compressed = new byte[length];
		data.readFully(compressed);
		this.setTagCompound(CompressedStreamTools.decompress(compressed));
	}
	
}
