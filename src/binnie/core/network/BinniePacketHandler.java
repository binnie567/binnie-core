package binnie.core.network;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import binnie.core.BinnieException;
import binnie.core.IBinnieMod;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;

public final class BinniePacketHandler implements IPacketHandler {
	
	IPacketProvider provider;
	
	public BinniePacketHandler(IBinnieMod mod) {
		setProvider(mod);
	}

	public void setProvider(IPacketProvider provider) {
		this.provider = provider;
	}
	
	@Override
	public void onPacketData(INetworkManager manager,
			Packet250CustomPayload packet, Player player) {

		if(this.provider == null)
			throw new BinnieException("A BinniePacketHandler has no IPacketProvider associated with it.");
		
		DataInputStream data = new DataInputStream(new ByteArrayInputStream(
				packet.data));

		try {
			int packetId = data.readByte();
			for(IPacketID id : provider.getPacketIDs()) {
				if(id.ordinal() == packetId) {
					id.onPacketData(manager, packet, player, data);
					data.close();
					return;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
