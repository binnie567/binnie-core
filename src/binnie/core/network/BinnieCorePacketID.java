package binnie.core.network;

import java.io.DataInputStream;
import java.io.IOException;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.tileentity.TileEntity;
import binnie.core.BinnieCore;
import binnie.core.block.TileEntityMetadata;
import binnie.core.genetics.BreedingMessageHandler;
import binnie.core.network.packet.PacketCraftGUI;
import binnie.core.network.packet.PacketErrorUpdate;
import binnie.core.network.packet.PacketMetadata;
import binnie.core.network.packet.PacketNBT;
import binnie.core.network.packet.PacketPowerUpdate;
import binnie.core.network.packet.PacketTankUpdate;
import binnie.core.network.packet.PacketUpdate;
import binnie.craftgui.minecraft.ContainerCraftGUI;
import cpw.mods.fml.common.network.Player;

public enum BinnieCorePacketID implements IPacketID {

	NetworkEntityUpdate, TileMetadata, CraftGUIAction, Breeding, ;

	public void onPacketData(INetworkManager network, Packet250CustomPayload packet250,
			Player player, DataInputStream data) {
		if (this == NetworkEntityUpdate) {
			try {
				PacketUpdate packet = new PacketUpdate();
				packet.readData(data);
				TileEntity tile;
				if (player instanceof EntityPlayer) {
					tile = packet.getTileEntity(((EntityPlayer) player).worldObj);
				} else {
					tile = packet.getTileEntity(BinnieCore.proxy.getWorld());
				}
				if (tile instanceof INetworkedEntity)
					((INetworkedEntity) tile).readFromPacket(packet.payload);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (this == TileMetadata) {
			try {
				PacketMetadata packet = new PacketMetadata();
				packet.readData(data);
				TileEntity tile;
				if (player instanceof EntityPlayer) {
					tile = packet.getTileEntity(((EntityPlayer) player).worldObj);
				} else {
					tile = packet.getTileEntity(BinnieCore.proxy.getWorld());
				}
				if (tile instanceof TileEntityMetadata)
					((TileEntityMetadata) tile).setTileMetadata(packet.meta);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (this == Breeding) {

			PacketNBT packet = new PacketNBT();
			try {
				packet.readData(data);
				if (player instanceof EntityPlayer)
					BreedingMessageHandler.instance.recieve(packet.getTagCompound(),
							(EntityPlayer) player);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (this == CraftGUIAction) {

			PacketCraftGUI packet = new PacketCraftGUI();
			try {
				packet.readData(data);
				if (!(player instanceof EntityPlayer))
					return;
				EntityPlayer playerMP = (EntityPlayer) player;
				if (playerMP.openContainer == null)
					return;
				if (!(playerMP.openContainer instanceof ContainerCraftGUI))
					return;
				((ContainerCraftGUI) playerMP.openContainer).recieveNBT(null, playerMP,
						packet.action);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
