package binnie.core;


public class Constants {

	public static final String LiquidJuice = "juice";
	
	public static final String LiquidWater = "Water";
	public static final String LiquidLava = "Lava";
	
	public static final String LiquidSeedOil = "seedoil";
	
	public static final String LiquidCreosote = "Creosote Oil";

	public static final String LiquidHoney = "honey";

	public static final String LiquidDNA = "liquidDNA";;

}
