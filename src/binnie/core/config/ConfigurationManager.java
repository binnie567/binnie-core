package binnie.core.config;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.minecraftforge.common.Configuration;
import binnie.core.BinnieCore;
import binnie.core.IBinnieMod;

public class ConfigurationManager {

	static Map<Class, Configuration> configurations = new LinkedHashMap<Class, Configuration>();

	public static void init() {
	}

	public static void registerConfiguration(Class cls, IBinnieMod mod) {
		if (cls.isAnnotationPresent(ConfigFile.class))
			loadConfiguration(cls, mod);
	}

	public static void loadConfiguration(Class cls, IBinnieMod mod) {

		try {

			String filename = ((ConfigFile) cls
					.getAnnotation(ConfigFile.class)).filename();
			
			BinnieConfiguration config = new BinnieConfiguration(filename, mod);
			
			config.load();
			
			// Iterate through all variable fields of the configuration class file
			for (Field field : cls.getFields()) {
				
				// Checks if the variable is a config property
				if (field.isAnnotationPresent(ConfigProperty.class)) {
					
					ConfigProperty propertyAnnot = field
							.getAnnotation(ConfigProperty.class);
					
					for (Annotation annotation : field.getAnnotations()) {
						
						// If the property has an annotation which is a type of property
						if(annotation.annotationType().isAnnotationPresent(ConfigProperty.Type.class)) {
							
							Class propertyClass = annotation.annotationType().getAnnotation(ConfigProperty.Type.class).propertyClass();
							
							PropertyBase property = (PropertyBase) propertyClass.
									getConstructor(new Class[] {Field.class, BinnieConfiguration.class, ConfigProperty.class, annotation.annotationType()}).
									newInstance(new Object[] {field, config, propertyAnnot, annotation.annotationType().cast(annotation)});
						
						}
					}
				}
				
			}

			config.save();

			configurations.put(cls, config);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static Map<IBinnieMod, List<BinnieItemData>> itemIDs = new HashMap<IBinnieMod, List<BinnieItemData>>();

	public static void addItemID(Integer configValue, String configKey, BinnieConfiguration configFile) {
		if(!itemIDs.containsKey(configFile.mod))
			itemIDs.put(configFile.mod, new ArrayList<BinnieItemData>());
		itemIDs.get(configFile.mod).add(new BinnieItemData(configValue+256, configFile, configKey));
		
	}
}
