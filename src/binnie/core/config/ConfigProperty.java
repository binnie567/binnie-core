package binnie.core.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import net.minecraftforge.common.Configuration;

/**
 * Annotation for all properties in configuration files
 * @author Alex Binnie
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.FIELD})
public @interface ConfigProperty {
	String key();
	String category() default "";
	String[] comment() default {};
	
	/**
	 * Annotation for types of properties, annotates other annotations
	 * @author Alex Binnie
	 *
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(value = {ElementType.ANNOTATION_TYPE})
	public static @interface Type {
		Class<? extends PropertyBase> propertyClass();
		String category() default Configuration.CATEGORY_GENERAL;
	}
}
