package binnie.core.config;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;

public abstract class PropertyBase<ValueType, AnnotationType extends Annotation> {

	Configuration file;
	Property property;

	ValueType defaultValue;
	ConfigProperty configProperty;
	AnnotationType annotatedProperty;

	List<String> comments = new ArrayList<String>();
	
	Field field;

	protected PropertyBase(Field field, BinnieConfiguration file, ConfigProperty configProperty,
			AnnotationType annotedProperty) throws IllegalArgumentException, IllegalAccessException {
		super();
		this.field = field;
		this.file = file;
		this.configProperty = configProperty;
		this.annotatedProperty = annotedProperty;
		this.defaultValue = getDefaultValue(field);
		property = getProperty();
		for(String comment : configProperty.comment())
			addComment(comment);
		addComments();
		property.comment = getComment();
		field.set(null, getConfigValue());
	}
	
	protected abstract Property getProperty();
	protected abstract ValueType getConfigValue();
	protected abstract void addComments();


	protected String getCategory() {
		return configProperty.category().equals("") ? 
				annotatedProperty.annotationType().getAnnotation(ConfigProperty.Type.class).category()
				: 
			configProperty.category();
	}

	protected String getKey() {
		return configProperty.key();
	}

	protected ValueType getDefaultValue(Field field) throws IllegalArgumentException, IllegalAccessException {
		return (ValueType) field.get(null);
		
	}
	
	protected void addComment(String comment) {
		comments.add(comment);
	}

	protected String getComment() {
		String comment = "";
		for(String com : comments)
			comment += com + " ";
		return comment;
	}
	// public static class PropertyInteger extends BinnieProperty<Integer> {
	//
	// protected PropertyInteger(ConfigFile file, String key,
	// String category, Integer defaultValue) {
	// super(file, key, category, defaultValue);
	// addComment("Default value is "+defaultValue);
	// }
	//
	// @Override
	// protected Property getProperty() {
	// return file.getConfig().get(category, key, defaultValue);
	// }
	//
	// @Override
	// protected Integer getConfigValue() {
	// return property.getInt(defaultValue);
	// }
	//
	// }
	//
	

	

}
