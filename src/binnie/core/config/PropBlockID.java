package binnie.core.config;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;

import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;

@Retention(RetentionPolicy.RUNTIME)
@ConfigProperty.Type(propertyClass = PropBlockID.PropertyBlockID.class, category=Configuration.CATEGORY_BLOCK)
public @interface PropBlockID {

	String name();

	public static class PropertyBlockID extends
			PropertyBase<Integer, PropBlockID> {

		public PropertyBlockID(Field field, BinnieConfiguration file,
				ConfigProperty configProperty, PropBlockID annotedProperty) throws IllegalArgumentException, IllegalAccessException {
			super(field, file, configProperty, annotedProperty);
		}

		@Override
		protected Property getProperty() {
			return file.getBlock(getCategory(), getKey(), defaultValue);
		}

		@Override
		protected Integer getConfigValue() {
			return property.getInt(defaultValue);
		}

		@Override
		protected void addComments() {
			addComment("Block ID for " + annotatedProperty.name() + ".");
			addComment("Default ID is " + defaultValue + ".");
		}

	}

}
