package binnie.core.config;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;

import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;

@Retention(RetentionPolicy.RUNTIME)
@ConfigProperty.Type(propertyClass = PropItemID.PropertyItemID.class, category=Configuration.CATEGORY_ITEM)
public @interface PropItemID {

	String name();

	public static class PropertyItemID extends
			PropertyBase<Integer, PropItemID> {

		public PropertyItemID(Field field, BinnieConfiguration file,
				ConfigProperty configProperty, PropItemID annotedProperty) throws IllegalArgumentException, IllegalAccessException {
			super(field, file, configProperty, annotedProperty);
			ConfigurationManager.addItemID(getConfigValue(), configProperty.key(), file);
		}

		@Override
		protected Property getProperty() {
			return file.getItem(getCategory(), getKey(), defaultValue);
		}

		@Override
		protected Integer getConfigValue() {
			return property.getInt(defaultValue);
		}

		@Override
		protected void addComments() {
			addComment("Item ID for " + annotatedProperty.name() +".");
			addComment("Default ID is " + defaultValue + ".");
		}

	}

}
