package binnie.core.config;


public class BinnieItemData {
	
	public int item;
	public BinnieConfiguration configFile;
	public String configKey;
	public String modName;
	
	public BinnieItemData(int item, BinnieConfiguration configFile, String configKey) {
		super();
		this.item = item;
		this.configFile = configFile;
		this.configKey = configKey;
		this.modName = modName;
	}

}
