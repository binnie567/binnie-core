package binnie.core.config;

import java.io.File;

import binnie.core.BinnieCore;
import binnie.core.IBinnieMod;
import net.minecraftforge.common.Configuration;

public class BinnieConfiguration extends Configuration {

	public IBinnieMod mod;
	public String filename;

	public BinnieConfiguration(String filename, IBinnieMod mod) {
		super(new File(BinnieCore.proxy.getDirectory(), filename));
		this.mod = mod;
		this.filename = filename;
	}
}
