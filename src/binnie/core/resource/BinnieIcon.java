package binnie.core.resource;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;
import binnie.core.BinnieCore;
import binnie.core.IBinnieMod;

/**
 * Stores a location of an icon, which is auto registered
 * @author Alex Binnie
 *
 */
public class BinnieIcon extends BinnieResource {
	
	public BinnieIcon(IBinnieMod mod, ResourceType type, String path) {
		super(mod, type, path);
		textureSheet = type == ResourceType.Block ? 0 : 1;
		ResourceManager.registerIcon(this);
	}

	public int textureSheet = 0;
	public Icon icon = null;

	public Icon getIcon() {
		return icon;
	}
	
	public Icon getIcon(IconRegister register) {
		this.registerIcon(register);
		return icon;
	}

	public void registerIcon(IconRegister register) {
		icon = BinnieCore.proxy.getIcon(register, mod.getId(), path);
	}

	public int getTextureSheet() {
		return this.textureSheet;
	}

}
