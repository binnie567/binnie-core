package binnie.core.resource;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;

import binnie.core.IBinnieMod;

public class ResourceManager {
	
	public static BinnieResource getPNG(IBinnieMod mod, ResourceType type, String path) {
		return getFile(mod, type, path + ".png");
	}
	
	public static BinnieResource getFile(IBinnieMod mod, ResourceType type, String path) {
		return new BinnieResource(mod, type, path);
	}

	static List<BinnieIcon> icons = new ArrayList<BinnieIcon>();
	
	public static void registerIcon(BinnieIcon binnieIcon) {
		icons.add(binnieIcon);
	}

	public static BinnieIcon getItemIcon(IBinnieMod mod, String iconFile) {
		return new BinnieIcon(mod, ResourceType.Item, iconFile);
	}
	
	public static BinnieIcon getBlockIcon(IBinnieMod mod, String iconFile) {
		return new BinnieIcon(mod, ResourceType.Block, iconFile);
	}

	public static void registerIcons(IconRegister register, int type) {
		for(BinnieIcon icon : icons)
			if(icon.getTextureSheet() == type)
				icon.registerIcon(register);
	}
}
