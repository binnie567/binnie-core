package binnie.core.resource;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;
import binnie.core.BinnieCore;
import binnie.core.IBinnieMod;

public class BinnieResource {
	
	IBinnieMod mod;

	ResourceType type;
	String path;
	
	public BinnieResource(IBinnieMod mod, ResourceType type, String path) {
		super();
		this.mod = mod;
		this.type = type;
		this.path = path;
	}

	public String getFullPath() {
		return "/mods/"+mod.getId()+"/textures/"+type.toString()+"/"+path;
	}

}
