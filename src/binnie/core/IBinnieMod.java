package binnie.core;

import java.util.logging.Logger;

import binnie.core.gui.IBinnieGUID;
import binnie.core.network.IPacketProvider;
import binnie.core.proxy.IProxyCore;
import binnie.core.resource.IBinnieTexture;
import cpw.mods.fml.common.ModMetadata;

public interface IBinnieMod extends IPacketProvider {

	public IBinnieGUID[] getGUIDs();

	public IBinnieTexture[] getTextures();

	public Class[] getConfigs();
	
	public IProxyCore getProxy();

	public String getId();

	public void addModMetadata(ModMetadata metadata);
	
	public Logger log();

	public void preInit();
	public void init();
	public void postInit();
}
