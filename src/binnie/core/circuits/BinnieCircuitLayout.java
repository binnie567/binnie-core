package binnie.core.circuits;

import forestry.api.circuits.ChipsetManager;
import forestry.api.circuits.ICircuitLayout;

public class BinnieCircuitLayout implements ICircuitLayout {

	String name;
	String uid;
	
	
	
	public BinnieCircuitLayout(String name, String uid) {
		super();
		this.name = name;
		this.uid = "binnie.circuitLayout"+uid;
		ChipsetManager.circuitRegistry.registerLayout(this);
	}

	@Override
	public String getUID() {
		return uid;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getUsage() {
		return "";
	}

}
