package binnie.core.circuits;

import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import forestry.api.circuits.ChipsetManager;
import forestry.api.circuits.ICircuit;
import forestry.api.circuits.ICircuitLayout;

public class BinnieCircuit implements ICircuit {

	String name;
	String uid;
	int limit;
	
	
	public BinnieCircuit(String name, String uid, int limit, ICircuitLayout layout, ItemStack itemStack) {
		this.name = name;
		this.uid = "binnie.circuit"+uid;
		this.limit = limit;
		ChipsetManager.circuitRegistry.registerCircuit(this);
		if(itemStack != null)
			ChipsetManager.solderManager.addRecipe(layout, itemStack, this);
	}

	public BinnieCircuit(String name, String uid, int limit, ICircuitLayout layout, int itemID, int itemMeta) {
		this(name, uid, limit, layout, new ItemStack(itemID, 1, itemMeta));
	}

	@Override
	public String getUID() {
		return uid;
	}

	@Override
	public boolean requiresDiscovery() {
		return false;
	}

	@Override
	public int getLimit() {
		return limit;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean isCircuitable(TileEntity tile) {
		return false;
	}

	@Override
	public void onInsertion(int slot, TileEntity tile) {
	}

	@Override
	public void onLoad(int slot, TileEntity tile) {
	}

	@Override
	public void onRemoval(int slot, TileEntity tile) {
	}

	@Override
	public void onTick(int slot, TileEntity tile) {
	}

	@Override
	public void addTooltip(List<String> list) {
	}
	
}
