package binnie.core.block;

import java.util.List;

import binnie.extratrees.carpentry.ModuleCarpentry;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemMetadata extends ItemBlock {

	public ItemMetadata(int id) {
		super(id);
	}

	public int getMetadata(int par1) {
		return 0;
	}

	@Override
	public boolean placeBlockAt(ItemStack stack, EntityPlayer player,
			World world, int x, int y, int z, int side, float hitX, float hitY,
			float hitZ, int metadata) {
		
		
		
		Block block = (Block) Block.blocksList[getBlockID()];
		if(!(block instanceof IBlockMetadata))
			return false;
		int placedMeta = ((IBlockMetadata) block).getPlacedMeta(stack,
				world, x, y, z, ForgeDirection.values()[side]);

		if(placedMeta < 0)
			return false;
		if (!world.setBlock(x, y, z, getBlockID(), metadata, 2)) {
			return false;
		}
		if (world.getBlockId(x, y, z) == getBlockID()) {
			TileEntityMetadata tile = TileEntityMetadata.getTile(world, x, y, z);
			if(tile != null)
				tile.setTileMetadata(placedMeta);
			block.onBlockPlacedBy(world, x, y, z, player, stack);
			block.onPostBlockPlaced(world, x, y, z, metadata);
		}

		return true;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemDisplayName(ItemStack par1ItemStack) {
		return ((IBlockMetadata) Block.blocksList[getBlockID()])
				.getBlockName(par1ItemStack);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack par1ItemStack,
			EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
		((IBlockMetadata) Block.blocksList[getBlockID()])
		.getBlockTooltip(par1ItemStack, par3List);
	}

	public Icon getIconFromDamage(int par1)
    {
        return Block.blocksList[getBlockID()].getIcon(1, par1);
    }
	
}
