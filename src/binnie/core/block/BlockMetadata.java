package binnie.core.block;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;
import binnie.core.BinnieCore;
import binnie.extratrees.carpentry.ModuleCarpentry;

public class BlockMetadata extends BlockContainer implements IBlockMetadata {

	public BlockMetadata(int par1, Material par2Material) {
		super(par1, par2Material);
	}

	// Methods that need to be copied into any IBlockMetadata block

	@Override
	public ArrayList<ItemStack> getBlockDropped(World world, int x, int y,
			int z, int blockMeta, int fortune) {
		//return new ArrayList<ItemStack>();
		return BlockMetadata.getBlockDropped(this, world, x, y, z, blockMeta);
	}
	
	@Override
	 public boolean removeBlockByPlayer(World world, EntityPlayer player, int x, int y, int z)
   {
       return BlockMetadata.breakBlock(this, player, world, x, y, z);
   }

	@Override
	public TileEntity createNewTileEntity(World var1) {
		return new TileEntityMetadata();
	}
	
	@Override
	public boolean hasTileEntity(int meta) {
		return true;
	}

	public boolean onBlockEventReceived(World par1World, int par2, int par3,
			int par4, int par5, int par6) {
		super.onBlockEventReceived(par1World, par2, par3, par4, par5, par6);
		TileEntity tileentity = par1World.getBlockTileEntity(par2, par3, par4);
		return tileentity != null ? tileentity.receiveClientEvent(par5, par6)
				: false;
	}

	
	
	
	
	
	
	public Icon getBlockTexture(IBlockAccess par1IBlockAccess, int par2,
			int par3, int par4, int par5) {
		int metadata = TileEntityMetadata.getTileMetadata(par1IBlockAccess, par2, par3, par4);
		return this.getIcon(par5, metadata);
	}


	public String getBlockName(ItemStack par1ItemStack) {
		return getLocalizedName();
	}

	public void getBlockTooltip(ItemStack par1ItemStack, List par3List) {
	}

	public int getPlacedMeta(ItemStack item, World world, int x, int y, int z, ForgeDirection clickedBlock) {
		int damage = TileEntityMetadata.getItemDamage(item);
		return damage;
	}

	public int getDroppedMeta(int tileMeta, int blockMeta) {
		return tileMeta;
	}

	@Override
	public int getBlockID() {
		return blockID;
	}
	
	
	
	

	public static ArrayList<ItemStack> getBlockDropped(IBlockMetadata block,
			World world, int x, int y, int z, int blockMeta) {
		ArrayList<ItemStack> array = new ArrayList<ItemStack>();
		TileEntityMetadata tile = TileEntityMetadata.getTile(world, x, y, z);
		if(tile != null && !tile.hasDroppedBlock()) {
			int meta = block.getDroppedMeta(world.getBlockMetadata(x, y, z), tile.getTileMetadata());
			array.add(TileEntityMetadata.getItemStack(block.getBlockID(), meta));
		}
		return array;
	}

	static int temporyMeta = -1;

	public static boolean breakBlock(IBlockMetadata block, EntityPlayer player, World world, int i, int j, int k) {
		List<ItemStack> drops = new ArrayList<ItemStack>();
		
		Block block2 = (Block) block;
		
		TileEntityMetadata tile = TileEntityMetadata.getTile(world, i, j, k);
		if(tile != null && !tile.hasDroppedBlock()) {
			int tileMeta = TileEntityMetadata.getTileMetadata(world, i, j, k);
			drops = block2.getBlockDropped(world, i, j, k, world.getBlockMetadata(i, j, k), 0);
			
		}
		
		boolean hasBeenBroken = world.setBlockToAir(i, j, k);
		
		if(hasBeenBroken && BinnieCore.proxy.isSimulating(world) && drops.size() > 0 && (player == null || !player.capabilities.isCreativeMode)) {
			for(ItemStack drop : drops) block.dropAsStack(world, i, j, k, drop);
			tile.dropBlock();
		}
		
		return hasBeenBroken;
		
	}

	@Override
	public void dropAsStack(World world, int x, int y, int z, ItemStack drop) {
		this.dropBlockAsItem_do(world, x, y, z, drop);
	}
	
	 public void breakBlock(World par1World, int par2, int par3, int par4, int par5, int par6)
	    {
	        super.breakBlock(par1World, par2, par3, par4, par5, par6);
	        par1World.removeBlockTileEntity(par2, par3, par4);
	    }


}
