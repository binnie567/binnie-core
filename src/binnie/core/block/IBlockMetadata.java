package binnie.core.block;

import java.util.List;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;

public interface IBlockMetadata extends ITileEntityProvider {

	int getPlacedMeta(ItemStack stack, World world, int x, int y, int z, ForgeDirection clickedBlock);
	
	int getDroppedMeta(int blockMeta, int tileMeta);

	String getBlockName(ItemStack par1ItemStack);

	void getBlockTooltip(ItemStack par1ItemStack, List par3List);

	int getBlockID();

	void dropAsStack(World world, int x, int y, int z, ItemStack drop);

}
