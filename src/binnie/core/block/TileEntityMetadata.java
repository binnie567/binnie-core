package binnie.core.block;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.packet.Packet;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import binnie.core.network.packet.PacketMetadata;

public class TileEntityMetadata extends TileEntity {

	public TileEntityMetadata() {
	}

	@Override
	public boolean receiveClientEvent(int par1, int par2) {
		if (par1 == 42) {
			meta = par2;
			worldObj.markBlockForRenderUpdate(xCoord, yCoord, zCoord);
		}
		return true;
	}

	protected int meta;

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		meta = nbt.getInteger("meta");
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setInteger("meta", meta);
	}

	@Override
	public boolean canUpdate() {
		return false;
	}

	public int getTileMetadata() {
		return meta;
	}

	public void setTileMetadata(int meta) {
		this.meta = meta;
	}

	public Packet getDescriptionPacket() {
		return new PacketMetadata(xCoord, yCoord, zCoord, getTileMetadata()).getPacket();
	}

	public static TileEntityMetadata getTile(IBlockAccess world, int x, int y, int z) {
		TileEntity tile = world.getBlockTileEntity(x, y, z);
		if (!(tile instanceof TileEntityMetadata))
			return null;

		return (TileEntityMetadata) tile;
	}

	public static ItemStack getItemStack(int id, int damage) {
		ItemStack item = new ItemStack(id, 1, 0);
		setItemDamage(item, damage);
		return item;
	}

	public static void setItemDamage(ItemStack item, int i) {
		item.setItemDamage(i < 16387 ? i : 16387);
		NBTTagCompound tag = new NBTTagCompound("tag");
		tag.setInteger("meta", i);
		item.setTagCompound(tag);
	}

	public static int getItemDamage(ItemStack item) {
		if (item.hasTagCompound() && item.getTagCompound().hasKey("meta"))
			return item.getTagCompound().getInteger("meta");
		else
			return item.getItemDamage();
	}

	public static int getTileMetadata(IBlockAccess world, int x, int y, int z) {
		TileEntityMetadata tile = getTile(world, x, y, z);
		return tile == null ? 0 : tile.getTileMetadata();
	}

	boolean droppedBlock = false;

	public boolean hasDroppedBlock() {
		return droppedBlock;
	}

	public void dropBlock() {
		this.droppedBlock = true;
	}

}
