package binnie.core;

import binnie.core.language.LanguageManager;

/**
 * Holder class for various managers. Stuff needs to be ported here, like liquid code
 * @author Alex Binnie
 *
 */
public class Binnie {
	
	public static LanguageManager Language = new LanguageManager();

}
