package binnie.core.liquid;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemLiquidContainer extends Item {

	ILiquidType[] liquids;
	
	public ItemLiquidContainer(int i, ILiquidType[] liquids) {
		super(i);
		this.liquids = liquids;
		maxStackSize = 64;
		setHasSubtypes(true);
		setUnlocalizedName("liquidContainer");
		setCreativeTab(CreativeTabs.tabMaterials);
		for(ILiquidType liquid : liquids) liquid.setContainerItem(this);
	}
	
	private ILiquidType getLiquid(int meta) {
		return meta >= liquids.length ? liquids[0] : liquids[meta];
	}

	@Override
	public void registerIcons(IconRegister register) {
		for(LiquidContainer container : LiquidContainer.values())
			container.updateIcons(register);
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		if (itemstack == null)
			return "???";
		int meta = itemstack.getItemDamage();
		int container = meta / 16;
		int liquid = meta % 16;
		return getLiquid(liquid).getName() + " "
				+ LiquidContainer.getContainer(container).getName();
	}

	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		for (LiquidContainer container : LiquidContainer.values()) {
			for (ILiquidType liquid : liquids) {
				itemList.add(new ItemStack(this, 1, liquid.ordinal() + container.ordinal() * 16));
			}
		}

	}
	
	@Override
	public Icon getIconFromDamageForRenderPass(int i, int j) {
		LiquidContainer container = LiquidContainer.getContainer(i / 16);
		if (j > 0) {
			return container.bottle;
		}

		return container.contents;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public int getColorFromItemStack(ItemStack item, int pass) {
		ILiquidType liquid = getLiquid(item.getItemDamage() % 16);
		if(pass == 0) return liquid.getColour();
		return super.getColorFromItemStack(item, pass);
	}
	
	@Override
	public boolean requiresMultipleRenderPasses() {
		return true;
	}
	
}
