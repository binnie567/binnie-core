package binnie.core.liquid;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraftforge.liquids.LiquidContainerData;
import net.minecraftforge.liquids.LiquidContainerRegistry;
import net.minecraftforge.liquids.LiquidStack;
import binnie.core.BinnieCore;
import forestry.api.core.ItemInterface;

public enum LiquidContainer {
	
	CAPSULE("Capsule"), REFRACTORY("Capsule"), CAN("Can"), BUCKET(
			"Bucket"), GLASS("Bottle");
	
	Icon bottle;
	Icon contents;
	String name;

	LiquidContainer(String name) {
		this.name = name;
	}
	
	public void updateIcons(IconRegister register) {
		bottle = BinnieCore.proxy.getIcon(register, "forestry", "liquids/"+toString().toLowerCase()+".bottle");
		contents = BinnieCore.proxy.getIcon(register, "forestry", "liquids/"+toString().toLowerCase()+".contents");
	}

	public static LiquidContainer getContainer(int container) {
		return container >= values().length ? CAPSULE : values()[container];
	}

	public String getName() {
		return name;
	}

	public void register(ItemLiquid itemLiquid, ItemLiquidContainer itemContainer, ILiquidType liquid) {
		ItemStack empty = null;
		switch(this) {
		case BUCKET:
			empty = new ItemStack(Item.bucketEmpty, 1, 0);
			break;
		case CAN:
			empty = ItemInterface.getItem("canEmpty");
			break;
		case CAPSULE:
			empty = ItemInterface.getItem("waxCapsule");
			break;
		case GLASS:
			empty = new ItemStack(Item.glassBottle, 1, 0);
			break;
		case REFRACTORY:
			empty = ItemInterface.getItem("refractoryEmpty");
			break;
		}
		
		LiquidContainerData data = new LiquidContainerData(new LiquidStack(itemLiquid.itemID, 1000, liquid.ordinal()),
				new ItemStack(itemContainer.itemID, 1, liquid.ordinal() + this.ordinal() * 16), empty);
		
		LiquidContainerRegistry.registerLiquid(data);

	}

}
