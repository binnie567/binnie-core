package binnie.core.liquid;

import net.minecraftforge.liquids.LiquidDictionary;
import net.minecraftforge.liquids.LiquidStack;
import binnie.core.BinnieCore;

public class LiquidManager {
	
	public static void createLiquids(ILiquidType[] liquids, int liquidID, int liquidContainerID) {
		ItemLiquid liquidItem = new ItemLiquid(liquidID, liquids);
		ItemLiquidContainer containerItem = new ItemLiquidContainer(liquidContainerID, liquids);
		
		for(ILiquidType liquid : liquids) {
			LiquidStack liquidStack = BinnieCore.proxy.registerLiquid(
					liquid.getIdentifier(), liquidItem.itemID, liquid.ordinal());
			
			for(LiquidContainer container : LiquidContainer.values()) {
				container.register(liquidItem, containerItem, liquid);
			}
		}
		
		
	}
	
	
	
	public static LiquidStack getLiquidStack(String name, int amount) {
		return LiquidDictionary.getLiquid(name, amount);
	}

}
