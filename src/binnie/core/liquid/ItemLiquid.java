package binnie.core.liquid;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class ItemLiquid extends Item {
	
	ILiquidType[] liquids;
	
	public ItemLiquid(int par1, ILiquidType[] liquids) {
		super(par1);
		this.liquids = liquids;
		setUnlocalizedName("liquid");
		setCreativeTab(CreativeTabs.tabMaterials);
		for(ILiquidType liquid : liquids) liquid.setItem(this);
	}
	
	private ILiquidType getLiquid(int meta) {
		return meta >= liquids.length ? liquids[0] : liquids[meta];
	}
	
	@Override
	public Icon getIconFromDamage(int par1) {
		return getLiquid(par1).getIcon();
	}
	
	@Override
	public void registerIcons(IconRegister register) {
		for(ILiquidType liquid : liquids) {
			liquid.registerIcon(register);
		}
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		if (itemstack == null)
			return "Unknown Liquid";
		int meta = itemstack.getItemDamage();
		return getLiquid(meta).getName();
	}

	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		for (ILiquidType liquid : liquids)
			itemList.add(new ItemStack(this, 1, liquid.ordinal()));
	}

}
