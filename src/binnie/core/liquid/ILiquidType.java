package binnie.core.liquid;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;
import net.minecraftforge.liquids.LiquidStack;

public interface ILiquidType {
	
	int ordinal();
	
	boolean canBePlacedInContainer(LiquidContainer container);

	Icon getIcon();

	void registerIcon(IconRegister register);

	String getName();

	void setItem(ItemLiquid itemLiquid);
	
	void setContainerItem(ItemLiquidContainer itemContainer);

	String getIdentifier();
	
	int getColour();

	LiquidStack get(int amount);
}
